import pathlib
import itertools
import functools
import time

import matplotlib

matplotlib.rc("svg", fonttype="none")
matplotlib.rc(
    "font", **{"serif": ["Times"], "sans-serif": ["Arial"], "monospace": ["Courier"]}
)

import numpy as np
import scipy.stats
import pandas as pd
import h5py

import seaborn as sns
import matplotlib.pyplot as plt

from symbolicSBML import Parameters
from pipeline.util import load_ensemble_samples

import plot_util

if __name__ == "__main__":
    interactive = False
    limit_samples_testing = None
    output_location = pathlib.Path("./results/sampling_results")
    output_location.mkdir(exist_ok=True, parents=True)

    # Data location and runs
    base_path = pathlib.Path("./results/sampling/sampling")
    runs = "full", "minnw", "fastcore", "networkreducer"

    # Load all data of all runs
    all_runs = runs
    all_state = []
    all_dydt = []
    all_parameters = []
    all_flux = []
    all_jacobian = []
    all_sensitivities = []
    all_flux_sensitivities = []
    all_metabolite_ccs = []
    all_flux_ccs = []
    for run in runs:
        run_path = base_path / run / "samples.h5"
        results = load_ensemble_samples(
            run_path, limit_samples_testing=limit_samples_testing
        )

        all_state.append(results[0])
        all_dydt.append(results[1])
        all_parameters.append(results[2])
        all_flux.append(results[3])
        all_jacobian.append(results[4])
        all_sensitivities.append(results[5])
        all_flux_sensitivities.append(results[6])

    # Calculate control coefficients.
    for dfdp, f, p in zip(all_flux_sensitivities, all_flux, all_parameters):
        # Outer product of p and f
        # outer([n_samples x n_parameters], f[n_samples x n_fluxes])
        #   -> [n_samples x n_parameters x n_fluxes]
        #   -> [n_samples x (n_parameters x n_fluxes)]
        # Same dimension/order as dfdp
        #      [n_samples x (n_fluxes x n_parameters)]
        # Note, however, that dfdp has fluxes varying first, then parameters, so
        # we should reshape it in the reverse (fortran) order!
        p_over_f = np.einsum("ij,ik->ijk", p, 1 / f).reshape(p.shape[0], -1, order="F")
        all_flux_ccs.append((dfdp * p_over_f).fillna(0))
    for dcdp, c, p in zip(all_sensitivities, all_state, all_parameters):
        p_over_c = np.einsum("ij,ik->ijk", p, 1 / c).reshape(p.shape[0], -1, order="F")
        all_metabolite_ccs.append((dcdp * p_over_c).fillna(0))

    # --- Plotting ---
    plt.interactive(interactive)

    # Compare control coefficient distribution.
    if False:
        data = list(zip(all_runs, all_metabolite_ccs, all_flux_ccs))

        ref_run, ref_met, ref_flux = data[0]
        frames = []
        for run, met, flux in data[1:]:
            # Filter R & T, full zero columns and get shared columns only.
            for variable_type, ref, df in zip(
                ("metabolite", "flux"), (ref_met, ref_flux), (met, flux)
            ):
                columns = plot_util.get_nonzero_axis([df, ref])
                columns = columns.intersection(plot_util.get_shared_axis([df, ref]))
                columns = columns[~columns.get_level_values(1).isin(("R", "T"))]

                trim_args = "IQR", (0.05, 0.95)
                ref = plot_util.trim(ref[columns], *trim_args)
                df = plot_util.trim(df[columns], *trim_args)

                frames.append(
                    plot_util.jaccard_index(ref, df)
                    .reset_index()
                    .set_axis(["variable", "parameter", "ji"], axis=1)
                    .assign(model=run, variable_type=variable_type)
                )

        df = pd.concat(frames, ignore_index=True)
        df = df.assign(ptype=plot_util.get_parameter_types(df.parameter))

        # Rename categories
        km = "Michaelis constant ($K^{M}$)"
        uv = "Rate constant ($u^{V}$)"
        mu = "Standard chemical\npotential ($\\mu'^{\\circ}$)"
        df.ptype = df.ptype.replace(
            {
                Parameters.u_v: uv,
                Parameters.km_tot_rate: uv,
                Parameters.mu: mu,
                Parameters.km: km,
            }
        )
        # Rename methods
        df.model = df.model.replace(
            {
                "minnw": "minNW",
                "networkreducer": "NetworkReducer",
                "fastcore": "FastCore",
            }
        )

        fig, (ax1, ax2) = plt.subplots(1, 2)
        plot_function = sns.violinplot
        kwargs = {
            "order": (mu, km, uv),
            "hue_order": ("minNW", "FastCore", "NetworkReducer"),
            "palette": plot_util.colors("models"),
            "hue": "model",
            "x": "ji",
            "y": "ptype",
            "orient": "h",
            "cut": 0,  # Do not cross boundaries with kde.
            "scale": "count",  # Scale with number of points.
            "inner": None,  # Decrease clutter.
        }
        plot_function(data=df[df.variable_type == "flux"], ax=ax1, **kwargs)
        ax1.legend().remove()
        ax1.set_title("Flux control coefficients")
        ax1.set_xlabel("Jaccard Index")
        ax1.set_ylabel(None)
        ax1.set_xlim(0, 1)
        plot_function(
            data=df[df.variable_type == "metabolite"], ax=ax2, **kwargs,
        )
        legend = ax2.legend(
            title="Model",
            bbox_to_anchor=(1.02, 1.02),  # Top right
            loc="upper left",
            fontsize="small",
            frameon=False,
        )
        legend._legend_box.align = "left"
        ax2.set_title("Metabolite control coefficients")
        ax2.set_xlabel("Jaccard Index")
        ax2.set_ylabel(None)
        ax2.set_yticklabels([])
        ax2.set_xlim(0, 1)
        sns.despine(fig)

        w, h = fig.get_size_inches()
        scale = (190 / 25.4) / w
        fig.set_size_inches(w * scale, h * scale)
        fig.tight_layout()

        fig.savefig(output_location / "sampled_cc.png", dpi=300, bbox_inches='tight')