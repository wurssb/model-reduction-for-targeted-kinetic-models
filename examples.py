"""Script for the test-cases as used in the paper.

There are two test-cases included.
1) Reduction of a toy metabolic model:
   Verified with multi-start optimization using a test/training dataset.
2) Reduction of the E. coli core model:
   Verified with (steady state) sensitivity analysis & parameter sampling.

Author: Rik van Rosmalen
"""
import pathlib
import warnings
import copy

import numpy as np
import pandas as pd
import scipy.stats

import cobra
import matlab.engine as ml

import symbolicSBML
from symbolicSBML import Parameters

from pipeline.pipeline import ReductionOptimizationPipeline, ReductionSamplingPipeline
from pipeline.reduction_steps import FastCore, MinNW, NetworkReducer
from pipeline.amici_steps import AmiciCompiler, AmiciSimulator
from pipeline.pesto_steps import PestoOptimizer
from pipeline.optimization_steps import LHSStartingPoints
from pipeline.kinetics_steps import (
    ModelKineticizer,
    SteadyStateModelParameterizer,
    NA_IDENTIFIER,
)
from pipeline.util import read_config, scan_flux_possibility, fit_distribution_from_ci

from model_settings import model_settings

# ------------------------------------------------------------------------
#                           General set-up.
# ------------------------------------------------------------------------
# Paths to dependencies: cplex / cobra (matlab) etc.
paths = read_config(pathlib.Path("./config.json"))["paths"]
# Working directory.
working_directory = pathlib.Path("./results")
if working_directory.exists():
    warnings.warn(
        "Working directory {} already exists. Previous results may be overwritten.".format(
            str(working_directory)
        )
    )
else:
    working_directory.mkdir(parents=True)

# Which experiments do we run?
# 1 - Multi-start optimization of small model.
# 2 - Sensitivity sampling of core model parametric sensitivities at predetermined steady state.
# Note: 1 can be further specified to indicate which specific test models should be used.
run_experiments = [1]
run_optimizations = ["toy", "large_cycle", "edemp", "curcumin", "co_factors"]

if 1 in run_experiments:
    # Create a Matlab instance
    # A common Matlab instance will speed up things as the setup is rather slow.
    # If a Matlab instance is not created, it will be done automatically during
    # each of the steps that require it - this might be faster for testing
    # the python interfaces in combination with caches.
    matlab_instance = ml.start_matlab()

    # If you're using parallel optimization in Pesto, you can already create
    # the parallel processing pool here as well.
    par = True
    if par and matlab_instance is not None:
        n_cores = matlab_instance.feature("numcores")
        # Note: If you have other stuff to do,
        # create a parallel pool of less then n_cores here.
        matlab_instance.parpool(n_cores)
else:
    par = False
    matlab_instance = None

# Use cache?
# This will check if the input and output files of each step are modified,
# and will only redo the step if these changed.
use_cache = True

# Silence output from the tools we run.
silence = False


def experiment_1(model, settings):
    # ------------------------------------------------------------------------
    # Test-case I: Optimization on toy metabolic model.
    # ------------------------------------------------------------------------
    # ------------
    #   0. Set-up
    # ------------
    optimization_case_directory = working_directory / "optimization" / model
    # ------------
    #   1. Define model structure.
    # ------------
    # Note that the paper uses E and I, while the model uses EE and II to avoid issues
    # when parsing symbolic equations.
    model_path = pathlib.Path(settings["model_path"])
    symbolic_model = symbolicSBML.SymbolicModel.from_sbml(model_path)
    cobra_model = cobra.io.read_sbml_model(str(model_path), f_replace={})
    # ------------
    #   2. Generate parameters.
    #       a. Pick realistic ranges.
    #       b. Balance.
    #       c. Fix to FBA solution.
    #       d. Verify with original range.
    # ------------
    fixed_parameters = {"R": 0.008314, "T": 300}
    # Also define any other parameters that might be required later in model construction.
    fixed_parameters.update(settings["fixed_parameters"])

    # Save priors for example
    priors = {
        Parameters.mu: [50, 25],
        Parameters.kv: [1, 0.25],
        Parameters.km: [1, 0.25],
        Parameters.c: [1, 0.25],
        Parameters.u: [0.1, 0.025],
        # Derived parameters
        # Assuming a perfect equilibrium reaction, these can be derived rather easily:
        # keq = 1 (by definition)
        Parameters.keq: [1, 10],
        # kcat = kv
        Parameters.kcat_prod: [1, 1],
        Parameters.kcat_sub: [1, 1],
        # vmax = u * kv
        Parameters.vmax: [0.1, 1],
        # A = 0
        Parameters.A: [0, 1],
        # mu_p = mu + R*T*ln(c) ~ mu - 5 (@ T=300, R=0.008134)
        Parameters.mu_p: [45, 25],
    }

    # Parameter data for the example
    data = settings["data"]

    # Save fluxes from the FBA reference solution.
    cobra_model.objective = cobra_model.reactions.get_by_id(settings["flux_objective"])
    # Note that all input exchanges are defined as ... -> X
    # and output as X -> ...
    for reaction_id, lb, ub in settings["flux_constraints"]:
        reaction = cobra_model.reactions.get_by_id(reaction_id)
        if lb is not None:
            reaction.lower_bound = lb
        if ub is not None:
            reaction.upper_bound = ub
    # Don't forget to convert from mmol/h to mmol/s!
    fluxes = cobra_model.optimize().to_frame().fluxes.to_frame() / 3600

    # ------------
    #   3. Reduce model:
    #       a. MinNW.
    #       b. NetworkReducer.
    #       c. FastCore.
    # ------------
    # Set up global reduction settings.
    # Reactions that cannot be removed. These should match the SBML ids.
    protected_reactions = settings["protected_reactions"]
    # Metabolites that cannot be removed.
    protected_metabolites = settings["protected_metabolites"]
    # Behaviour that should be preserved.
    # Note that behaviour should already be valid in the base model!
    # Behaviour is defined as a set of reactions, with their directions and
    # values that should be feasible during FBA simulation.
    # FastCore does NOT use this, but only allows for protected reactions/metabolites.
    protected_behaviour = settings["protected_behaviour"]
    reduction_settings = {
        "silence_output": silence,
        "protected_reactions": protected_reactions,
        "protected_metabolites": protected_metabolites,
        "protected_behaviour": protected_behaviour,
    }

    # Set up reducers and individual settings. For more individual settings,
    # see the relevant classes in `pipeline/reduction_steps.py`
    # NetworkReducer
    nw_reducer_settings = reduction_settings.copy()
    nw_reducer_settings.update({})
    nw_reducer = NetworkReducer(
        paths["cplex"],
        paths["cna"],
        paths["networkreducer"],
        matlab_instance,
        use_cache,
        **nw_reducer_settings
    )

    # MinNW
    mn_reducer_settings = reduction_settings.copy()
    mn_reducer_settings.update({"delete_blocked": 0})
    mn_reducer = MinNW(
        paths["cplex"],
        paths["cobra"],
        paths["min_nw"],
        matlab_instance,
        use_cache,
        **mn_reducer_settings
    )

    # FastCore
    fc_reducer_settings = reduction_settings.copy()
    fc_reducer_settings.update({})
    fc_reducer = FastCore(
        paths["cplex"],
        paths["cobra"],
        paths["fastcore"],
        matlab_instance,
        use_cache,
        **fc_reducer_settings
    )

    reducers = [nw_reducer, mn_reducer, fc_reducer]
    # ------------
    #   4. Generate model rate laws:
    #       a. Pick right rate laws per reaction.
    #       b. Apply relevant conservations.
    # ------------

    # Reactions that will have an abstract pool added to balance out the chemical energy.
    # [reaction, side (reactant / product), pool name, add as observable?]
    abstract_metabolites = settings["abstract_metabolites"]
    additional_input_reactions = settings["additional_input_reactions"]

    # Reactions/metabolites that are fixed at a constant value
    fixed_reactions = {}
    fixed_metabolites = []

    # Reactions with a custom rate law.
    custom_reactions = {}

    # Reactions that should be ignored
    ignore_reactions = []

    # Extra parameters required for the model that are not fixed.
    scaling_factor_id = "scaling_factor"
    extra_parameters = {scaling_factor_id: 1}

    # Reactions that cannot use the default scaling parameter (u)
    simplify_reactions = settings["simplify_reactions"]
    scaling_parameters = {
        r: Parameters.sep.join((Parameters.km_tot_rate, r)) for r in simplify_reactions
    }
    extra_parameters_scaling = extra_parameters.copy()
    extra_parameters_scaling.update(
        (Parameters.sep.join((Parameters.u_v, r)), 1)
        for r in symbolic_model.reactions
        if r not in scaling_parameters
    )
    extra_parameters_scaling.update(
        (Parameters.sep.join((Parameters.km_tot_rate, r)), 1)
        for r in scaling_parameters
    )

    default_rate_law = "modular"
    default_rate_law_args = {"parametrisation": "weg", "sub_rate_law": "CM"}
    simplified_rate_law = "modular"
    simplified_rate_law_args = {"parametrisation": "weg", "sub_rate_law": "PM"}
    merge_parameters = ["enzyme_rate", "km_merged", "km_rate"]
    limit_total_lumped_stoichiometry = True
    max_reaction_order = 3
    positive_fractional_powers = False

    kineticizer_settings = {
        "silence_output": silence,
        "rate_law_type": default_rate_law,
        "rate_law_args": default_rate_law_args,
        "simplified_rate_law_type": simplified_rate_law,
        "simplified_rate_law_args": simplified_rate_law_args,
        "ignore_reactions": ignore_reactions,
        "fixed_reactions": fixed_reactions,
        "fixed_metabolites": fixed_metabolites,
        "replace_reactions": custom_reactions,
        "additional_input_reactions": additional_input_reactions,
        "add_abstract_metabolites": abstract_metabolites,
        "allow_missing_abstract_metabolites": True,
        "simplify_reactions": simplify_reactions,
        "simplify_lumped_reactions": True,
        "limit_total_lumped_stoichiometry": limit_total_lumped_stoichiometry,
        "max_reaction_order": max_reaction_order,
        "merge_parameters": merge_parameters,
        "biomass_scaling": None,
        "scaling_factor": False,
        "positive_fractional_powers": positive_fractional_powers,
        "cythonize": False,
        "cythonize_settings": {"skip_jacobian": True},
    }
    kineticizer = ModelKineticizer(use_cache=use_cache, **kineticizer_settings)
    # Separate settings for the parametrization procedure
    kineticizer_parameterizer_settings = copy.deepcopy(kineticizer_settings)
    # We do NOT want system to have a continuous inflow feed when balanced around
    # the steady state flux profile.
    kineticizer_parameterizer_settings["additional_input_reactions"] = False
    kineticizer_parameterizer_settings["cythonize"] = True
    kineticizer_parameterizer_settings["cythonize_settings"]["skip_jacobian"] = True
    kineticizer_parameterizer = ModelKineticizer(
        use_cache=use_cache, **kineticizer_parameterizer_settings
    )

    parameterizer_settings = {
        "R": fixed_parameters["R"],
        "T": fixed_parameters["T"],
        "fixed_metabolites": fixed_metabolites,
        "silence_output": silence,
        "max_samples": 50,
        "sample_batch_size": 30,
        "A_tol": 1,
        "A_max": 40,
        "constraint_factor": 3,
        "zero_concentration": 1e-9,
        "optimized_directionality_data_error": {
            Parameters.c: 0.01,
            Parameters.mu: 0.01,
            Parameters.A: 0.01,
        },
        "constrain_inactive_reactions": False,
        "ss_error_tolerance": 1e-6,
        "ignore_reactions": sorted(ignore_reactions + list(fixed_reactions.keys())),
        "ignore_reactions_balancing": [],
        "ignore_metabolites_balancing": [scaling_factor_id],
        # Even though we don't need these parameters for parameterizing, we do when
        # updating the final model file from `kineticizer` with the new parameters.
        "extra_parameters": dict(
            extra_parameters_scaling, **settings["fixed_parameters"]
        ),
        "default_scaling_parameter": Parameters.u_v,
        "scaling_parameters": scaling_parameters,
        "augment": {"pre": True, "post": True},
    }
    parameterizer = SteadyStateModelParameterizer(
        use_cache=use_cache, **parameterizer_settings
    )

    # ------------
    #   5. Simulate training & testing data:
    #       a. Pick perturbations.
    #       b. Simulate models.
    # ------------
    # Set up experiments
    # Start with defining perturbations
    # A perturbation has:
    # 1. A trigger (defined as a function of time, metabolites or parameters)
    # 2. An event with a metabolic (1) state that it changes and (2) the magnitude of the change.
    #    the magnitude is defined also defined as a function of time, metabolites or parameters.
    # Everything can be defined either as sympy expressions, or a strings that can evaluate to
    # sympy expressions.
    # Examples:
    #   Trigger: Predetermined time
    #       trigger = t >= x
    #   Trigger: Predetermined concentration
    #       trigger = trigger_metabolite >= x
    #   Event: Set to fixed concentration
    #       event = {pulsed_metabolite: '-pulsed_metabolite + x'}
    #   Event: Absolute additive pulse
    #       event = {pulsed_metabolite: x}
    #   Event: Relative additive pulse
    #       event = {pulsed_metabolite: 'pulsed_metabolite * x - 1'}
    perturbations = settings["perturbations"]

    feed_rates = settings["feed_rates"]
    batch_amounts = settings["batch_amounts"]

    experiments = []
    for experiment in settings["experiments"]:
        experiments.append(
            {
                "set": experiment["set"],
                "fixed_parameters": dict(
                    fixed_parameters,
                    **{
                        k: feed_rates[v]
                        for k, v in experiment["extra_fixed_parameters"].items()
                    }
                ),
                "perturbations": [
                    perturbations[p] for p in experiment["perturbations"]
                ],
                "initial_concentrations": {
                    m: batch_amounts[c]
                    for m, c in experiment["initial_concentrations"].items()
                },
            }
        )

    # ------------
    #   6. Optimize reduced and full models with training data:
    #       a. Pick initial parameters. (LHS)
    #       b. Pick hyper-parameters for optimization.
    # ------------
    # Create parameter search ranges based on parameter class.
    # Note that minimum always has to be strictly greater then maximum.
    # Equal values are not allowed in Pesto!
    # Instead mark those as fixed parameters - this means they'll be replaced
    # on the symbolic level and cannot be varied at all.
    parameter_ranges = {
        "general": {
            # For full reactions according to the common modular rate law.
            Parameters.km: (0.1, 2.5),
            Parameters.mu: (-5, 125),
            Parameters.u_v: (0, 1),
            # For simplified reactions with power law modular rate law i.e. mass-action kinetics.
            Parameters.km_tot_rate: (0, 10),
        },
        "specific": settings["specific_parameter_ranges"],
    }

    # Set up compiler
    amici_compiler_settings = {"silence_output": silence, "generate_hessian": False}
    compiler = AmiciCompiler(
        paths["amici_mlab"],
        matlab_instance,
        use_cache=use_cache,
        **amici_compiler_settings
    )

    atol, rtol = 1e-12, 1e-9

    # Set up simulators (One for data generation, one for simulating the solutions)
    amici_settings_data = {
        "silence_output": silence,
        "t_start": 0,
        "t_step": 0.1,
        "t_end": 150,
        "atol": atol,
        "rtol": rtol,
        "maxsteps": 1e6,
        "pscale": "auto",
    }
    amici_settings_solutions = amici_settings_data.copy()
    amici_settings_solutions["sensi"] = 1

    simulator_data = AmiciSimulator(
        paths["amici_mlab"], matlab_instance, use_cache=use_cache, **amici_settings_data
    )
    simulator_solutions = AmiciSimulator(
        paths["amici_mlab"],
        matlab_instance,
        use_cache=use_cache,
        **amici_settings_solutions
    )

    # Set up optimizer
    n_starts = 100
    optimizer_settings = {
        "silence_output": silence,
        "localOptimizerOptions": {
            "Algorithm": "interior-point",
            "Display": "off",
            "GradObj": "on",
            "MaxIter": 1000,
            "PrecondBandWidth": float("inf"),
        },
        "proposal": "user-supplied",
        "n_starts": n_starts,  # TODO: Fix bug with n_starts == 1.
        # Sensitivity order (Cannot be 2 if Hessian was not generated during compilation!)
        "sensi": 1,
        # Sensitivity method (0 - none, 1 - forward, 2 - adjoint)
        "sensi_meth": 1,
        "atol": atol,
        "rtol": rtol,
        "auto_scale": True,
        "scale_default": "lin",
    }
    n_starting_points = (
        n_starts if optimizer_settings["proposal"] == "user-supplied" else n_starts - 1
    )

    starting_points_generator_settings = {
        # When using PESTO, the first run is always the initial guess indicated,
        # After which the actual samples are used as starting points.
        # Thus we need one less sample then the number of starts.
        "n_samples": n_starting_points,
        "criterion": "random",
        "auto_scale": True,
        "scale_default": "linear",
        "random_seed": 1001,
    }

    starting_points_generator = LHSStartingPoints(
        use_cache=use_cache, **starting_points_generator_settings
    )

    if par:
        optimizer_settings["comp_type"] = "parallel"
    optimizer = PestoOptimizer(
        paths["pesto_mlab"],
        paths["amici_mlab"],
        matlab_instance,
        use_cache=use_cache,
        **optimizer_settings
    )

    # Run everything!
    test = ReductionOptimizationPipeline(
        optimization_case_directory,
        model_path,
        experiments,
        parameter_ranges,
        fluxes,
        priors,
        data,
        kineticizer,
        kineticizer_parameterizer,
        parameterizer,
        starting_points_generator,
        reducers,
        compiler,
        simulator_data,
        simulator_solutions,
        optimizer,
        use_cache=use_cache,
    )

    warnings.simplefilter("always")
    try:
        test.run()
    except RuntimeError:
        raise


def experiment_2():
    # ------------------------------------------------------------------------
    # Test-case II: Sampling of E. coli core model:
    # ------------------------------------------------------------------------
    #   0. Set-up.
    # ------------
    sampling_case_directory = working_directory / "sampling"
    par = True
    # ------------
    #   1. Define model structure:
    #       a. As given in existing model.
    #       b. Remove exchanges etc.
    # ------------
    core_model_path = pathlib.Path("./data/ecoli_core.xml")
    symbolic_model_core = symbolicSBML.SymbolicModel.from_sbml(core_model_path)
    cobra_model_core = cobra.io.read_sbml_model(str(core_model_path), f_replace={})
    glc_uptake = 10
    cobra_model_core.reactions.R_EX_glc_e.lower_bound = -glc_uptake
    # ------------
    #   2. Generate parameters:
    #       a. Pick realistic ranges:
    #                  mu: eQuilibrator
    #                  c: Databases / references
    #           km & kcat: Optional: Brenda / Sabio
    #       b. Balance
    #       c. Fix to FBA solution
    #       d. Verify with original range
    # ------------
    fixed_parameters = {"R": 0.008314, "T": 300}

    # These priors are more closely aligned to biological averages.
    priors = {
        # Base quantities
        Parameters.mu: (-880.0, 680.00),
        Parameters.kv: (10.0, 6.26),
        Parameters.km: (0.1, 6.26),
        Parameters.c: (0.1, 10.32),
        Parameters.u: (0.0001, 10.32),
        Parameters.ki: (0.1, 6.26),
        Parameters.ka: (0.1, 6.26),
        # Derived quantities
        Parameters.keq: (1.0, 10.32),
        Parameters.kcat_prod: (10.0, 10.32),
        Parameters.kcat_sub: (10.0, 10.32),
        Parameters.vmax: (0.001, 17.01),
        Parameters.A: (0.0, 10.00),
        Parameters.mu_p: (-880.0, 680.00),
    }

    # Load concentrations and chemical energies from
    # data/coli_core_{concentrations|equilibrator}.csv
    data = []
    # Add chemical energies to data.
    mu_df = pd.read_csv(
        "./data/coli_core_equilibrator.csv",
        quotechar='"',
        skipinitialspace=True,
        comment="#",
    )
    mu_df = mu_df.set_index("model_name")
    for met in symbolic_model_core.metabolites:
        entry = mu_df.loc[met]
        data.append([Parameters.mu, met, NA_IDENTIFIER, entry.value, entry.error])

    # Add internal concentrations to data (from ECMDB)
    # Most data from the ECMDB is from the paper of Bennett et al. (2009)
    # where 3 conditions were measured - growth on glucose, acetate and glycerol.
    # We take the glucose data here with the marked 95% confidence interval as s.d.
    # for the acetate and glycerol we double the marked confidence interval.
    # for other data sources we take the s.d. as given, but if it's 0 we assume a default 10%.
    concentration_df = pd.read_csv("./data/coli_core_concentrations.csv")
    for metabolite in symbolic_model_core.metabolites:
        entries = concentration_df[concentration_df.model_name == metabolite].T
        for i in ["Glucose", "Glycerol", "Acetate"]:
            entry = entries.loc["Benett_{}".format(i)].iloc[0]
            if (
                not entry
                or (isinstance(entry, float) and np.isnan(entry))
                or "<" in entry
            ):
                continue
            # Note that these are in M instead of mM.
            mean, low, high = (
                float(i.strip("()")) * 1e3 for i in entry.split() if i != "to"
            )
            # Since these intervals are in log-normal, determine the original distribution and use
            # that instead.
            res = fit_distribution_from_ci(
                low, high, interval=0.95, start=(1, 1), distribution=scipy.stats.lognorm
            )
            if not res["success"]:
                raise ValueError("Couldn't fit measurement to distribution.")
            mean = res.distribution.mean()
            sd = res.distribution.std()
            if i != "Glucose":
                sd *= 2
            data.append([Parameters.c, metabolite, NA_IDENTIFIER, mean, sd])
        for i in range(1, 5):
            entry = entries.loc["EMCDB{}".format(i)].iloc[0]
            if not entry or (isinstance(entry, float) and np.isnan(entry)):
                continue
            mean, sd = (float(i.strip().strip("()")) for i in entry.split())
            if sd == 0.0:
                sd = mean * 0.1
            # Note that these are in µM instead of mM.
            data.append(
                [Parameters.c, metabolite, NA_IDENTIFIER, mean * 1e-3, sd * 1e-3]
            )

    # Add concentrations of external metabolites and some non-measured estimates.
    data.extend(
        [
            # Glucose is defined here as per our medium.
            [Parameters.c, "M_glc_D_e", NA_IDENTIFIER, 5, 1e-6],
            # CO2 should be around 10 µM = 0.01 mM (Bionumbers)
            [Parameters.c, "M_co2_e", NA_IDENTIFIER, 0.01, 0.0001],
            [Parameters.c, "M_co2_c", NA_IDENTIFIER, 0.01, 0.0001],
            # Oxygen Based on 8 mg O2/L (100% DO for fresh water @25 *C / 1 bar / 20% oxygen)
            # 8e-3 (g/L) / 32e3 (mmol/g) = .25 (mmol/L)
            # Corresponds to Bionumbers (250 µM)
            [Parameters.c, "M_o2_e", NA_IDENTIFIER, 0.25, 0.0025],
            [Parameters.c, "M_o2_c", NA_IDENTIFIER, 0.25, 0.0025],
            # Hydrogen based on pH 7
            # 10 ** -7 M = 1e-4 mmol/L
            [Parameters.c, "M_h_e", NA_IDENTIFIER, 1e-4, 1e-6],
            [Parameters.c, "M_h_c", NA_IDENTIFIER, 1e-4, 1e-6],
            # Water based on 1 kg / L
            # 1e3 (g/L) / 18 (g/mol) = 55.555 M = 55555 mmol/L
            [Parameters.c, "M_h2o_e", NA_IDENTIFIER, 55555, 55],
            [Parameters.c, "M_h2o_c", NA_IDENTIFIER, 55555, 55],
            # Phosphate concentration taken from M9 medium (Sigma)
            [Parameters.c, "M_pi_e", NA_IDENTIFIER, 64.3, 6.43],
            # Ammonium concentration taken from M9 medium (Sigma)
            [Parameters.c, "M_nh4_e", NA_IDENTIFIER, 18.7, 1.87],
            [Parameters.c, "M_nh4_c", NA_IDENTIFIER, 18.7, 1.87],
            # Other external concentrations should be around zero.
            [Parameters.c, "M_ac_e", NA_IDENTIFIER, 1e-3, 1e-4],
            [Parameters.c, "M_acald_e", NA_IDENTIFIER, 1e-3, 1e-4],
            [Parameters.c, "M_akg_e", NA_IDENTIFIER, 1e-3, 1e-4],
            [Parameters.c, "M_etoh_e", NA_IDENTIFIER, 1e-3, 1e-4],
            [Parameters.c, "M_for_e", NA_IDENTIFIER, 1e-3, 1e-4],
            [Parameters.c, "M_fru_e", NA_IDENTIFIER, 1e-3, 1e-4],
            [Parameters.c, "M_fum_e", NA_IDENTIFIER, 1e-3, 1e-4],
            [Parameters.c, "M_gln_L_e", NA_IDENTIFIER, 1e-3, 1e-4],
            [Parameters.c, "M_glu_L_e", NA_IDENTIFIER, 1e-3, 1e-4],
            [Parameters.c, "M_lac_D_e", NA_IDENTIFIER, 1e-3, 1e-4],
            [Parameters.c, "M_mal_L_e", NA_IDENTIFIER, 1e-3, 1e-4],
            [Parameters.c, "M_pyr_e", NA_IDENTIFIER, 1e-3, 1e-4],
            [Parameters.c, "M_succ_e", NA_IDENTIFIER, 1e-3, 1e-4],
            # For transport reactions, we can assume a keq of 1,
            # as the only driving force is the concentration gradient.
            # Diffusion based transport
            [Parameters.keq, NA_IDENTIFIER, "R_CO2t", 1, 0.001],
            [Parameters.keq, NA_IDENTIFIER, "R_H2Ot", 1, 0.001],
            [Parameters.keq, NA_IDENTIFIER, "R_O2t", 1, 0.001],
            [Parameters.keq, NA_IDENTIFIER, "R_FORti", 1, 0.001],
        ]
    )

    # Save fluxes from FBA solution.
    # Round fluxes to the largest tolerance.
    # Make sure to convert fluxes to mmol/gdw/s from mmol/gdw/h.
    tolerance = max(
        cobra_model_core.solver.configuration.tolerances.feasibility,
        cobra_model_core.solver.configuration.tolerances.optimality,
        cobra_model_core.solver.configuration.tolerances.integrality,
    )
    fluxes = cobra_model_core.optimize().to_frame().fluxes.to_frame() / 3600
    # Find reactions not active currently, and not active in any circumstance.
    # The last category can be removed from the model as it will never carry flux.
    # The first category cannot be removed, as it might be important for non-steady state
    # kinetics. However, in the steady state the flux should be (close to) 0.
    _, inactive = scan_flux_possibility(
        cobra_model_core, tol=tolerance, fraction_of_optimum=0.95
    )

    # ------------
    #   3. Reduce model:
    #       a. MinNW.
    #       b. NetworkReducer.
    #       c. FastCore.
    # ------------
    # Reactions that cannot be removed. These should match the SBML ids.
    protected_reactions = [
        "R_ATPM",
        "R_ATPS4r",
        "R_Biomass_Ecoli_core_w_GAM",
        "R_CO2t",
        "R_EX_co2_e",
        "R_EX_glc_e",
        "R_EX_h_e",
        "R_EX_h2o_e",
        "R_EX_nh4_e",
        "R_EX_o2_e",
        "R_EX_pi_e",
        "R_EX_pyr_e",
        "R_GLCpts",
        "R_H2Ot",
        "R_NADTRHD",
        "R_NH4t",
        "R_O2t",
        "R_PIt2r",
    ]
    # Metabolites that cannot be removed.
    protected_metabolites = [
        "M_adp_c",
        "M_amp_c",
        "M_atp_c",
        "M_co2_c",
        "M_co2_e",
        "M_etoh_c",
        "M_glc_D_e",
        "M_h_c",
        "M_h_e",
        "M_h2o_c",
        "M_h2o_e",
        "M_nad_c",
        "M_nadh_c",
        "M_nadp_c",
        "M_nadph_c",
        "M_nh4_c",
        "M_nh4_e",
        "M_o2_c",
        "M_o2_e",
        "M_pep_c",
        "M_pi_c",
        "M_pi_e",
        "M_pyr_c",
    ]

    with cobra_model_core:
        cobra_model_core.reactions.R_EX_glc_e.lower_bound = -glc_uptake
        solution = cobra_model_core.optimize().to_frame().fluxes

    optimization_space = 0.995  # (+/- .5% difference is tolerated)
    protected_behaviour = [
        # Growth on glucose with oxygen
        {
            "reactions": ["R_EX_glc_e", "R_EX_o2_e", "R_Biomass_Ecoli_core_w_GAM"],
            "directions": [-1.0, -1.0, 1.0],
            "values": [
                abs(solution.loc["R_EX_glc_e"]),
                abs(solution.loc["R_EX_o2_e"]) * 1 / optimization_space,
                abs(solution.loc["R_Biomass_Ecoli_core_w_GAM"]) * optimization_space,
            ],
        }
    ]

    reduction_settings = {
        "silence_output": silence,
        "protected_reactions": protected_reactions,
        "protected_metabolites": protected_metabolites,
        "protected_behaviour": protected_behaviour,
    }

    # Set up reducers and individual settings. For more individual settings,
    # see the relevant classes in `pipeline/reduction_steps.py`
    # NetworkReducer
    nw_reducer_settings = reduction_settings.copy()
    nw_reducer_settings.update({})
    nw_reducer = NetworkReducer(
        paths["cplex"],
        paths["cna"],
        paths["networkreducer"],
        matlab_instance,
        use_cache,
        **nw_reducer_settings
    )

    # MinNW
    mn_reducer_settings = reduction_settings.copy()
    mn_reducer_settings.update({"delete_blocked": 0})
    mn_reducer = MinNW(
        paths["cplex"],
        paths["cobra"],
        paths["min_nw"],
        matlab_instance,
        use_cache,
        **mn_reducer_settings
    )

    # FastCore
    fc_reducer_settings = reduction_settings.copy()
    fc_reducer_settings.update({})
    fc_reducer = FastCore(
        paths["cplex"],
        paths["cobra"],
        paths["fastcore"],
        matlab_instance,
        use_cache,
        **fc_reducer_settings
    )

    reducers = [nw_reducer, mn_reducer, fc_reducer]
    # ------------
    #   4. Generate model rate laws:
    #       a. Pick right rate laws per reaction.
    #       b. Apply relevant conservations.
    # ------------
    # Ignore all exchanges (We'll keep the transports, but remove the exports and just use
    # the external metabolites)
    ignore = []  # list(i.id for i in cobra_model_core.exchanges) + [
    #     "R_Biomass_Ecoli_core_w_GAM"
    # ]
    # Ignore Biomass reaction for balancing purposes only.
    # We do need to provide the parameters manually then.
    ignore_balancing = list(i.id for i in cobra_model_core.exchanges) + [
        "R_Biomass_Ecoli_core_w_GAM"
    ]
    ignore_metabolites_balancing = []
    extra_parameters = {}

    # Reactions that will have an abstract pool added to balance out the chemical energy.
    abstract_metabolites = []
    # Metabolites that are fixed at a constant value.
    # All exchange metabolites.
    fixed_metabolites = []

    # Reactions that are fixed at a constant value
    # All inactive reactions.
    fixed_reactions = {r: 0 for r in inactive}
    # All exports, biomass reaction and ATP maintenance reaction.
    fixed_reactions.update(
        {
            r.id: fluxes.loc[r.id, "fluxes"]
            for r in cobra_model_core.exchanges
            + [
                cobra_model_core.reactions.R_Biomass_Ecoli_core_w_GAM,
                cobra_model_core.reactions.R_ATPM,
            ]
        }
    )
    # If a reaction is marked as fixed, we need to remove any parameters that are
    # related from the data.
    data = [
        (p, m, r, v, sd)
        for (p, m, r, v, sd) in data
        if r not in fixed_reactions or r in ignore
    ]

    # Reactions that should always be simplified:
    simplify_reactions = [
        # Diffusion based transport reactions
        "R_CO2t",
        "R_H2Ot",
        "R_O2t",
        "R_FORti",
    ]

    # Reactions that cannot use the default scaling parameter (u)
    scaling_parameters = {
        r: Parameters.sep.join((Parameters.km_tot_rate, r)) for r in simplify_reactions
    }
    # Fixed reactions don't need a scaling parameter? TODO: Check if automatic.

    extra_parameters_scaling = extra_parameters.copy()
    # We need to add all the u_v parameters to make sure that they can be updated.
    # The value doesn't really matter since it will be overwritten.
    extra_parameters_scaling.update(
        (Parameters.sep.join((Parameters.u_v, r)), 1)
        for r in symbolic_model_core.reactions
        if r not in scaling_parameters
    )
    # For the simplified reactions, use km_tot_rate instead.
    extra_parameters_scaling.update(
        (Parameters.sep.join((Parameters.km_tot_rate, r)), 1)
        for r in scaling_parameters
    )

    custom_reactions = {}

    # Parameters that are fixed to a set value, and will not have sensitivities calculated.
    fixed_parameters = {"R": 0.008314, "T": 300}

    # Kineticzation rate law settings:
    default_rate_law = "modular"
    default_rate_law_args = {"parametrisation": "weg", "sub_rate_law": "CM"}
    simplified_rate_law = "modular"
    simplified_rate_law_args = {"parametrisation": "weg", "sub_rate_law": "PM"}
    merge_parameters = ["enzyme_rate", "km_merged", "km_rate"]
    limit_total_lumped_stoichiometry = True
    max_reaction_order = 3
    positive_fractional_powers = False

    # ------------
    #   5. Simulate all models:
    #       a. Pick sample parameters (Multivariate log-normal from balancing + LHS).
    #       b. Simulate (?):
    #           I. Sensitivity at steady state.
    # ------------
    # This is the settings for the model that will be compiled.
    kineticizer_settings = {
        "silence_output": silence,
        "rate_law_type": default_rate_law,
        "rate_law_args": default_rate_law_args,
        "simplified_rate_law_type": simplified_rate_law,
        "simplified_rate_law_args": simplified_rate_law_args,
        "ignore_reactions": ignore,
        "fixed_reactions": fixed_reactions,
        "fixed_metabolites": fixed_metabolites,
        "replace_reactions": custom_reactions,
        "add_abstract_metabolites": abstract_metabolites,
        "simplify_reactions": simplify_reactions,
        "simplify_lumped_reactions": True,
        "limit_total_lumped_stoichiometry": limit_total_lumped_stoichiometry,
        "max_reaction_order": max_reaction_order,
        "merge_parameters": merge_parameters,
        "biomass_scaling": False,
        "scaling_factor": False,
        "positive_fractional_powers": positive_fractional_powers,
        "cythonize": True,
        "cythonize_settings": {"skip_jacobian": False, "skip_sensitivity": False},
    }
    kineticizer = ModelKineticizer(use_cache=use_cache, **kineticizer_settings)
    # Separate settings for the parametrization procedure
    # Here we use several different settings.
    kineticizer_parameterizer_settings = copy.deepcopy(kineticizer_settings)
    # We do need a cython model to test the reaction speeds,
    # but we don't need to spend the time to compile the jacobian
    kineticizer_parameterizer_settings["cythonize"] = True
    kineticizer_parameterizer_settings["cythonize_settings"]["skip_jacobian"] = True
    kineticizer_parameterizer_settings["cythonize_settings"]["skip_sensitivity"] = True
    kineticizer_parameterizer = ModelKineticizer(
        use_cache=use_cache, **kineticizer_parameterizer_settings
    )

    parameterizer_settings = {
        "R": fixed_parameters["R"],
        "T": fixed_parameters["T"],
        "fixed_metabolites": fixed_metabolites,
        "silence_output": silence,
        "max_samples": 10000,
        "sample_batch_size": 10000,
        "A_tol": 0.1,
        "A_max": float("inf"),
        "constraint_factor": 5,
        "zero_concentration": 1e-9,
        "optimized_directionality_data_error": {
            Parameters.c: 0.01,
            Parameters.mu: 0.01,
            Parameters.A: 0.01,
        },
        "constrain_inactive_reactions": False,
        "ss_error_tolerance": 1e-6,
        "ignore_reactions": sorted(ignore + list(fixed_reactions.keys())),
        "ignore_reactions_balancing": ignore_balancing,
        "ignore_metabolites_balancing": ignore_metabolites_balancing,
        "extra_parameters": extra_parameters_scaling,
        "default_scaling_parameter": Parameters.u_v,
        "scaling_parameters": scaling_parameters,
        "augment": {"pre": False, "post": False},
    }
    parameterizer = SteadyStateModelParameterizer(
        use_cache=use_cache, **parameterizer_settings
    )

    test = ReductionSamplingPipeline(
        sampling_case_directory,
        core_model_path,
        fluxes,
        priors,
        data,
        kineticizer,
        kineticizer_parameterizer,
        parameterizer,
        reducers,
        use_cache=use_cache,
    )

    warnings.simplefilter("always")
    test.run()


if 1 in run_experiments:
    for model, settings in model_settings.items():
        if model in run_optimizations or run_optimizations == "all":
            experiment_1(model, settings)
if 2 in run_experiments:
    experiment_2()
