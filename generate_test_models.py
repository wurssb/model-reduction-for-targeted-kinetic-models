"""Generate the testing models for optimization."""
import pathlib
import cobra
import pandas as pd
import libsbml


def to_model(name, reaction_lines, metabolite_lines):
    """Convert the specification to a cobrapy model."""
    model = cobra.Model(name)
    metabolites = []
    for line in metabolite_lines.splitlines():
        if not line.strip():
            continue
        name, formula = line.replace(" ", "").split(":")
        if name.startswith("o_"):
            metabolites.append(
                cobra.Metabolite(
                    name.strip(), formula.strip(), compartment="observables"
                )
            )
            name = name[2:]
        metabolites.append(
            cobra.Metabolite(name.strip(), formula.strip(), compartment="cytosol")
        )

    model.add_metabolites(sorted(metabolites, key=lambda m: m.id))

    for line in reaction_lines.splitlines():
        if not line.strip():
            continue
        name, reaction = line.split(":")
        r = cobra.Reaction(name.strip())
        model.add_reactions([r])
        r.build_reaction_from_string(reaction.strip())
        if r.id.startswith("input_"):
            r.bounds = (0.0, 10.0)
        elif r.id.startswith("output_"):
            r.bounds = (0.0, 1000.0)
            r.objective_coefficient = 1
        else:
            r.bounds = (-1000.0, 1000.0)

    for reaction, balance in cobra.manipulation.check_mass_balance(model).items():
        if any(i in reaction.id for i in ("input", "output", "exchange")):
            continue

        energy = balance.pop("E", 0)
        if abs(energy) > 25:
            print("Excess driving force: {} - {}".format(reaction.id, energy))

        if balance:
            print("Invalid balance: {} - {}".format(reaction.id, balance))

    return model


def verify_sbml(document, strict=False, verbose=True):
    """Check for sbml errors.

    Raises ValueError on errors (and warnings with strict=True)
    """
    document.checkConsistency()
    n_errors = document.getNumErrors()
    fail = False
    for i in range(n_errors):
        error = document.getError(i)
        if verbose:
            print(
                "{} ({}): {}".format(
                    error.getSeverityAsString(),
                    error.getLine(),
                    error.getShortMessage(),
                )
            )
        if not strict and error.isWarning():
            continue
        else:
            fail = True
    if fail:
        raise ValueError("Invalid SBML document.")


def update_sbml(path):
    """Update the SBML file with some extra annotation for the observables."""
    path = str(path)
    doc = libsbml.readSBMLFromFile(path)
    verify_sbml(doc, strict=False, verbose=False)
    model = doc.getModel()

    # # Convert to fbc v1.
    # # TODO: Check why we use v1 for our input before?
    # #       I don't think this is strictly necessary?
    # conversion = libsbml.ConversionProperties()
    # conversion.addOption("convert fbc v2 to fbc v1")
    # success = doc.convert(conversion)
    # if success == libsbml.LIBSBML_OPERATION_FAILED:
    #     raise ValueError("SBML conversion failed.")
    # elif success == libsbml.LIBSBML_CONV_CONVERSION_NOT_AVAILABLE:
    #     raise NotImplementedError("SBML conversion not available")

    # Add a rule for each observable and correct SBO specification.
    for species in model.getListOfSpecies():
        id = species.getId()
        if not id.startswith("o_"):
            continue
        species.setSBOTerm("SBO:0000406")

        r = model.createAssignmentRule()
        r.setVariable(id)
        r.setMath(libsbml.parseFormula(id[2:]))

    verify_sbml(doc, strict=False, verbose=False)

    success = libsbml.writeSBMLToFile(doc, path)
    if not success:
        raise ValueError("Could not write SBMLfile to: {}".format(path))


def fva(model, fraction=0):
    """FVA with extended output."""
    df = pd.concat(
        {
            "loop": cobra.flux_analysis.flux_variability_analysis(
                model, loopless=False, fraction_of_optimum=fraction
            ),
            "less": cobra.flux_analysis.flux_variability_analysis(
                model, loopless=True, fraction_of_optimum=fraction
            ),
        },
        axis=1,
    )
    df = (
        df.assign(
            lb=[model.reactions.get_by_id(r).lower_bound for r in df.index],
            ub=[model.reactions.get_by_id(r).upper_bound for r in df.index],
            ΔG=[
                model.reactions.get_by_id(r).check_mass_balance().pop("E", 0)
                for r in df.index
            ],
        )
        .round(0)
        .astype(int)
    )
    return df.assign(
        reactions=[
            model.reactions.get_by_id(r).build_reaction_string() for r in df.index
        ]
    ).sort_index()


def production(model):
    """Check production capabilities per input source."""
    with model:
        inputs = [
            reaction for reaction in model.reactions if reaction.id.startswith("input_")
        ]
        outputs = [
            reaction
            for reaction in model.reactions
            if reaction.id.startswith("output_")
        ]

        results = {}

        for reaction in inputs:
            reaction.upper_bound = 0
        for input in inputs:
            with model:
                input.upper_bound = 1
                for output in outputs:
                    model.objective = output
                    results[
                        (
                            input.id.replace("input_", "-> "),
                            output.id.replace("output_", "max "),
                        )
                    ] = cobra.flux_analysis.flux_variability_analysis(
                        model, reaction_list=outputs
                    )
    df = pd.concat(results, axis=1)
    df.index = [r.replace("output_", "") + " ->" for r in df.index]
    return df


# Define some models, structure and metabolites.
models = {
    "toy": (
        """
r1: A + Y <-> B + Z
r2: B <-> 2 C
r3: B <-> 2 D
r4: C <-> EE
r5: D <-> EE
r6: EE + H <-> F + X
r7: F <-> G + X
r8: G <-> H + X
r9: II <-> J
r10: J <-> B
r11: J <-> K + L
r12: L <-> M + N
r13: M + Y <-> D + Z
r14: L <-> H
r15: G <-> 2 K + X
r16: Z <-> Y
input_A: <-> A
input_I: <-> II
output_N: N <->
output_K: K <->
output_X: X <->
""",
        """
o_A: C6  E 100
B:   C6  E  85
C:   C3  E  40
o_D: C3  E  40
EE:   C3 E  35
F:   C6  E  70
G:   C5  E  60
H:   C4  E  45
o_II: C6 E 100
J:   C6  E  95
K:   C2  E  20
L:   C4  E  70
M:   C3  E  65
N:   C1  E   5
o_X: C1  E  10
o_Y: R   E  50
o_Z: R   E  60
""",
    ),
    "large_cycle": (
        """
r1: A <-> B + Z
r2: B <-> C + Z
r3: C <-> G
r4: A + X <-> D + N + Y
r5: D <-> EE
r6: EE + Y <-> F + X
r7: F <-> G
r8: G <-> H + N
r9: H <-> II + Z
r10: II <-> J
r11: J <-> K
r12: H <-> K + Z
r13: K + X <-> L + Y
r14: L <-> M
r15: 2 L <-> EE + P
r16: D + X <-> 2 M + Y
r17: X <-> Y + P
input_A: <-> A
output_N: N <->
output_II: II <->
output_K: K <->
output_M: M <->
output_Z: Z <->
exchange_P: <-> P
""",
        """
o_A: C8  E 100
B: C7    E  90
C: C6    E  80
D: C6P   E  85
EE: C6P  E  85
F: C6    E  70
G: C6    E  72
H: C4    E  50
o_II: C3 E  40
J: C3    E  35
o_K: C3  E  33
L: C3P   E  45
o_M: C3P E  40
o_N: C2  E  20
o_X: RP  E  20
o_Y: R   E  10
P: P     E   0
Z: C1    E   5
""",
    ),
    "edemp": (
        """
r1: A <-> B
r2: B <-> C
r3: C + X <-> D + Y
r4: A + X <-> EE + Y
r5: EE <-> F
r6: B + X <-> F + Y
r7: D <-> F
r8: F <-> G + J
r9: G <-> M
r10: G + M <-> H
r11: H + Y <-> II + X
r12: II <-> EE
r13: G <-> K
r14: K <-> L
r15: L + Y <-> J + X
r16: M + Y <-> N + X
input_A: <-> A
input_B: <-> B
input_C: <-> C
output_J: J <->
output_N: N <->
""",
        """
o_A: C6 E 112
o_B: C6 E 110
o_C: C6 E 108
D: C6P  E 114
EE: C6P E 105
F: C6P  E 102
G: C3P  E  65
H: C6P2 E 115
II: C6P E 107
o_J: C3 E  25
K: C3P  E  55
L: C3P  E  45
M: C3P  E  50
o_N: C3 E  30
o_X: RP E  15
o_Y: R  E   5
""",
    ),
    "curcumin": (
        """
r1: A <-> B + C
r2: B <-> C
r3: B <-> D + EE
r4: C <-> EE + F
r5: B + D <-> G
r6: C + D <-> H
r7: B + F <-> II
r8: C + F <-> J
r9: EE + EE <-> K
r10: K + EE <-> B + C
input_A: <-> A
output_G: G <->
output_H: H <->
output_II: II <->
output_J: J <->
""",
        """
o_A:  C6 E90
B:    C3 E43
C:    C3 E42
D:    C1 E6
EE:   C2 E31
F:    C1 E7
o_G:  C4 E46
o_H:  C4 E45
o_II: C4 E44
o_J:  C4 E46
K:    C4 E60
""",
    ),
    "co_factors": (
        """
r1: A + X1 <-> C + Y1
r2: B + X2 <-> D + Y2
r3: C <-> EE + F
r4: EE + X2 <-> G + Y2
r5: D + Y2 <-> F + X2
r6: G + F <-> II
r7: F <-> H + Z
r8: H <-> J
r9: II + Y1 <-> K + X1
r10: K + Y2 <-> L + X2
r11: C + X2 <-> II + Y2
r12: X1 <-> Y1 + P
r13: X2 <-> Y2 + P
r14: X1 + Y2 <-> Y1 + X2
input_A: <-> A
input_B: <-> B
output_J: J <->
output_L: L <->
output_Z: Z <->
""",
        """
o_A: C6  E 100
o_B: C3  E  50
C: C6P   E 110
D: C3P   E  60
EE: C3P  E  60
F: C3    E  40
G: C3P2  E  70
H: C2    E  25
II: C6P2 E 115
o_J: C2  E  20
K: C6P   E 100
o_L: C6  E  80
o_X1: RP E  21
o_Y1: R  E   9
o_X2: RP E  21
o_Y2: R  E   9
P: P     E   0
o_Z: C   E   5
""",
    ),
}
write_out = False
if write_out:
    out_dir = pathlib.Path("../data/test_models")
    out_dir.mkdir(exist_ok=True)

for name, (reactions, metabolites) in models.items():
    print(name)
    if name != 'curcumin':
        continue
    model = to_model(name, reactions, metabolites)
    print(fva(model))
    print(production(model))

    if write_out:
        path = str(out_dir / (name + ".xml"))
        cobra.io.write_sbml_model(model, path, f_replace={})
        update_sbml(path)
