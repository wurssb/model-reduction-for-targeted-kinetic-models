import collections
import pathlib
import itertools

import matplotlib

# This makes sure that the text is exported as text and not as paths
# when saving to svg.
matplotlib.rc("svg", fonttype="none")
matplotlib.rc(
    "font", **{"serif": ["Times"], "sans-serif": ["Arial"], "monospace": ["Courier"]}
)

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import scipy
import h5py

import symbolicSBML
from symbolicSBML import Parameters

import plot_util
from model_settings import model_settings


def load_sensitivity(base_directory):
    if not base_directory.exists():
        raise ValueError("Non-existant base directory.")
    data_paths = sorted(
        (base_directory / "amici_simulator/solutions").glob(
            "*/experiment_*/simulations.h5"
        )
    )
    if not data_paths:
        raise ValueError("No data could be found in base directory.")
    # These data files are about 2 gb each, so we want to load and process things
    # right away, then caching the results to disk for next time we need it.
    data = []
    runs = []  # models x experiments x run
    for i, path in enumerate(data_paths):
        print(i)
        experiment = path.parents[0].name.split("_")[-1]
        model = path.parents[1].name
        with h5py.File(path, "r") as h5f:
            # Each run should have a seperate entry.
            # Since it comes from matlab it starts at 1 instead of 0!
            for run in h5f.keys():
                p = h5f[run]["parameters"]
                s = h5f[run]["states"]
                t = h5f[run]["t"]

                index = pd.Series(np.array(p).astype("U")).str.strip()
                columns = pd.Series(np.array(s).astype("U")).str.strip()
                timepoints = np.array(t).squeeze()

                # Load actual sensitivity data
                sx = h5f[run]["sx"][:]
                # Check that the shape is correct.
                assert sx.shape == index.shape + columns.shape + timepoints.shape

                # Absolute mean of all timepoints
                sx = np.abs(sx).mean(axis=-1)
                data.append(pd.DataFrame(data=sx, index=index, columns=columns))
                runs.append((model, experiment, int(run)))

    df = pd.concat((df.stack() for df in data), axis=1).T
    df.columns.set_names(["parameter", "state"], inplace=True)
    df = df.set_index(
        pd.MultiIndex.from_tuples(runs, names=["model", "experiment", "run"])
    )
    df.to_hdf(
        base_directory / "cached_sensitivity.h5",
        key="sensitivity",
        complevel=5,
        format="fixed",
    )
    return df


def write_error_cache(error_cache_path, optimized_simulations, ref):
    # Error per state / experiment of output simulations
    simulations = {
        key: df.pivot_table(
            index="time", columns=["run", "experiment", "state"], values="value"
        )
        for key, df in optimized_simulations.items()
    }
    truth = ref.pivot_table(
        index="time", columns=["experiment", "state"], values="value"
    )

    # Have to remove the categorical data type as pandas can't handle it for hdf.
    truth.columns = pd.MultiIndex.from_tuples(
        truth.columns.tolist(), names=truth.columns.names
    )
    truth.to_hdf(error_cache_path, key="truth", complevel=5, format="fixed")

    for run in simulations:
        # Same here with columns
        simulations[run].columns = pd.MultiIndex.from_tuples(
            [tuple(str(i) for i in j) for j in simulations[run].columns.tolist()],
            names=simulations[run].columns.names,
        )
        diff = (
            # This used to broadcast fine without the concat but gives a recursion error now?
            (
                simulations[run]
                - truth.reindex(
                    simulations[run].columns.droplevel("run"), axis=1
                ).values
            )
            # ((simulations[run] - truth) / truth)
            .dropna(axis=1)
            .reorder_levels(["run", "experiment", "state"], axis=1)
            .sort_index(axis=1)
        )
        rmse = np.sqrt((diff * diff).mean(axis=0))
        # Note that the run index of traces starts with 0, but for simulations
        # it starts with 1, so let's change that.
        rmse = rmse.reset_index()
        rmse.run = rmse.run.astype(int) - 1
        rmse = rmse.set_index(["run", "experiment", "state"]).set_axis(
            ["rmse"], axis=1, inplace=False
        )

        rmse.to_hdf(error_cache_path, key=run, complevel=5, format="fixed")


if __name__ == "__main__":
    interactive = False

    scaling = {
        "toy": {
            "ref_conc": (1e-3, 1e2),
            "opt_RMSE": (0, 6.2),
            "opt_scale": "linear",
            "opt_experiments": ("experiment_2", "experiment_8"),
            "opt_states": {"A", "D", "I", "X"},
            "opt_conc_top": (0, 15),
            "opt_conc_bot": (0, 19),
            "trace_n": (1, 1000),
            "trace_t": (0.1, 125),
            "trace_score": (1.6e6, 12e6),
            "dist_ji_example": True,
        },
        "edemp": {
            "ref_conc": (1e-3, 1e2),
            "opt_RMSE": (0, 6.2),
            "opt_scale": "linear",
            "opt_experiments": ("experiment_4", "experiment_8"),
            "opt_states": {"C", "A", "B", "X"},
            "opt_conc_top": (0, 10),
            "opt_conc_bot": (0, 11),
            "trace_n": (1, 1000),
            "trace_t": (0.1, 150),
            "trace_score": (1.5e5, 2.5e7),
            "dist_ji_example": False,
        },
        "co_factors": {
            "ref_conc": (1e-3, 1e2),
            "opt_RMSE": (0, 3.5),
            "opt_scale": "linear",
            "opt_experiments": ("experiment_3", "experiment_8"),
            "opt_states": {"L", "Y1", "Y2", "Z"},
            "opt_conc_top": (0, 3.5),
            "opt_conc_bot": (0, 3.5),
            "trace_n": (1, 1000),
            "trace_t": (0.1, 155),
            "trace_score": (1.4e5, 2.8e6),
            "dist_ji_example": False,
        },
        "curcumin": {
            "ref_conc": (1e-3, 1e2),
            "opt_RMSE": (1e-3, 1.8),
            "opt_scale": "log",
            "opt_experiments": ("experiment_3", "experiment_7"),
            "opt_states": {"G", "H", "I", "J"},
            "opt_conc_top": (0, 1e-3),
            "opt_conc_bot": (0, 1e-3),
            "trace_n": (1, 1000),
            "trace_t": (0.1, 125),
            "trace_score": (8e4, 5e5),
            "dist_ji_example": False,
        },
        "large_cycle": {
            "ref_conc": (1e-3, 1e2),
            "opt_RMSE": (0, 2.6),
            "opt_scale": "linear",
            "opt_experiments": ("experiment_3", "experiment_7"),
            "opt_states": {"A", "I", "P", "X"},
            "opt_conc_top": (0, 3),
            "opt_conc_bot": (0, 5),
            "trace_n": (1, 1000),
            "trace_t": (0.1, 130),
            "trace_score": (1.8e5, 7e6),
            "dist_ji_example": False,
        },
    }

    model = "large_cycle"
    settings = model_settings[model]
    scaling = scaling[model]
    optimization_case_directory = pathlib.Path("./results/optimization") / model
    output_location = pathlib.Path("./results/optimization_results") / model
    output_location.mkdir(exist_ok=True, parents=True)
    runs = ["full", "networkreducer", "minnw", "fastcore"]

    # Load reference model and parameters
    symbolic_reference_model = symbolicSBML.SymbolicModel.from_sbml(
        optimization_case_directory / "parameterizer" / "parametrized_model.xml"
    )
    parameters = symbolic_reference_model.sbml_model.parameter_values
    initial_concentrations = symbolic_reference_model.sbml_model.initial_concentrations

    # Load reference simulation and optimized simulations.
    simulation_results_dir = optimization_case_directory / "amici_simulator"
    simulation_cache_path = optimization_case_directory / "simulations_cache.h5"
    if not simulation_cache_path.exists():
        ref = plot_util.parse_reference_simulations(simulation_results_dir, "full")
        optimized_simulations = {
            run: plot_util.parse_optimized_simulations(simulation_results_dir, run)
            for run in runs
        }

        for name, df in itertools.chain([("ref", ref)], optimized_simulations.items()):
            df.to_hdf(simulation_cache_path, key=name, complevel=5, format="table")
    else:
        optimized_simulations = {
            run: pd.read_hdf(simulation_cache_path, key=run) for run in runs
        }
        ref = pd.read_hdf(simulation_cache_path, key="ref")

    # Load optimization traces.
    optimized_traces = {
        run: dict(
            zip(
                ["traces", "end", "ranges", "settings"],
                plot_util.parse_optimization_results(optimization_case_directory, run),
            )
        )
        for run in runs
    }

    # Load simulation errors
    error_cache_path = optimization_case_directory / "error_cache.h5"
    if not error_cache_path.exists():
        write_error_cache(error_cache_path, optimized_simulations, ref)
    simulation_errors = {
        run: pd.read_hdf(error_cache_path, key=run).reset_index() for run in runs
    }

    # Some useful sets to mark what goes where.
    helper_states = {i[2] for i in settings["abstract_metabolites"]}
    observed_states = set(symbolic_reference_model.metabolites) - helper_states
    fitted_states = (
        set([i[2:] for i in symbolic_reference_model.observables]) - helper_states
    )

    training_experiments = {
        "experiment_{}".format(i)
        for i, experiment in enumerate([i["set"] for i in settings["experiments"]])
        if experiment == "optimization"
    }
    verification_experiments = {
        "experiment_{}".format(i)
        for i, experiment in enumerate([i["set"] for i in settings["experiments"]])
        if experiment == "test"
    }

    # Error per experiment / model for supplementary information
    if False:
        df = (
            pd.concat(simulation_errors)
            .reset_index()
            .drop("level_1", axis=1)
            .rename({"level_0": "model"}, axis=1)
        )

        for i, states in enumerate((fitted_states, observed_states)):
            error = (
                df[df.state.isin(states)]
                # Average per run
                .groupby(["model", "experiment", "run"])
                .rmse.mean()
                # mean and standard deviation over the runs.
                .groupby(["model", "experiment"])
                .agg([np.mean, np.std])
                # Reshape
                .reset_index()
                .pivot(index="experiment", columns="model")
                .sort_index(axis=1, level=1)
                .swaplevel(i=0, j=1, axis=1)
                .round(2)
            )
            mode = "w" if i == 0 else "a"
            with open(output_location / "error_table.txt", mode) as out:
                print(error[["full", "minnw", "fastcore", "networkreducer"]], file=out)
                print(file=out)

    # Plot reference simulations.
    if False:
        # Remove output / input states by filtering on underscores.
        # Filter on experiments
        states = {}
        plot_ref = ref[(~ref.state.str.contains("_")) & ~ref.state.isin(states)]
        # Replace names that were changed for Matlab.
        plot_ref.loc[:, "state"] = plot_ref.loc[:, "state"].replace(
            {"EE": "E", "II": "I"}
        )

        # plot_ref = plot_ref.assign(fitted=ref.state.isin(fitted_states))

        fig, axes = plt.subplots(3, 3, sharex=True, sharey=True)
        # Colors are already unique, so we just rotate the 4 first styles
        # to avoid the more indistinctive styles.
        dashes = plot_util.unique_dashes(4) * (1 + plot_ref.state.unique().size // 4)
        for i, (ax, experiment) in enumerate(
            zip(axes.flat, sorted(plot_ref.experiment.unique()))
        ):
            sns.lineplot(
                data=plot_ref[plot_ref.experiment == experiment],
                x="time",
                y="value",
                hue="state",
                style="state",
                dashes=dashes,
                ax=ax,
            )
            ax.set_title("Experiment {}".format(i + 1), pad=-5)
            ax.set_xlabel("")
            ax.set_ylabel("")
            handles, labels = ax.get_legend_handles_labels()
            ax.get_legend().remove()

        axes.flat[0].set_xlim(ref.time.min(), ref.time.max())
        axes.flat[0].set_ylim(*scaling["ref_conc"])
        axes.flat[0].set_yscale("log")
        axes.flat[3].set_ylabel("Concentration (mM)", fontsize="large")
        axes.flat[-2].set_xlabel("Time (s)", fontsize="large")
        sns.despine(fig)
        fig.subplots_adjust(hspace=0.275)

        labels[0] = "Metabolite"
        labels[-3] = labels[-3].capitalize()
        legend = axes.flat[2].legend(
            handles=handles,
            labels=labels,
            frameon=False,
            bbox_to_anchor=(1.01, 1.01),
            fontsize="small",
        )

        fig.savefig(
            output_location / "reference_simulations.png",
            bbox_extra_artists=(legend,),
            bbox_inches="tight",
            dpi=300,
        )

    # Plot optimization result simulations.
    if False:
        states = scaling["opt_states"]
        experiments = scaling["opt_experiments"]

        def plot_fit_range(
            opt_df,
            ref_df,
            ax,
            color_map=None,
            plot_type="lines",
            line_alpha=0.1,
            quantile_alpha=0.2,
            quantiles=[0.1, 0.2, 0.3, 0.4],
        ):
            # First plot the fit.
            for state, df in opt_df.groupby("state", observed=True):
                df = df.drop("state", axis=1).pivot(
                    index="time", columns="run", values="value"
                )
                if plot_type == "lines":
                    ax.plot(
                        df.index.values,
                        df.values,
                        c=color_map[state],
                        alpha=line_alpha,
                        linewidth=1.5,
                    )
                elif plot_type == "quantiles":
                    for q_low in quantiles:
                        ax.fill_between(
                            df.index.values,
                            df.quantile(q_low, axis=1).values,
                            df.quantile(1 - q_low, axis=1).values,
                            color=color_map[state],
                            alpha=quantile_alpha,
                        )
                    # Add median as extra line.
                    ax.plot(
                        df.index.values,
                        df.median(axis=1).values,
                        c=color_map[state],
                        linewidth=1.5,
                    )

            # Next plot the reference.
            for state, df in ref_df.groupby("state", observed=True):
                df = df.drop("state", axis=1).set_index("time")
                ax.plot(
                    df.index.values,
                    df.values,
                    c=color_map[state],
                    label=state,
                    linewidth=1.5,
                    linestyle="dashed",
                )
            return ax

        fig = plt.figure()
        gs = fig.add_gridspec(nrows=10, ncols=4, hspace=0.3)

        rows = [(0, 3), (3, 6), (7, 9)]

        axes = np.array(
            [
                [fig.add_subplot(gs[start:end, j]) for start, end in rows]
                for j in range(4)
            ]
        )

        model_map = {
            "full": "Original",
            "minnw": "minNW",
            "fastcore": "FastCore",
            "networkreducer": "NetworkReducer",
        }
        colors = plot_util.colors("base")
        color_map = dict(
            zip(sorted(states), [colors[i] for i in ("orange", "blue", "cyan", "red")])
        )
        stored_errors = {}
        ref_renamed = ref.copy()
        ref_renamed.loc[:, "state"] = ref_renamed.loc[:, "state"].replace(
            {"EE": "E", "II": "I"}
        )
        for i, run in enumerate(model_map.keys()):
            df = optimized_simulations[run]
            df.loc[:, "state"] = df.loc[:, "state"].replace({"EE": "E", "II": "I"})

            # Plot of simulation results on first two rows.
            for j, experiment in enumerate(experiments):
                ax = axes[i, j]
                ax.set_xlim(0, ref_renamed.time.max())

                if i == 0:
                    pass
                else:
                    ax.set_yticklabels([])

                if j == 0:
                    ax.set_ylim(*scaling["opt_conc_top"])
                    ax.set_xticklabels([])
                    ax.set_title(model_map[run])
                elif j == 1:
                    ax.set_ylim(*scaling["opt_conc_bot"])

                if i == len(model_map) - 1:
                    ax.yaxis.set_label_position("right")
                    if j == 0:
                        # In the data we count from 0, so add 1 to the figure index.
                        ax.set_ylabel(
                            "Experiment {}\n(Training)".format(
                                int(scaling["opt_experiments"][0].split("_")[1]) + 1
                            )
                        )
                    elif j == 1:
                        ax.set_ylabel(
                            "Experiment {}\n(Test)".format(
                                int(scaling["opt_experiments"][1].split("_")[1]) + 1
                            )
                        )

                plot_fit_range(
                    df[df.state.isin(states) & (df.experiment == experiment)].drop(
                        "experiment", axis=1
                    ),
                    ref_renamed[
                        ref_renamed.state.isin(states)
                        & (ref_renamed.experiment == experiment)
                    ].drop("experiment", axis=1),
                    ax=axes[i, j],
                    color_map=color_map,
                    plot_type="lines",
                )

            # Add a legend using the final ax.
            if i == 3:
                handles = [
                    matplotlib.lines.Line2D([0, 1], [0, 1], linestyle="-", color=c)
                    for c in color_map.values()
                ]
                labels = [k.replace("II", "I") for k in color_map.keys()]
                lgd1 = ax.legend(
                    handles,
                    labels,
                    frameon=False,
                    title="Metabolite",
                    fontsize="small",
                    loc="lower left",
                    bbox_to_anchor=(1.2, 0.55),
                )

            # Plot of error distribution on last row.
            ax = axes[i, -1]

            df_error = simulation_errors[run]
            # Remove helper states
            df_error = df_error[~df_error.state.isin(helper_states)]
            df_error = (
                df_error.groupby(
                    [
                        df_error.experiment.isin(training_experiments),
                        df_error.state.isin(fitted_states),
                        "run",
                    ]
                )
                .mean()
                .rmse.reset_index()
                .set_axis(["Experiment", "Metabolites", "Run", "RMSE"], axis=1)
            )
            df_error.loc[:, "Experiment"] = df_error.loc[:, "Experiment"].replace(
                {True: "Training", False: "Test"}
            )
            df_error.loc[:, "Metabolites"] = df_error.loc[:, "Metabolites"].replace(
                {True: "Observed", False: "Hidden"}
            )
            df_error = (
                df_error.groupby(["Experiment", "Metabolites", "Run"])
                .RMSE.mean()
                .reset_index()
            )
            stored_errors[run] = df_error  # .groupby(
            #     ["Experiment", "Metabolites"]
            # ).RMSE.aggregate(["mean", "std"])
            sns.barplot(
                data=df_error,
                y="RMSE",
                hue="Experiment",
                x="Metabolites",
                ci="sd",
                hue_order=["Training", "Test"],
                order=["Observed", "Hidden"],
                capsize=0.035,
                palette={"Training": colors["orange"], "Test": colors["cyan"]},
                ax=ax,
            )

            if i == len(model_map) - 1:
                lgd2 = ax.legend(
                    frameon=False,
                    title="Experiment",
                    fontsize="small",
                    loc="lower left",
                    bbox_to_anchor=(1.0, 0.4),
                )
            else:
                ax.legend().remove()

            ax.set_yscale(scaling["opt_scale"])
            if i == 0:
                ax.set_ylabel("RMSE (mM)")
            else:
                ax.set_yticklabels([])
                ax.set_ylabel(None)

            ax.tick_params(axis="x", which="major", labelsize="small")
            ax.set_ylim(*scaling["opt_RMSE"])
            ax.set_xlabel(None)

        # We need this for another figure.
        pd.concat(stored_errors).to_excel(
            output_location / "rmse.xlsx", header=True, index=True
        )

        # Add a big hidden background axes for the common axes labels
        ax_bg_top = fig.add_subplot(gs[:6, :], frameon=False)
        ax_bg_top.set_xlabel("Time (s)", labelpad=22)
        ax_bg_top.set_xticks([])
        ax_bg_top.set_ylabel("Concentration (mM)", labelpad=20)
        ax_bg_top.set_yticks([])
        sns.despine(ax=ax_bg_top, left=True, bottom=True)

        ax_bg_bot = fig.add_subplot(gs[-1, :], frameon=False)
        ax_bg_bot.set_xticks([])
        ax_bg_bot.set_xlabel("Metabolites", labelpad=-10)
        ax_bg_bot.set_yticks([])
        sns.despine(ax=ax_bg_bot, left=True, bottom=True)

        sns.despine(fig)

        # Add A & B markings
        fig.text(0.07, 0.905, "A", fontsize="x-large", fontweight="heavy")
        fig.text(0.07, 0.355, "B", fontsize="x-large", fontweight="heavy")

        # Scale to 190 mm (25.4 mm / inch)
        w, h = fig.get_size_inches()
        scale = (190 / 25.4) / w
        fig.set_size_inches(w * scale, h * scale)

        fig.savefig(
            output_location / "optimization_fit.png",
            dpi=300,
            bbox_inches="tight",
            bbox_extra_artists=[lgd1, lgd2],
        )

    # Plot optimization traces
    if False:
        fig, ((axn, axn_best), (axt, axt_best)) = plt.subplots(2, 2, sharey=True)
        run_formatted = {
            "Original": "full",
            "minNW": "minnw",
            "FastCore": "fastcore",
            "NetworkReducer": "networkreducer",
        }
        colors = plot_util.colors("models")
        second_per_minute = 60
        for name, run in run_formatted.items():
            color = colors[name]
            df_trace = optimized_traces[run]["traces"].copy()
            n_traces = np.unique(df_trace.index.get_level_values("run")).size
            alpha = 0.1
            n = df_trace.index.get_level_values("iteration").values
            fval = df_trace.fval.unstack().values
            t = df_trace.time.unstack().values / second_per_minute
            # Iteration trace.
            plot_util.trace_plot(
                n,
                fval,
                ax=axn,
                plot_args={"c": color, "alpha": alpha},
                scatter_args={"c": color, "alpha": alpha, "marker": "x"},
            )
            axn.set_xlabel("Iterations (n)")
            axn.set_ylabel("Optimisation Score")
            axn.set_title("All Runs (n=100)")
            axn.set_xlim(*scaling["trace_n"])
            axn.set_xscale("log")

            # Time trace.
            plot_util.trace_plot(
                t,
                fval,
                ax=axt,
                plot_args={"c": color, "alpha": alpha},
                scatter_args={"c": color, "alpha": alpha, "marker": "x"},
            )
            axt.set_xlabel("Time (min)")
            axt.set_ylabel("Optimisation Score")
            axt.set_xlim(*scaling["trace_t"])
            axt.set_xscale("log")

            # Best plots
            df_trace = optimized_traces[run]["traces"].copy()
            df_trace.time = df_trace.time / second_per_minute

            df_trace.loc[:, ["time", "fval"]].dropna(how="all").reset_index().groupby(
                "iteration"
            ).fval.min().cummin().plot(ax=axn_best, color=color)
            axn_best.set_xlabel("Iterations (n)")
            axn_best.set_xlim(*scaling["trace_n"])
            axn_best.set_xscale("log")
            axn_best.set_title("Best Run")

            df_trace.loc[:, ["time", "fval"]].dropna(how="all").reset_index().groupby(
                "time"
            ).fval.min().cummin().plot(ax=axt_best, color=color)
            axt_best.set_xlabel("Time (min)")
            axt_best.set_xlim(*scaling["trace_t"])
            axt_best.set_xscale("log")

        axn.set_ylim(*scaling["trace_score"])
        sns.despine(fig)

        legend = axn_best.legend(run_formatted.keys(), frameon=False)
        for line, name in zip(legend.legendHandles, run_formatted.keys()):
            line.set_color(colors[name])
            line.set_alpha(1)

        fig.subplots_adjust(hspace=0.3)

        fig.savefig(
            output_location / "optimization_trace.png",
            bbox_extra_artists=(legend,),
            bbox_inches="tight",
            dpi=300,
        )

    # Plot parameter and sensitivity distributions + JI example.
    if False:
        include_example = scaling["dist_ji_example"]
        # Common settings for this plot.
        trim_args = "percentile", (0.05, 0.95)
        epsilon = 1e-12
        ptype_map = {
            Parameters.mu: "Standard chemical\npotential ($\\mu'^{\\circ}$)",
            Parameters.km: "Michaelis constant ($K^{M}$)",
            Parameters.u_v: "Rate constant ($u^{V}$)",
            Parameters.km_tot_rate: "Rate constant ($u^{V}$)",
        }
        model_map = {
            "full": "Original",
            "minnw": "minNW",
            "fastcore": "FastCore",
            "networkreducer": "NetworkReducer",
        }

        # Summary figure of parameter optimization results
        truth = optimized_traces["full"]["ranges"].copy()
        truth = truth.assign(
            log=truth.index.str.split("__").str[0].isin(Parameters.log)
        )
        truth.loc[truth.log, ["min", "max", "truth"]] = np.log10(
            truth.loc[truth.log, ["min", "max", "truth"]] + epsilon
        )
        truth = truth.assign(range=truth["max"] - truth["min"])
        truth = truth[["log", "min", "max", "range"]]

        estimates = pd.DataFrame(
            index=truth.index,
            columns=pd.MultiIndex.from_product(
                (runs, ["min", "max", "range", "decrease", "ji"])
            ),
        )
        # Save some for examples for ji-plot explanation.
        example_keys = [
            # "km__A__r1",
            # "u_v__r1",
            "mu__A",
            # "km__D__r5",
            # "u_v__r5",
            # "mu__D",
            # "u_v__r11",
        ]
        example_data = collections.defaultdict(dict)

        for run in runs:
            ends = optimized_traces[run]["end"]
            values = ends.loc[:, ends.columns.isin(truth.index)].reset_index(
                level=1, drop=True
            )
            example_data[run] = values.reindex(example_keys, axis=1)
            values.loc[:, truth.log[values.columns]] = np.log10(
                values.loc[:, truth.log[values.columns]] + epsilon
            )
            values = plot_util.trim(values, *trim_args)

            estimates[(run, "min")] = values.min()
            estimates[(run, "max")] = values.max()
            estimates[(run, "range")] = (
                estimates[(run, "max")] - estimates[(run, "min")]
            )
            estimates[(run, "decrease")] = (
                1 - (estimates[(run, "range")] / truth["range"])
            ) * 100

            if run == "full":
                reference_values = values
                estimates[(run, "ji")] = 1
            else:
                estimates[(run, "ji")] = plot_util.jaccard_index(
                    values, reference_values
                )

        # with pd.ExcelWriter("temp.xlsx") as excel:
        #     estimates.to_excel(excel, startrow=0, startcol=truth.shape[1])
        #     truth.to_excel(excel, startrow=2, startcol=0)

        example_data = pd.concat(example_data, axis=1)
        example_data.columns.names = "model", "parameter"

        par_distr = (
            estimates.loc[:, pd.IndexSlice[:, ("ji", "decrease")]]
            .unstack()
            .reset_index()
            .set_axis(["model", "property", "parameter", "value"], axis=1)
        )
        par_distr = par_distr.assign(
            type=plot_util.get_parameter_types(par_distr.parameter)
        )

        # Summary figure of sensitivities.
        cache_path = optimization_case_directory / "cached_sensitivity.h5"

        if cache_path.exists():
            sensitivity = pd.read_hdf(cache_path, key="sensitivity")
        else:
            sensitivity = load_sensitivity(optimization_case_directory)

        # Take the mean sensitivity over all experiments.
        mean_sensitivity = sensitivity.groupby(["model", "run"]).mean()
        # Compare distribution of the sensitivities.
        sref = mean_sensitivity.loc["full"]
        # Trim NA only columns
        sref = sref.loc[:, ~sref.isna().all(axis=0)]
        sref = plot_util.trim(sref, *trim_args)

        data = []
        for model, df in mean_sensitivity.groupby("model"):
            if model == "full":
                continue
            # Trim NA only columns
            df = df.loc[:, ~df.isna().all(axis=0)]

            # Only keep shared columns
            columns = plot_util.get_shared_axis([df, sref])
            df = df[columns]

            # Trim percentiles
            df = plot_util.trim(df, *trim_args)

            # Calculate jaccard-index
            ji = (
                plot_util.jaccard_index(df, sref[columns])
                .reset_index()
                .set_axis(["parameter", "state", "value"], axis=1)
                .assign(model=model)
            )
            data.append(ji)

        sens_distr = pd.concat(data, ignore_index=True)
        sens_distr = sens_distr.assign(
            type=plot_util.get_parameter_types(sens_distr.parameter)
        )

        # Rename parameter types.
        par_distr.type = par_distr.type.replace(ptype_map)
        sens_distr.type = sens_distr.type.replace(ptype_map)

        # Rename models.
        par_distr.model = par_distr.model.replace(model_map)
        sens_distr.model = sens_distr.model.replace(model_map)

        fig = plt.figure()
        if include_example:
            gs = fig.add_gridspec(ncols=3, nrows=5)
            ax_x = fig.add_subplot(gs[1:4, 0])
            ax_p = fig.add_subplot(gs[:, 1])
            ax_s = fig.add_subplot(gs[:, 2])
        else:
            fig, (ax_p, ax_s) = plt.subplots(1, 2)

        if include_example:
            kde_kwargs = {"fit": {"bw_method": "scott"}, "points": 1000, "plot": {}}
            example_parameter = "mu__A"
            example_models = ["full", "minnw"]
            data = example_data.loc[
                :, pd.IndexSlice[example_models, [example_parameter]]
            ]
            data.columns = data.columns.droplevel(-1)

            range_min = data.min().min()
            range_max = data.max().max()

            plot_min = range_min - 0.5 * (range_max - range_min)
            plot_max = range_max + 0.5 * (range_max - range_min)

            colors = plot_util.colors("models")
            hatch = {"full": "//", "minnw": "\\\\"}
            saved_points = {}
            for run, points in reversed(list(data.T.iterrows())):
                trimmed_points = plot_util.trim(points, *trim_args)
                saved_points[run] = trimmed_points
                trimmed_min = trimmed_points.min()
                trimmed_max = trimmed_points.max()
                print(run, trimmed_min, trimmed_max)

                # Add a little bit to the range of one to avoid plotting the lines with overlap.
                if run == "full":
                    trimmed_max *= 1.001

                # Since we want access to the kde fit, we do it ourselves.
                kde = scipy.stats.gaussian_kde(points.dropna(), **kde_kwargs["fit"])

                # From trimmed_min to trimmed_max we plot normally.
                x = np.linspace(trimmed_min, trimmed_max, kde_kwargs["points"])
                y = kde.evaluate(x)
                ax_x.plot(x, y, color=colors[model_map[run]], label=model_map[run])
                ax_x.fill_between(x, y, 0, color=colors[model_map[run]], alpha=0.5)

                # Outside the trimmed range we plot highlighted (as it is deleted)
                x = np.linspace(plot_min, trimmed_min, kde_kwargs["points"])
                y = kde.evaluate(x)
                ax_x.plot(x, y, color=colors[model_map[run]])
                ax_x.fill_between(
                    x, y, 0, color=colors[model_map[run]], alpha=0.2, hatch=hatch[run]
                )
                x = np.linspace(trimmed_max, plot_max, kde_kwargs["points"])
                y = kde.evaluate(x)
                ax_x.plot(x, y, color=colors[model_map[run]])
                ax_x.fill_between(
                    x, y, 0, color=colors[model_map[run]], alpha=0.2, hatch=hatch[run]
                )

                # Add vertical line at trimmed_min and trimmed_max
                x = [trimmed_min, trimmed_max]
                y = kde.evaluate(x)
                ax_x.vlines(x, [0, 0], y, color=colors[model_map[run]])

            # Plot overlap below as lines only.
            # Plot horizontal lines (after the ylimits are set)
            y_space_end = -0.3 * ax_x.get_ylim()[1]
            y_space_start = -0.2 * ax_x.get_ylim()[1]
            y_points = np.linspace(y_space_start, y_space_end, 2)
            for y, run in zip(y_points, data.columns):
                points = saved_points[run]
                x1 = saved_points[run].min()
                x2 = saved_points[run].max()
                ax_x.plot(
                    [points.min(), points.max()], [y, y], color=colors[model_map[run]]
                )
                ax_x.plot(
                    points,
                    np.ones_like(points) * y,
                    marker="|",
                    markersize=4,
                    color=colors[model_map[run]],
                )
            # Add labels to horizontal lines
            ji = plot_util.jaccard_index(*plot_util.trim(data, *trim_args).values.T).at[
                0
            ]

            for run, y in zip(data.columns, y_points):
                x = saved_points[run].max()
                ax_x.annotate(
                    model_map[run],
                    (x + 2, y),
                    None,
                    "data",
                    horizontalalignment="left",
                    verticalalignment="center",
                    fontsize="medium",
                )

            ax_x.set_xlim(85, 120)
            # Move the x-axis to the zero point.
            ax_x.spines["bottom"].set_position("zero")
            # Make some space for the horizonal guidelines
            ax_x.set_ylim()
            # Set the bounds of the y axis to start at zero and fix the ticks.
            yticks = ax_x.get_yticks()
            ax_x.set_yticks(yticks[yticks > 0])
            ax_x.spines["left"].set_bounds(0, ax_x.get_ylim()[1])
            # Set title
            ax_x.set_title(
                "$\\mu'^{{\\circ}}_A$\nJaccard-Index$=${:.2f}".format(ji),
                fontsize="medium",
            )

        violin_kwargs = {
            "order": list(ptype_map.values())[:-1],
            "hue_order": list(model_map.values())[1:],
            "palette": plot_util.colors("models"),
            "hue": "model",
            "x": "value",
            "y": "type",
            "orient": "h",
            "cut": 0,  # Do not cross boundaries with kde.
            "scale": "count",  # Scale with number of points.
            "inner": None,  # Less messy then points.
        }
        sns.violinplot(
            data=par_distr[
                (par_distr.model != "Original") & (par_distr.property == "ji")
            ],
            ax=ax_p,
            **violin_kwargs,
        )
        ax_p.get_legend().remove()
        ax_p.set_xlim(0, 1.0)
        ax_p.set_title("Parameters")
        ax_p.set_xlabel("Jaccard Index")
        ax_p.set_ylabel(None)
        if include_example:
            ax_p.set_yticklabels([])
        sns.violinplot(data=sens_distr, ax=ax_s, **violin_kwargs)
        if include_example:
            ax_s.legend(bbox_to_anchor=(-1.5, 0.0), loc="lower right", frameon=False)
        else:
            ax_s.legend(
                bbox_to_anchor=(1.02, 1.02),
                loc="upper left",
                frameon=False,
                fontsize="small",
            )
        ax_s.set_xlim(0, 1.0)
        ax_s.set_title("Sensitivities")
        ax_s.set_xlabel("Jaccard Index")
        ax_s.set_ylabel(None)
        if include_example:
            ax_s.yaxis.tick_right()
        else:
            ax_s.set_yticklabels([])

        sns.despine(fig)

        w, h = fig.get_size_inches()
        scale = (190 / 25.4) / w
        fig.set_size_inches(w * scale, h * scale)
        fig.tight_layout()

        # A / B / C subfigure markings
        if include_example:
            fig.text(0.070, 0.905, "A", fontsize="x-large", fontweight="heavy")
            fig.text(0.375, 0.905, "B", fontsize="x-large", fontweight="heavy")
            fig.text(0.650, 0.905, "C", fontsize="x-large", fontweight="heavy")

        fig.savefig(
            output_location / "optimization_parameters.png",
            dpi=300,
            bbox_inches="tight",
        )

    # Plot results from time scale seperation.
    if False:
        tss_path = pathlib.Path("../time_scale_seperation/scores.csv")
        tss = pd.read_csv(tss_path, index_col=0)

        model_size = tss.loc["size"].astype(int)
        tss = tss.loc[tss.index != "size"]
        tss.index = tss.index.str.split("_", expand=True)
        tss = tss.rename(
            index={
                "train": "Training",
                "test": "Test",
                "observed": "Observed",
                "hidden": "Hidden",
            }
        )
        tss.columns = [
            "Case {}\n{} Components".format(*i)
            for i in list(enumerate(model_size, start=1))
        ]
        tss = tss.T.unstack().reset_index()
        tss.columns = ["Metabolites", "Experiment", "Case", "RMSE (mM)"]

        plot_colors = plot_util.colors("base")

        fig = (
            sns.catplot(
                data=tss,
                y="RMSE (mM)",
                hue="Experiment",
                x="Metabolites",
                ci=None,
                kind="bar",
                col="Case",
                palette={
                    "Training": plot_colors["orange"],
                    "Test": plot_colors["cyan"],
                },
                hue_order=["Training", "Test"],
                order=["Observed", "Hidden"],
                legend_out=True,
                sharey=True,
                height=2.5,
                aspect=(190 / 25.4) / 9,
            )
            .set_titles(template="{col_name}", fontsize="large")
            .set_xticklabels(fontsize="small")
            .set_xlabels("")
        )

        fig.savefig(output_location / "tss_fit.png", bbox_inches="tight", dpi=300)

    # Plot comparison with different models.
    if False:
        model_names = ["edemp", "co_factors", "curcumin", "large_cycle"]
        plot_names = ["EDEMP", "Co-factors", "Branched", "Cycles"]
        version_names = {"full": "Original", "minnw": "minNW", "fastcore": "FastCore"}

        settings = {
            "edemp": ("linear", (0, 2.3)),
            "co_factors": ("linear", (0, 1.6)),
            "curcumin": ("log", (0.001, 2.0)),
            "large_cycle": ("linear", (0, 1.3)),
        }

        all_data = {}
        for model in model_names:
            df = pd.read_excel(
                pathlib.Path("./results/optimization_results") / model / "rmse.xlsx",
                engine="openpyxl",
            )
            df = df.drop(columns=df.columns[1]).rename(
                columns={"Unnamed: 0": "Version"}
            )
            df.Version = df.Version.fillna(method="ffill")
            all_data[model] = df
        df = (
            pd.concat(all_data)
            .reset_index(0)
            .reset_index(0, drop=True)
            .rename(columns={"level_0": "Model"})
        )

        colors = plot_util.colors("base")

        fig, all_axes = plt.subplots(
            nrows=len(model_names),
            ncols=3,
            gridspec_kw={"width_ratios": [1.5, 1, 1]},
            constrained_layout=True,
        )

        for i, (axes, model, name) in enumerate(zip(all_axes, model_names, plot_names)):
            # First axis is reserved for the model layout, so we remove everything.
            axes[0].set_title(name)
            axes[0].axis("off")

            for j, (ax, version) in enumerate(zip(axes[1:], ("full", "fastcore"))):
                sns.barplot(
                    data=df[(df.Model == model) & (df.Version == version)],
                    y="RMSE",
                    hue="Experiment",
                    x="Metabolites",
                    ci="sd",
                    hue_order=["Training", "Test"],
                    order=["Observed", "Hidden"],
                    capsize=0.035,
                    palette={"Training": colors["orange"], "Test": colors["cyan"]},
                    ax=ax,
                )
                ax.set_yscale(settings[model][0])
                ax.set_ylim(settings[model][1])

                if j == 1:
                    ax.set_yticklabels([])
                    ax.set_ylabel(None)

                if i == 0:
                    ax.set_title(version_names[version])

                if i == 0 and j == 1:
                    lgd = ax.legend(
                        frameon=False,
                        title="Experiment",
                        fontsize="small",
                        loc="upper right",
                        bbox_to_anchor=(1.1, 1.1),
                    )
                else:
                    ax.legend().remove()

                if i != len(model_names) - 1:
                    ax.set_xticklabels([])
                    ax.set_xlabel(None)

        sns.despine(fig)
        w, h = fig.get_size_inches()
        scale = (190 / 25.4) / w
        fig.set_size_inches(w * scale, h * scale)
        fig.savefig(
            output_location / "additional_models.png", bbox_inches="tight", dpi=300
        )
