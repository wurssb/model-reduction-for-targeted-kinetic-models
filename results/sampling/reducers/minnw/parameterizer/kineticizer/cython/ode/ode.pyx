"""Automatically generated Cython module build template.
Do not modify: changes might be overwritten.

Author: symbolicSBML.CythonODE (Rik van Rosmalen / rik.vanrosmalen@wur.nl)
"""
# cython: language_level=3, embedsignature=True
cimport cython

import numpy as np
cimport numpy as cnp

cdef extern from "dydt.h":
    void flux(double *p, double *y, double *flux)
    void dydt(double *p, double *y, double *dydt)
#    void jacobian(double *p, double *y, double *jacobian)
#    void sensitivity(double *p, double *y, double *dydp)
#    void flux_sensitivity(double *p, double *y, double *dfdp)

cdef class CythonODE(object):
    """A optimized Cython ODE system.

    Significantly more performant then SympyLambdifyODE, but requires compilation.
    Note that the interface is slightly different as parameters should be set manually.
    """
    # We re-use the same views for the c bindings to avoid overhead.
    # Note that all numpy arrays received as input are copied and converted to C-layout.
    # All outputs are also copied. This is done to avoid bugs and subtle interations
    # with numpy's system of views and actual copies.
    cdef double[:] p_view
    cdef double[:] flux_view
    cdef double[:] dy_view
#    cdef double[:,:] J_view
#    cdef double[:,:] dydp_view
#    cdef double[:,:] dfdp_view

    ODE_DTYPE = np.double

    n_parameters = 254
    n_metabolites = 52
    n_reactions = 49

    parameters = np.array(["R","T","km__M_13dpg_c__R_GAPD","km__M_13dpg_c__R_PGK","km__M_2pg_c__R_ENO","km__M_2pg_c__R_PGM","km__M_3pg_c__R_PGK","km__M_3pg_c__R_PGM","km__M_6pgc_c__R_GND","km__M_6pgc_c__R_PGL","km__M_6pgl_c__R_G6PDH2r","km__M_6pgl_c__R_PGL","km__M_accoa_c__R_CS","km__M_accoa_c__R_MALS","km__M_accoa_c__R_PDH","km__M_acon_C_c__R_ACONTa","km__M_acon_C_c__R_ACONTb","km__M_adp_c__R_ATPS4r","km__M_adp_c__R_GLNS","km__M_adp_c__R_PFK","km__M_adp_c__R_PGK","km__M_adp_c__R_PYK","km__M_adp_c__R_SUCOAS","km__M_akg_c__R_AKGDH","km__M_akg_c__R_GLUDy","km__M_akg_c__R_ICDHyr","km__M_atp_c__R_ATPS4r","km__M_atp_c__R_GLNS","km__M_atp_c__R_PFK","km__M_atp_c__R_PGK","km__M_atp_c__R_PYK","km__M_atp_c__R_SUCOAS","km__M_cit_c__R_ACONTa","km__M_cit_c__R_CS","km__M_co2_c__R_AKGDH","km__M_co2_c__R_GND","km__M_co2_c__R_ICDHyr","km__M_co2_c__R_PDH","km__M_coa_c__R_AKGDH","km__M_coa_c__R_CS","km__M_coa_c__R_MALS","km__M_coa_c__R_PDH","km__M_coa_c__R_SUCOAS","km__M_dhap_c__R_FBA","km__M_dhap_c__R_TPI","km__M_e4p_c__R_TALA","km__M_e4p_c__R_TKT2","km__M_f6p_c__R_PFK","km__M_f6p_c__R_PGI","km__M_f6p_c__R_TALA","km__M_f6p_c__R_TKT2","km__M_fdp_c__R_FBA","km__M_fdp_c__R_PFK","km__M_fum_c__R_FUM","km__M_fum_c__R_SUCDi","km__M_g3p_c__R_FBA","km__M_g3p_c__R_GAPD","km__M_g3p_c__R_TALA","km__M_g3p_c__R_TKT1","km__M_g3p_c__R_TKT2","km__M_g3p_c__R_TPI","km__M_g6p_c__R_G6PDH2r","km__M_g6p_c__R_GLCpts","km__M_g6p_c__R_PGI","km__M_glc_D_e__R_GLCpts","km__M_gln_L_c__R_GLNS","km__M_glu_L_c__R_GLNS","km__M_glu_L_c__R_GLUDy","km__M_glx_c__R_ICL","km__M_glx_c__R_MALS","km__M_h2o_c__R_ACONTa","km__M_h2o_c__R_ACONTb","km__M_h2o_c__R_ATPS4r","km__M_h2o_c__R_CS","km__M_h2o_c__R_CYTBD","km__M_h2o_c__R_ENO","km__M_h2o_c__R_FUM","km__M_h2o_c__R_GLUDy","km__M_h2o_c__R_MALS","km__M_h2o_c__R_PGL","km__M_h_c__R_ATPS4r","km__M_h_c__R_CS","km__M_h_c__R_CYTBD","km__M_h_c__R_G6PDH2r","km__M_h_c__R_GAPD","km__M_h_c__R_GLNS","km__M_h_c__R_GLUDy","km__M_h_c__R_MALS","km__M_h_c__R_MDH","km__M_h_c__R_NADH16","km__M_h_c__R_PFK","km__M_h_c__R_PGL","km__M_h_c__R_PIt2r","km__M_h_c__R_PYK","km__M_h_e__R_ATPS4r","km__M_h_e__R_CYTBD","km__M_h_e__R_NADH16","km__M_h_e__R_PIt2r","km__M_icit_c__R_ACONTb","km__M_icit_c__R_ICDHyr","km__M_icit_c__R_ICL","km__M_mal_L_c__R_FUM","km__M_mal_L_c__R_MALS","km__M_mal_L_c__R_MDH","km__M_nad_c__R_AKGDH","km__M_nad_c__R_GAPD","km__M_nad_c__R_MDH","km__M_nad_c__R_NADH16","km__M_nad_c__R_PDH","km__M_nadh_c__R_AKGDH","km__M_nadh_c__R_GAPD","km__M_nadh_c__R_MDH","km__M_nadh_c__R_NADH16","km__M_nadh_c__R_PDH","km__M_nadp_c__R_G6PDH2r","km__M_nadp_c__R_GLUDy","km__M_nadp_c__R_GND","km__M_nadp_c__R_ICDHyr","km__M_nadph_c__R_G6PDH2r","km__M_nadph_c__R_GLUDy","km__M_nadph_c__R_GND","km__M_nadph_c__R_ICDHyr","km__M_nh4_c__R_GLNS","km__M_nh4_c__R_GLUDy","km__M_nh4_c__R_NH4t","km__M_nh4_e__R_NH4t","km__M_o2_c__R_CYTBD","km__M_oaa_c__R_CS","km__M_oaa_c__R_MDH","km__M_pep_c__R_ENO","km__M_pep_c__R_GLCpts","km__M_pep_c__R_PYK","km__M_pi_c__R_ATPS4r","km__M_pi_c__R_GAPD","km__M_pi_c__R_GLNS","km__M_pi_c__R_PIt2r","km__M_pi_c__R_SUCOAS","km__M_pi_e__R_PIt2r","km__M_pyr_c__R_GLCpts","km__M_pyr_c__R_PDH","km__M_pyr_c__R_PYK","km__M_q8_c__R_CYTBD","km__M_q8_c__R_NADH16","km__M_q8_c__R_SUCDi","km__M_q8h2_c__R_CYTBD","km__M_q8h2_c__R_NADH16","km__M_q8h2_c__R_SUCDi","km__M_r5p_c__R_RPI","km__M_r5p_c__R_TKT1","km__M_ru5p_D_c__R_GND","km__M_ru5p_D_c__R_RPE","km__M_ru5p_D_c__R_RPI","km__M_s7p_c__R_TALA","km__M_s7p_c__R_TKT1","km__M_succ_c__R_ICL","km__M_succ_c__R_SUCDi","km__M_succ_c__R_SUCOAS","km__M_succoa_c__R_AKGDH","km__M_succoa_c__R_SUCOAS","km__M_xu5p_D_c__R_RPE","km__M_xu5p_D_c__R_TKT1","km__M_xu5p_D_c__R_TKT2","km_tot_rate__R_CO2t","km_tot_rate__R_H2Ot","km_tot_rate__R_O2t","mu__M_13dpg_c","mu__M_2pg_c","mu__M_3pg_c","mu__M_6pgc_c","mu__M_6pgl_c","mu__M_accoa_c","mu__M_acon_C_c","mu__M_adp_c","mu__M_akg_c","mu__M_atp_c","mu__M_cit_c","mu__M_co2_c","mu__M_co2_e","mu__M_coa_c","mu__M_dhap_c","mu__M_e4p_c","mu__M_f6p_c","mu__M_fdp_c","mu__M_fum_c","mu__M_g3p_c","mu__M_g6p_c","mu__M_glc_D_e","mu__M_gln_L_c","mu__M_glu_L_c","mu__M_glx_c","mu__M_h2o_c","mu__M_h2o_e","mu__M_h_c","mu__M_h_e","mu__M_icit_c","mu__M_mal_L_c","mu__M_nad_c","mu__M_nadh_c","mu__M_nadp_c","mu__M_nadph_c","mu__M_nh4_c","mu__M_nh4_e","mu__M_o2_c","mu__M_o2_e","mu__M_oaa_c","mu__M_pep_c","mu__M_pi_c","mu__M_pi_e","mu__M_pyr_c","mu__M_q8_c","mu__M_q8h2_c","mu__M_r5p_c","mu__M_ru5p_D_c","mu__M_s7p_c","mu__M_succ_c","mu__M_succoa_c","mu__M_xu5p_D_c","u_v__R_ACONTa","u_v__R_ACONTb","u_v__R_AKGDH","u_v__R_ATPS4r","u_v__R_CS","u_v__R_CYTBD","u_v__R_ENO","u_v__R_FBA","u_v__R_FUM","u_v__R_G6PDH2r","u_v__R_GAPD","u_v__R_GLCpts","u_v__R_GLNS","u_v__R_GLUDy","u_v__R_GND","u_v__R_ICDHyr","u_v__R_ICL","u_v__R_MALS","u_v__R_MDH","u_v__R_NADH16","u_v__R_NH4t","u_v__R_PDH","u_v__R_PFK","u_v__R_PGI","u_v__R_PGK","u_v__R_PGL","u_v__R_PGM","u_v__R_PIt2r","u_v__R_PYK","u_v__R_RPE","u_v__R_RPI","u_v__R_SUCDi","u_v__R_SUCOAS","u_v__R_TALA","u_v__R_TKT1","u_v__R_TKT2","u_v__R_TPI"])
    metabolites = np.array(["M_13dpg_c","M_2pg_c","M_3pg_c","M_6pgc_c","M_6pgl_c","M_accoa_c","M_acon_C_c","M_adp_c","M_akg_c","M_atp_c","M_cit_c","M_co2_c","M_co2_e","M_coa_c","M_dhap_c","M_e4p_c","M_f6p_c","M_fdp_c","M_fum_c","M_g3p_c","M_g6p_c","M_glc_D_e","M_gln_L_c","M_glu_L_c","M_glx_c","M_h2o_c","M_h2o_e","M_h_c","M_h_e","M_icit_c","M_mal_L_c","M_nad_c","M_nadh_c","M_nadp_c","M_nadph_c","M_nh4_c","M_nh4_e","M_o2_c","M_o2_e","M_oaa_c","M_pep_c","M_pi_c","M_pi_e","M_pyr_c","M_q8_c","M_q8h2_c","M_r5p_c","M_ru5p_D_c","M_s7p_c","M_succ_c","M_succoa_c","M_xu5p_D_c"])
    reactions = np.array(["R_ACONTa","R_ACONTb","R_AKGDH","R_ATPS4r","R_CO2t","R_CS","R_CYTBD","R_ENO","R_FBA","R_FUM","R_G6PDH2r","R_GAPD","R_GLCpts","R_GLNS","R_GLUDy","R_GND","R_H2Ot","R_ICDHyr","R_ICL","R_MALS","R_MDH","R_NADH16","R_NH4t","R_O2t","R_PDH","R_PFK","R_PGI","R_PGK","R_PGL","R_PGM","R_PIt2r","R_PYK","R_RPE","R_RPI","R_SUCDi","R_SUCOAS","R_TALA","R_TKT1","R_TKT2","R_TPI","R_ATPM","R_EX_glc_e","R_EX_o2_e","R_EX_pi_e","R_EX_h2o_e","R_Biomass_Ecoli_core_w_GAM","R_EX_nh4_e","R_EX_h_e","R_EX_co2_e"])

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def __init__(self):
        """Generated ODE system."""
        self.p_view = np.zeros((254,), dtype=np.double, order='C')
        self.dy_view = np.zeros((52,), dtype=np.double, order='C')
        self.flux_view = np.zeros((49,), dtype=np.double, order='C')
#        self.J_view = np.zeros((52, 52), dtype=np.double, order='C')
#        self.dydp_view = np.zeros((52, 254), dtype=np.double, order='C')
#        self.dfdp_view = np.zeros((49, 254), dtype=np.double, order='C')

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def get_current_parameters(self):
        """View the current parameters of the ODE system."""
        return np.asarray(self.p_view).copy()

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def update_parameters(self, cnp.ndarray[cnp.double_t, ndim=1] p):
        """Update the parameters of the ODE system.

        Input:
        p  - parameters: numpy array [double 254x1]
        """
        cdef int px, py
        px = p.shape[0]
        py = p.ndim
        if px != 254 or py != 1:
            raise ValueError("Wrong dimension for p, should be [254x1]")
        self.p_view = np.require(p.copy(order='C'), dtype=np.double, requirements='C')

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def dydt(self, double t, double[:] y):
        """Calculate state change (integration step) for the state y.

        Input:
        t  - time: [double] -> Ignored. (For scipy.integrate.ode compatibility)
        y  - state: numpy array [double 52x1]
        Output:
        dy - state change: numpy array [double 52x1]
        """
        cdef int yx
        yx = y.shape[0]
        if yx != 52:
            raise ValueError("Wrong dimension for y, should be [52x1]")

        y = np.require(y, dtype=np.double, requirements='C')
        dydt(&self.p_view[0], &y[0], &self.dy_view[0])
        return np.asarray(self.dy_view).copy()

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def flux(self, double[:] y):
        """Calculate reaction flux for the state y.

        Input:
        y  - state: numpy array [double 52x1]
        Output:
        flux - state change: numpy array [double 49x1]
        """
        cdef int yx
        yx = y.shape[0]
        if yx != 52:
            raise ValueError("Wrong dimension for y, should be [52x1]")

        y = np.require(y, dtype=np.double, requirements='C')
        flux(&self.p_view[0], &y[0], &self.flux_view[0])
        return np.asarray(self.flux_view).copy()

#    @cython.boundscheck(False)
#    @cython.wraparound(False)
#    def jacobian(self, double t, double[:] y):
#        """Calculate the Jacobian for the state y (dy/dx).
#
#        Input:
#        t  - time: [double] -> Ignored. (For scipy.integrate.ode compatibility)
#        y  - state: numpy array [double 52x1]
#        Output:
#        J  - Jacobian: numpy array [double 52x52]
#        """
#        cdef int yx
#        yx = y.shape[0]
#        if yx != 52:
#            raise ValueError("Wrong dimension for y, should be [52x1]")
#
#        y = np.require(y, dtype=np.double, requirements='C')
#        jacobian(&self.p_view[0], &y[0], &self.J_view[0, 0])
#        return np.asarray(self.J_view).copy()


#    @cython.boundscheck(False)
#    @cython.wraparound(False)
#    def dydp(self, double[:] y):
#        """Calculate the parametric sensitivity for the state y (dy/dp).
#
#        Input:
#        y  - state: numpy array [double 52x1]
#        Output:
#        dydp  - parametric sensitivity: numpy array [double 52x254]
#        """
#        cdef int yx
#        yx = y.shape[0]
#        if yx != 52:
#            raise ValueError("Wrong dimension for y, should be [52x1]")
#
#        y = np.require(y, dtype=np.double, requirements='C')
#        sensitivity(&self.p_view[0], &y[0], &self.dydp_view[0, 0])
#        return np.asarray(self.dydp_view).copy()

#    @cython.boundscheck(False)
#    @cython.wraparound(False)
#    def dfdp(self, double[:] y):
#        """Calculate the parametric flux sensitivity for the fluxes (df/dp).
#
#        Input:
#        y  - state: numpy array [double 52x1]
#        Output:
#        dydp  - parametric flux sensitivity: numpy array [double 49x254]
#        """
#        cdef int yx
#        yx = y.shape[0]
#        if yx != 52:
#            raise ValueError("Wrong dimension for y, should be [52x1]")
#
#        y = np.require(y, dtype=np.double, requirements='C')
#        flux_sensitivity(&self.p_view[0], &y[0], &self.dfdp_view[0, 0])
#        return np.asarray(self.dfdp_view).copy()
