/******************************************************************************
 *                       Code generated with sympy 1.4                        *
 *                                                                            *
 *              See http://www.sympy.org/ for more information.               *
 *                                                                            *
 *                       This file is part of 'project'                       *
 ******************************************************************************/


#ifndef PROJECT__DYDT__H
#define PROJECT__DYDT__H

void dydt(double *p, double *y, double *dydt);
void flux(double *p, double *y, double *flux);

#endif

