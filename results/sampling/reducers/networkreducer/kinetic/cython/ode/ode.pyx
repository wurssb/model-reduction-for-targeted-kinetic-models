"""Automatically generated Cython module build template.
Do not modify: changes might be overwritten.

Author: symbolicSBML.CythonODE (Rik van Rosmalen / rik.vanrosmalen@wur.nl)
"""
# cython: language_level=3, embedsignature=True
cimport cython

import numpy as np
cimport numpy as cnp

cdef extern from "dydt.h":
    void flux(double *p, double *y, double *flux)
    void dydt(double *p, double *y, double *dydt)
    void jacobian(double *p, double *y, double *jacobian)
    void sensitivity(double *p, double *y, double *dydp)
    void flux_sensitivity(double *p, double *y, double *dfdp)

cdef class CythonODE(object):
    """A optimized Cython ODE system.

    Significantly more performant then SympyLambdifyODE, but requires compilation.
    Note that the interface is slightly different as parameters should be set manually.
    """
    # We re-use the same views for the c bindings to avoid overhead.
    # Note that all numpy arrays received as input are copied and converted to C-layout.
    # All outputs are also copied. This is done to avoid bugs and subtle interations
    # with numpy's system of views and actual copies.
    cdef double[:] p_view
    cdef double[:] flux_view
    cdef double[:] dy_view
    cdef double[:,:] J_view
    cdef double[:,:] dydp_view
    cdef double[:,:] dfdp_view

    ODE_DTYPE = np.double

    n_parameters = 126
    n_metabolites = 39
    n_reactions = 38

    parameters = np.array(["R","T","km__M_accoa_c__R_PDH","km__M_adp_c__R_ATPS4r","km__M_adp_c__R_PYK","km__M_atp_c__R_ATPS4r","km__M_atp_c__R_PYK","km__M_co2_c__R_PDH","km__M_coa_c__R_PDH","km__M_e4p_c__R_TKT2","km__M_f6p_c__R_PGI","km__M_f6p_c__R_TKT2","km__M_g3p_c__R_TKT2","km__M_g6p_c__R_GLCpts","km__M_g6p_c__R_PGI","km__M_glc_D_e__R_GLCpts","km__M_h2o_c__R_ATPS4r","km__M_h2o_c__R_CYTBD","km__M_h_c__R_ATPS4r","km__M_h_c__R_CYTBD","km__M_h_c__R_NADH16","km__M_h_c__R_PIt2r","km__M_h_c__R_PYK","km__M_h_c__R_PYRt2r","km__M_h_e__R_ATPS4r","km__M_h_e__R_CYTBD","km__M_h_e__R_NADH16","km__M_h_e__R_PIt2r","km__M_h_e__R_PYRt2r","km__M_nad_c__R_NADH16","km__M_nad_c__R_NADTRHD","km__M_nad_c__R_PDH","km__M_nadh_c__R_NADH16","km__M_nadh_c__R_NADTRHD","km__M_nadh_c__R_PDH","km__M_nadp_c__R_NADTRHD","km__M_nadph_c__R_NADTRHD","km__M_nh4_c__R_NH4t","km__M_nh4_e__R_NH4t","km__M_o2_c__R_CYTBD","km__M_pep_c__R_GLCpts","km__M_pep_c__R_PYK","km__M_pi_c__R_ATPS4r","km__M_pi_c__R_PIt2r","km__M_pi_e__R_PIt2r","km__M_pyr_c__R_GLCpts","km__M_pyr_c__R_PDH","km__M_pyr_c__R_PYK","km__M_pyr_c__R_PYRt2r","km__M_pyr_e__R_PYRt2r","km__M_q8_c__R_CYTBD","km__M_q8_c__R_NADH16","km__M_q8h2_c__R_CYTBD","km__M_q8h2_c__R_NADH16","km__M_r5p_c__R_RPI","km__M_ru5p_D_c__R_RPE","km__M_ru5p_D_c__R_RPI","km__M_xu5p_D_c__R_RPE","km__M_xu5p_D_c__R_TKT2","km_tot_rate__R_CO2t","km_tot_rate__R_H2Ot","km_tot_rate__R_O2t","km_tot_rate__R_lumped_0","km_tot_rate__R_lumped_1","km_tot_rate__R_lumped_10","km_tot_rate__R_lumped_2","km_tot_rate__R_lumped_3","km_tot_rate__R_lumped_4","km_tot_rate__R_lumped_5","km_tot_rate__R_lumped_6","km_tot_rate__R_lumped_7","km_tot_rate__R_lumped_8","km_tot_rate__R_lumped_9","mu__M_3pg_c","mu__M_accoa_c","mu__M_adp_c","mu__M_akg_c","mu__M_atp_c","mu__M_co2_c","mu__M_co2_e","mu__M_coa_c","mu__M_e4p_c","mu__M_etoh_c","mu__M_f6p_c","mu__M_g3p_c","mu__M_g6p_c","mu__M_glc_D_e","mu__M_gln_L_c","mu__M_glu_L_c","mu__M_h2o_c","mu__M_h2o_e","mu__M_h_c","mu__M_h_e","mu__M_nad_c","mu__M_nadh_c","mu__M_nadp_c","mu__M_nadph_c","mu__M_nh4_c","mu__M_nh4_e","mu__M_o2_c","mu__M_o2_e","mu__M_oaa_c","mu__M_pep_c","mu__M_pi_c","mu__M_pi_e","mu__M_pyr_c","mu__M_pyr_e","mu__M_q8_c","mu__M_q8h2_c","mu__M_r5p_c","mu__M_ru5p_D_c","mu__M_xu5p_D_c","u_v__R_ATPS4r","u_v__R_CYTBD","u_v__R_GLCpts","u_v__R_NADH16","u_v__R_NADTRHD","u_v__R_NH4t","u_v__R_PDH","u_v__R_PGI","u_v__R_PIt2r","u_v__R_PYK","u_v__R_PYRt2r","u_v__R_RPE","u_v__R_RPI","u_v__R_TKT2"])
    metabolites = np.array(["M_3pg_c","M_accoa_c","M_adp_c","M_akg_c","M_atp_c","M_co2_c","M_co2_e","M_coa_c","M_e4p_c","M_etoh_c","M_f6p_c","M_g3p_c","M_g6p_c","M_glc_D_e","M_gln_L_c","M_glu_L_c","M_h2o_c","M_h2o_e","M_h_c","M_h_e","M_nad_c","M_nadh_c","M_nadp_c","M_nadph_c","M_nh4_c","M_nh4_e","M_o2_c","M_o2_e","M_oaa_c","M_pep_c","M_pi_c","M_pi_e","M_pyr_c","M_pyr_e","M_q8_c","M_q8h2_c","M_r5p_c","M_ru5p_D_c","M_xu5p_D_c"])
    reactions = np.array(["R_ATPS4r","R_CO2t","R_CYTBD","R_GLCpts","R_H2Ot","R_NADH16","R_NADTRHD","R_NH4t","R_O2t","R_PDH","R_PGI","R_PIt2r","R_PYK","R_PYRt2r","R_RPE","R_RPI","R_TKT2","R_ATPM","R_EX_glc_e","R_EX_pyr_e","R_EX_o2_e","R_EX_pi_e","R_EX_h2o_e","R_Biomass_Ecoli_core_w_GAM","R_EX_nh4_e","R_EX_h_e","R_EX_co2_e","R_lumped_0","R_lumped_1","R_lumped_2","R_lumped_3","R_lumped_4","R_lumped_5","R_lumped_6","R_lumped_7","R_lumped_8","R_lumped_9","R_lumped_10"])

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def __init__(self):
        """Generated ODE system."""
        self.p_view = np.zeros((126,), dtype=np.double, order='C')
        self.dy_view = np.zeros((39,), dtype=np.double, order='C')
        self.flux_view = np.zeros((38,), dtype=np.double, order='C')
        self.J_view = np.zeros((39, 39), dtype=np.double, order='C')
        self.dydp_view = np.zeros((39, 126), dtype=np.double, order='C')
        self.dfdp_view = np.zeros((38, 126), dtype=np.double, order='C')

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def get_current_parameters(self):
        """View the current parameters of the ODE system."""
        return np.asarray(self.p_view).copy()

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def update_parameters(self, cnp.ndarray[cnp.double_t, ndim=1] p):
        """Update the parameters of the ODE system.

        Input:
        p  - parameters: numpy array [double 126x1]
        """
        cdef int px, py
        px = p.shape[0]
        py = p.ndim
        if px != 126 or py != 1:
            raise ValueError("Wrong dimension for p, should be [126x1]")
        self.p_view = np.require(p.copy(order='C'), dtype=np.double, requirements='C')

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def dydt(self, double t, double[:] y):
        """Calculate state change (integration step) for the state y.

        Input:
        t  - time: [double] -> Ignored. (For scipy.integrate.ode compatibility)
        y  - state: numpy array [double 39x1]
        Output:
        dy - state change: numpy array [double 39x1]
        """
        cdef int yx
        yx = y.shape[0]
        if yx != 39:
            raise ValueError("Wrong dimension for y, should be [39x1]")

        y = np.require(y, dtype=np.double, requirements='C')
        dydt(&self.p_view[0], &y[0], &self.dy_view[0])
        return np.asarray(self.dy_view).copy()

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def flux(self, double[:] y):
        """Calculate reaction flux for the state y.

        Input:
        y  - state: numpy array [double 39x1]
        Output:
        flux - state change: numpy array [double 38x1]
        """
        cdef int yx
        yx = y.shape[0]
        if yx != 39:
            raise ValueError("Wrong dimension for y, should be [39x1]")

        y = np.require(y, dtype=np.double, requirements='C')
        flux(&self.p_view[0], &y[0], &self.flux_view[0])
        return np.asarray(self.flux_view).copy()

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def jacobian(self, double t, double[:] y):
        """Calculate the Jacobian for the state y (dy/dx).

        Input:
        t  - time: [double] -> Ignored. (For scipy.integrate.ode compatibility)
        y  - state: numpy array [double 39x1]
        Output:
        J  - Jacobian: numpy array [double 39x39]
        """
        cdef int yx
        yx = y.shape[0]
        if yx != 39:
            raise ValueError("Wrong dimension for y, should be [39x1]")

        y = np.require(y, dtype=np.double, requirements='C')
        jacobian(&self.p_view[0], &y[0], &self.J_view[0, 0])
        return np.asarray(self.J_view).copy()


    @cython.boundscheck(False)
    @cython.wraparound(False)
    def dydp(self, double[:] y):
        """Calculate the parametric sensitivity for the state y (dy/dp).

        Input:
        y  - state: numpy array [double 39x1]
        Output:
        dydp  - parametric sensitivity: numpy array [double 39x126]
        """
        cdef int yx
        yx = y.shape[0]
        if yx != 39:
            raise ValueError("Wrong dimension for y, should be [39x1]")

        y = np.require(y, dtype=np.double, requirements='C')
        sensitivity(&self.p_view[0], &y[0], &self.dydp_view[0, 0])
        return np.asarray(self.dydp_view).copy()

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def dfdp(self, double[:] y):
        """Calculate the parametric flux sensitivity for the fluxes (df/dp).

        Input:
        y  - state: numpy array [double 39x1]
        Output:
        dydp  - parametric flux sensitivity: numpy array [double 38x126]
        """
        cdef int yx
        yx = y.shape[0]
        if yx != 39:
            raise ValueError("Wrong dimension for y, should be [39x1]")

        y = np.require(y, dtype=np.double, requirements='C')
        flux_sensitivity(&self.p_view[0], &y[0], &self.dfdp_view[0, 0])
        return np.asarray(self.dfdp_view).copy()
