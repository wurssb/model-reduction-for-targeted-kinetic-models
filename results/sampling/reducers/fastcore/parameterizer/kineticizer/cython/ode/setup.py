"""Automatically generated Cython module build template.
Do not modify: changes might be overwritten.

Author: symbolicSBML.CythonODE (Rik van Rosmalen / rik.vanrosmalen@wur.nl)
"""
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize
import numpy

extensions = [Extension('par_fastcore_ode',
                        sources=['ode.pyx', 'dydt.c'],
                        extra_compile_args=['-O3'],
                        )
              ]
extensions[0].include_dirs += [numpy.get_include()]
setup(
    name='par_fastcore_ode',
    ext_modules=cythonize(extensions, nthreads=4),
    cmdclass=dict([('build_ext', build_ext)]),
)
