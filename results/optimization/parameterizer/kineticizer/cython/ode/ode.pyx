"""Automatically generated Cython module build template.
Do not modify: changes might be overwritten.

Author: symbolicSBML.CythonODE (Rik van Rosmalen / rik.vanrosmalen@wur.nl)
"""
# cython: language_level=3, embedsignature=True
cimport cython

import numpy as np
cimport numpy as cnp

cdef extern from "dydt.h":
    void flux(double *p, double *y, double *flux)
    void dydt(double *p, double *y, double *dydt)
#    void jacobian(double *p, double *y, double *jacobian)
#    void sensitivity(double *p, double *y, double *dydp)
#    void flux_sensitivity(double *p, double *y, double *dfdp)

cdef class CythonODE(object):
    """A optimized Cython ODE system.

    Significantly more performant then SympyLambdifyODE, but requires compilation.
    Note that the interface is slightly different as parameters should be set manually.
    """
    # We re-use the same views for the c bindings to avoid overhead.
    # Note that all numpy arrays received as input are copied and converted to C-layout.
    # All outputs are also copied. This is done to avoid bugs and subtle interations
    # with numpy's system of views and actual copies.
    cdef double[:] p_view
    cdef double[:] flux_view
    cdef double[:] dy_view
#    cdef double[:,:] J_view
#    cdef double[:,:] dydp_view
#    cdef double[:,:] dfdp_view

    ODE_DTYPE = np.double

    n_parameters = 88
    n_metabolites = 22
    n_reactions = 21

    parameters = np.array(["R","T","km__A__r1","km__B__r1","km__B__r10","km__B__r2","km__B__r3","km__C__r2","km__C__r4","km__D__r13","km__D__r3","km__D__r5","km__EE__r4","km__EE__r5","km__EE__r6","km__F__r6","km__F__r7","km__G__r15","km__G__r7","km__G__r8","km__H__r14","km__H__r6","km__H__r8","km__II__r9","km__J__r10","km__J__r11","km__J__r9","km__K__r11","km__K__r15","km__L__r11","km__L__r12","km__L__r14","km__M__r12","km__M__r13","km__N__r12","km__X__r15","km__X__r6","km__X__r7","km__X__r8","km__Y__r1","km__Y__r13","km__Y__r16","km__Z__r1","km__Z__r13","km__Z__r16","km_tot_rate__input_A","km_tot_rate__input_II","km_tot_rate__output_K","km_tot_rate__output_N","km_tot_rate__output_X","mu__A","mu__B","mu__C","mu__D","mu__EE","mu__F","mu__G","mu__H","mu__II","mu__J","mu__K","mu__L","mu__M","mu__N","mu__X","mu__Y","mu__Z","mu__input_A_pool","mu__input_II_pool","mu__output_K_pool","mu__output_N_pool","mu__output_X_pool","u_v__r1","u_v__r10","u_v__r11","u_v__r12","u_v__r13","u_v__r14","u_v__r15","u_v__r16","u_v__r2","u_v__r3","u_v__r4","u_v__r5","u_v__r6","u_v__r7","u_v__r8","u_v__r9"])
    metabolites = np.array(["A","B","C","D","EE","F","G","H","II","J","K","L","M","N","X","Y","Z","input_A_pool","input_II_pool","output_X_pool","output_K_pool","output_N_pool"])
    reactions = np.array(["input_A","input_II","output_K","output_N","output_X","r1","r2","r3","r4","r5","r6","r7","r8","r9","r10","r11","r12","r13","r14","r15","r16"])

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def __init__(self):
        """Generated ODE system."""
        self.p_view = np.zeros((88,), dtype=np.double, order='C')
        self.dy_view = np.zeros((22,), dtype=np.double, order='C')
        self.flux_view = np.zeros((21,), dtype=np.double, order='C')
#        self.J_view = np.zeros((22, 22), dtype=np.double, order='C')
#        self.dydp_view = np.zeros((22, 88), dtype=np.double, order='C')
#        self.dfdp_view = np.zeros((21, 88), dtype=np.double, order='C')

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def get_current_parameters(self):
        """View the current parameters of the ODE system."""
        return np.asarray(self.p_view).copy()

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def update_parameters(self, cnp.ndarray[cnp.double_t, ndim=1] p):
        """Update the parameters of the ODE system.

        Input:
        p  - parameters: numpy array [double 88x1]
        """
        cdef int px, py
        px = p.shape[0]
        py = p.ndim
        if px != 88 or py != 1:
            raise ValueError("Wrong dimension for p, should be [88x1]")
        self.p_view = np.require(p.copy(order='C'), dtype=np.double, requirements='C')

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def dydt(self, double t, double[:] y):
        """Calculate state change (integration step) for the state y.

        Input:
        t  - time: [double] -> Ignored. (For scipy.integrate.ode compatibility)
        y  - state: numpy array [double 22x1]
        Output:
        dy - state change: numpy array [double 22x1]
        """
        cdef int yx
        yx = y.shape[0]
        if yx != 22:
            raise ValueError("Wrong dimension for y, should be [22x1]")

        y = np.require(y, dtype=np.double, requirements='C')
        dydt(&self.p_view[0], &y[0], &self.dy_view[0])
        return np.asarray(self.dy_view).copy()

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def flux(self, double[:] y):
        """Calculate reaction flux for the state y.

        Input:
        y  - state: numpy array [double 22x1]
        Output:
        flux - state change: numpy array [double 21x1]
        """
        cdef int yx
        yx = y.shape[0]
        if yx != 22:
            raise ValueError("Wrong dimension for y, should be [22x1]")

        y = np.require(y, dtype=np.double, requirements='C')
        flux(&self.p_view[0], &y[0], &self.flux_view[0])
        return np.asarray(self.flux_view).copy()

#    @cython.boundscheck(False)
#    @cython.wraparound(False)
#    def jacobian(self, double t, double[:] y):
#        """Calculate the Jacobian for the state y (dy/dx).
#
#        Input:
#        t  - time: [double] -> Ignored. (For scipy.integrate.ode compatibility)
#        y  - state: numpy array [double 22x1]
#        Output:
#        J  - Jacobian: numpy array [double 22x22]
#        """
#        cdef int yx
#        yx = y.shape[0]
#        if yx != 22:
#            raise ValueError("Wrong dimension for y, should be [22x1]")
#
#        y = np.require(y, dtype=np.double, requirements='C')
#        jacobian(&self.p_view[0], &y[0], &self.J_view[0, 0])
#        return np.asarray(self.J_view).copy()


#    @cython.boundscheck(False)
#    @cython.wraparound(False)
#    def dydp(self, double[:] y):
#        """Calculate the parametric sensitivity for the state y (dy/dp).
#
#        Input:
#        y  - state: numpy array [double 22x1]
#        Output:
#        dydp  - parametric sensitivity: numpy array [double 22x88]
#        """
#        cdef int yx
#        yx = y.shape[0]
#        if yx != 22:
#            raise ValueError("Wrong dimension for y, should be [22x1]")
#
#        y = np.require(y, dtype=np.double, requirements='C')
#        sensitivity(&self.p_view[0], &y[0], &self.dydp_view[0, 0])
#        return np.asarray(self.dydp_view).copy()

#    @cython.boundscheck(False)
#    @cython.wraparound(False)
#    def dfdp(self, double[:] y):
#        """Calculate the parametric flux sensitivity for the fluxes (df/dp).
#
#        Input:
#        y  - state: numpy array [double 22x1]
#        Output:
#        dydp  - parametric flux sensitivity: numpy array [double 21x88]
#        """
#        cdef int yx
#        yx = y.shape[0]
#        if yx != 22:
#            raise ValueError("Wrong dimension for y, should be [22x1]")
#
#        y = np.require(y, dtype=np.double, requirements='C')
#        flux_sensitivity(&self.p_view[0], &y[0], &self.dfdp_view[0, 0])
#        return np.asarray(self.dfdp_view).copy()
