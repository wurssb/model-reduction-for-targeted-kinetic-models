import collections
import json
import pathlib
import warnings
import functools

import h5py
import pandas as pd
import numpy as np

import matplotlib

import matplotlib.pyplot as plt
import seaborn as sns

from symbolicSBML.parameters import Parameters
import symbolicSBML

from pipeline.util import (
    get_last_valid_index,
    parse_optimization_results,
    parse_reference_simulations,
    parse_optimized_simulations,
    parse_simulation_results,
)


def colors(use="base"):
    """Color palette for all visualizations in paper.

    Based on Paul Tol's Notes (https://personal.sron.nl/~pault/) and the
    provided python code (https://personal.sron.nl/~pault/data/tol_colors.py)
    under the BSD-3 clause license.
    """
    base = {
        "blue": tuple(i / 256.0 for i in (0, 119, 187)),
        "cyan": tuple(i / 256.0 for i in (51, 187, 238)),
        "teal": tuple(i / 256.0 for i in (0, 153, 136)),
        "orange": tuple(i / 256.0 for i in (238, 119, 51)),
        "red": tuple(i / 256.0 for i in (204, 51, 17)),
        "magenta": tuple(i / 256.0 for i in (238, 51, 119)),
        "grey": tuple(i / 256.0 for i in (187, 187, 187)),
        "black": tuple(i / 256.0 for i in (0, 0, 0)),
    }
    background = {
        "pale blue": tuple(i / 256.0 for i in (187, 204, 238)),
        "pale cyan": tuple(i / 256.0 for i in (204, 238, 255)),
        "pale green": tuple(i / 256.0 for i in (204, 221, 170)),
        "pale yellow": tuple(i / 256.0 for i in (238, 238, 187)),
        "pale red": tuple(i / 256.0 for i in (255, 204, 204)),
        "pale grey": tuple(i / 256.0 for i in (221, 221, 221)),
    }

    if use == "models" or use == "distributions":
        return {
            "Original": base["black"],
            "minNW": base["blue"],
            "FastCore": base["red"],
            "NetworkReducer": base["orange"],
        }
    elif use == "lines" or use == "metabolites":
        return {k: v for k, v in base.items() if k not in ("gray", "black")}
    elif use == "background":
        return background
    elif use == "base":
        return base
    else:
        raise ValueError("Invalid use: {}.".format(use))


def unique_dashes(n):
    """Build an arbitrarily long list of unique dash styles for lines.
    Parameters
    ----------
    n : int
        Number of unique dash specs to generate.
    Returns
    -------
    dashes : list of strings or tuples
        Valid arguments for the ``dashes`` parameter on
        :class:`matplotlib.lines.Line2D`. The first spec is a solid
        line (``""``), the remainder are sequences of long and short
        dashes.

    This code was taken from a newer version of Seaborn (version 0.11+)
    under the 3-cause BSD license.
    https://seaborn.pydata.org
    """
    # Start with dash specs that are well distinguishable
    dashes = [
        "",
        (4, 1.5),
        (1, 1),
        (3, 1.25, 1.5, 1.25),
        (5, 1, 1, 1),
    ]

    # Now programatically build as many as we need
    p = 3
    while len(dashes) < n:

        # Take combinations of long and short dashes
        a = itertools.combinations_with_replacement([3, 1.25], p)
        b = itertools.combinations_with_replacement([4, 1], p)

        # Interleave the combinations, reversing one of the streams
        segment_list = itertools.chain(*zip(list(a)[1:-1][::-1], list(b)[1:-1]))

        # Now insert the gaps
        for segments in segment_list:
            gap = min(segments)
            spec = tuple(itertools.chain(*((seg, gap) for seg in segments)))
            dashes.append(spec)

        p += 1

    return dashes[:n]


def get_shared_axis(dataframes, axis="columns"):
    """Get the shared index of the axis over an iterable of data frames."""
    if axis == "columns":
        axis = 1
    elif axis == "index":
        axis = 0
    elif axis not in (0, 1):
        raise ValueError("Invalid axis: {}".format(axis))
    dataframes = iter(dataframes)
    intersect = next(dataframes).axes[axis]
    for df in dataframes:
        intersect = intersect.intersection(df.axes[axis])
    return intersect


def get_nonzero_axis(dataframes, how="any", axis="columns"):
    """Get the nonzero index of the axis over an iterable of data frames."""
    if isinstance(dataframes, pd.DataFrame):
        dataframes = [dataframes]
    # If we get an iterator, we need to duplicate it to use the length later,
    # or it will be exhausted already.
    if how == "all":
        dataframes, i_len = itertools.tee(dataframes)
    if axis == "columns":
        axis = 1
        other_axis = 0
    elif axis == "index":
        axis = 0
        other_axis = 1
    elif axis not in (0, 1):
        raise ValueError("Invalid axis: {}".format(axis))
    else:
        other_axis = int(not axis)
    nonzero_count = functools.reduce(
        lambda x, y: x.add(y, fill_value=0),
        ((df != 0).any(axis=other_axis) for df in dataframes),
    )
    if how == "any":
        return nonzero_count[nonzero_count > 0].index
    elif how == "all":
        return nonzero_count[nonzero_count == sum(1 for _ in i_len)].index
    else:
        raise ValueError("Invalid setting for how: use 'any' or 'all'")


def get_parameter_types(series):
    """Extract the paramater type of a series with parameter names."""
    return series.str.split(Parameters.sep, expand=True)[0]


def create_parameter_name_map(names):
    plot_parameter_names = {}
    for p in names:
        # Split type and metabolite/reaction
        p_type, *variable = p.split(Parameters.sep)
        # Merge metabolite and reaction into one, replacing underscores in the name.
        # [These would otherwise be interpreted as subscript modifiers]
        variable = ", ".join(variable).replace("_", r"\_")
        # Get the texified parameter name
        try:
            p_type = Parameters.to_latex([p_type], add_units=False, matplotlib=True)[0]
        except ValueError:
            # Not a standard parameter, keep as is.
            p_type = "${}$".format(p_type.replace("_", r"\_"))
        # If we have a variable, join it as a subscript.
        if variable:
            plot_parameter_names[p] = "".join(
                (p_type[:-1], "_{", variable, "}", p_type[-1:])
            )
        else:
            plot_parameter_names[p] = p_type
    return plot_parameter_names


def trim(dataframe, method="IQR", sigma_factor=2, percentile_range=(0.05, 0.95)):
    """Trim outliers of a dataframe (works on columns), replacing values with NaN.

    Methods:
        "IQR" - Trim everything outside the inter quantile range.
        "percentile" - Trim a percentile on both sides (adjust with percentile_range).
        "sigma" - Trim outside x standard deviations of the mean (adjust with sigma_factor).

    """
    if method == "IQR":
        Q1 = dataframe.quantile(0.25)
        Q3 = dataframe.quantile(0.75)
        IQR = Q3 - Q1
        allowed_min = Q1 - 1.5 * IQR
        allowed_max = Q3 + 1.5 * IQR
    elif method == "percentile":
        allowed_min = dataframe.quantile(percentile_range[0])
        allowed_max = dataframe.quantile(percentile_range[1])
    elif method.endswith("sigma"):
        mu = dataframe.mean()
        sigma = dataframe.std()
        allowed_min = mu - sigma_factor * sigma
        allowed_max = mu + sigma_factor * sigma
    else:
        raise ValueError("Invalid method: {}".format(method))
    return dataframe[(dataframe >= allowed_min) & (dataframe <= allowed_max)]


def jaccard_index(a, b):
    """Calculate the jaccard index for two dataframes.

    If passed two dataframes, will align based on the columns.
    For arrays, will align based on the column indices instead."""
    a, b = (pd.DataFrame(i) for i in (a, b))

    intersect_min = (
        pd.concat((df.min(skipna=True) for df in (a, b)), axis=1)
        .max(skipna=False, axis=1)
        .dropna()
    )
    intersect_max = (
        pd.concat((df.max(skipna=True) for df in (a, b)), axis=1)
        .min(skipna=False, axis=1)
        .dropna()
    )

    intersect_count = pd.concat(
        (
            ((df >= intersect_min) & (df <= intersect_max)).sum()[intersect_min.index]
            for df in (a, b)
        ),
        axis=1,
    ).sum(axis=1)
    union_count = pd.concat(
        ((~df.isna()).sum()[intersect_min.index] for df in (a, b)), axis=1
    ).sum(axis=1)
    return intersect_count / union_count


def trace_plot(x, y, ax=None, log=True, plot_args=None, scatter_args=None):
    """Create a trace plot, marking the end of each trace with a scatterplot."""
    # Prepare default arguments
    if ax is None:
        fig, ax = plt.subplots("trace")
    else:
        fig = None
    if plot_args is None:
        plot_args = {"alpha": 0.5, "c": "blue"}
    if scatter_args is None:
        scatter_args = {"alpha": 0.5, "marker": "x", "c": "blue"}

    x_2d = len(x.shape) == 2
    n_runs = y.shape[0]
    max_valid_index = get_last_valid_index(y, axis=1)

    # First plot the traces as lines.
    for i, end in zip(np.arange(n_runs), max_valid_index):
        if end < 0:
            continue
        end = end + 1
        y_i = y[i, :end]
        if x_2d:
            x_i = x[i, :end]
        else:
            x_i = x[:end]
        ax.plot(x_i, y_i, **plot_args)

    # Next plot the ends as a scatter.
    if x_2d:
        x_end = x[np.arange(n_runs), max_valid_index][max_valid_index != -1]
    else:
        x_end = x[max_valid_index][max_valid_index != -1]
    y_end = y[np.arange(n_runs), max_valid_index][max_valid_index != -1]
    ax.scatter(x_end, y_end, **scatter_args)

    # Usually optimization traces are best viewed logarithmic.
    if log:
        ax.set_yscale("log")

    # Return the figure only if we made it ourselves.
    if fig:
        return fig, ax
    else:
        return ax
