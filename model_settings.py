"""Settings for each of the test models used in `paper.py`."""
import sympy

from symbolicSBML import Parameters
from pipeline.kinetics_steps import NA_IDENTIFIER
from pipeline.symbolic_util import SymbolicPerturbation

t = sympy.Symbol("t")


model_settings = {
    "toy": {
        "model_path": "../data/models/toy_model_large_less_obs.xml",
        "fixed_parameters": {
            # These are used later in the experiments.
            "a_feed_rate": 0.0,
            "ii_feed_rate": 0.0,
        },
        "data": [
            [Parameters.mu, "A", NA_IDENTIFIER, 100, 1e-1],
            [Parameters.mu, "B", NA_IDENTIFIER, 85, 1e-1],
            [Parameters.mu, "C", NA_IDENTIFIER, 40, 1e-1],
            [Parameters.mu, "D", NA_IDENTIFIER, 40, 1e-1],
            [Parameters.mu, "EE", NA_IDENTIFIER, 35, 1e-1],
            [Parameters.mu, "F", NA_IDENTIFIER, 70, 1e-1],
            [Parameters.mu, "G", NA_IDENTIFIER, 60, 1e-1],
            [Parameters.mu, "H", NA_IDENTIFIER, 45, 1e-1],
            [Parameters.mu, "II", NA_IDENTIFIER, 100, 1e-1],
            [Parameters.mu, "J", NA_IDENTIFIER, 95, 1e-1],
            [Parameters.mu, "K", NA_IDENTIFIER, 20, 1e-1],
            [Parameters.mu, "L", NA_IDENTIFIER, 70, 1e-1],
            [Parameters.mu, "M", NA_IDENTIFIER, 65, 1e-1],
            [Parameters.mu, "N", NA_IDENTIFIER, 5, 1e-1],
            [Parameters.mu, "X", NA_IDENTIFIER, 10, 1e-1],
            [Parameters.mu, "Y", NA_IDENTIFIER, 50, 1e-1],
            [Parameters.mu, "Z", NA_IDENTIFIER, 60, 1e-1],
            # All input pools have high energy and concentration,
            # while all outputs have low energy and concentration.
            # This will cause a driving force for input to output.
            # We should aim for a delta of 10~30, as we limit the driving
            # force from 1 to 40 in the optimization step where we generate
            # the reference parameter set.
            [Parameters.mu, "input_A_pool", NA_IDENTIFIER, 120, 1e-1],
            [Parameters.c, "input_A_pool", NA_IDENTIFIER, 10, 1e-3],
            [Parameters.mu, "input_II_pool", NA_IDENTIFIER, 120, 1e-1],
            [Parameters.c, "input_II_pool", NA_IDENTIFIER, 10, 1e-3],
            [Parameters.mu, "output_X_pool", NA_IDENTIFIER, 0, 1e-1],
            [Parameters.c, "output_X_pool", NA_IDENTIFIER, 1e-3, 1e-6],
            [Parameters.mu, "output_K_pool", NA_IDENTIFIER, 0, 1e-1],
            [Parameters.c, "output_K_pool", NA_IDENTIFIER, 1e-3, 1e-6],
            [Parameters.mu, "output_N_pool", NA_IDENTIFIER, 0, 1e-1],
            [Parameters.c, "output_N_pool", NA_IDENTIFIER, 1e-3, 1e-6],
        ],
        "flux_objective": "output_X",
        "flux_constraints": [
            ("input_II", None, 10),
            ("input_A", None, 10),
            ("output_N", 10, None),
            ("output_K", 10, None),
        ],
        "protected_reactions": [
            "input_A",
            "input_II",
            "output_K",
            "output_N",
            "output_X",
        ],
        "protected_metabolites": ["A", "II", "D", "X", "Y", "Z"],
        "protected_behaviour": [
            # In this example, uptake of 1 unit of input A,
            # should be able to lead to 6 units of the objective.
            {
                "reactions": ["input_A", "output_X"],
                "directions": [1, 1],
                "values": [1, 6],
            },
            # We also want the model to be able to produce waste products K + N.
            {
                "reactions": [
                    "input_A",
                    "input_II",
                    "output_X",
                    "output_K",
                    "output_N",
                ],
                "directions": [1, 1, 1, 1, 1],
                "values": [1, 1, 9, 1, 1],
            },
        ],
        "abstract_metabolites": [
            ["input_A", "reactant", "input_A_pool", True],
            ["input_II", "reactant", "input_II_pool", True],
            ["output_X", "product", "output_X_pool", True],
            ["output_K", "product", "output_K_pool", True],
            ["output_N", "product", "output_N_pool", True],
        ],
        "additional_input_reactions": {
            "feed_input_A": ["input_A_pool", "a_feed_rate"],
            "feed_input_II": ["input_II_pool", "ii_feed_rate"],
        },
        "simplify_reactions": [
            "input_A",
            "input_II",
            "output_X",
            "output_K",
            "output_N",
        ],
        "perturbations": {
            "pulse_a": SymbolicPerturbation(t >= 75, {"input_A_pool": 5}),
            "pulse_ii": SymbolicPerturbation(t >= 75, {"input_II_pool": 5}),
        },
        "feed_rates": {"a_feed_rate": 0.05, "ii_feed_rate": 0.05},
        "batch_amounts": {"a_batch": 10, "ii_batch": 10, "no_batch": 1e-3},
        "experiments": [
            # 0: Scenario 2 (feed I + pulse A)
            {
                "set": "optimization",
                "extra_fixed_parameters": {"ii_feed_rate": "ii_feed_rate"},
                "perturbations": ["pulse_a"],
                "initial_concentrations": {
                    "input_A_pool": "no_batch",
                    "input_II_pool": "no_batch",
                },
            },
            # 1: Scenario 8 (feed A + batch I)
            {
                "set": "optimization",
                "extra_fixed_parameters": {"a_feed_rate": "a_feed_rate"},
                "perturbations": [],
                "initial_concentrations": {
                    "input_A_pool": "no_batch",
                    "input_II_pool": "ii_batch",
                },
            },
            # 2: Scenario 5 (batch A + pulse I)
            {
                "set": "optimization",
                "extra_fixed_parameters": {},
                "perturbations": ["pulse_ii"],
                "initial_concentrations": {
                    "input_A_pool": "no_batch",
                    "input_II_pool": "a_batch",
                },
            },
            # 3: Scenario 12 (pulse A + I)
            {
                "set": "optimization",
                "extra_fixed_parameters": {},
                "perturbations": ["pulse_a", "pulse_ii"],
                "initial_concentrations": {
                    "input_A_pool": "no_batch",
                    "input_II_pool": "no_batch",
                },
            },
            # 4: Scenario 17 (feed A + I + pulse A + I)
            {
                "set": "optimization",
                "extra_fixed_parameters": {
                    "a_feed_rate": "a_feed_rate",
                    "ii_feed_rate": "ii_feed_rate",
                },
                "perturbations": ["pulse_a", "pulse_ii"],
                "initial_concentrations": {
                    "input_A_pool": "no_batch",
                    "input_II_pool": "no_batch",
                },
            },
            # 5: Scenario 21 (batch A + I)
            {
                "set": "optimization",
                "extra_fixed_parameters": {},
                "perturbations": [],
                "initial_concentrations": {
                    "input_A_pool": "a_batch",
                    "input_II_pool": "ii_batch",
                },
            },
            # 6: Scenario 11 (batch I + pulse A)
            {
                "set": "test",
                "extra_fixed_parameters": {},
                "perturbations": ["pulse_a"],
                "initial_concentrations": {
                    "input_A_pool": "no_batch",
                    "input_II_pool": "ii_batch",
                },
            },
            # 7: Scenario 1 (batch A + feed A)
            {
                "set": "test",
                "extra_fixed_parameters": {"a_feed_rate": "a_feed_rate"},
                "perturbations": [],
                "initial_concentrations": {
                    "input_A_pool": "a_batch",
                    "input_II_pool": "no_batch",
                },
            },
            # 8: Scenario 20 (batch A + I + pulse A + I)
            {
                "set": "test",
                "extra_fixed_parameters": {},
                "perturbations": ["pulse_a", "pulse_ii"],
                "initial_concentrations": {
                    "input_A_pool": "a_batch",
                    "input_II_pool": "ii_batch",
                },
            },
        ],
        "specific_parameter_ranges": {
            # For the chemical energies we give some extra help of the real value +/- 10.
            # Equilibrator has good coverage and usually predicts with <1 std, so having this
            # as data is generally possible.
            "mu__A": (90.0, 110.0),
            "mu__B": (70.0, 90.0),
            "mu__C": (30.0, 50.0),
            "mu__D": (30.0, 50.0),
            "mu__EE": (25.0, 45.0),
            "mu__F": (60.0, 80.0),
            "mu__G": (50.0, 70.0),
            "mu__H": (35.0, 55.0),
            "mu__II": (90.0, 110.0),
            "mu__J": (85.0, 105.0),
            "mu__K": (10.0, 30.0),
            "mu__L": (60.0, 80.0),
            "mu__M": (55.0, 75.0),
            "mu__N": (-5.0, 15.0),
            "mu__X": (0.0, 20.0),
            "mu__Y": (40.0, 60.0),
            "mu__Z": (50.0, 70.0),
            "mu__input_A_pool": (110.0, 130.0),
            "mu__input_II_pool": (110.0, 130.0),
            "mu__output_K_pool": (-10.0, 10.0),
            "mu__output_N_pool": (-10.0, 10.0),
            "mu__output_X_pool": (-10.0, 10.0),
        },
    },
    "large_cycle": {
        "model_path": "../data/test_models/large_cycle.xml",
        "fixed_parameters": {"a_feed_rate": 0.0},
        "data": [
            [Parameters.mu, "A", NA_IDENTIFIER, 100, 1e-1],
            [Parameters.mu, "B", NA_IDENTIFIER, 90, 1e-1],
            [Parameters.mu, "C", NA_IDENTIFIER, 80, 1e-1],
            [Parameters.mu, "D", NA_IDENTIFIER, 85, 1e-1],
            [Parameters.mu, "EE", NA_IDENTIFIER, 85, 1e-1],
            [Parameters.mu, "F", NA_IDENTIFIER, 70, 1e-1],
            [Parameters.mu, "G", NA_IDENTIFIER, 72, 1e-1],
            [Parameters.mu, "H", NA_IDENTIFIER, 50, 1e-1],
            [Parameters.mu, "II", NA_IDENTIFIER, 40, 1e-1],
            [Parameters.mu, "J", NA_IDENTIFIER, 35, 1e-1],
            [Parameters.mu, "K", NA_IDENTIFIER, 30, 1e-1],
            [Parameters.mu, "L", NA_IDENTIFIER, 45, 1e-1],
            [Parameters.mu, "M", NA_IDENTIFIER, 40, 1e-1],
            [Parameters.mu, "N", NA_IDENTIFIER, 20, 1e-1],
            [Parameters.mu, "X", NA_IDENTIFIER, 20, 1e-1],
            [Parameters.mu, "Y", NA_IDENTIFIER, 10, 1e-1],
            [Parameters.mu, "P", NA_IDENTIFIER, 0, 1e-1],
            [Parameters.mu, "Z", NA_IDENTIFIER, 5, 1e-1],
            # Input and output pools
            [Parameters.mu, "input_A_pool", NA_IDENTIFIER, 120, 1e-1],
            [Parameters.c, "input_A_pool", NA_IDENTIFIER, 10, 1e-3],
            [Parameters.mu, "output_N_pool", NA_IDENTIFIER, 0, 1e-1],
            [Parameters.c, "output_N_pool", NA_IDENTIFIER, 1e-3, 1e-6],
            [Parameters.mu, "output_II_pool", NA_IDENTIFIER, 15, 1e-1],
            [Parameters.c, "output_II_pool", NA_IDENTIFIER, 1e-3, 1e-6],
            [Parameters.mu, "output_K_pool", NA_IDENTIFIER, 10, 1e-1],
            [Parameters.c, "output_K_pool", NA_IDENTIFIER, 1e-3, 1e-6],
            [Parameters.mu, "output_M_pool", NA_IDENTIFIER, 10, 1e-1],
            [Parameters.c, "output_M_pool", NA_IDENTIFIER, 1e-3, 1e-6],
            [Parameters.mu, "output_Z_pool", NA_IDENTIFIER, 0, 1e-1],
            [Parameters.c, "output_Z_pool", NA_IDENTIFIER, 1e-3, 1e-6],
            [Parameters.mu, "exchange_P_pool", NA_IDENTIFIER, 0, 1e-1],
            [Parameters.c, "exchange_P_pool", NA_IDENTIFIER, 3, 1e-3],
        ],
        "flux_objective": "output_N",
        "flux_constraints": [
            ("input_A", None, 10),
            ("output_II", 1, None),
            ("output_K", 1, None),
            ("output_N", 1, None),
            ("output_Z", 1, None),
            ("r1", 1, None),
            ("r10", 0, None),
            ("r11", 0, None),
            ("r16", 0, None),
            ("r17", 0, None),
        ],
        "protected_reactions": [
            "input_A",
            "output_N",
            "output_II",
            "output_K",
            "output_M",
            "output_Z",
            "exchange_P",
        ],
        "protected_metabolites": ["A", "N", "II", "K", "M", "Z", "P", "D"],
        "protected_behaviour": [
            {
                "reactions": ["input_A", "output_K", "output_N"],
                "directions": [1, 1, 1],
                "values": [1, 2, 1],
            },
            {
                "reactions": ["input_A", "output_Z", "output_N"],
                "directions": [1, 1, 1],
                "values": [1, 2, 3],
            },
            {
                "reactions": ["input_A", "output_II", "output_N"],
                "directions": [1, 1, 1],
                "values": [1, 2, 3],
            },
        ],
        "abstract_metabolites": [
            ["input_A", "reactant", "input_A_pool", True],
            ["output_N", "product", "output_N_pool", True],
            ["output_II", "product", "output_II_pool", True],
            ["output_K", "product", "output_K_pool", True],
            ["output_M", "product", "output_M_pool", True],
            ["output_Z", "product", "output_Z_pool", True],
            ["exchange_P", "product", "exchange_P_pool", True],
        ],
        "additional_input_reactions": {"feed_input_A": ["input_A_pool", "a_feed_rate"]},
        "simplify_reactions": [
            "input_A",
            "output_N",
            "output_II",
            "output_K",
            "output_M",
            "output_Z",
            "exchange_P",
        ],
        "perturbations": {
            "pulse_a": SymbolicPerturbation(t >= 75, {"input_A_pool": 5}),
            "pulse_a_small_1": SymbolicPerturbation(t >= 50, {"input_A_pool": 2.5}),
            "pulse_a_small_2": SymbolicPerturbation(t >= 100, {"input_A_pool": 2.5}),
            "pulse_d": SymbolicPerturbation(t >= 75, {"D": 2.5}),
            "pulse_d_small_1": SymbolicPerturbation(t >= 50, {"D": 1}),
            "pulse_d_small_2": SymbolicPerturbation(t >= 100, {"D": 1}),
        },
        "feed_rates": {"a_feed_rate": 0.025},
        "batch_amounts": {"high_batch": 10, "medium_batch": 5, "small_batch": 1},
        "experiments": [
            # 0: Batch (A)
            {
                "set": "optimization",
                "extra_fixed_parameters": {},
                "perturbations": [],
                "initial_concentrations": {"input_A_pool": "high_batch"},
            },
            # 1: Batch (A) + Pulse (A)
            {
                "set": "optimization",
                "extra_fixed_parameters": {},
                "perturbations": ["pulse_a"],
                "initial_concentrations": {"input_A_pool": "high_batch"},
            },
            # 2: Small batch (A) + Small pulses (A)
            {
                "set": "optimization",
                "extra_fixed_parameters": {},
                "perturbations": ["pulse_a_small_1", "pulse_a_small_2"],
                "initial_concentrations": {"input_A_pool": "medium_batch"},
            },
            # 3: Feed (A) + Small pulses (D)
            {
                "set": "optimization",
                "extra_fixed_parameters": {"a_feed_rate": "a_feed_rate"},
                "perturbations": ["pulse_d_small_1", "pulse_d_small_2"],
                "initial_concentrations": {"input_A_pool": "small_batch"},
            },
            # 4: Pulse (A) + small pulses (D)
            {
                "set": "optimization",
                "extra_fixed_parameters": {},
                "perturbations": ["pulse_a", "pulse_d_small_1", "pulse_d_small_2"],
                "initial_concentrations": {"input_A_pool": "small_batch"},
            },
            # 5: Batch (A) + Feed (A)
            {
                "set": "test",
                "extra_fixed_parameters": {"a_feed_rate": "a_feed_rate"},
                "perturbations": [],
                "initial_concentrations": {"input_A_pool": "high_batch"},
            },
            # 6: Batch (A) + Pulse (D)
            {
                "set": "test",
                "extra_fixed_parameters": {},
                "perturbations": ["pulse_d"],
                "initial_concentrations": {"input_A_pool": "high_batch"},
            },
            # 7: Batch (A) + small pulse (A) + small pulse (D)
            {
                "set": "test",
                "extra_fixed_parameters": {},
                "perturbations": ["pulse_a_small_1", "pulse_d_small_2"],
                "initial_concentrations": {"input_A_pool": "high_batch"},
            },
        ],
        "specific_parameter_ranges": {
            "mu__A": (100 - 10, 100 + 10),
            "mu__B": (90 - 10, 90 + 10),
            "mu__C": (80 - 10, 80 + 10),
            "mu__D": (85 - 10, 85 + 10),
            "mu__EE": (85 - 10, 85 + 10),
            "mu__F": (70 - 10, 70 + 10),
            "mu__G": (72 - 10, 72 + 10),
            "mu__H": (50 - 10, 50 + 10),
            "mu__II": (40 - 10, 40 + 10),
            "mu__J": (35 - 10, 35 + 10),
            "mu__K": (30 - 10, 30 + 10),
            "mu__L": (45 - 10, 45 + 10),
            "mu__M": (40 - 10, 40 + 10),
            "mu__N": (20 - 10, 20 + 10),
            "mu__X": (20 - 10, 20 + 10),
            "mu__Y": (10 - 10, 10 + 10),
            "mu__P": (0 - 10, 0 + 10),
            "mu__Z": (5 - 10, 5 + 10),
            "mu__input_A_pool": (120 - 10, 120 + 10),
            "mu__output_N_pool": (0 - 10, 0 + 10),
            "mu__output_II_pool": (15 - 10, 15 + 10),
            "mu__output_K_pool": (10 - 10, 10 + 10),
            "mu__output_M_pool": (10 - 10, 10 + 10),
            "mu__output_Z_pool": (0 - 10, 0 + 10),
            "mu__exchange_P_pool": (0 - 10, 0 + 10),
        },
    },
    "edemp": {
        "model_path": "../data/test_models/edemp.xml",
        "fixed_parameters": {
            "a_feed_rate": 0.0,
            "b_feed_rate": 0.0,
            "c_feed_rate": 0.0,
        },
        "data": [
            [Parameters.mu, "A", NA_IDENTIFIER, 112, 1e-1],
            [Parameters.mu, "B", NA_IDENTIFIER, 110, 1e-1],
            [Parameters.mu, "C", NA_IDENTIFIER, 108, 1e-1],
            [Parameters.mu, "D", NA_IDENTIFIER, 114, 1e-1],
            [Parameters.mu, "EE", NA_IDENTIFIER, 105, 1e-1],
            [Parameters.mu, "F", NA_IDENTIFIER, 102, 1e-1],
            [Parameters.mu, "G", NA_IDENTIFIER, 65, 1e-1],
            [Parameters.mu, "H", NA_IDENTIFIER, 115, 1e-1],
            [Parameters.mu, "II", NA_IDENTIFIER, 107, 1e-1],
            [Parameters.mu, "J", NA_IDENTIFIER, 25, 1e-1],
            [Parameters.mu, "K", NA_IDENTIFIER, 55, 1e-1],
            [Parameters.mu, "L", NA_IDENTIFIER, 45, 1e-1],
            [Parameters.mu, "M", NA_IDENTIFIER, 50, 1e-1],
            [Parameters.mu, "N", NA_IDENTIFIER, 30, 1e-1],
            [Parameters.mu, "X", NA_IDENTIFIER, 15, 1e-1],
            [Parameters.mu, "Y", NA_IDENTIFIER, 5, 1e-1],
            [Parameters.mu, "input_A_pool", NA_IDENTIFIER, 140, 1e-1],
            [Parameters.c, "input_A_pool", NA_IDENTIFIER, 10, 1e-3],
            [Parameters.mu, "input_B_pool", NA_IDENTIFIER, 140, 1e-1],
            [Parameters.c, "input_B_pool", NA_IDENTIFIER, 10, 1e-3],
            [Parameters.mu, "input_C_pool", NA_IDENTIFIER, 140, 1e-1],
            [Parameters.c, "input_C_pool", NA_IDENTIFIER, 10, 1e-3],
            [Parameters.mu, "output_J_pool", NA_IDENTIFIER, 0, 1e-1],
            [Parameters.c, "output_J_pool", NA_IDENTIFIER, 1e-3, 1e-6],
            [Parameters.mu, "output_N_pool", NA_IDENTIFIER, 0, 1e-1],
            [Parameters.c, "output_N_pool", NA_IDENTIFIER, 1e-3, 1e-6],
        ],
        "flux_objective": "output_J",
        "flux_constraints": [
            ("input_A", None, 1),
            ("input_B", None, 1),
            ("input_C", None, 1),
            ("output_J", 2, None),
            ("output_N", 2, None),
            ("r1", 0.25, None),
            ("r2", 0.25, None),
            ("r3", 0.25, None),
            ("r11", 0.01, None),
        ],
        "protected_reactions": [
            "input_A",
            "input_B",
            "input_C",
            "output_J",
            "output_N",
            # Added to force NetworkReducer not to lump & remove X.
            "r16",
        ],
        "protected_metabolites": ["A", "B", "C", "J", "N", "X", "Y"],
        "protected_behaviour": [
            {
                "reactions": ["input_A", "output_J"],
                "directions": [1, 1],
                "values": [1, 2],
            },
            {
                "reactions": ["input_B", "output_N"],
                "directions": [1, 1],
                "values": [1, 2],
            },
        ],
        "abstract_metabolites": [
            ["input_A", "reactant", "input_A_pool", True],
            ["input_B", "reactant", "input_B_pool", True],
            ["input_C", "reactant", "input_C_pool", True],
            ["output_J", "product", "output_J_pool", True],
            ["output_N", "product", "output_N_pool", True],
        ],
        "additional_input_reactions": {
            "feed_input_A": ["input_A_pool", "a_feed_rate"],
            "feed_input_B": ["input_B_pool", "b_feed_rate"],
            "feed_input_C": ["input_C_pool", "c_feed_rate"],
        },
        "simplify_reactions": ["input_A", "input_B", "input_C", "output_J", "output_N"],
        "perturbations": {
            "pulse_a": SymbolicPerturbation(t >= 75, {"input_A_pool": 5}),
            "pulse_b": SymbolicPerturbation(t >= 75, {"input_B_pool": 5}),
            "pulse_c": SymbolicPerturbation(t >= 75, {"input_C_pool": 5}),
            "pulse_x": SymbolicPerturbation(t >= 75, {"X": 1}),
        },
        "feed_rates": {
            "a_feed_rate": 0.025,
            "b_feed_rate": 0.025,
            "c_feed_rate": 0.025,
        },
        "batch_amounts": {"high_batch": 10, "medium_batch": 5, "small_batch": 1},
        "experiments": [
            # 0: Feed A + Batch (C)
            {
                "set": "optimization",
                "extra_fixed_parameters": {"a_feed_rate": "a_feed_rate"},
                "perturbations": [],
                "initial_concentrations": {
                    "input_A_pool": "small_batch",
                    "input_B_pool": "small_batch",
                    "input_C_pool": "high_batch",
                },
            },
            # 1: Medium batch (A/B/C)
            {
                "set": "optimization",
                "extra_fixed_parameters": {},
                "perturbations": [],
                "initial_concentrations": {
                    "input_A_pool": "medium_batch",
                    "input_B_pool": "medium_batch",
                    "input_C_pool": "medium_batch",
                },
            },
            # 2: Feed (B) + Pulse (X)
            {
                "set": "optimization",
                "extra_fixed_parameters": {"b_feed_rate": "b_feed_rate"},
                "perturbations": ["pulse_x"],
                "initial_concentrations": {
                    "input_A_pool": "small_batch",
                    "input_B_pool": "small_batch",
                    "input_C_pool": "small_batch",
                },
            },
            # 3: Feed (A/C)
            {
                "set": "optimization",
                "extra_fixed_parameters": {
                    "a_feed_rate": "a_feed_rate",
                    "c_feed_rate": "c_feed_rate",
                },
                "perturbations": [],
                "initial_concentrations": {
                    "input_A_pool": "small_batch",
                    "input_B_pool": "small_batch",
                    "input_C_pool": "small_batch",
                },
            },
            # 4: Pulse (B/C)
            {
                "set": "optimization",
                "extra_fixed_parameters": {},
                "perturbations": ["pulse_b", "pulse_c"],
                "initial_concentrations": {
                    "input_A_pool": "small_batch",
                    "input_B_pool": "small_batch",
                    "input_C_pool": "small_batch",
                },
            },
            # 5: Batch (A) + Pulse (X)
            {
                "set": "optimization",
                "extra_fixed_parameters": {},
                "perturbations": ["pulse_x"],
                "initial_concentrations": {
                    "input_A_pool": "high_batch",
                    "input_B_pool": "small_batch",
                    "input_C_pool": "small_batch",
                },
            },
            # 6: Feed (A) + medium batch (C) + pulse (B)
            {
                "set": "test",
                "extra_fixed_parameters": {"a_feed_rate": "a_feed_rate"},
                "perturbations": ["pulse_b"],
                "initial_concentrations": {
                    "input_A_pool": "small_batch",
                    "input_B_pool": "small_batch",
                    "input_C_pool": "medium_batch",
                },
            },
            # 7: Batch (C) + pulse (A/C)
            {
                "set": "test",
                "extra_fixed_parameters": {},
                "perturbations": ["pulse_a", "pulse_c"],
                "initial_concentrations": {
                    "input_A_pool": "small_batch",
                    "input_B_pool": "small_batch",
                    "input_C_pool": "high_batch",
                },
            },
            # 8: Medium batch (A) + Feed (B) + Pulse (X)
            {
                "set": "test",
                "extra_fixed_parameters": {"b_feed_rate": "b_feed_rate"},
                "perturbations": ["pulse_x"],
                "initial_concentrations": {
                    "input_A_pool": "medium_batch",
                    "input_B_pool": "small_batch",
                    "input_C_pool": "small_batch",
                },
            },
        ],
        "specific_parameter_ranges": {
            "mu__A": (112 - 10, 112 + 10),
            "mu__B": (110 - 10, 110 + 10),
            "mu__C": (108 - 10, 108 + 10),
            "mu__D": (114 - 10, 114 + 10),
            "mu__EE": (105 - 10, 105 + 10),
            "mu__F": (102 - 10, 102 + 10),
            "mu__G": (65 - 10, 65 + 10),
            "mu__H": (115 - 10, 115 + 10),
            "mu__II": (107 - 10, 107 + 10),
            "mu__J": (25 - 10, 25 + 10),
            "mu__K": (55 - 10, 55 + 10),
            "mu__L": (45 - 10, 45 + 10),
            "mu__M": (50 - 10, 50 + 10),
            "mu__N": (30 - 10, 30 + 10),
            "mu__X": (15 - 10, 15 + 10),
            "mu__Y": (5 - 10, 5 + 10),
            "mu__input_A_pool": (140 - 10, 140 + 10),
            "mu__input_B_pool": (140 - 10, 140 + 10),
            "mu__input_C_pool": (140 - 10, 140 + 10),
            "mu__output_J_pool": (0 - 10, 0 + 10),
            "mu__output_N_pool": (0 - 10, 0 + 10),
        },
    },
    "curcumin": {
        "model_path": "../data/test_models/curcumin.xml",
        "fixed_parameters": {"a_feed_rate": 0.0},
        "data": [
            [Parameters.mu, "A", NA_IDENTIFIER, 90, 1e-1],
            [Parameters.mu, "B", NA_IDENTIFIER, 43, 1e-1],
            [Parameters.mu, "C", NA_IDENTIFIER, 42, 1e-1],
            [Parameters.mu, "D", NA_IDENTIFIER, 6, 1e-1],
            [Parameters.mu, "EE", NA_IDENTIFIER, 31, 1e-1],
            [Parameters.mu, "F", NA_IDENTIFIER, 7, 1e-1],
            [Parameters.mu, "G", NA_IDENTIFIER, 46, 1e-1],
            [Parameters.mu, "H", NA_IDENTIFIER, 45, 1e-1],
            [Parameters.mu, "II", NA_IDENTIFIER, 44, 1e-1],
            [Parameters.mu, "J", NA_IDENTIFIER, 46, 1e-1],
            [Parameters.mu, "K", NA_IDENTIFIER, 60, 1e-1],
            [Parameters.mu, "input_A_pool", NA_IDENTIFIER, 110, 1e-1],
            [Parameters.c, "input_A_pool", NA_IDENTIFIER, 10, 1e-3],
            [Parameters.mu, "output_G_pool", NA_IDENTIFIER, 10, 1e-1],
            [Parameters.c, "output_G_pool", NA_IDENTIFIER, 1e-3, 1e-6],
            [Parameters.mu, "output_H_pool", NA_IDENTIFIER, 10, 1e-1],
            [Parameters.c, "output_H_pool", NA_IDENTIFIER, 1e-3, 1e-6],
            [Parameters.mu, "output_II_pool", NA_IDENTIFIER, 10, 1e-1],
            [Parameters.c, "output_II_pool", NA_IDENTIFIER, 1e-3, 1e-6],
            [Parameters.mu, "output_J_pool", NA_IDENTIFIER, 10, 1e-1],
            [Parameters.c, "output_J_pool", NA_IDENTIFIER, 1e-3, 1e-6],
        ],
        "flux_objective": "output_G",
        "flux_constraints": [
            ("input_A", None, 1),
            ("output_G", 0.1, None),
            ("output_H", 0.1, None),
            ("output_II", 0.1, None),
            ("output_J", 0.1, None),
        ],
        "protected_reactions": [
            "input_A",
            "output_G",
            "output_H",
            "output_II",
            "output_J",
        ],
        "protected_metabolites": ["A", "G", "H", "II", "J"],
        "protected_behaviour": [
            {
                "reactions": [
                    "input_A",
                    "output_G",
                    "output_H",
                    "output_II",
                    "output_J",
                ],
                "directions": [1, 1, 1, 1, 1],
                "values": [4, 1, 1, 1, 1],
            },
            {
                "reactions": ["input_A", "output_G"],
                "directions": [1, 1],
                "values": [2, 3],
            },
        ],
        "abstract_metabolites": [
            ["input_A", "reactant", "input_A_pool", True],
            ["output_G", "product", "output_G_pool", True],
            ["output_H", "product", "output_H_pool", True],
            ["output_II", "product", "output_II_pool", True],
            ["output_J", "product", "output_J_pool", True],
        ],
        "additional_input_reactions": {"feed_input_A": ["input_A_pool", "a_feed_rate"]},
        "simplify_reactions": [
            "input_A",
            "output_G",
            "output_H",
            "output_II",
            "output_J",
        ],
        "perturbations": {
            "pulse_a": SymbolicPerturbation(t >= 75, {"input_A_pool": 5}),
            "pulse_a_small_1": SymbolicPerturbation(t >= 50, {"input_A_pool": 2}),
            "pulse_a_small_2": SymbolicPerturbation(t >= 100, {"input_A_pool": 2}),
            # Technically D & F are not marked as conserved, but they are in our cases.
            # Experimentally, we can still perturb anything - even if we don't model it.
            "pulse_c_small": SymbolicPerturbation(t >= 100, {"C": 1}),
            "pulse_d_small": SymbolicPerturbation(t >= 100, {"D": 1}),
        },
        "feed_rates": {"a_feed_rate": 0.025},
        "batch_amounts": {"high_batch": 10, "medium_batch": 5, "small_batch": 1},
        "experiments": [
            # 0: Batch (A)
            {
                "set": "optimization",
                "extra_fixed_parameters": {},
                "perturbations": [],
                "initial_concentrations": {"input_A_pool": "high_batch"},
            },
            # 1: Small pulses (A) + Feed (A)
            {
                "set": "optimization",
                "extra_fixed_parameters": {"a_feed_rate": "a_feed_rate"},
                "perturbations": ["pulse_a_small_1", "pulse_a_small_2"],
                "initial_concentrations": {"input_A_pool": "small_batch"},
            },
            # 2: Batch (A) + Pulse (A)
            {
                "set": "optimization",
                "extra_fixed_parameters": {},
                "perturbations": ["pulse_a"],
                "initial_concentrations": {"input_A_pool": "high_batch"},
            },
            # 3: Medium batch (A) + pulse (A/C)
            {
                "set": "optimization",
                "extra_fixed_parameters": {},
                "perturbations": ["pulse_a", "pulse_c_small"],
                "initial_concentrations": {"input_A_pool": "medium_batch"},
            },
            # 4: Feed (A) + Pulse (D)
            {
                "set": "optimization",
                "extra_fixed_parameters": {"a_feed_rate": "a_feed_rate"},
                "perturbations": ["pulse_d_small"],
                "initial_concentrations": {"input_A_pool": "small_batch"},
            },
            # 5: Batch (A) + Feed (A) + Small pulse (A)
            {
                "set": "test",
                "extra_fixed_parameters": {"a_feed_rate": "a_feed_rate"},
                "perturbations": ["pulse_a_small_2"],
                "initial_concentrations": {"input_A_pool": "medium_batch"},
            },
            # 6: Feed (A) + Small pulse (A/C)
            {
                "set": "test",
                "extra_fixed_parameters": {"a_feed_rate": "a_feed_rate"},
                "perturbations": ["pulse_a_small_1", "pulse_c_small"],
                "initial_concentrations": {"input_A_pool": "small_batch"},
            },
            # 7: Batch (A) + pulse (C/D)
            {
                "set": "test",
                "extra_fixed_parameters": {"a_feed_rate": "a_feed_rate"},
                "perturbations": ["pulse_c_small", "pulse_d_small"],
                "initial_concentrations": {"input_A_pool": "high_batch"},
            },
        ],
        "specific_parameter_ranges": {
            "mu__A": (90 - 10, 90 + 10),
            "mu__B": (43 - 10, 43 + 10),
            "mu__C": (42 - 10, 42 + 10),
            "mu__D": (6 - 10, 6 + 10),
            "mu__EE": (31 - 10, 31 + 10),
            "mu__F": (7 - 10, 7 + 10),
            "mu__G": (46 - 10, 46 + 10),
            "mu__H": (45 - 10, 45 + 10),
            "mu__II": (44 - 10, 44 + 10),
            "mu__J": (46 - 10, 46 + 10),
            "mu__K": (60 - 10, 60 + 10),
            "mu__input_A_pool": (110 - 10, 110 + 10),
            "mu__output_G_pool": (10 - 10, 10 + 10),
            "mu__output_H_pool": (10 - 10, 10 + 10),
            "mu__output_II_pool": (10 - 10, 10 + 10),
            "mu__output_J_pool": (10 - 10, 10 + 10),
        },
    },
    "co_factors": {
        "model_path": "../data/test_models/co_factors.xml",
        "fixed_parameters": {"a_feed_rate": 0.0, "b_feed_rate": 0.0},
        "data": [
            [Parameters.mu, "A", NA_IDENTIFIER, 100, 1e-1],
            [Parameters.mu, "B", NA_IDENTIFIER, 50, 1e-1],
            [Parameters.mu, "C", NA_IDENTIFIER, 110, 1e-1],
            [Parameters.mu, "D", NA_IDENTIFIER, 60, 1e-1],
            [Parameters.mu, "EE", NA_IDENTIFIER, 60, 1e-1],
            [Parameters.mu, "F", NA_IDENTIFIER, 40, 1e-1],
            [Parameters.mu, "G", NA_IDENTIFIER, 70, 1e-1],
            [Parameters.mu, "H", NA_IDENTIFIER, 25, 1e-1],
            [Parameters.mu, "II", NA_IDENTIFIER, 115, 1e-1],
            [Parameters.mu, "J", NA_IDENTIFIER, 20, 1e-1],
            [Parameters.mu, "K", NA_IDENTIFIER, 100, 1e-1],
            [Parameters.mu, "L", NA_IDENTIFIER, 80, 1e-1],
            [Parameters.mu, "X1", NA_IDENTIFIER, 21, 1e-1],
            [Parameters.mu, "Y1", NA_IDENTIFIER, 9, 1e-1],
            [Parameters.mu, "X2", NA_IDENTIFIER, 21, 1e-1],
            [Parameters.mu, "Y2", NA_IDENTIFIER, 9, 1e-1],
            [Parameters.mu, "P", NA_IDENTIFIER, 0, 1e-1],
            [Parameters.mu, "Z", NA_IDENTIFIER, 5, 1e-1],
            # Input and output pools
            [Parameters.mu, "input_A_pool", NA_IDENTIFIER, 120, 1e-1],
            [Parameters.c, "input_A_pool", NA_IDENTIFIER, 10, 1e-3],
            [Parameters.mu, "input_B_pool", NA_IDENTIFIER, 70, 1e-1],
            [Parameters.c, "input_B_pool", NA_IDENTIFIER, 10, 1e-3],
            [Parameters.mu, "output_J_pool", NA_IDENTIFIER, 5, 1e-1],
            [Parameters.c, "output_J_pool", NA_IDENTIFIER, 1e-3, 1e-6],
            [Parameters.mu, "output_L_pool", NA_IDENTIFIER, 55, 1e-1],
            [Parameters.c, "output_L_pool", NA_IDENTIFIER, 1e-3, 1e-6],
            [Parameters.mu, "output_Z_pool", NA_IDENTIFIER, 0, 1e-1],
            [Parameters.c, "output_Z_pool", NA_IDENTIFIER, 1e-3, 1e-6],
        ],
        "flux_objective": "output_L",
        "flux_constraints": [
            ("input_A", None, 10),
            ("input_B", None, 10),
            ("output_J", 10, None),
            ("output_L", 10, None),
            ("output_Z", 10, None),
        ],
        "protected_reactions": [
            "input_A",
            "input_B",
            "output_J",
            "output_L",
            "output_Z",
            "r14",
        ],
        "protected_metabolites": ["A", "B", "J", "L", "X1", "X2", "Y1", "Y2", "Z"],
        "protected_behaviour": [
            {
                "reactions": ["input_A", "input_B", "output_J", "output_L", "output_Z"],
                "directions": [1, 1, 1, 1, 1],
                "values": [1, 1, 1, 1, 1],
            }
        ],
        "abstract_metabolites": [
            ["input_A", "reactant", "input_A_pool", True],
            ["input_B", "reactant", "input_B_pool", True],
            ["output_J", "product", "output_J_pool", True],
            ["output_L", "product", "output_L_pool", True],
            ["output_Z", "product", "output_Z_pool", True],
        ],
        "additional_input_reactions": {
            "feed_input_A": ["input_A_pool", "a_feed_rate"],
            "feed_input_B": ["input_B_pool", "b_feed_rate"],
        },
        "simplify_reactions": [
            "input_A",
            "input_B",
            "output_J",
            "output_L",
            "output_Z",
        ],
        "perturbations": {
            "pulse_a": SymbolicPerturbation(t >= 75, {"input_A_pool": 5}),
            "pulse_b": SymbolicPerturbation(t >= 75, {"input_A_pool": 5}),
            "pulse_x1": SymbolicPerturbation(t >= 75, {"X1": 1}),
            "pulse_x2": SymbolicPerturbation(t >= 75, {"X2": 1}),
        },
        "feed_rates": {"a_feed_rate": 0.025, "b_feed_rate": 0.025},
        "batch_amounts": {"high_batch": 10, "medium_batch": 5, "small_batch": 1},
        "experiments": [
            # 0: Batch (A) + feed (B)
            {
                "set": "optimization",
                "extra_fixed_parameters": {"b_feed_rate": "b_feed_rate"},
                "perturbations": [],
                "initial_concentrations": {
                    "input_A_pool": "high_batch",
                    "input_B_pool": "small_batch",
                },
            },
            # 1: Feed (A/B) + pulse (X1)
            {
                "set": "optimization",
                "extra_fixed_parameters": {
                    "a_feed_rate": "a_feed_rate",
                    "b_feed_rate": "b_feed_rate",
                },
                "perturbations": ["pulse_x1"],
                "initial_concentrations": {
                    "input_A_pool": "small_batch",
                    "input_B_pool": "small_batch",
                },
            },
            # 2: Batch (A) + pulse (A)
            {
                "set": "optimization",
                "extra_fixed_parameters": {},
                "perturbations": ["pulse_a"],
                "initial_concentrations": {
                    "input_A_pool": "high_batch",
                    "input_B_pool": "small_batch",
                },
            },
            # 3: Batch (A/B) + pulse (A/B)
            {
                "set": "optimization",
                "extra_fixed_parameters": {"b_feed_rate": "b_feed_rate"},
                "perturbations": ["pulse_a", "pulse_b"],
                "initial_concentrations": {
                    "input_A_pool": "high_batch",
                    "input_B_pool": "high_batch",
                },
            },
            # 4: Feed (A) + Batch (B) + pulse (X2)
            {
                "set": "optimization",
                "extra_fixed_parameters": {"a_feed_rate": "a_feed_rate"},
                "perturbations": ["pulse_x2"],
                "initial_concentrations": {
                    "input_A_pool": "small_batch",
                    "input_B_pool": "high_batch",
                },
            },
            # 5: Batch (A) + Feed (B) + pulse (B)
            {
                "set": "optimization",
                "extra_fixed_parameters": {"b_feed_rate": "b_feed_rate"},
                "perturbations": ["pulse_b"],
                "initial_concentrations": {
                    "input_A_pool": "high_batch",
                    "input_B_pool": "medium_batch",
                },
            },
            # 6: Batch (B) + pulse (x2)
            {
                "set": "test",
                "extra_fixed_parameters": {},
                "perturbations": ["pulse_x2"],
                "initial_concentrations": {
                    "input_A_pool": "small_batch",
                    "input_B_pool": "high_batch",
                },
            },
            # 7: Batch (A/B) + pulse (A)
            {
                "set": "test",
                "extra_fixed_parameters": {},
                "perturbations": ["pulse_a"],
                "initial_concentrations": {
                    "input_A_pool": "medium_batch",
                    "input_B_pool": "medium_batch",
                },
            },
            # 8: Feed (A/B) + Pulse (B/X1)
            {
                "set": "test",
                "extra_fixed_parameters": {
                    "a_feed_rate": "a_feed_rate",
                    "b_feed_rate": "b_feed_rate",
                },
                "perturbations": ["pulse_b", "pulse_x1"],
                "initial_concentrations": {
                    "input_A_pool": "small_batch",
                    "input_B_pool": "small_batch",
                },
            },
        ],
        "specific_parameter_ranges": {
            "mu__A": (100 - 10, 100 + 10),
            "mu__B": (50 - 10, 50 + 10),
            "mu__C": (110 - 10, 110 + 10),
            "mu__D": (60 - 10, 60 + 10),
            "mu__EE": (60 - 10, 60 + 10),
            "mu__F": (40 - 10, 40 + 10),
            "mu__G": (70 - 10, 70 + 10),
            "mu__H": (25 - 10, 25 + 10),
            "mu__II": (115 - 10, 115 + 10),
            "mu__J": (20 - 10, 20 + 10),
            "mu__K": (100 - 10, 100 + 10),
            "mu__L": (80 - 10, 80 + 10),
            "mu__X1": (21 - 10, 21 + 10),
            "mu__Y1": (9 - 10, 9 + 10),
            "mu__X2": (21 - 10, 21 + 10),
            "mu__Y2": (9 - 10, 9 + 10),
            "mu__P": (0 - 10, 0 + 10),
            "mu__Z": (5 - 10, 5 + 10),
            "mu__input_A_pool": (120 - 10, 120 + 10),
            "mu__input_B_pool": (70 - 10, 70 + 10),
            "mu__output_J_pool": (5 - 10, 5 + 10),
            "mu__output_L_pool": (55 - 10, 55 + 10),
            "mu__output_Z_pool": (0 - 10, 0 + 10),
        },
    },
    "template": {
        "model_path": "",
        "fixed_parameters": {},
        "data": [],
        "flux_objective": "",
        "flux_constraints": [],
        "protected_reactions": [],
        "protected_metabolites": [],
        "protected_behaviour": [],
        "abstract_metabolites": [],
        "additional_input_reactions": {},
        "simplify_reactions": [],
        "perturbations": {},
        "feed_rates": {},
        "batch_amounts": {},
        "experiments": [],
        "specific_parameter_ranges": {},
    },
}

del model_settings["template"]
