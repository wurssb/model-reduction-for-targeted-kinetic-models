% Script that outlines the steps followed for time-scale separation
% analysis

% Step 1: simulate model and find sets of fast reversible reactions
for ep = 0:8
    [S_full,t,y,p] = simulate_full_model(ep);
    ep_full_out{ep+1} = [t,y];
    [flux,f_reactions,r_reactions] = calculate_fluxes_full_model(t,y,p);
    [fastest_reactions,fast_reactions,f_reactions] = find_fast_subnetworks(S_full,f_reactions,r_reactions);
    fastest_reaction_set{ep+1} = fastest_reactions;
    fast_reaction_set{ep+1} = fast_reactions;
    f_reaction_set{ep+1} = f_reactions;
    
    % We only take the set of reactions that are deemed fast under all 9
    % experimental conditions (training + test)
    if ep == 0
        intersect_fastest_reactions = fastest_reaction_set{ep+1};
        intersect_fast_reactions = fast_reaction_set{ep+1};
        intersect_f_reactions = f_reaction_set{ep+1};
    else
        intersect_fastest_reactions = intersect(intersect_fastest_reactions,fastest_reaction_set{ep+1});
        intersect_fast_reactions = intersect(intersect_fast_reactions,fast_reaction_set{ep+1});
        intersect_f_reactions = intersect(intersect_f_reactions,f_reaction_set{ep+1});
    end
end

% Step 2: We now construct new stoichiometric matrices and determine how components
% should be lumped into phi terms. We do this for the three cases d = 0.01,
% 0.05, and 0.1.

S_fastest = zeros(size(S_full));
for i = 1:length(intersect_fastest_reactions)
    S_fastest(:,intersect_fastest_reactions(i)) = S_full(:,intersect_fastest_reactions(i));
end
null_fastest = null(transpose(S_fastest));
Phi_fastest = [null_fastest(:,end),null_fastest(:,1),null_fastest(:,2:end-1)];
S_slowest = S_full-S_fastest;
S_slowestPhi = [S_slowest(1,:);0.8944*S_slowest(2,:)+0.4472*S_slowest(3,:);S_slowest(4:end,:)];

S_fast = zeros(size(S_full));
for i = 1:length(intersect_fast_reactions)
    S_fast(:,intersect_fast_reactions(i)) = S_full(:,intersect_fast_reactions(i));
end
null_fast = null(transpose(S_fast));
Phi_fast = [null_fast(:,end),null_fast(:,1),null_fast(:,3),null_fast(:,18:-1:16),null_fast(:,2),null_fast(:,4:15)];
S_slow = S_full-S_fast;
S_slowPhi = [S_slow(1,:);0.6325*S_slow(2,:)+0.3162*S_slow(3,:)+0.3162*S_slow(4,:)+0.6325*S_slow(10,:);S_slow(5:9,:);S_slow(11:end,:)];

S_f = zeros(size(S_full));
for i = 1:length(intersect_f_reactions)
    S_f(:,intersect_f_reactions(i)) = S_full(:,intersect_f_reactions(i));
end
null_f = null(transpose(S_f));
Phi_f = [null_f(:,end),null_f(:,end-1),null_f(:,1:6),null_f(:,8),null_f(:,10),null_f(:,13:-1:11),null_f(:,9),null_f(:,7)];
S_s = S_full-S_f;
S_sPhi = [S_s(1,:);
    0.6325*S_s(2,:)+0.3162*S_s(3,:)+0.3162*S_s(4,:)+0.6325*S_s(10,:);
    0.0694*S_slow(5,:)-0.1479*S_s(6,:)+0.076*S_s(7,:)+0.2999*S_s(8,:)-0.6432*S_s(9,:)-0.2239*S_s(15,:)-0.6432*S_s(19,:);
    -0.2247*S_slow(5,:)+0.7528*S_s(6,:)+0.5012*S_s(7,:)+0.2497*S_s(8,:)-0.0546*S_s(9,:)+0.2516*S_s(15,:)-0.0546*S_s(19,:);
    0.96*S_slow(5,:)+0.2151*S_s(6,:)+0.0694*S_s(7,:)-0.0764*S_s(8,:)-0.012*S_s(9,:)+0.1457*S_s(15,:)-0.012*S_s(19,:);
    -0.1523*S_slow(5,:)+0.1784*S_s(6,:)-0.2674*S_s(7,:)-0.7131*S_s(8,:)-0.2883*S_s(9,:)+0.4457*S_s(15,:)-0.2883*S_s(19,:);
    0.6961*S_s(11,:)+0.1755*S_s(12,:)+0.6961*S_s(21,:);
    0.1241*S_s(11,:)-0.9845*S_s(12,:)+0.1241*S_s(21,:);
    0.9999*S_s(13,:)-0.0112*S_s(14,:);
    0.0112*S_s(13,:)+0.9999*S_s(14,:);
    S_s(16:18,:);
    S_s(20,:);
    S_s(22,:)];

% Step 3: Using the reduced network descriptions we simulate the models.
for ep = 0:8
    [t,y,p] = simulate_fastest_reduced_model(ep,S_slowestPhi,Phi_fastest);
    ep_fastest_reduced_out{ep+1} = [t,y];
    [t,y,p] = simulate_fast_reduced_model(ep,S_slowPhi,Phi_fast);
    ep_fast_reduced_out{ep+1} = [t,y];
    [t,y,p] = simulate_f_reduced_model(ep,S_sPhi,Phi_f);
    ep_f_reduced_out{ep+1} = [t,y];
end

% Divide metabolites into observed and hidden variables.
measured_concentrations = [1,4,9,15:22];
hidden_concentrations = [2,3,5:8,10:14];
line_colors = lhsdesign(length(measured_concentrations),3);

% Step 4: We plot the output and obtain a score comparing the reduced
% models with the full system.
score_obs_training = zeros(1,3);
figure()
for i = 1:6
    subplot(2,3,i)
    for j = 1:length(measured_concentrations)
        plot(ep_full_out{i}(:,1),ep_full_out{i}(:,measured_concentrations(j)+1),'LineStyle','-','Color',line_colors(j,:))
        hold on
        plot(ep_fastest_reduced_out{i}(:,1),ep_fastest_reduced_out{i}(:,measured_concentrations(j)+1),'LineStyle','--','Color',line_colors(j,:))
        hold on
        plot(ep_fast_reduced_out{i}(:,1),ep_fast_reduced_out{i}(:,measured_concentrations(j)+1),'LineStyle','-.','Color',line_colors(j,:))
        hold on
        plot(ep_f_reduced_out{i}(:,1),ep_f_reduced_out{i}(:,measured_concentrations(j)+1),'LineStyle',':','Color',line_colors(j,:))
        hold on
        if measured_concentrations(j) < 18
            score_obs_training(1) = score_obs_training(1) + sqrt(mean((ep_fastest_reduced_out{i}(:,measured_concentrations(j)+1)-ep_full_out{i}(:,measured_concentrations(j)+1)).^2));
            score_obs_training(2) = score_obs_training(2) + sqrt(mean((ep_fast_reduced_out{i}(:,measured_concentrations(j)+1)-ep_full_out{i}(:,measured_concentrations(j)+1)).^2));
            score_obs_training(3) = score_obs_training(3) + sqrt(mean((ep_f_reduced_out{i}(:,measured_concentrations(j)+1)-ep_full_out{i}(:,measured_concentrations(j)+1)).^2));
        end
    end
    hold off
end

score_obs_validation = zeros(1,3);
figure()
for i = 1:3
    subplot(1,3,i)
    for j = 1:length(measured_concentrations)
        plot(ep_full_out{i+6}(:,1),ep_full_out{i+6}(:,measured_concentrations(j)+1),'LineStyle','-','Color',line_colors(j,:))
        hold on
        plot(ep_fastest_reduced_out{i+6}(:,1),ep_fastest_reduced_out{i+6}(:,measured_concentrations(j)+1),'LineStyle','--','Color',line_colors(j,:))
        hold on
        plot(ep_fast_reduced_out{i+6}(:,1),ep_fast_reduced_out{i+6}(:,measured_concentrations(j)+1),'LineStyle','-.','Color',line_colors(j,:))
        hold on
        plot(ep_f_reduced_out{i+6}(:,1),ep_f_reduced_out{i+6}(:,measured_concentrations(j)+1),'LineStyle',':','Color',line_colors(j,:))
        hold on
        if measured_concentrations(j) < 18
            score_obs_validation(1) = score_obs_validation(1) + sqrt(mean((ep_fastest_reduced_out{i}(:,measured_concentrations(j)+1)-ep_full_out{i}(:,measured_concentrations(j)+1)).^2));
            score_obs_validation(2) = score_obs_validation(2) + sqrt(mean((ep_fast_reduced_out{i}(:,measured_concentrations(j)+1)-ep_full_out{i}(:,measured_concentrations(j)+1)).^2));
            score_obs_validation(3) = score_obs_validation(3) + sqrt(mean((ep_f_reduced_out{i}(:,measured_concentrations(j)+1)-ep_full_out{i}(:,measured_concentrations(j)+1)).^2));
        end
    end
    hold off
end

score_hidden_training = zeros(1,3);
figure()
for i = 1:6
    subplot(2,3,i)
    for j = 1:length(hidden_concentrations)
        plot(ep_full_out{i}(:,1),ep_full_out{i}(:,hidden_concentrations(j)+1),'LineStyle','-','Color',line_colors(j,:))
        hold on
        plot(ep_fastest_reduced_out{i}(:,1),ep_fastest_reduced_out{i}(:,hidden_concentrations(j)+1),'LineStyle','--','Color',line_colors(j,:))
        hold on
        plot(ep_fast_reduced_out{i}(:,1),ep_fast_reduced_out{i}(:,hidden_concentrations(j)+1),'LineStyle','-.','Color',line_colors(j,:))
        hold on
        plot(ep_f_reduced_out{i}(:,1),ep_f_reduced_out{i}(:,hidden_concentrations(j)+1),'LineStyle',':','Color',line_colors(j,:))
        hold on
        score_hidden_training(1) = score_hidden_training(1) + sqrt(mean((ep_fastest_reduced_out{i}(:,hidden_concentrations(j)+1)-ep_full_out{i}(:,hidden_concentrations(j)+1)).^2));
        score_hidden_training(2) = score_hidden_training(2) + sqrt(mean((ep_fast_reduced_out{i}(:,hidden_concentrations(j)+1)-ep_full_out{i}(:,hidden_concentrations(j)+1)).^2));
        score_hidden_training(3) = score_hidden_training(3) + sqrt(mean((ep_f_reduced_out{i}(:,hidden_concentrations(j)+1)-ep_full_out{i}(:,hidden_concentrations(j)+1)).^2));
    end
    hold off
end

score_hidden_validation = zeros(1,3);
figure()
for i = 1:3
    subplot(1,3,i)
    for j = 1:length(hidden_concentrations)
        plot(ep_full_out{i+6}(:,1),ep_full_out{i+6}(:,hidden_concentrations(j)+1),'LineStyle','-','Color',line_colors(j,:))
        hold on
        plot(ep_fastest_reduced_out{i+6}(:,1),ep_fastest_reduced_out{i+6}(:,hidden_concentrations(j)+1),'LineStyle','--','Color',line_colors(j,:))
        hold on
        plot(ep_fast_reduced_out{i+6}(:,1),ep_fast_reduced_out{i+6}(:,hidden_concentrations(j)+1),'LineStyle','-.','Color',line_colors(j,:))
        hold on
        plot(ep_f_reduced_out{i+6}(:,1),ep_f_reduced_out{i+6}(:,hidden_concentrations(j)+1),'LineStyle',':','Color',line_colors(j,:))
        hold on
        score_hidden_validation(1) = score_hidden_validation(1) + sqrt(mean((ep_fastest_reduced_out{i}(:,hidden_concentrations(j)+1)-ep_full_out{i}(:,hidden_concentrations(j)+1)).^2));
        score_hidden_validation(2) = score_hidden_validation(2) + sqrt(mean((ep_fast_reduced_out{i}(:,hidden_concentrations(j)+1)-ep_full_out{i}(:,hidden_concentrations(j)+1)).^2));
        score_hidden_validation(3) = score_hidden_validation(3) + sqrt(mean((ep_f_reduced_out{i}(:,hidden_concentrations(j)+1)-ep_full_out{i}(:,hidden_concentrations(j)+1)).^2));
    end
    hold off
end

% Normalise scores by number of measured metabolites and experimental
% conditions to obtain a score that is per measured datapoint.
score_obs_training = score_obs_training/(6*8);
score_obs_validation = score_obs_validation/(3*8);
score_hidden_training = score_hidden_training/(6*11);
score_hidden_validation = score_hidden_validation/(3*11);
scores = [score_obs_training;score_obs_validation;score_hidden_training;score_hidden_validation];

% Write data to file.
sizes = [16, 14, 10]
scores_w_size = [sizes; scores]
rows = {'size', 'observed_train', 'observed_test', 'hidden_train', 'hidden_test'};
columns = {'m1', 'm2', 'm3'};
writetable(array2table(scores_w_size, 'VariableNames', columns, 'RowNames', rows), 'scores.csv', 'WriteRowNames', true);

figure()
bar(scores')
hold off