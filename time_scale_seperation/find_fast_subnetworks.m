function [fastest_reactions,fast_reactions,f_reactions] = find_fast_subnetworks(S,f_reactions,r_reactions)

for i = 1:length(S(:,1))
    f = find(abs(S(i,:))>0);
    num{i} = abs(sum(S(i,f).*f_reactions(:,f),2));
    denom{i} = abs(sum(S(i,f).*r_reactions(:,f),2));
    frac_fr{i} = num{i}./denom{i};
    subtract_frac{i} = abs(1-frac_fr{i});
    delta_frac(i) = subtract_frac{i}(end);
end

fastest_reactions = find(delta_frac <= 0.01);
fast_reactions = find(delta_frac <= 0.05);
f_reactions = find(delta_frac <= 0.1);

end



