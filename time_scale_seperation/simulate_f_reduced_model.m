% MATLAB code to simulate reduced toy metabolic model under the different
% experimental conditions (ep). Fastest reversible reactions have been 
% removed. 

% Removed reactions
% r2: B, C form a combined variable phi1
% r3: D considered part of phi
% r10: B, J form part of phi
% input_I: II and input_II_pool form part of phi2-5
% output_K: K and output_K_pool form part of phi6-7
% r7: F, G, and X form part of phi2-5
% r8: G, H, and X form part of phi2-5

% ep = 0: feed I + pulse A
% ep = 1: feed A + batch I
% ep = 2: batch A + pulse I
% ep = 3: pulse A + pulse I
% ep = 4: feed A + feed I + pulse A + pulse I
% ep = 5: batch A + batch I
% ep = 6: batch I + pulse A
% ep = 7: batch A + feed A
% ep = 8: batch A + batch I + pulse A + pulse I

function [t,y_new,p_new] = simulate_f_reduced_model(ep,S_slow,Phi_fast)


% Set up stoichiometric matrix
S = S_slow;

% set up base parameter values
R = 0.008314;
T = 300.0;
a_feed_rate = 0.0;
ii_feed_rate = 0.0;
km__A__r1 = 0.757922063746332;
km__B__r1 = 1.48702035804568;
km__B__r10 = 1.01704812975092;
km__B__r2 = 0.879053372881791;
km__B__r3 = 0.727864205221617;
km__C__r2 = 1.11902385221954;
km__C__r4 = 0.564318328935336;
km__D__r13 = 1.20159121976121;
km__D__r3 = 1.09187829974085;
km__D__r5 = 0.722211587854906;
km__EE__r4 = 1.12805951735263;
km__EE__r5 = 1.14224688363879;
km__EE__r6 = 0.672381581746838;
km__F__r6 = 1.38263900289396;
km__F__r7 = 0.774604724591608;
km__G__r15 = 0.785001450352912;
km__G__r7 = 0.981108076594383;
km__G__r8 = 0.698537304602286;
km__H__r14 = 1.38301188998044;
km__H__r6 = 0.885171438394547;
km__H__r8 = 0.808755900933306;
km__II__r9 = 0.874034722274701;
km__J__r10 = 0.70997782823837;
km__J__r11 = 0.65931022460374;
km__J__r9 = 1.14662624063209;
km__K__r11 = 1.60303003533329;
km__K__r15 = 1.00271848528967;
km__L__r11 = 0.830255284054964;
km__L__r12 = 0.758734671552387;
km__L__r14 = 0.52866674095723;
km__M__r12 = 1.01946968410867;
km__M__r13 = 1.07936746778462;
km__N__r12 = 1.06839874783885;
km__X__r15 = 1.31174615837827;
km__X__r6 = 0.917528758678096;
km__X__r7 = 0.920832502941564;
km__X__r8 = 1.06759007996342;
km__Y__r1 = 0.511612148523896;
km__Y__r13 = 0.57050481509746;
km__Y__r16 = 1.10982608737183;
km__Z__r1 = 1.4296998269172;
km__Z__r13 = 0.834149207822029;
km__Z__r16 = 1.01285970598895;
km_tot_rate__input_A = 5.42291352856069e-06;
km_tot_rate__input_II = 5.58297177331253e-06;
km_tot_rate__output_K = 9.59461135121314e-05;
km_tot_rate__output_N = 0.00869361497206652;
km_tot_rate__output_X = 0.00690651747723885;
mu__A = 99.8959755597017;
mu__B = 84.8843680191409;
mu__C = 39.9410997756922;
mu__D = 40.0429590181583;
mu__EE = 35.0328079341281;
mu__F = 69.8664715721184;
mu__G = 59.7059977172459;
mu__H = 44.952309041996;
mu__II = 100.060287340789;
mu__J = 94.7896625875647;
mu__K = 19.8908162927209;
mu__L = 69.6164797187518;
mu__M = 64.6106691874474;
mu__N = 4.86799940667051;
mu__X = 9.96564660676297;
mu__Y = 49.4844458043324;
mu__Z = 59.5205086613255;
mu__input_A_pool = 119.57002196535;
mu__input_II_pool = 119.6589724819;
mu__output_K_pool = 0.392423370895035;
mu__output_N_pool = 0.177390661817454;
mu__output_X_pool = 0.275933260141405;
u_v__r1 = 0.00586662029306105;
u_v__r10 = 0.0;
u_v__r11 = 0.00445279209296169;
u_v__r12 = 0.0341961601772536;
u_v__r13 = 0.00435962177240333;
u_v__r14 = 0.0;
u_v__r15 = 0.0;
u_v__r16 = 0.00168571548709808;
u_v__r2 = 0.00506831795100189;
u_v__r3 = 0.0;
u_v__r4 = 0.00642805346221623;
u_v__r5 = 0.00243824200698209;
u_v__r6 = 0.0237705475059004;
u_v__r7 = 0.0248429954399766;
u_v__r8 = 0.161516542592942;
u_v__r9 = 0.00141079198269804;

% Set up initial conditions
A = 7.99606848527122;
B = 2.35158121736075;
C = 1.74190897696561;
D = 2.63045491716244;
EE = 1.86929769653354;
F = 1.24585809715048;
G = 0.428439491883569;
H = 3.79895886860774;
II = 4.85977519537765;
J = 0.507284104909009;
K = 0.580929212842158;
L = 0.186903795791817;
M = 0.451858988378558;
N = 0.12492874215958;
X = 0.518923534368531;
Y = 0.664903946676341;
Z = 1.40941631243846;
input_A_pool = 0.001;
input_II_pool = 0.001;
output_X_pool = 0.00101295519507365;
output_K_pool = 0.00101082426079809;
output_N_pool = 0.00100626817992828;

% Place initial conditions and parameters into vectors for ODE model
% simulation
x0_1 = [A,B,C,D,EE,F,G,H,II,J,K,L,M,N,X,Y,Z,input_A_pool,input_II_pool,output_X_pool,output_K_pool,output_N_pool];
x0 = zeros(1,length(Phi_fast(1,:)));
for i = 1:length(Phi_fast(1,:))
    x0(1,i) = x0_1*Phi_fast(:,i);
end

p = [   R
        T
        a_feed_rate
        ii_feed_rate
        km__A__r1
        km__B__r1
        km__B__r10
        km__B__r2
        km__B__r3
        km__C__r2
        km__C__r4
        km__D__r13
        km__D__r3
        km__D__r5
        km__EE__r4
        km__EE__r5
        km__EE__r6
        km__F__r6
        km__F__r7
        km__G__r15
        km__G__r7
        km__G__r8
        km__H__r14
        km__H__r6
        km__H__r8
        km__II__r9
        km__J__r10
        km__J__r11
        km__J__r9
        km__K__r11
        km__K__r15
        km__L__r11
        km__L__r12
        km__L__r14
        km__M__r12
        km__M__r13
        km__N__r12
        km__X__r15
        km__X__r6
        km__X__r7
        km__X__r8
        km__Y__r1
        km__Y__r13
        km__Y__r16
        km__Z__r1
        km__Z__r13
        km__Z__r16
        km_tot_rate__input_A
        km_tot_rate__input_II
        km_tot_rate__output_K
        km_tot_rate__output_N
        km_tot_rate__output_X
        mu__A
        mu__B
        mu__C
        mu__D
        mu__EE
        mu__F
        mu__G
        mu__H
        mu__II
        mu__J
        mu__K
        mu__L
        mu__M
        mu__N
        mu__X
        mu__Y
        mu__Z
        mu__input_A_pool
        mu__input_II_pool
        mu__output_K_pool
        mu__output_N_pool
        mu__output_X_pool
        u_v__r1
        u_v__r10
        u_v__r11
        u_v__r12
        u_v__r13
        u_v__r14
        u_v__r15
        u_v__r16
        u_v__r2
        u_v__r3
        u_v__r4
        u_v__r5
        u_v__r6
        u_v__r7
        u_v__r8
        u_v__r9
    ];

% Set up ODE tolerances
options = odeset('RelTol',1e-9,'AbsTol',1e-12);

% Simulate the ODE model as per input experimental conditions ep
if ep == 0
    p_new = p;
    p_new(4) = 0.05; % constant feed rate of I
    
    time = 0:0.1:75;
    [t0,y0] = ode15s(@full_model,time,x0,options,S,p_new);
    
    % set up pulse of A
    x0_new = y0(end,:);
    x0_new(13) = x0_new(13) + 5;
    time = 0:0.1:75;
    [t1,y1] = ode15s(@full_model,time,x0_new,options,S,p_new);
    
    t = [t0(1:end-1);t1+75];
    y = [y0(1:end-1,:);y1];
elseif ep == 1
    p_new = p;
    p_new(3) = 0.05; % constant feed of A
    x0_new = x0;
    
    % now need to change multiple ICs
    x0_new(6) = 6.432+x0(6); % batch feed of I
    x0_new(5) = 0.546+x0(5); % batch feed of I
    x0_new(4) = 0.12+x0(4); % batch feed of I
    x0_new(3) = 2.883+x0(3); % batch feed of I
    
    time = 0:0.1:150;
    [t,y] = ode15s(@full_model,time,x0_new,options,S,p_new);
elseif ep == 2
    p_new = p;
    x0_new = x0;
    x0_new(13) = 10; % batch feed of A
    
    time = 0:0.1:75;
    [t0,y0] = ode15s(@full_model,time,x0_new,options,S,p_new);
    
    % set up pulse of I
    x0_new = y0(end,:);
    
    % now need to change multiple ICs
    x0_new(6) = 6.432/2+x0(6); % pulse of I
    x0_new(5) = 0.546/2+x0(5); % pulse of I
    x0_new(4) = 0.12/2+x0(4); % pulse of I
    x0_new(3) = 2.883/2+x0(3); % pulse of I
    
    time = 0:0.1:75;
    [t1,y1] = ode15s(@full_model,time,x0_new,options,S,p_new);
    
    t = [t0(1:end-1);t1+75];
    y = [y0(1:end-1,:);y1];
elseif ep == 3
    p_new = p;
    
    time = 0:0.1:75;
    [t0,y0] = ode15s(@full_model,time,x0,options,S,p_new);
    
    x0_new = y0(end,:);
    x0_new(13) = x0_new(13) + 5; % set up pulse of A
    % now need to change multiple ICs
    x0_new(6) = 6.432/2+x0(6); % pulse of I
    x0_new(5) = 0.546/2+x0(5); % pulse of I
    x0_new(4) = 0.12/2+x0(4); % pulse of I
    x0_new(3) = 2.883/2+x0(3); % pulse of I
    time = 0:0.1:75;
    [t1,y1] = ode15s(@full_model,time,x0_new,options,S,p_new);
    
    t = [t0(1:end-1);t1+75];
    y = [y0(1:end-1,:);y1];
elseif ep == 4
    p_new = p;
    p_new(3) = 0.05; % constant feed of A
    p_new(4) = 0.05; % constant feed of I
    
    time = 0:0.1:75;
    [t0,y0] = ode15s(@full_model,time,x0,options,S,p_new);
    
    x0_new = y0(end,:);
    x0_new(13) = x0_new(13) + 5; % set up pulse of A
    % now need to change multiple ICs
    x0_new(6) = 6.432/2+x0(6); % pulse of I
    x0_new(5) = 0.546/2+x0(5); % pulse of I
    x0_new(4) = 0.12/2+x0(4); % pulse of I
    x0_new(3) = 2.883/2+x0(3); % pulse of I
    [t1,y1] = ode15s(@full_model,time,x0_new,options,S,p_new);
    
    t = [t0(1:end-1);t1+75];
    y = [y0(1:end-1,:);y1];
elseif ep == 5
    p_new = p;
    x0_new = x0;
    x0_new(13) = 10; % batch feed of A
    % now need to change multiple ICs
    x0_new(6) = 6.432+x0(6); % pulse of I
    x0_new(5) = 0.546+x0(5); % pulse of I
    x0_new(4) = 0.12+x0(4); % pulse of I
    x0_new(3) = 2.883+x0(3); % pulse of I
    
    time = 0:0.1:150;
    [t,y] = ode15s(@full_model,time,x0_new,options,S,p_new);
elseif ep == 6
    p_new = p;
    x0_new = x0;
    % now need to change multiple ICs
    x0_new(6) = 6.432+x0(6); % batch feed of I
    x0_new(5) = 0.546+x0(5); % batch feed of I
    x0_new(4) = 0.12+x0(4); % batch feed of I
    x0_new(3) = 2.883+x0(3); % batch feed of I
    
    time = 0:0.1:75;
    [t0,y0] = ode15s(@full_model,time,x0_new,options,S,p_new);
    
    % set up pulse of A
    x0_new = y0(end,:);
    x0_new(13) = x0_new(13) + 5;
    time = 0:0.1:75;
    [t1,y1] = ode15s(@full_model,time,x0_new,options,S,p_new);
    
    t = [t0(1:end-1);t1+75];
    y = [y0(1:end-1,:);y1];
elseif ep == 7
    p_new = p;
    p_new(3) = 0.05; % constant feed of A
    x0_new = x0;
    x0_new(13) = 10; % batch feed of A
    
    time = 0:0.1:150;
    [t,y] = ode15s(@full_model,time,x0_new,options,S,p_new);
elseif ep == 8
    p_new = p;
    x0_new = x0;
    x0_new(13) = 10; % batch feed of A
    % now need to change multiple ICs
    x0_new(6) = 6.432+x0(6); % batch feed of I
    x0_new(5) = 0.546+x0(5); % batch feed of I
    x0_new(4) = 0.12+x0(4); % batch feed of I
    x0_new(3) = 2.883+x0(3); % batch feed of I
    
    time = 0:0.1:75;
    [t0,y0] = ode15s(@full_model,time,x0_new,options,S,p_new);
    
    x0_new = y0(end,:);
    x0_new(13) = x0_new(13) + 5; % set up pulse of A
    % now need to change multiple ICs
    x0_new(6) = 6.432/2+x0(6); % pulse of I
    x0_new(5) = 0.546/2+x0(5); % pulse of I
    x0_new(4) = 0.12/2+x0(4); % pulse of I
    x0_new(3) = 2.883/2+x0(3); % pulse of I
    time = 0:0.1:75;
    [t1,y1] = ode15s(@full_model,time,x0_new,options,S,p_new);
    
    t = [t0(1:end-1);t1+75];
    y = [y0(1:end-1,:);y1];
end

phi1 = y(:,2);
gamma1 = (-0.5 * mu__B + 1.0 * mu__C) / (R * T);
gamma2 = (-0.5 * mu__B + 1.0 * mu__D) / (R * T);
gamma3 = (0.5 * mu__B - 0.5 * mu__J) / (R * T);
C = (-0.3162*(1+exp(gamma1-gamma2))+sqrt((0.3162*(1+exp(gamma1-gamma2)))^2+4*0.6325*phi1*(exp(2*gamma1)+exp(2*(gamma1+gamma3)))))./(2*0.6325*(exp(2*gamma1)+exp(2*(gamma1+gamma3))));
B = (C.^2).*exp(2*gamma1);
J = B.*exp(2*gamma3);
D = C.*exp(gamma1-gamma2);

phi2 = y(:,3);
phi3 = y(:,4);
phi4 = y(:,5);
phi5 = y(:,6);
gamma4 = (-0.5 * mu__F + 0.5 * mu__G + 0.5 * mu__X) / (R * T);
gamma5 = (-0.5 * mu__G + 0.5 * mu__H + 0.5 * mu__X) / (R * T);
gamma6 = (0.5 * mu__II - 0.5 * mu__input_II_pool) / (R * T);
rho1 = phi5-11.7802*phi4 - (2.952/0.9663)*(phi3-0.0416*phi2);
rho2 = phi5-53.6*phi3 - (51.386/0.2159)*(phi4-0.0546*phi2/0.2883);
for j = 1:length(phi2)
    Xs = roots([-653.7669*exp(2*(gamma4+gamma5))+461.512*exp(2*(gamma4+gamma5)),-482.3099*exp(2*gamma5)-182.8105*rho1(j)*exp(2*(gamma4+gamma5))+9.6505*rho2(j)*exp(2*(gamma4+gamma5))+290.4697*exp(2*gamma5),-134.8666*rho1(j)*exp(2*gamma5)-311.7735+6.0739*rho2(j)*exp(2*gamma5)+119.5139,-87.1801*rho1(j)+2.4989*rho2(j)]);
    for i = 1:3
        if abs(imag(Xs(i)))<1E-7 %&& real(Xs)>=0
            X(j,1) = Xs(i);
        end
    end
end
H = (rho2+47.8266*X)./(-87.1801-134.8666*X*exp(2*gamma5)-182.8105*(X.^2)*exp(2*(gamma4+gamma5)));
G = H.*X*exp(2*gamma5);
F = G.*X*exp(2*gamma4);
E = (phi3-(0.012/0.2883)*phi2+(-0.1457+0.012*0.4457/0.2883)*X+(0.0764-0.012*0.7131/0.2883)*H+(-0.0694-0.012*0.2647/0.2883)*G+(-0.2151+0.012*0.1784/0.2883)*F)/(0.96+0.012*0.1523/0.2883);
II = (1/(-0.6432-0.6432*exp(-2*gamma6))).*(phi5+0.2239*X-0.2999*H-0.076*G+0.1479*F-0.0694*E);
input_II_pool = II*exp(2*gamma6);

phi6 = y(:,7);
phi7 = y(:,8);
gamma7 = (-0.5 * mu__K + 0.5 * mu__output_K_pool) / (R * T);
K = (phi7+0.1755*phi6/0.9845)./((1+exp(-2*gamma7))*(0.6961+0.1755*0.1241/0.9845));
output_K_pool = K.*exp(-2*gamma7);
L = (phi6-0.1241*(K+output_K_pool))/(-0.9845);

phi8 = y(:,9);
phi9 = y(:,10);
M = (phi8+0.9999*phi9/0.0112)./(0.0112+0.9999^2/0.0112);
N = (0.9999*M-phi9)/0.0112;

y_new = [y(:,1),B,C,D,E,F,G,H,II,J,K,L,M,N,X,y(:,11:13),input_II_pool,y(:,14),output_K_pool,y(:,15)];

end

% Function for ODE model. Inputs are time (t), concentrations (x),
% stoichiometric matrix (S) and parameter vector (p)
function dydt = full_model(t, x, S, p)
    
    % Unpack concentration and parameter vectors
    A = x(1);
    phi1 = x(2);
    phi2 = x(3);
    phi3 = x(4);
    phi4 = x(5);
    phi5 = x(6);
    phi6 = x(7);
    phi7 = x(8);
    phi8 = x(9);
    phi9 = x(10);
    Y = x(11);
    Z = x(12);
    input_A_pool = x(13);
    output_X_pool = x(14);
    output_N_pool = x(15);
    
    R = p(1);
    T = p(2);
    a_feed_rate = p(3);
    ii_feed_rate = p(4);
    km__A__r1 = p(5);
    km__B__r1 = p(6);
    km__B__r10 = p(7);
    km__B__r2 = p(8);
    km__B__r3 = p(9);
    km__C__r2 = p(10);
    km__C__r4 = p(11);
    km__D__r13 = p(12);
    km__D__r3 = p(13);
    km__D__r5 = p(14);
    km__EE__r4 = p(15);
    km__EE__r5 = p(16);
    km__EE__r6 = p(17);
    km__F__r6 = p(18);
    km__F__r7 = p(19);
    km__G__r15 = p(20);
    km__G__r7 = p(21);
    km__G__r8 = p(22);
    km__H__r14 = p(23);
    km__H__r6 = p(24);
    km__H__r8 = p(25);
    km__II__r9 = p(26);
    km__J__r10 = p(27);
    km__J__r11 = p(28);
    km__J__r9 = p(29);
    km__K__r11 = p(30);
    km__K__r15 = p(31);
    km__L__r11 = p(32);
    km__L__r12 = p(33);
    km__L__r14 = p(34);
    km__M__r12 = p(35);
    km__M__r13 = p(36);
    km__N__r12 = p(37);
    km__X__r15 = p(38);
    km__X__r6 = p(39);
    km__X__r7 = p(40);
    km__X__r8 = p(41);
    km__Y__r1 = p(42);
    km__Y__r13 = p(43);
    km__Y__r16 = p(44);
    km__Z__r1 = p(45);
    km__Z__r13 = p(46);
    km__Z__r16 = p(47);
    km_tot_rate__input_A = p(48);
    km_tot_rate__input_II = p(49);
    km_tot_rate__output_K = p(50);
    km_tot_rate__output_N = p(51);
    km_tot_rate__output_X = p(52);
    mu__A = p(53);
    mu__B = p(54);
    mu__C = p(55);
    mu__D = p(56);
    mu__EE = p(57);
    mu__F = p(58);
    mu__G = p(59);
    mu__H = p(60);
    mu__II = p(61);
    mu__J = p(62);
    mu__K = p(63);
    mu__L = p(64);
    mu__M = p(65);
    mu__N = p(66);
    mu__X = p(67);
    mu__Y = p(68);
    mu__Z = p(69);
    mu__input_A_pool = p(70);
    mu__input_II_pool = p(71);
    mu__output_K_pool = p(72);
    mu__output_N_pool = p(73);
    mu__output_X_pool = p(74);
    u_v__r1 = p(75);
    u_v__r10 = p(76);
    u_v__r11 = p(77);
    u_v__r12 = p(78);
    u_v__r13 = p(79);
    u_v__r14 = p(80);
    u_v__r15 = p(81);
    u_v__r16 = p(82);
    u_v__r2 = p(83);
    u_v__r3 = p(84);
    u_v__r4 = p(85);
    u_v__r5 = p(86);
    u_v__r6 = p(87);
    u_v__r7 = p(88);
    u_v__r8 = p(89);
    u_v__r9 = p(90);
    
    gamma1 = (-0.5 * mu__B + 1.0 * mu__C) / (R * T);
    gamma2 = (-0.5 * mu__B + 1.0 * mu__D) / (R * T);
    gamma3 = (0.5 * mu__B - 0.5 * mu__J) / (R * T);
    
    C = (-0.3162*(1+exp(gamma1-gamma2))+sqrt((0.3162*(1+exp(gamma1-gamma2)))^2+4*0.6325*phi1*(exp(2*gamma1)+exp(2*(gamma1+gamma3)))))./(2*0.6325*(exp(2*gamma1)+exp(2*(gamma1+gamma3))));
    B = (C.^2).*exp(2*gamma1);
    J = B.*exp(2*gamma3);
    D = C.*exp(gamma1-gamma2);
    
    gamma4 = (-0.5 * mu__F + 0.5 * mu__G + 0.5 * mu__X) / (R * T);
    gamma5 = (-0.5 * mu__G + 0.5 * mu__H + 0.5 * mu__X) / (R * T);
    gamma6 = (0.5 * mu__II - 0.5 * mu__input_II_pool) / (R * T);
    rho1 = phi5-11.7802*phi4 - (2.952/0.9663)*(phi3-0.0416*phi2);
    rho2 = phi5-53.6*phi3 - (51.386/0.2159)*(phi4-0.0546*phi2/0.2883);
    Xs = roots([-653.7669*exp(2*(gamma4+gamma5))+461.512*exp(2*(gamma4+gamma5)),-482.3099*exp(2*gamma5)-182.8105*rho1*exp(2*(gamma4+gamma5))+9.6505*rho2*exp(2*(gamma4+gamma5))+290.4697*exp(2*gamma5),-134.8666*rho1*exp(2*gamma5)-311.7735+6.0739*rho2*exp(2*gamma5)+119.5139,-87.1801*rho1+2.4989*rho2]);
    for i = 1:3
        if abs(imag(Xs(i)))<1E-7 %&& real(Xs)>=0
            X = Xs(i);
        end
    end
    H = (rho2+47.8266*X)./(-87.1801-134.8666*X*exp(2*gamma5)-182.8105*(X.^2)*exp(2*(gamma4+gamma5)));
    G = H.*X*exp(2*gamma5);
    F = G.*X*exp(2*gamma4);
    EE = (phi3-(0.012/0.2883)*phi2+(-0.1457+0.012*0.4457/0.2883)*X+(0.0764-0.012*0.7131/0.2883)*H+(-0.0694-0.012*0.2647/0.2883)*G+(-0.2151+0.012*0.1784/0.2883)*F)/(0.96+0.012*0.1523/0.2883);
    II = (1/(-0.6432-0.6432*exp(-2*gamma6))).*(phi5+0.2239*X-0.2999*H-0.076*G+0.1479*F-0.0694*EE);
    input_II_pool = II*exp(2*gamma6);
    
    gamma7 = (-0.5 * mu__K + 0.5 * mu__output_K_pool) / (R * T);
    K = (phi7+0.1755*phi6/0.9845)./((1+exp(-2*gamma7))*(0.6961+0.1755*0.1241/0.9845));
    output_K_pool = K.*exp(-2*gamma7);
    L = (phi6-0.1241*(K+output_K_pool))/(-0.9845);
    
    M = (phi8+0.9999*phi9/0.0112)./(0.0112+0.9999^2/0.0112);
    N = (0.9999*M-phi9)/0.0112;
    
    % Calculate flux functions for (reversible) reactions in model
    input_A = km_tot_rate__input_A * (...
        -A * exp((0.5 * mu__A - 0.5 * mu__input_A_pool) / (R * T))...
        + input_A_pool * exp(-(0.5 * mu__A - 0.5 * mu__input_A_pool) / (R * T)));
    input_II = 0;
    output_K = 0;
    output_N = km_tot_rate__output_N * (...
        N * exp(-(-0.5 * mu__N + 0.5 * mu__output_N_pool) / (R * T))...
        - output_N_pool * exp((-0.5 * mu__N + 0.5 * mu__output_N_pool) / (R * T))...
    );
    output_X = km_tot_rate__output_X * (...
        X * exp(-(-0.5 * mu__X + 0.5 * mu__output_X_pool) / (R * T))...
        - output_X_pool * exp((-0.5 * mu__X + 0.5 * mu__output_X_pool) / (R * T))...
    );
    r1 = (...
        u_v__r1...
        * (...
            A...
            * Y...
            * exp(-(-0.5 * mu__A + 0.5 * mu__B - 0.5 * mu__Y + 0.5 * mu__Z) / (R * T))...
            - B...
            * Z...
            * exp((-0.5 * mu__A + 0.5 * mu__B - 0.5 * mu__Y + 0.5 * mu__Z) / (R * T))...
        )...
        / (...
            sqrt(km__A__r1 * km__B__r1 * km__Y__r1 * km__Z__r1)...
            * (...
                (A / km__A__r1 + 1.0) * (Y / km__Y__r1 + 1.0)...
                + (B / km__B__r1 + 1.0) * (Z / km__Z__r1 + 1.0)...
                - 1.0...
            )...
        )...
    );
    r2 = 0;
    r3 = 0;
    r4 = (...
        u_v__r4...
        * (...
            C * exp(-(-0.5 * mu__C + 0.5 * mu__EE) / (R * T))...
            - EE * exp((-0.5 * mu__C + 0.5 * mu__EE) / (R * T))...
        )...
        / (sqrt(km__C__r4 * km__EE__r4) * (C / km__C__r4 + EE / km__EE__r4 + 1.0))...
    );
    r5 = (...
        u_v__r5...
        * (...
            D * exp(-(-0.5 * mu__D + 0.5 * mu__EE) / (R * T))...
            - EE * exp((-0.5 * mu__D + 0.5 * mu__EE) / (R * T))...
        )...
        / (sqrt(km__D__r5 * km__EE__r5) * (D / km__D__r5 + EE / km__EE__r5 + 1.0))...
    );
    r6 = (...
        u_v__r6...
        * (...
            EE...
            * H...
            * exp(-(-0.5 * mu__EE + 0.5 * mu__F - 0.5 * mu__H + 0.5 * mu__X) / (R * T))...
            - F...
            * X...
            * exp((-0.5 * mu__EE + 0.5 * mu__F - 0.5 * mu__H + 0.5 * mu__X) / (R * T))...
        )...
        / (...
            sqrt(km__EE__r6 * km__F__r6 * km__H__r6 * km__X__r6)...
            * (...
                (EE / km__EE__r6 + 1.0) * (H / km__H__r6 + 1.0)...
                + (F / km__F__r6 + 1.0) * (X / km__X__r6 + 1.0)...
                - 1.0...
            )...
        )...
    );
    r7 = 0;
    r8 = 0;
    r9 = (...
        u_v__r9...
        * (...
            II * exp(-(-0.5 * mu__II + 0.5 * mu__J) / (R * T))...
            - J * exp((-0.5 * mu__II + 0.5 * mu__J) / (R * T))...
        )...
        / (sqrt(km__II__r9 * km__J__r9) * (II / km__II__r9 + J / km__J__r9 + 1.0))...
    );
    r10 = 0;
    r11 = (...
        u_v__r11...
        * (...
            J * exp(-(-0.5 * mu__J + 0.5 * mu__K + 0.5 * mu__L) / (R * T))...
            - K * L * exp((-0.5 * mu__J + 0.5 * mu__K + 0.5 * mu__L) / (R * T))...
        )...
        / (...
            sqrt(km__J__r11 * km__K__r11 * km__L__r11)...
            * (J / km__J__r11 + (K / km__K__r11 + 1.0) * (L / km__L__r11 + 1.0))...
        )...
    );
    r12 = (...
        u_v__r12...
        * (...
            L * exp(-(-0.5 * mu__L + 0.5 * mu__M + 0.5 * mu__N) / (R * T))...
            - M * N * exp((-0.5 * mu__L + 0.5 * mu__M + 0.5 * mu__N) / (R * T))...
        )...
        / (...
            sqrt(km__L__r12 * km__M__r12 * km__N__r12)...
            * (L / km__L__r12 + (M / km__M__r12 + 1.0) * (N / km__N__r12 + 1.0))...
        )...
    );
    r13 = (...
        u_v__r13...
        * (...
            -D...
            * Z...
            * exp((0.5 * mu__D - 0.5 * mu__M - 0.5 * mu__Y + 0.5 * mu__Z) / (R * T))...
            + M...
            * Y...
            * exp(-(0.5 * mu__D - 0.5 * mu__M - 0.5 * mu__Y + 0.5 * mu__Z) / (R * T))...
        )...
        / (...
            sqrt(km__D__r13 * km__M__r13 * km__Y__r13 * km__Z__r13)...
            * (...
                (D / km__D__r13 + 1.0) * (Z / km__Z__r13 + 1.0)...
                + (M / km__M__r13 + 1.0) * (Y / km__Y__r13 + 1.0)...
                - 1.0...
            )...
        )...
    );
    r14 = (...
        u_v__r14...
        * (...
            -H * exp((0.5 * mu__H - 0.5 * mu__L) / (R * T))...
            + L * exp(-(0.5 * mu__H - 0.5 * mu__L) / (R * T))...
        )...
        / (sqrt(km__H__r14 * km__L__r14) * (H / km__H__r14 + L / km__L__r14 + 1.0))...
    );
    r15 = (...
        u_v__r15...
        * (...
            G * exp(-(-0.5 * mu__G + 1.0 * mu__K + 0.5 * mu__X) / (R * T))...
            - K ^ 2.0 * X * exp((-0.5 * mu__G + 1.0 * mu__K + 0.5 * mu__X) / (R * T))...
        )...
        / (...
            sqrt(km__G__r15 * km__K__r15 ^ 2.0 * km__X__r15)...
            * (G / km__G__r15 + (K / km__K__r15 + 1.0) ^ 2.0 * (X / km__X__r15 + 1.0))...
        )...
    );
    r16 = (...
        u_v__r16...
        * (...
            -Y * exp((0.5 * mu__Y - 0.5 * mu__Z) / (R * T))...
            + Z * exp(-(0.5 * mu__Y - 0.5 * mu__Z) / (R * T))...
        )...
        / (sqrt(km__Y__r16 * km__Z__r16) * (Y / km__Y__r16 + Z / km__Z__r16 + 1.0))...
    );
    feed_input_A = a_feed_rate;
    feed_input_II = ii_feed_rate;

    % Place fluxes within a vector structure
    flux = [input_A;
            input_II;
            output_K;
            output_N;
            output_X;
            r1;
            r2;
            r3;
            r4;
            r5;
            r6;
            r7;
            r8;
            r9;
            r10;
            r11;
            r12;
            r13;
            r14;
            r15;
            r16;
            feed_input_A;
            feed_input_II;
        ];

    % Calculate RHS of ODE function through matrix multiplication
    dydt = S*flux;
    
end