# Test model time scale separation.

Time scale separation applied on the test model using the method as described by [1,2]. The parametrisation of the model is the same as the reference model used in the main text. See `get_fast_reactions_from_ep.m` for the code & simulations, and the appendix for the detailed process.

# Author:
Rob Smith

# References:
1. Gerdtzen, Z. P., Daoutidis, P. and Hu, W. (2004). Non-Linear Reduction for Kinetic Models of Metabolic Reaction Networks. Metabolic engineering, 6 (2), 140-154. [10.1016/j.ymben.2003.11.003](https://doi.org/10.1016/j.ymben.2003.11.003)
2. Gupta, U. and Heo, S. and Bhan, A. and Daoutidis, P. (2016). Time Scale Decomposition in Complex Reaction Systems: A Graph Theoretic Analysis. Computers & Chemical Engineering. 95, 170-181. [10.1016/j.compchemeng.2016.09.011](https://doi.org/10.1016/j.compchemeng.2016.09.011)
