# Model Reduction for targeted kinetic models.


Repository for the publication "Model reduction of genome-scale metabolic models as a basis for targeted kinetic models". ([10.1016/j.ymben.2021.01.008](https://doi.org/10.1016/j.ymben.2021.01.008)) [1]

An example of the complete pipeline can be found in `reduction_test_cases/examples.py`, which contains the script to run the two scenarios tested in the paper. Scripts to reproduce the figures as shown in the paper are given in `plot_optimization.py` and `plot_samples.py`. Output (including all intermediate results) for the two test scenarios can be found in the `results` folder, excluding the large data files for the optimized simulation trajectories for the test model scenario and the parameter sensitivity samples for the *E. coli* core scenario.

The pipeline automatically caches all steps and their input, arguments and output. Steps will only be rerun when input files or arguments change or when the output files have been modified externally. To re-run a step in the pipeline, simply change the arguments or delete the output folder.

Note: Some of the steps dealing with the creation of the kinetic models create a (Cython) extension module for simulating the model. However, if a module was already previously loaded with the same name, this can lead to a `Parameter set mismatch` error when trying to use the module to simulate the kinetic model as an older model is erroneously used. Reloading your python session should fix this, or name your models uniquely to avoid the issue.

# General overview:

- `examples.py` - Annotated example of the optimization based process for the test model, and the ensemble based process for the *E. coli* core model.

- `results/optimization/` - Results and data files resulting from the test model example. Some large simulation result files are omitted due to size (>1gb), as well as the data for the additional testing models.

- `plot_optimization.py` - Code to reproduce figures 4 & 5 of the main text, figures 1 and 2 of the supplementary and table 3 and 4 of the supplementary.

- `results/sampling/` - All intermediate results and data files resulting from the test model example. Some large simulation result files are ommitted due to size (>1gb).

- `plot_samples.py` - Code to reproduce figure 6 of the main text.

- `data/` - The base models and reference values for the concentrations and chemical potentials for the *E. coli* test case.

- `pipeline/` - Python code of the automated pipeline. Pipelines for the two examples can be found in `pipeline/pipeline.py`, with each individual step in `pipeline/*_steps.py`. Steps are reproducible units of work with a fixed set of input and output files and arguments. When running the example, the pipeline runs the steps in the correct order and links the outputs and inputs of subsequent steps. Each step creates a folder in the results folder with its own output and the arguments used (in a `json` file). (Intermediate) results are saved in standard formats: `SBML` for models, `json` for settings and small data files, `csv` for tabular data and compressed `hdf5` for large or multidimensional results.

- `pipeline/matlab/` - Matlab scripts to interface with individual tools that are called during the pipeline.

- `time_scale_seperation/` - Results of model reduction using time scale separation for the toy model.

# Requirements:

-  [Matlab](https://mathworks.com/products/matlab.html) (License required) - Used version: `2017b` with:

    -  [Symbolic tool box](https://mathworks.com/products/symbolic.html) (License required)

    - Configured [C++ compiler](https://mathworks.com/help/matlab/matlab_external/choose-c-or-c-compilers.html)

    -  [Amici](https://github.com/ICB-DCM/AMICI) [2] (Does not work with Matlab 2018+. Used commit: `4da5dd99`, with `amici.patch` applied.)

    -  [Pesto](https://github.com/ICB-DCM/PESTO) [3] (Used commit: `8278a1a`, with `pesto.patch` applied.)

-  [Python](https://python.org) - Used version: `3.6` with:

    -  [MatlabEngine](https://mathworks.com/help/matlab/matlab-engine-for-python.html) - Used version: `2017b`

    -  [Numpy](https://www.numpy.org) - Used version: `1.18.5`

    -  [Scipy](https://www.scipy.org) - Used version: `1.5.0`

    -  [Cython](https://cython.org) - Used version: `0.29.20`

    -  [Sympy](https://www.sympy.org) - Used version: `1.6`

    -  [Pandas](https://pandas.pydata.org) - Used version: `1.0.5`. Including the optional dependency [PyTables](https://www.pytables.org) (Used version `3.6.1`)

    -  [h5py](https://www.h5py.org) - Used version: `2.10.0`

    -  [lxml](https://lxml.de) - Used version: `4.5.1`

    -  [libsbml](http://sbml.org/Software/libSBML) - Used version: `5.18.0`

    -  [COBRApy](https://github.com/opencobra/cobrapy) - Used version: `0.18.1`

    -  [pyDOE2](https://github.com/clicumu/pydoe2) - Used version `1.3.0`

    -  [gurobipy](https://www.gurobi.com) - Academic licenses are available (Used version `9.0.2`)

    -  [SymbolicSBML library](https://gitlab.com/wurssb/Modelling/symbolicsbml) - Used commit: `1f7ede5d`

    -  [Parameter Balancer library](https://gitlab.com/wurssb/Modelling/parameter-balancer) - Used commit: `006e3d39`

- For recreation of the figures [matplotlib](https://matplotlib.org) (Used version: `3.2.2`) and [seaborn](https://seaborn.pydata.org) (Used version: `0.10.0`) are required.

- Depending on which reduction algorithm you're interested in, one or more of the following (See `model-reduction-methods/README.txt` for details):

    - NetworkReducer [4] ([CellNetAnalyzer](http://www2.mpi-magdeburg.mpg.de/projects/cna/cna.html) contains the actual reduction functionality, and is included in the bundle.)

    - FastCore [5]

    - MinNW [6]

- These in turn have the following dependencies:

    -  [CobraToolbox](https://github.com/opencobra/cobratoolbox/) - Used commit: `f3fe20d`

    -  [Cplex for Matlab](https://www.ibm.com/analytics/cplex-optimizer) - Academic licenses are available (Used version - `12.10.0.0`).

  

# Detailed Installation Instructions:

Note: Installation instructions were verified on Linux only (Ubuntu). Details may be different for mac or windows users.

0. Clone or download this repository.

1. Set up your Matlab (2017b) installation with the symbolic toolbox (and optionally the parallel computation toolbox).

2. Set up [Cplex](https://www.ibm.com/analytics/cplex-optimizer) and [CobraToolbox](https://github.com/opencobra/cobratoolbox/). Note: It is not required to manually update your Matlab path when Matlab functionality is only used through the pipeline interface.

3.  [Set up your C++ compiler](https://mathworks.com/help/matlab/matlab_external/choose-c-or-c-compilers.html)

4. Set up your python installation (3.4+). The easiest is to use [conda](https://conda.io):

- Download [miniconda](https://conda.io/miniconda.html) and install according to the instructions.

- Create a new environment (This will make sure the installation does not impact your system python. You'll have to make sure to activate the environment when you run the pipeline):

```

conda create --name pipeline python=3.6

```

- Activate the environment:

```

conda activate pipeline

```

- Install the relevant packages:

```

conda install pandas numpy sympy scipy h5py pytables cython lxml matplotlib seaborn

conda install -c SBMLTeam python-libsbml

conda install -c gurobi gurobi

pip install cobra

pip install git+https://gitlab.com/wurssb/Modelling/symbolicsbml.git#egg=symbolicSBML

pip install git+https://gitlab.com/wurssb/Modelling/parameter-balancer#egg=parameter-balancer

```

  

- Install [MatlabEngine](https://mathworks.com/help/matlab/matlab-engine-for-python.html)

  

```

cd {matlab_root}/extern/engines/python

python setup.py install

```

Warning: Never use ```sudo python setup.py install``` as that will install the Matlab engine for the system python version instead of the local environment.

On Linux, Matlab might have been installed using sudo, which makes you unable to install the Matlab engine.

If you get a permission error, the easiest way to fix it is to make Matlab write/readable for the local user:

```

sudo chmod -R 777 {matlab_root}

```

If you do not have local admin rights, have a look at this [Matlab engine documentation page](https://mathworks.com/help/matlab/matlab_external/install-matlab-engine-api-for-python-in-nondefault-locations.html).

5. Clone [Pesto](https://github.com/ICB-DCM/PESTO), [Amici](https://github.com/ICB-DCM/AMICI) and this repository:

  

```

git clone https://github.com/ICB-DCM/AMICI

git clone https://github.com/ICB-DCM/PESTO

```

  

We also need to make sure that the versions of AMICI and PESTO are at the right commit with the correct patch applied.

```

cd {amici_dir}

git checkout 4da5dd993b2c0e14bcebc3445201f3c178510b98

git apply {reduction-test-pipeline_dir}/amici.patch

  

cd {pesto_dir}

git checkout 8278a1a74839e5ca87fe3220efab4cd42385ad05

git apply {reduction-test-pipeline_dir}/pesto.patch

```

  

6. Make sure to update all file paths to dependencies in ```config.json```.

  

# Authors:

- Rik van Rosmalen

  

# License:

This project is licensed under the MIT License - see the LICENSE file for details.

# References:

1. van Rosmalen, R. P., Smith, R. W., Martins dos Santos, V. A. P., Fleck, C., Suarez-Diez, M. (2021). Model Reduction of Genome-Scale Metabolic Models as a Basis for Targeted Kinetic Models. Metabolic Engineering, 64, 74-84. https://doi.org/10.1016/j.ymben.2021.01.008

2. Fröhlich, F., Kaltenbacher, B., Theis, F. J., & Hasenauer, J. (2017). Scalable Parameter Estimation for Genome-Scale Biochemical Reaction Networks. PLOS Computational Biology, 13(1), e1005331. https://doi.org/10.1371/journal.pcbi.1005331

3. Stapor, P., Weindl, D., Ballnus, B., Hug, S., Loos, C., Fiedler, A., … Hasenauer, J. (2017). PESTO: Parameter EStimation TOolbox. Bioinformatics, 2–4. https://doi.org/10.1093/bioinformatics/btx676

4. Erdrich, P., Steuer, R., & Klamt, S. (2015). An algorithm for the reduction of genome-scale metabolic network models to meaningful core models. BMC Systems Biology, 9(1), 48. https://doi.org/10.1186/s12918-015-0191-x

5. Vlassis, N., Pacheco, M. P., & Sauter, T. (2014). Fast Reconstruction of Compact Context-Specific Metabolic Network Models. PLoS Computational Biology, 10(1), e1003424. https://doi.org/10.1371/journal.pcbi.1003424

6. Röhl, A., & Bockmayr, A. (2017). A mixed-integer linear programming approach to the reduction of genome-scale metabolic networks. BMC Bioinformatics, 18(1), 2. https://doi.org/10.1186/s12859-016-1412-z
