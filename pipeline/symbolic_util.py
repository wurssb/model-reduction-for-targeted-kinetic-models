"""Utility functions to expand the functionality of symbolicSBML for use in the pipeline.

Author: Rik van Rosmalen
"""
import json
import collections
import pathlib

import numpy as np
import pandas as pd
import sympy

import h5py


class SymbolicPerturbation(object):
    def __init__(self, trigger, event):
        """Create a perturbation.

        This is done in the style of Amici and consists of the following:
            - trigger: Function that decides when to trigger the event.
                       It is triggered when the trigger function crosses 0.
            - event  : The change in state. Should be the same dimensionality
                       as the state, and will be added if the event is triggered.
                       Noted as "bolus" in Amici.
            - output : [Unused] The output the event should trigger which will
                       be recorded.
        The trigger can be defined as a symbolic functions expressions.
        If the event is a constant, it is added.
        All symbols from the model can be used, including the reserved symbol
        for time ('t'). Most Sympy functions can be used.

        Note: Strings can be provided, which will be converted using sympify.
        Warning: Sympify is used wich is *unsafe* on untrusted input!

        :param trigger: Symbolic function that crosses 0 when the perturbation should be triggered.
        :type trigger: sympy.Expr
        :param event: Variable that should be changed
        :type event: dict[sympy.Symbol: sympy.Expr]
        """
        self.trigger = sympy.sympify(trigger)
        self.event = {sympy.sympify(var): sympy.sympify(change) for var, change in event.items()}

    def to_dict(self):
        """Convert to a dictionairy."""
        data = {}
        data['trigger'] = str(self.trigger)
        data['event'] = {str(k): str(v) for k, v in self.event.items()}
        return data

    def to_json(self, path):
        """Save the SymbolicPerturbation to a json file at path, overwriting if necessary."""
        data = self.to_dict()
        with open(path, 'w') as outfile:
            json.dump(data, outfile, indent=2, sort_keys=True)

    @classmethod
    def from_dict(cls, data):
        """Load the SymbolicPerturbation from a dictionairy."""
        return cls(data['trigger'], data['event'])

    @classmethod
    def from_json(cls, path):
        """Load the SymbolicPerturbation from a json file at path."""
        with open(path, 'r') as infile:
            data = json.load(infile)
        return cls.from_dict(data)


class ModelConditions(object):
    def __init__(self, parameter_values, initial_concentrations):
        """Create an object describing the simulation conditions for a model."""
        self.parameter_values = self._to_frame(parameter_values)
        self.initial_concentrations = self._to_frame(initial_concentrations)

        if len(self.parameter_values) != len(self.initial_concentrations):
            raise ValueError("Different number of parameters and initial states.")
        self.n_conditions = len(self.parameter_values)

    def _to_frame(self, value):
        """Convert a dictionairy to a single row dataframe, keeps a dataframe as is."""
        if isinstance(value, collections.Mapping):
            return pd.DataFrame.from_dict(value, 'index').T
        elif isinstance(value, pd.DataFrame):
            return value
        else:
            raise ValueError("Provide dictionairy or Pandas DataFrame as input.")

    def to_hdf(self, path, compression=4, shuffle=True, timestamps=False):
        """Save the ModelConditions to hdf5.

        Timestamps are disabled by default to play nice with version control.
        """
        with h5py.File(path, 'w') as hf:
            hf.create_dataset('parameter_values',
                              compression=compression, shuffle=shuffle, track_times=timestamps,
                              data=self.parameter_values.values)
            hf.create_dataset('parameter_columns',
                              compression=compression, shuffle=shuffle, track_times=timestamps,
                              data=self.parameter_values.columns.values.astype('S'))
            hf.create_dataset('initial_concentrations',
                              compression=compression, shuffle=shuffle, track_times=timestamps,
                              data=self.initial_concentrations.values)
            hf.create_dataset('initial_concentrations_columns',
                              compression=compression, shuffle=shuffle, track_times=timestamps,
                              data=(self.initial_concentrations.columns.values.astype('S')))

    @classmethod
    def from_hdf(cls, path):
        """Load the ModelConditions from hdf5."""
        with h5py.File(path, 'r') as hf:
            parameter_values = np.array(hf['parameter_values'])
            parameter_columns = np.array(hf['parameter_columns']).astype(str)
            parameter_values = pd.DataFrame(parameter_values, columns=parameter_columns)

            initial_concentrations = np.array(hf['initial_concentrations'])
            initial_concentrations_columns = (np.array(hf['initial_concentrations_columns'])
                                              .astype(str))
            initial_concentrations = pd.DataFrame(initial_concentrations,
                                                  columns=initial_concentrations_columns)

        return cls(parameter_values, initial_concentrations)


class AMICISymbolicFormatter(object):
    template = """
function [model] = {function_name}()

    % STATES
    syms {states_sep_space}
    model.sym.x = [{states_sep_comma}];

    % PARAMETERS
    syms {parameters_sep_space}
    model.sym.p = [{parameters_sep_comma}];

    % CONSTANTS
    % model.param = TODO;
    % model.sym.k = TODO;

    % Symbol for time (Has to be t!)
    syms t

    % INPUT
    % TODO

    % SYSTEM EQUATIONS
    model.sym.xdot = sym(zeros(size(model.sym.x)));
    {xdot_expressions}

    % INITIAL CONDITIONS
    model.sym.x0 = [{initial_concentrations_sep_comma}];

    % OBSERVABLES
    {observable_expressions}
    model.sym.y = [{observables_sep_comma}];

    % SIGMA
    % model.sym.sigma_y = TODO;

    % EVENTS
    {events}

"""

    def __init__(self, model, perturbations=None, fixed_parameters=None):
        """Generate an AMICI syms file for a model with optional perturbations."""
        self.model = model
        self.perturbations = [] if perturbations is None else perturbations
        # Make sure these are replaced everywhere!
        # 1) Perturbation (trigger + event)
        # 2) Expressions
        # 3) Observations
        if fixed_parameters is None:
            self.fixed_parameters = {}
            self.fixed_parameters_names = set()
        else:
            # Make sure it is a Sympy Symbol or it won't match!
            self.fixed_parameters = {sympy.Symbol(k): float(v)
                                     for k, v in fixed_parameters.items()}
            self.fixed_parameters_names = {i.name for i in self.fixed_parameters}
        self.printer = AmiciCodePrinter()

    def translate_perturbations(self):
        """Translate perturbations to model events."""
        base = 'model.event({i}) = amievent({trigger}, {event}, {output});'

        # Return early if no events are defined.
        if not self.perturbations:
            return ''

        lines = []
        for i, p in enumerate(self.perturbations):
            trigger = p.trigger.xreplace(self.fixed_parameters)
            # Map the metabolite events to the right index of the state array.
            event_array = [0] * len(self.model.metabolites)
            for metabolite, value in p.event.items():
                idx = self.model.metabolites.index(metabolite.name)
                event_array[idx] = value
            event = sympy.Matrix(sympy.sympify(np.array(event_array)))
            event = event.xreplace(self.fixed_parameters)
            lines.append(base.format(i=i + 1, output='[]',
                                     trigger=self.printer.doprint(trigger),
                                     event=self.printer.doprint(event)))
        return '\n\t'.join(lines)

    def translate_expressions(self):
        """Translate ODE expressions to xdot."""
        base = "model.sym.xdot({i}) = {expr};"
        lines = []
        for i, expr in enumerate(list(self.model.dydt)):
            expr_fixed_replaced = expr.xreplace(self.fixed_parameters)
            matlab_expr = self.printer.doprint(expr_fixed_replaced)
            lines.append(base.format(i=i + 1, expr=matlab_expr))
        return '\n\t'.join(lines)

    def translate_observables(self):
        """Translate observables."""
        base = "{key} = {expr};"
        lines = []
        for key, expr in self.model.observables.items():
            expr_fixed_replaced = expr.xreplace(self.fixed_parameters)
            matlab_expr = self.printer.doprint(expr_fixed_replaced)
            lines.append(base.format(key=key, expr=matlab_expr))
        return '\n\t'.join(lines)

    def create_symbolic_model_variables(self, function_name):
        """Create all variables required to format the symbolic model."""
        return {
            'function_name': function_name,
            'states_sep_space': ' '.join(self.model.metabolites),
            'states_sep_comma': ', '.join(self.model.metabolites),
            'parameters_sep_space': ' '.join(p for p in self.model.parameters
                                             if p not in self.fixed_parameters_names),
            'parameters_sep_comma': ', '.join(p for p in self.model.parameters
                                              if p not in self.fixed_parameters_names),
            'observables_sep_comma': ', '.join(self.model.observables.keys()),
            'initial_concentrations_sep_comma': ', '.join(['0'] * len(self.model.metabolites)),
            'xdot_expressions': self.translate_expressions(),
            'observable_expressions': self.translate_observables(),
            'events': self.translate_perturbations(),
        }

    def generate(self, path):
        """Use the template, the model and the optional perturbations, generate a syms file."""
        function_name = path.with_suffix('').name
        variables = self.create_symbolic_model_variables(function_name)
        with open(path, 'w') as outfile:
            outfile.write(self.template.format(**variables))


class AmiciCodePrinter(sympy.printing.octave.OctaveCodePrinter):
    """Modified Octave printer that uses the AMICI custom logic functions.

    Note that 'if', 'piecewise', 'spline' and 'stepfun' are not implemented.
    """

    _operators = {}

    # For these operators, fall back to generalized function printing
    # where we added the amici specific functions.
    print_f = sympy.printing.octave.OctaveCodePrinter._print_Function
    _print_And = print_f
    _print_Or = print_f
    _print_Xor = print_f
    _print_Relational = print_f
    _print_Not = print_f
    _print_Eq = print_f
    _print_Max = print_f
    _print_Min = print_f

    # Taken from octave printing module
    known_fcns_src1 = sympy.printing.octave.known_fcns_src1
    known_fcns_src2 = sympy.printing.octave.known_fcns_src2

    # Amici specific
    extra_funcs = {
        'And': 'am_and',
        'Or': 'am_or',
        'Xor': 'am_xor',
        'Equality': 'am_eq',
        'Not': 'am_not',
        'Min': 'am_min',
        'Max': 'am_max',
        'GreaterThan': 'am_ge',
        'StrictGreaterThan': 'am_gt',
        'LessThan': 'am_le',
        'StrictLessThan': 'am_lt',
        'Piecewise': 'am_piecewise',
    }
    # Missing:
    #   am_if
    #   am_piecewise
    #   am_spline
    #   am_spline_pos
    #   am_stepfun

    def __init__(self, settings=None):
        """Initialize the Octave code printer and add the modified Amici functions."""
        if settings is None:
            settings = {}
        super(AmiciCodePrinter, self).__init__(settings)
        self.known_functions = dict(zip(self.known_fcns_src1, self.known_fcns_src1))
        self.known_functions.update(dict(self.known_fcns_src2))
        self.known_functions.update(self.extra_funcs)
        userfuncs = settings.get('user_functions', {})
        self.known_functions.update(userfuncs)
