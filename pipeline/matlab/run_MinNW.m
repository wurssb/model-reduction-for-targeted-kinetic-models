function run_MinNW(model_path, output_path, ...
        protected_reactions, protected_metabolites, protected_behaviour, ...
        min_freedom, tolerance, n_results, fast, delete_blocked, bigM)
    % Read SBML model (uses CobraToolbox)
    load_CobraLimited();
    model = readCbModel(model_path);

    % Reversibility is required as well
    model.rev = model.lb < 0 & model.ub > 0;

    % Cobra adds the compartment when loading, so reassign to just the
    % metabolite name.
    model.mets = model.metNames;

    % Create the protected behaviour
    direction = containers.Map([-1, 1], {'<', '>'});

    f = cell(size(protected_behaviour, 2));
    for i = 1:size(protected_behaviour, 2)
        f{i}.names = protected_behaviour{i}.reactions';
        % Depending on whether it came from Matlab or trough the python bridge, this might be a cell.
        if iscell(protected_behaviour{i}.directions)
            f{i}.type = arrayfun(@(x) direction(x), cell2mat(protected_behaviour{i}.directions))';
        else
            f{i}.type = arrayfun(@(x) direction(x), protected_behaviour{i}.directions)';
        end

        if iscell(protected_behaviour{i}.values)
            f{i}.rhsValues = cellfun(@double, protected_behaviour{i}.values');
        else
            f{i}.rhsValues = protected_behaviour{i}.values';
        end

    end

    % Settings
    requirements.n_functionalities = size(protected_behaviour, 2);
    requirements.prot_mets = protected_metabolites';
    requirements.prot_rxns = protected_reactions';
    requirements.dof = double(min_freedom);
    requirements.f = f;
    requirements.tol = double(tolerance);
    requirements.number = double(n_results);
    requirements.fast = double(fast);
    requirements.use_F2C2 = double(delete_blocked);
    requirements.use_bigM = double(bigM);

    % Reduce
    models = reduce_model(model, requirements);

    % Clean up old fields that are not valid any more.
    models = rmfield(models, 'c');
    models = rmfield(models, 'rules');
    models = rmfield(models, 'subSystems');

    % Save output as sbml (Uses CobraToolbox)
    % To add the missing fields to be able to save, (ab)use the convertOldStyleModel function.
    if n_results > 1
        for i = 1:n_results
            writeSBML(convertOldStyleModel(models{i}), strcat(output_path, '.', num2str(i)));
        end

    else
        writeSBML(convertOldStyleModel(models), output_path);
    end

end
