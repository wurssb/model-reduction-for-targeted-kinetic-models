function run_FastCore(model_path, output_path, protected_reactions, protected_metabolites, eps)
    % Load model using Cobra toolbox
    load_CobraLimited();
    model = readCbModel(model_path);

    % FastCore requires an additional reversibility array in the model struct.
    model.rev = model.lb < 0 & model.ub > 0;

    % Get conserved core reactions as index
    [~, core_idx, ~] = intersect(model.rxns, protected_reactions);

    % Run FastCore
    conserved_reactions = fastcore(core_idx, model, eps);

    % Remove reactions that are not protected.
    to_remove = model.rxnNames(setdiff(1:size(model.rxns), conserved_reactions));
    model = removeRxns(model, to_remove, false, false);

    % Remove unused metabolites that are not protected
    to_remove = setdiff(model.metNames(sum(abs(model.S')) == 0), protected_metabolites);
    model = removeMetabolites(model, to_remove, false);

    % Save output as sbml (Uses CobraToolbox)
    writeSBML(model, output_path);
end
