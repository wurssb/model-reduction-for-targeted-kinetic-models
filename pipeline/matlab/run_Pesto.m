function [optimized_parameters] = run_Pesto(model_data_struct, parameters, ...
        starting_points_path, results_path, settings)
    installAMICI();
    % Set the result path in the settings.
    settings.pesto_settings.foldername = results_path;

    % Load settings.
    [pesto_options, ami_options] = load_Options(settings);

    % Load parameters.
    parameters = load_Parameters(parameters);

    % Load starting points if needed.
    if starting_points_path
        parameter_names = deblank(h5read(starting_points_path, '/parameter_names'));
        starting_points = h5read(starting_points_path, '/parameter_values');

        if settings.pesto_settings.n_starts > size(starting_points, 1)
            error('More starting points requested (%d) then provided by user (%d).', ...
                  settings.pesto_settings.n_starts, size(starting_points, 1))
        end

        % For each of the parameters, check if it's required by this model.
        % Currently this assumes that all experiments use the exact same parameters.
        % (TODO)
        model_description = model_data_struct.model_description_paths{1};
        model_description = jsondecode(fileread(model_description));
        wanted_parameter_names = model_description.parameters;
        valid_parameters = cellfun(@(c) any(strcmp(c, wanted_parameter_names)), ...
            parameter_names, 'UniformOutput', true);
        % Pesto wants a function that accepts (guess, min, max, n)
        % and returns an array of p x n starting points.
        parameters.init_fun = @(~, ~, ~, n) starting_points(1:n, valid_parameters)';

    end

    % Make sure all models have their own data, description and initial conditions.
    field_lengths = structfun(@(x) size(x, 2), model_data_struct);

    if ~field_lengths == field_lengths(1)
        error('Uneven field lengths in model data structure.');
    end

    % Load all models & data sets
    opt_struct.f = cell(field_lengths(1), 1);
    opt_struct.amiData = cell(field_lengths(1), 1);
    opt_struct.amiOptions = ami_options;
    opt_struct.initial_conditions = cell(field_lengths(1), 1);

    for i = 1:field_lengths(1)
        amici_simulation_function = model_data_struct.model_paths{i};
        data_file = model_data_struct.data_paths{i};
        model_description = model_data_struct.model_description_paths{i};
        initial_conditions = model_data_struct.initial_concentrations{i};

        % Load which observables we need to fit.
        model_description = jsondecode(fileread(model_description));
        opt_struct.amiData{i} = load_AmiData(data_file, model_description.observables);

        % Make sure initial condition are in the right format.
        if iscell(initial_conditions)
            opt_struct.initial_conditions{i} = double(cell2mat(initial_conditions))';
        else
            opt_struct.initial_conditions{i} = initial_conditions;
        end

        % Find the model function
        opt_struct.f{i} = str2func(amici_simulation_function);
    end

    % Create objective function with extracted data, only accepts parameters as input.
    f = @(theta) objective_function_llh(opt_struct, theta);

    % Optimize
    optimized_parameters = getMultiStarts(parameters, f, pesto_options);

    save_Results(optimized_parameters, results_path, pesto_options.trace, ...
        double(settings.h5_settings.deflate), settings.h5_settings.shuffle);

    % We don't care about this as output, and it will cause issues when
    % passing results back to python if we are using the matlab bridge.
    if isfield(optimized_parameters, 'init_fun')
        optimized_parameters = rmfield(optimized_parameters, 'init_fun');
    end

    % TODO: Profiles
    % if options.calc_profiles
    %     % Calculate profiles
    %     optimized_profiles = getParameterProfiles(optimized_parameters, f, options);
    %     % Save profiles
    %     TODO
    % end
end

function varargout = objective_function_llh(opt_struct, theta)
    % Preallocate for results
    n_par = length(theta);
    n_runs = length(opt_struct.f);
    llh = zeros(n_runs, 1);
    sllh = zeros(n_runs, n_par);
    s2llh = zeros(n_runs, n_par, n_par);

    % Simulate each condition with the right model and data set.
    for i = 1:n_runs
        f = opt_struct.f{i};
        amiOptions = opt_struct.amiOptions;
        amiOptions.x0 = opt_struct.initial_conditions{i};
        amiData = opt_struct.amiData{i};

        % You can leave out the time since it will be retrieved from amiData.t
        if (nargout <= 1)
            amiOptions.sensi = 0;
            sol = f([], theta, [], amiData, amiOptions);
            % Log-likelihood
            llh(i, :) = sol.llh;
        elseif (nargout == 2)
            amiOptions.sensi = 1;
            sol = f([], theta, [], amiData, amiOptions);
            llh(i, :) = sol.llh;
            % Gradient of log-likelihood (1st order adjoint sensitivity to parameters)
            sllh(i, :, :) = sol.sllh;
        elseif (nargout == 3)
            amiOptions.sensi = 2;
            sol = f([], theta, [], amiData, amiOptions);
            llh(i) = sol.llh;
            sllh(i, :, :) = sol.sllh;
            % Hessian of log-likelihood (2nd order adjoint sensitivity to parameters)
            s2llh(i, :, :, :) = sol.s2llh;
        end

    end

    % Sum objectives and gradients
    if nargout == 1
        varargout{1} = sum(llh, 1);
    elseif nargout == 2
        varargout{1} = sum(llh, 1);
        varargout{2} = sum(sllh, 1);
    elseif nargout == 3
        varargout{1} = sum(llh, 1);
        varargout{2} = sum(sllh, 1);
        varargout{3} = sum(s2llh, 1);
    end

end

function [parameters] = load_Parameters(parameters)
    % Set parameter bounds (min / max)
    if iscell(parameters.min)
        parameters.min = double(cell2mat(parameters.min))';
    end

    if iscell(parameters.max)
        parameters.max = double(cell2mat(parameters.max))';
    end

    % Initial points
    if iscell(parameters.guess)
        parameters.guess = double(cell2mat(parameters.guess))';
    end

    % Number of parameters (inferred)
    parameters.number = length(parameters.min);
    % Display names for parameters (not required?)
    parameters.name = [];

end

function [optionsPesto, optionsAmici] = load_Options(settings)
    pesto_settings = settings.pesto_settings;
    % Create the pesto option struct.
    optionsPesto = PestoOptions();
    % 'sequential' or 'parallel'
    % Parallel seems to only work with fmincon.
    optionsPesto.comp_type = pesto_settings.comp_type;
    % Save results in an extra folder
    optionsPesto.save = false;
    % Name of results folder (uses current time stamp if not provided)
    optionsPesto.foldername = pesto_settings.foldername;
    % or 'negative log-posterior' for minimization vs maximization.
    optionsPesto.obj_type = 'log-posterior';
    % How many results can be gotten (objective, gradient, Hessian) from the
    % objective function. Does not determine whether it actually uses them.
    optionsPesto.objOutNumber = 3;
    % Indices of fixed parameters (Does this work?)
    optionsPesto.fixedParameters = [];
    % Values of fixed parameters
    optionsPesto.fixedParameterValues = [];
    % number of data points (optional - for model selection using the BIC)
    optionsPesto.nDatapoints = [];
    % Output mode ('visual', 'text', 'silent', 'debug')
    optionsPesto.mode = 'text';
    % Figure handle for visual output
    optionsPesto.fh = [];
    % Number of local optimizations.
    optionsPesto.n_starts = double(pesto_settings.n_starts);
    % vector of indices which starts should be performed. (default is 1:n_starts)
    optionsPesto.start_index = [];
    % log-likelihood / log-posterior threshold for initialization of optimization.
    optionsPesto.init_threshold = -inf;
    % Offset between log-likelihood and sum of squared residuals (?)
    % (important only for lsqnonlin so far)
    optionsPesto.logPostOffset = [];
    % Local optimizer ('lsqnonlin', 'fmincon', 'meigo-ess', 'meigo-vns' or 'pswarm')
    optionsPesto.localOptimizer = pesto_settings.localOptimizer;
    % Options for local optimizer
    if ismember(optionsPesto.localOptimizer, {'lsqnonlin', 'fmincon'})
        optionsPesto.localOptimizerOptions = optimset(pesto_settings.localOptimizerOptions);
    else
        optionsPesto.localOptimizerOptions = pesto_settings.localOptimizerOptions;
    end

    % Store the Hessian at optimal values?
    optionsPesto.localOptimizerSaveHessian = pesto_settings.localOptimizerSaveHessian;
    % Starting points for fmincon ('latin hypercube', 'uniform', 'user-supplied')
    optionsPesto.proposal = pesto_settings.proposal;
    % Store trace information about parameter values, computation time and objective value.
    % Only works in sequential mode (?)
    optionsPesto.trace = pesto_settings.trace;
    % Store intermediates every 10 iteration (Does this work?)
    optionsPesto.tempsave = false; % pesto_settings.tempsave;
    % Calculate profiles? (TODO)
    optionsPesto.calc_profiles = false;
    % Retrieve the options for Amici
    settings.ami_settings.sensi = double(settings.ami_settings.sensi);
    settings.ami_settings.pscale = double(cell2mat(settings.ami_settings.pscale));
    optionsAmici = amioption(settings.ami_settings);
end

function save_Results(opt, results_path, traces, deflate, shuffle)
    % Create a JSON File with settings (remove traces from JSON - too big!)
    opt_no_trace = opt.MS;
    fields = {'par_trace', 'fval_trace', 'time_trace'};

    for i = 1:length(fields)
        field = fields{i};

        if isfield(opt_no_trace, field)
            opt_no_trace = rmfield(opt_no_trace, field);
        end

    end

    json = jsonencode(opt_no_trace, 'ConvertInfAndNaN', false);
    fileID = fopen(fullfile(results_path, 'optimization_results.json'), 'w');
    fprintf(fileID, json);
    fclose(fileID);

    % If traces are requested, save them to a hdf5 file.
    if traces
        h5path = fullfile(results_path, 'traces.h5');

        if exist(h5path, 'file')
            delete(h5path);
        end

        s = size(opt.MS.par_trace);
        cs = s;
        cs(end) = 1;
        h5create(h5path, '/par', s, ...
            'Deflate', deflate, 'Shuffle', shuffle, 'ChunkSize', cs);
        h5write(h5path, '/par', opt.MS.par_trace);

        s = size(opt.MS.fval_trace);
        cs = s;
        cs(end) = 1;
        h5create(h5path, '/fval', s, ...
            'Deflate', deflate, 'Shuffle', shuffle, 'ChunkSize', cs);
        h5write(h5path, '/fval', opt.MS.fval_trace);

        s = size(opt.MS.time_trace);
        cs = s;
        cs(end) = 1;
        h5create(h5path, '/time', s, ...
            'Deflate', deflate, 'Shuffle', shuffle, 'ChunkSize', cs);
        h5write(h5path, '/time', opt.MS.time_trace);

    end

end
