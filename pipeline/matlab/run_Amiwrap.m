function run_Amiwrap(name, sym_function, out_path, generate_hessian)
    % Set up AMICI
    installAMICI();
    % Compile the model
    % Generating the second order sensitivities or hessian is left optional
    % since this can slow things down quite a bit if not required.
    amiwrap(name, sym_function, out_path, generate_hessian);
end
