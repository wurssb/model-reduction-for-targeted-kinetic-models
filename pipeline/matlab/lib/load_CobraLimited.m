function load_CobraLimited()
    % This loads a subset of the cobra toolbox without doing all the additional
    % time-consuming setup that initCobraToolbox does.

    % Note that it won't setup the solvers.
    % Only file in/output and some other scripts.

    % If the cobra version changes this will probably require updating.

    % Find root based on the initCobraToolbox function.
    cobra_dir = fileparts(which('initCobraToolbox'));
    % Go to cobra location.
    pwd = cd(cobra_dir);

    % Add the src folder
    addpath(genpath([cobra_dir filesep 'src']), '-end');

    % Add the binary folder (TranslateSBML is required for io)
    addpath(genpath([cobra_dir filesep 'binary']), '-end');

    % Add the external folder (io/libsbml is required for io)
    addpath(genpath([cobra_dir filesep 'external' filesep 'base']), '-end');

    % Go back to original location.
    cd(pwd);
end
