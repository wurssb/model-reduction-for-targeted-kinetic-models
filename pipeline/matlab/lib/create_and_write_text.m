function create_and_write_text(file_path, h5_path, name, array)
    % Note that hdf5write can write strings, but it is deprecated.
    % Adapted from the H5D.create documentation.
    fid = H5F.open(file_path, 'H5F_ACC_RDWR', 'H5P_DEFAULT');
    % % Create group to allow automatic creation of intermediate groups
    % gcpl_id = H5P.create('H5P_LINK_CREATE');
    % H5P.set_create_intermediate_group(gcpl_id, 1);
    % group_id = H5G.create(file_path, h5_path, gcpl_id, 'H5P_DEFAULT', 'H5P_DEFAULT');
    % Create and appropriate type.
    type_id = H5T.copy('H5T_C_S1');
    % Set max length to the longest string length.
    H5T.set_size(type_id, max(cellfun('length', array)));
    % Only 1 dimension, the cell array length.
    h5_dims = [length(array)];
    h5_maxdims = h5_dims;
    % Create the space we require
    space_id = H5S.create_simple(1, h5_dims, h5_maxdims);
    % Create the dataset
    dset_id = H5D.create(fid, strcat(h5_path, name), type_id, space_id, 'H5P_DEFAULT');
    % Write the string
    H5D.write(dset_id, type_id, 'H5S_ALL', 'H5S_ALL', 'H5P_DEFAULT', char(array)');

    % Close all objects we crated
    % H5P.close(gcpl_id);
    % H5G.close(group_id);
    H5S.close(space_id);
    H5T.close(type_id);
    H5D.close(dset_id);
    H5F.close(fid);
end
