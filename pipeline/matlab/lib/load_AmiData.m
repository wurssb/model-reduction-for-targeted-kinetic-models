function [amiData] = load_AmiData(data_file, model_observables)
    % Split the data_file path into the path and the hdf5 path
    [file_path, h5_path] = split_h5_path(data_file);

    % Identfiers for metabolites
    h5_observables = deblank(h5read(data_file, strcat(h5_path, 'observables')));
    % We only want the observables that are also in observables
    model_observables = deblank(model_observables);
    [shared, ~, idx] = intersect(model_observables, h5_observables, 'stable');

    if ~all(strcmp(model_observables, shared))
        error('Not all model observables can be found in data!')
    end

    % Create the AmiData object, storing the experimental data to be fitted to.
    % time points [nt, 1]
    data.t = h5read(data_file, strcat(h5_path, 't'));
    % measurement [nt, ny]
    data.Y = h5read(data_file, strcat(h5_path, 'y'));
    data.Y = data.Y(:, idx);

    % These are optional, so only load if available in the data.
    info = h5info(file_path, h5_path);
    available = info.Datasets.Name;
    % standard deviation of y [nt, ny]
    if any(strcmp('sigma_y', available))
        data.Sigma_Y = h5read(data_file, strcat(h5_path, 'sigma_y'));
        data.Sigma_Y = data.Sigma_Y(:, idx);
    end

    % initial concentrations [nk, 1] (or is this kappa?)
    if any(strcmp('condition', available))
        data.condition = h5read(data_file, strcat(h5_path, 'condition'));
    end

    % event observations [ne, nz]
    if any(strcmp('z', available))
        data.Z = h5read(data_file, strcat(h5_path, 'z'));
    end

    % standard deviation of event observations [ne, nz]
    if any(strcmp('sigma_z', available))
        data.Sigma_Z = h5read(data_file, strcat(h5_path, 'sigma_z'));
    end

    % initial concentrations for pre-equilibration [nk, 1]
    if any(strcmp('condition_preq', available))
        data.conditionPreequilibration = h5read(data_file, strcat(h5_path, 'condition_preq'));
    end

    % Create the AmiData struct which will verify the dimensions.
    amiData = amidata(data);
end
