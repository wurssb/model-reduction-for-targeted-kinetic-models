function [file_path, h5_path] = split_h5_path(path)
    % Note that while the Matlab files that deal with h5 currently support the split path
    % functionallity, it is untested in combination with the rest of the pipeline.

    % Split a combined file path and hdf5 path into 2 separate strings.
    [matches, delim] = strsplit(path, '.h5');
    file_path = strcat(matches{1}, delim);
    file_path = file_path{1};
    h5_path = matches{2};
    % Make sure it ends with '/' so we can append to it without checking this later.
    % This also makes sure that the empty path is a valid root path instead.
    if ~endsWith(h5_path, '/')
        h5_path = strcat(h5_path, '/');
    end

end
