function write_AmiData(path, model_description_path, results, theta, h5settings, overwrite)
    % Load the state and observable names from the model_description_path
    model_description = jsondecode(fileread(model_description_path));

    % Write simulation data to a h5 file. (Currently only supports single simulations!)
    [file_path, h5_path] = split_h5_path(path);

    deflate = double(h5settings.deflate);
    shuffle = h5settings.shuffle;

    if exist(file_path, 'file') && overwrite
        delete(file_path);
    end

    h5save = @(name, array) create_and_write(file_path, h5_path, deflate, ...,
    shuffle, name, array);
    h5save_text = @(name, array) create_and_write_text(file_path, ...
        h5_path, name, array);
    h5save('/status', [results.status]);
    h5save('/t', [results.t]);
    h5save_text('/states', model_description.states);
    h5save('/x', [results.x]);
    h5save_text('/observables', model_description.observables);
    h5save('/y', [results.y]);
    h5save_text('/parameters', model_description.parameters);
    h5save('/p', [theta]);

    if sum(size(results.sx)) > 0
        h5save('/sx', [results.sx]);
    end

    if sum(size(results.sy)) > 0
        h5save('/sy', [results.sy]);
    end

end

function create_and_write(file_path, h5_path, deflate, shuffle, name, array)
    h5create(file_path, strcat(h5_path, name), size(array), ...
        'Deflate', deflate, 'Shuffle', shuffle, ...
        'ChunkSize', size(array));
    h5write(file_path, strcat(h5_path, name), array);
end
