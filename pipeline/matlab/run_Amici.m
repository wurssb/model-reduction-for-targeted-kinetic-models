function [result] = run_Amici(amici_simulation_function, data_out_path, model_description_path, ...
        theta, initial_conditions, t_out, settings)
    % Set up AMICI
    installAMICI();
    % Convert some options to double.
    settings.ami_settings.sensi = double(settings.ami_settings.sensi);
    settings.ami_settings.pscale = double(cell2mat(settings.ami_settings.pscale));

    % Convert to arrays of doubles if we get cells.
    if iscell(initial_conditions)
        initial_conditions = double(cell2mat(initial_conditions))';
    else
        initial_conditions = initial_conditions';
    end

    if iscell(theta)
        theta = double(cell2mat(theta))';
    else
        theta = theta';
    end

    if iscell(t_out)
        t_out = double(cell2mat(t_out));
    end

    if ndims(initial_conditions) ~= ndims(theta)
        error('Number of initial conditions and parameter sets is unequal.')
    end

    % Prepare inputs
    kappa = [];
    data = [];

    if ndims(initial_conditions) == 1 || size(initial_conditions, 2) == 1
        ami_options = amioption(settings.ami_settings);
        ami_options.x0 = initial_conditions;

        % Get handle to correct function for simulation and simulate.
        f = str2func(amici_simulation_function);
        result = f(t_out, theta, kappa, data, ami_options);

        % Write out results
        overwrite = true;
        write_AmiData(data_out_path, model_description_path, result, theta, settings.h5_settings, overwrite);
    else

        for i = 1:size(initial_conditions, 2)
            ami_options = amioption(settings.ami_settings);
            ami_options.x0 = initial_conditions(:, i);

            % Get handle to correct function for simulation and simulate.
            f = str2func(amici_simulation_function);
            result = f(t_out, theta(:, i), kappa, data, ami_options);

            % Write out results
            if i == 1
                % Overwrite only first time
                overwrite = true;
            else
                overwrite = false;
            end

            h5path = strcat(data_out_path, '/', num2str(i));
            write_AmiData(h5path, model_description_path, result, theta(:, i), settings.h5_settings, overwrite);
        end

    end

end
