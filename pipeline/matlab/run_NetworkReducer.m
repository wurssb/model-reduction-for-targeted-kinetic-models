function run_NetworkReducer(model_path, output_path, ...
        protected_reactions, protected_metabolites, protected_behaviour, ...
        min_reactions, min_freedom, compress, protected_nonblocked)
    % Start CNA toolbox withouth GUI
    startcna(1)

    % Load model
    ext_comparts = {};
    [model, ~] = CNAsbmlModel2MFNetwork(model_path, ext_comparts);

    % Create mapping from metabolite name to index
    met_map = containers.Map(cellstr(model.specID), 1:length(model.specID));
    protected_metabolites = cellfun(@(x) met_map(x), protected_metabolites);

    % Create mapping from reaction name to index
    reac_map = containers.Map(cellstr(model.reacID), 1:length(model.reacID));
    protected_reactions = cellfun(@(x) reac_map(x), protected_reactions);

    % Create protected behaviour
    for i = 1:size(protected_behaviour, 2)
        protect_func(i).D = zeros(0, model.numr);
        % Depending on whether it came from Matlab or trough the python bridge, this might be a cell.
        if iscell(protected_behaviour{i}.directions)
            directions = cell2mat(protected_behaviour{i}.directions);
        else
            directions = protected_behaviour{i}.directions;
        end

        for j = 1:size(protected_behaviour{i}.reactions, 2)
            reaction = reac_map(protected_behaviour{i}.reactions{j});
            protect_func(i).D(j, reaction) = directions(j);
        end

        if iscell(protected_behaviour{i}.values)
            values = cell2mat(protected_behaviour{i}.values);
        else
            values = protected_behaviour{i}.values;
        end

        protect_func(i).d = double(values');

    end

    % Reduction
    solver = 2; % Solver: Cplex(2)
    rational = 1; % Use rational arithmetic? (What does this change?)

    % Convert all variables to double (see also protect_func.d above.)
    % If called through the python engine, these variables might be int64.
    % Which will not play nice with CPLEX.
    compress = double(compress);
    min_freedom = double(min_freedom);
    min_reactions = double(min_reactions);
    protected_nonblocked = double(protected_nonblocked);

    [model, ~, ~] = CNAreduceMFNetwork(model, min_freedom, ...
        protect_func, protected_metabolites, ...
        protected_reactions, ...
        protected_nonblocked, min_reactions, ...
        solver, compress, rational);

    % Save model
    % macromol = 0; % Add concentrations
    % fbc = 1; % Add flux bounds and objective using the flux balance extension of SBML.
    % CNAMFNetwork2sbml(model, output_path, macromol, fbc);
    
    % This method rounds the stoichiometry coefficients to 5 digits, leading to errors.
    % Converting to cobra first and saving using their methods, however, solves the issue.
    load_CobraLimited();
    cobra_model = CNAcna2cobra(model);
    cobra_model.rxns = strrep(cobra_model.rxns, '*', '__');
    writeCbModel(cobra_model, 'format', 'sbml', 'file', output_path);
    
    % Clean up CNA toolbox
    clear cnan
end
