"""Pipeline steps for model reduction.

Implements:
    - A base Reducer step which takes care of standardizing the output.
    - A FastCore Reducer step
    - A MinNW Reducer step
    - A NetworkReducer Reducer step

Author: Rik van Rosmalen
"""
import collections
import re
import pathlib
import json

import lxml.etree

import numpy as np
import sympy

import symbolicSBML

from .step import Step
from .util import matlab_path_context, call_matlab_silenced


class Reducer(Step):
    def __init__(self, mlab_engine_instance=None, use_cache=True, **settings):
        """Create a Base Reducer for model reduction.

        Meant to be subclassed. Handles most of the in and output details and makes
        sure the return models are standardized compared to the output of the individual
        tools.

        Requires inputs to run are:
            'reduce_model': The model file to be reduced
            'full_model': The original model file (This is used to map back the results)
        Outputs are:
            - The reduced model as returned by the tool (./reduced_temp.xml)
            - The reduced model mapped back to the original model (./reduced.xml)
        """
        super().__init__(mlab_engine_instance, use_cache, **settings)

    def reduce(
        self, model_file, reduced_model_file, reactions, metabolites, behaviours
    ):
        """Run the actual reducer.

        This should be implemented by the child class.
        """
        raise NotImplementedError

    def _run(self, working_directory, infiles):
        """Set up for and run the reduction step."""
        model_file = infiles["reduce_model"]
        annotation_file = infiles["reduce_model_annotation"]

        reactions, metabolites, behaviours = self.map_reactions_and_metabolites(
            annotation_file
        )
        reduced_model_file = working_directory / pathlib.Path("reduced_temp.xml")
        self.reduce(model_file, reduced_model_file, reactions, metabolites, behaviours)

        return [reduced_model_file]

    def map_reactions_and_metabolites(self, annotation_path):
        """Map reaction and metabolite identifiers in the input according to the annotation."""
        annotation = json.loads(annotation_path.read_text())

        if "id_map" in annotation:
            reverse_reac_map = {
                v: k for k, v in annotation["id_map"]["reactions"].items()
            }
            reactions = [
                reverse_reac_map[i] for i in self.settings["protected_reactions"]
            ]

            reverse_met_map = {
                v: k for k, v in annotation["id_map"]["metabolites"].items()
            }
            metabolites = [
                reverse_met_map[i] for i in self.settings["protected_metabolites"]
            ]

            if "protected_behaviour" in self.settings:
                behaviours = []
                for behaviour in self.settings["protected_behaviour"]:
                    new = {
                        "reactions": [
                            reverse_reac_map[i] for i in behaviour["reactions"]
                        ],
                        # Convert everything to plain floats for the best Matlab compatibility.
                        # int and np.float might give issues when using the matlab engine.
                        "directions": [float(i) for i in behaviour["directions"]],
                        "values": [float(i) for i in behaviour["values"]],
                    }
                    behaviours.append(new)
            else:
                behaviours = None
        else:
            reactions = self.settings["protected_reactions"]
            metabolites = self.settings["protected_metabolites"]
            behaviours = self.settings.get("protected_behaviour", None)
        return reactions, metabolites, behaviours


class FastCore(Reducer):
    default_settings = {
        "protected_reactions": [],
        "protected_metabolites": [],
        # Solver tolerance.
        "epsilon": 1e-4,
        "silence_output": True,
    }

    def __init__(
        self,
        cplex_location,
        cobra_location,
        fastcore_location,
        mlab_engine_instance=None,
        use_cache=True,
        **settings
    ):
        """Create a reducers that uses FastCore.

        Vlassis, N., Pacheco, M. P., & Sauter, T. (2014).
        Fast Reconstruction of Compact Context-Specific Metabolic Network Models.
        PLoS Computational Biology, 10(1), e1003424.
        http://doi.org/10.1371/journal.pcbi.1003424
        """
        super().__init__(mlab_engine_instance, use_cache, **settings)
        self.cplex_location = cplex_location
        self.cobra_location = cobra_location
        self.fastcore_location = fastcore_location

    def reduce(
        self, model_file, reduced_model_file, reactions, metabolites, behaviours
    ):
        """Run the reducer."""
        mlab = self.get_matlab_engine()
        locations = [
            self.fastcore_location,
            self.cplex_location,
            self.cobra_location,
            self.MATLAB_FUNCTION_DIR,
            self.MATLAB_LIB_DIR,
        ]

        with matlab_path_context(mlab, locations):
            _ = call_matlab_silenced(
                mlab.run_FastCore,
                # Actual function inputs.
                str(model_file),
                str(reduced_model_file),
                reactions,
                metabolites,
                self.settings["epsilon"],
                # Matlab engine settings
                nargout=0,
                silence_std_out=self.settings["silence_output"],
                silence_std_err=self.settings["silence_output"],
            )


class MinNW(Reducer):
    default_settings = {
        "protected_reactions": [],
        "protected_metabolites": [],
        "protected_behaviour": [],
        # Minimum degrees of freedom that should be preserved.
        "min_freedom": 1,
        # Solver tolerance
        "tolerance": 1e-6,
        # Number of results that should be returned.
        # Note: Not implemented in the pipeline!
        "n_results": 1,
        # Use the fast method
        "fast": 1,
        # Delete blocked reactions before starting.
        "delete_blocked": 1,
        # Use the bigM method.
        # Note: Doesn't work with protected metabolites!
        "bigM": 0,
        "silence_output": True,
    }

    def __init__(
        self,
        cplex_location,
        cobra_location,
        min_nw_location,
        mlab_engine_instance=None,
        use_cache=True,
        **settings
    ):
        """Create a reducers that uses MinNW.

        Röhl, A., & Bockmayr, A. (2017).
        A mixed-integer linear programming approach to the reduction of
            genome-scale metabolic networks.
        BMC Bioinformatics, 18(1), 2.
        http://doi.org/10.1186/s12859-016-1412-z

        """
        super().__init__(mlab_engine_instance, use_cache, **settings)
        self.cplex_location = cplex_location
        self.cobra_location = cobra_location
        self.min_nw_location = min_nw_location

    def reduce(
        self, model_file, reduced_model_file, reactions, metabolites, behaviours
    ):
        """Run the reducer."""
        mlab = self.get_matlab_engine()
        locations = [
            self.min_nw_location,
            self.cplex_location,
            self.cobra_location,
            self.MATLAB_FUNCTION_DIR,
            self.MATLAB_LIB_DIR,
        ]

        with matlab_path_context(mlab, locations):
            _ = call_matlab_silenced(
                mlab.run_MinNW,
                # Actual function inputs.
                str(model_file),
                str(reduced_model_file),
                reactions,
                metabolites,
                behaviours,
                self.settings["min_freedom"],
                self.settings["tolerance"],
                self.settings["n_results"],
                self.settings["fast"],
                self.settings["delete_blocked"],
                self.settings["bigM"],
                # Matlab engine settings
                nargout=0,
                silence_std_out=self.settings["silence_output"],
                silence_std_err=self.settings["silence_output"],
            )


class NetworkReducer(Reducer):
    default_settings = {
        "protected_reactions": [],
        "protected_metabolites": [],
        "protected_behaviour": [],
        # Minimum amount of reactions that should be preserved.
        "min_reactions": 1,
        # Minimum degrees of freedom that should be preserved.
        "min_freedom": 1,
        # Compress linear pathways
        "compress": 1,
        # Make sure that protected metabolites do not become blocked
        # (no way in our out)
        "protected_nonblocked": 1,
        "silence_output": True,
    }

    def __init__(
        self,
        cplex_location,
        cna_location,
        networkreducer_location,
        mlab_engine_instance=None,
        use_cache=True,
        **settings
    ):
        """Create a reducers that uses NetworkReducer.

        Erdrich, P., Steuer, R., & Klamt, S. (2015).
        An algorithm for the reduction of genome-scale metabolic network models
            to meaningful core models.
        BMC Systems Biology, 9(1), 48.
        http://doi.org/10.1186/s12918-015-0191-x
        """
        super().__init__(mlab_engine_instance, use_cache, **settings)
        self.cplex_location = cplex_location
        self.cna_location = cna_location
        self.networkreducer_location = networkreducer_location

    def fix_gt_constraints(self, behaviours):
        # Note that for NetworkReducer these constraints get translated as:
        # sign(r) * flux(r) <= value
        # This means that if we get and entry for reaction r
        # of direction 1 (i.e. '>') with flux 2.
        # This would get translated as:
        # 1 * flux(r) <= 2
        # However, we want the reverse:
        # flux(r) >= 2
        # So we convert this to:
        # -1 * flux(r) <= 2
        fixed_behaviours = []
        for behaviour in behaviours:
            reactions = behaviour["reactions"]
            directions = behaviour["directions"]
            values = behaviour["values"]

            new_directions, new_values = [], []
            for direction, value in zip(directions, values):
                if direction == 1 and value >= 0:
                    new_directions.append(-1.0)
                    new_values.append(-value)
                else:
                    new_directions.append(direction)
                    new_values.append(value)
            fixed_behaviours.append(
                {
                    "reactions": reactions,
                    "directions": new_directions,
                    "values": new_values,
                }
            )
        return fixed_behaviours

    def reduce(
        self, model_file, reduced_model_file, reactions, metabolites, behaviours
    ):
        """Run the reducer."""
        mlab = self.get_matlab_engine()
        locations = [
            self.cplex_location,
            self.cna_location,
            self.networkreducer_location,
            self.MATLAB_FUNCTION_DIR,
            self.MATLAB_LIB_DIR,
        ]

        with matlab_path_context(mlab, locations):
            _ = call_matlab_silenced(
                mlab.run_NetworkReducer,
                # Actual function inputs.
                str(model_file),
                str(reduced_model_file),
                reactions,
                metabolites,
                self.fix_gt_constraints(behaviours),
                self.settings["min_reactions"],
                self.settings["min_freedom"],
                self.settings["compress"],
                self.settings["protected_nonblocked"],
                # Matlab engine settings
                nargout=0,
                silence_std_out=self.settings["silence_output"],
                silence_std_err=self.settings["silence_output"],
            )
