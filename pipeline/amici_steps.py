"""Pipeline steps for Amici compilation and simulation.

Implements:
    - A step to compile Amici Models
    - A step to simulate a compiled Amici Model

Author: Rik van Rosmalen
"""
import json

import numpy as np

import matlab

import symbolicSBML

from .step import Step
from .util import matlab_path_context, call_matlab_silenced, parameter_is_log_scaled
from .symbolic_util import ModelConditions, SymbolicPerturbation, AMICISymbolicFormatter


class AmiciCompiler(Step):
    default_settings = {
        # Generate the second order sensitivity equations (Can be *very* slow!)
        "generate_hessian": True,
        "silence_output": True,
    }

    def __init__(
        self, amici_mlab_location, mlab_engine_instance, use_cache, **settings
    ):
        """Compile a kinetic model for use with Amici.

        Required inputs to run are:
            'model': A SBML model file.
            'perturbation': A list of perturbation files.
        Outputs are:
            - The symbolic Amici definition (./model_syms.m)
            - An Amici simulation file (./simulate_{model_name}.m)
            - A model description file (./model_description.json)
        """
        self.amici_mlab_location = amici_mlab_location
        super().__init__(mlab_engine_instance, use_cache, **settings)
        self._dir_name = "amici_compiler"

    def _run(self, working_directory, infiles):
        """Set up for and run the compilation."""
        model_file = infiles["model"]
        perturbation_files = infiles["perturbation"]
        fixed_parameter_file = infiles["fixed_parameters"]

        model = symbolicSBML.SymbolicModel.from_sbml(model_file)

        with open(fixed_parameter_file, "r") as f:
            fixed_parameters = json.load(f)

        perturbations = []
        for perturbation in perturbation_files:
            perturbations.append(SymbolicPerturbation.from_json(perturbation))

        model_description_path = working_directory / "model_description.json"
        self.save_model_description(
            model, model_description_path, fixed_parameters, perturbations
        )

        symbolic_path = working_directory / "model_syms.m"
        compiled_model_path = working_directory
        name = "model"

        self.generate_symbolics(model, perturbations, fixed_parameters, symbolic_path)
        self.compile(name, working_directory, symbolic_path, compiled_model_path)
        return [
            symbolic_path,
            working_directory / "simulate_{}.m".format(name),
            model_description_path,
        ]

    def save_model_description(self, model, path, fixed_parameters, perturbations):
        """Generate and save the model description file.

        This contains information on all the reactions, states, observables and
        parameters contained in the compiled model, so the inputs and outputs can
        be matched later. (Amici cannot access this from the compiled model alone)
        """
        data = {
            "reactions": list(model.reactions),
            "states": list(model.metabolites),
            "observables": list(model.observables.keys()),
            "parameters": [p for p in model.parameters if p not in fixed_parameters],
            "fixed_parameters": fixed_parameters,
            "perturbations": [p.to_dict() for p in perturbations],
        }

        with open(path, "w") as outfile:
            json.dump(data, outfile, indent=2, sort_keys=True)

    def generate_symbolics(self, model, perturbations, fixed_parameters, symbolic_path):
        """Create and save the symbolic Amici definition."""
        AMICISymbolicFormatter(model, perturbations, fixed_parameters).generate(
            symbolic_path
        )

    def compile(self, name, working_directory, symbolic_path, compiled_model_path):
        """Run the compilation."""
        mlab = self.get_matlab_engine()

        locations = [
            self.amici_mlab_location,
            working_directory,
            self.MATLAB_FUNCTION_DIR,
            self.MATLAB_LIB_DIR,
        ]

        symbolic_function = symbolic_path.with_suffix("").name
        with matlab_path_context(mlab, locations):
            _ = call_matlab_silenced(
                mlab.run_Amiwrap,
                name,
                symbolic_function,
                str(compiled_model_path),
                self.settings["generate_hessian"],
                # Matlab engine settings
                nargout=0,
                silence_std_out=self.settings["silence_output"],
                silence_std_err=self.settings["silence_output"],
            )


class AmiciSimulator(Step):
    # TODO: Expose more Amici settings
    default_settings = {
        # The sensitivity order that should be included (0/1/2)
        "sensi": 0,
        # Whether to compress the h5 output (0 for no compression to 9 for max compression).
        "deflate": 4,
        # Enable shuffle in the h5 output to aid compression.
        "shuffle": True,
        # Either set these three for calculating the timesteps.
        "t_start": None,
        "t_step": None,
        "t_end": None,
        # Or just provide an array of predetermined timesteps.
        "t_out": None,
        # ODE simulation tolerances
        "atol": 1e-16,
        "rtol": 1e-8,
        # ODE simulation maximum amount of steps.
        "maxsteps": 1e6,
        # pscale: either single value of 'lin', 'log', or 'auto'
        # or an list of values for each parameter.
        "pscale": "auto",
        "silence_output": True,
    }
    ami_settings = {"sensi", "atol", "rtol", "maxsteps"}
    h5_settings = {"deflate", "shuffle"}

    def __init__(
        self, amici_mlab_location, mlab_engine_instance, use_cache, **settings
    ):
        """Simulate a compiled Amici model using Amici.

        Required inputs to run are:
            'compiled_model': A compiled Amici model file (i.e. simulate_{model_name}.m)
            'model_description': A model description file to annotate the output and map the
             parameters.
            'conditions': A model condition file.
        Outputs are:
            - Model simulations (./simulations.h5)
              See ../matlab/write_AmiData.m for details on the hdf5 structure.
            - parameter scaling (./parameter_scaling.json).
              Whether parameters were considered to be in logarithmic or linear scaling.
        """
        super().__init__(mlab_engine_instance, use_cache, **settings)
        self.amici_mlab_location = amici_mlab_location
        self._dir_name = "amici_simulator"

    def _run(self, working_directory, infiles):
        """Set up for and run the simulation."""
        compiled_model_path = infiles["compiled_model"]
        model_description_path = infiles["model_description"]
        conditions = ModelConditions.from_hdf(infiles["conditions"])

        result_path = working_directory / "simulations.h5"
        parameter_scaling_path = working_directory / "parameter_scaling.json"
        t_out = self.get_t_out()
        self.simulate(
            compiled_model_path,
            model_description_path,
            conditions,
            t_out,
            result_path,
            parameter_scaling_path,
        )
        return [result_path, parameter_scaling_path]

    def get_t_out(self):
        """Get the simulation output times array based on the settings."""
        t_start, t_step, t_end, t_out = (
            self.settings["t_start"],
            self.settings["t_step"],
            self.settings["t_end"],
            self.settings["t_out"],
        )
        seperate_t_set = all(i is not None for i in (t_start, t_step, t_end))
        t_out_set = t_out is not False and t_out is not None

        if seperate_t_set and not t_out_set:
            return np.arange(t_start, t_end + t_step, t_step)
        elif t_out_set and not seperate_t_set:
            return np.array(t_out)
        else:
            raise ValueError(
                "Either all of (t_start, t_step and t_end) must be set, or t_out."
            )

    def simulate(
        self,
        compiled_model_path,
        model_description_path,
        conditions,
        t_out,
        result_path,
        parameter_scaling_path,
    ):
        """Run the simulation."""
        mlab = self.get_matlab_engine()

        locations = [
            self.amici_mlab_location,
            compiled_model_path.parent,
            self.MATLAB_FUNCTION_DIR,
            self.MATLAB_LIB_DIR,
        ]

        # Only pass through settings for amici and hdf5 storage settings.
        settings = {"ami_settings": {}, "h5_settings": {}}
        for key in self.ami_settings:
            settings["ami_settings"][key] = self.settings[key]
        for key in self.h5_settings:
            settings["h5_settings"][key] = self.settings[key]

        # Only select what we need.
        with open(model_description_path, "r") as f:
            description = json.load(f)
        parameter_values = conditions.parameter_values[description["parameters"]]
        initial_concentrations = conditions.initial_concentrations[
            description["states"]
        ]

        # Set parameter scaling.
        if self.settings["pscale"] == "auto":
            settings["ami_settings"]["pscale"] = []
            for parameter in description["parameters"]:
                log_scaled = parameter_is_log_scaled(parameter, False)
                settings["ami_settings"]["pscale"].append(1 if log_scaled else 0)
        elif self.settings["pscale"] == "lin":
            settings["ami_settings"]["pscale"] = [0 for i in description["parameters"]]
        elif self.settings["pscale"] == "log":
            settings["ami_settings"]["pscale"] = [1 for i in description["parameters"]]
        else:
            settings["ami_settings"]["pscale"] = [
                1 if i == "log" else 0 for i in settings["pscale"]
            ]
        assert len(settings["ami_settings"]["pscale"]) == len(description["parameters"])
        with open(parameter_scaling_path, "w") as outfile:
            pscale = {
                "pscale": dict(
                    zip(description["parameters"], settings["ami_settings"]["pscale"])
                )
            }
            json.dump(pscale, outfile, indent=2, sort_keys=True)

        model_path = compiled_model_path.with_suffix("").name
        result_path = str(result_path)
        model_description_path = str(model_description_path)
        with matlab_path_context(mlab, locations):
            _ = call_matlab_silenced(
                mlab.run_Amici,
                model_path,
                result_path,
                model_description_path,
                matlab.double(parameter_values.values.tolist()),
                matlab.double(initial_concentrations.values.tolist()),
                matlab.double(t_out.tolist()),
                settings,
                # Matlab engine settings
                nargout=0,
                silence_std_out=self.settings["silence_output"],
                silence_std_err=self.settings["silence_output"],
            )

