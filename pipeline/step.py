"""Base Step class to derive other pipeline steps from.

Handles Matlab setup, saving settings and caching.

Child classes should:
    1. Implement the _run method, which accepts:
        1. The working directory
        2. A dictionary of absolute file paths
    2. Make sure all outputs are saved as files and return from _run.
       Paths will be automatically converted to absolute paths.
       Note: Make sure files are only dependent on the input and not on externals
       such as random parameters or the time.
Note: If the child class requires a Matlab instance. use self.get_matlab_engine()
      Setup and breakdown are automatically taken care of.
Note: When _run is called, the working directory is automatically set to the
      working directory provided to _run and reset upon exit.
      The same is done for the Matlab working directory, if an instance is
      already active.
Note: If changing a setting does not change the cached result, it can be added
      to ignore_settings, so that it won't invalidate the cache.

Author: Rik van Rosmalen
"""
import time
import json
import pathlib
import os
import itertools
import hashlib
import warnings
import subprocess

import matlab.engine as ml


class CacheWarning(UserWarning):
    """Custom warning type that warns about cache failures."""
    pass


class Step(object):
    default_settings = {}
    ignore_settings = {'silence_output'}

    MATLAB_FUNCTION_DIR = pathlib.Path(__file__).resolve().parent / 'matlab'
    MATLAB_LIB_DIR = MATLAB_FUNCTION_DIR / 'lib'

    def __init__(self, mlab_engine_instance=None, use_cache=True, **settings):
        """Inherit from this base step class.

        Provides functionality for caching, writing settings and obtaining
        a Matlab instance.

        :param mlab_engine_instance: A Matlab instance that can be used when
        running the step.
        :type mlab_engine_instance: matlab.engine instance
        :param use_cache: Use cached values from this step when possible
        :type use_cache: bool
        :param settings: Additional settings for the step.
        :type settings: dict
        """
        self.settings = dict(self.default_settings)
        self.settings.update(settings)
        self._mlab_engine_instance = mlab_engine_instance
        self._mlab_engine_instance_owned_by_self = False
        self.use_cache = use_cache

        # Name for creating a sub-directory
        self._dir_name = type(self).__name__.lower()

        self.settings_file_base = 'settings.json'

    def get_matlab_engine(self):
        """Get a Matlab instance, activating if required."""
        # Do we already have an instance?
        if self._mlab_engine_instance is not None:
            try:
                # If so, double check if it's alive by calling a simple function.
                self._mlab_engine_instance.disp('', nargout=0)
            except ml.RejectedExecutionError:
                pass
            else:
                return self._mlab_engine_instance
        # Create a new one if it's non-existent or dead.
        self._mlab_engine_instance_owned_by_self = True
        self._mlab_engine_instance = ml.start_matlab()
        return self._mlab_engine_instance

    def get_git_hash(self):
        """Get the current hash of the git repository.

        Returns None if git is not installed, times out or gives an error.
        """
        wd = str(pathlib.Path(__file__).resolve().parent)
        try:
            out = subprocess.check_output(
                ['git', 'rev-parse', 'HEAD'],
                shell=False,
                universal_newlines=True,
                timeout=10,
                cwd=wd,
            )
        except (FileNotFoundError, subprocess.CalledProcessError, subprocess.TimeoutExpired):
            warnings.warn("Could not get commit hash from git.")
            return "NA"
        return out.strip()

    def write_settings(self, settings_file, infiles, outfiles):
        """Write a json file consisting of each setting and a file hash of the model."""
        # Note: Git hash is currently only saved for reference, but not checked.
        # Keep in mind there might be uncommited changes as well in the current run.
        base_path = pathlib.Path(settings_file).resolve().parent
        json_content = {'settings': self.settings,
                        'git-hash': self.get_git_hash(),
                        'updated': time.strftime('%A - %d/%m/%Y - %X (%Z)'),
                        'base_path': str(base_path)}

        input_hashes = {}
        for file in sorted(infiles):
            hash_type, hash_value = self._get_file_hash(file)
            description = {'hash_type': hash_type,
                           'hash_value': hash_value}
            path = os.path.relpath(file, base_path)
            input_hashes[path] = description
        json_content['input_hashes'] = input_hashes

        output_hashes = {}
        for i, file in enumerate(outfiles):
            hash_type, hash_value = self._get_file_hash(file)
            # Save the output order since the dictionairy <-> json round trip does not guarantee
            # anything! (Even if we wouldn't be sorting output files!)
            description = {'hash_type': hash_type,
                           'hash_value': hash_value,
                           'output_order': i}
            path = os.path.relpath(file, base_path)
            output_hashes[path] = description
        json_content['output_hashes'] = output_hashes

        with open(settings_file, 'w') as f:
            json.dump(json_content, f, indent=2, sort_keys=True)

    def verify_cache(self, settings_file, infiles):
        """Verify that the settings and the model are still equivalent."""
        # Note: Git hash is currently only saved, but not checked.
        try:
            with open(settings_file, 'r') as f:
                previous_settings = json.load(f)
        except FileNotFoundError:
            warnings.warn(CacheWarning("Didn't find cache file."))
            return False
        except json.JSONDecodeError:
            warnings.warn(CacheWarning("Couldn't read cache file as json."))
            return False

        base_path = settings_file.resolve().parent
        if previous_settings['base_path'] != str(base_path):
            warnings.warn(CacheWarning("Absolute base path was modified. "
                                       "Perhaps files were moved? Continuing..."))
        infiles_relative = [os.path.relpath(file, base_path) for file in infiles]

        # Check if input file names match
        if set(str(i) for i in infiles_relative) != set(previous_settings['input_hashes'].keys()):
            diff = (set(str(i) for i in infiles_relative)
                    .symmetric_difference(set(previous_settings['input_hashes'].keys())))
            warnings.warn(CacheWarning("Input file names mismatch:\n\t{}".format(diff)))
            return False

        # Check input hashes
        for file, description in previous_settings['input_hashes'].items():
            new_path = (base_path / file).resolve()
            _, h = self._get_file_hash(new_path, description['hash_type'])
            if h != description['hash_value']:
                warnings.warn(CacheWarning("Input file hash mismatch:\n\t{}".format(new_path)))
                return False

        # Check settings
        for key in set(previous_settings['settings'].keys()) | set(self.settings.keys()):
            if key in self.ignore_settings:
                continue
            try:
                if previous_settings['settings'][key] != self.settings[key]:
                    warnings.warn("Settings mismatch: {}".format(key))
                    return False
            except KeyError:
                warnings.warn(CacheWarning("Unknown key found:\n\t{}".format(key)))
                return False

        # For each output file, check if the hash is still correct.
        outfiles = []
        for file, description in previous_settings['output_hashes'].items():
            new_path = (base_path / file).resolve()
            if new_path.exists():
                _, h = self._get_file_hash(new_path, description['hash_type'])
                if h != description['hash_value']:
                    warnings.warn("Output hash mismatch: {}".format(new_path))
                    return False
            else:
                warnings.warn(CacheWarning("Invalid path for saved output:\n\t{}".format(new_path)))
                return False
            outfiles.append((new_path, description['output_order']))

        # Make sure to return the files in the original order.
        outfiles = [p for p, idx in sorted(outfiles, key=lambda x: x[1])]

        # If everything is still correct, report back that the cache is still valid.
        # Also return the output files.
        return True, outfiles

    def _get_file_hash(self, filepath, hash_algorithm='md5', chunk_size=4096):
        """Calculate a hash value for the file or folder in the file path.

        If its a folder, the hash corresponds to the combined hash value of the
        sorted top level files, but will *not* recurse into sub directories.
        """
        if hash_algorithm not in hashlib.algorithms_guaranteed:
            warnings.warn("Using an unportable hash function: {}".format(hash_algorithm))

        h = hashlib.new(hash_algorithm)
        if filepath.is_file():
            with open(filepath, 'rb') as f:
                for chunk in iter(lambda: f.read(chunk_size), b""):
                    h.update(chunk)
        elif filepath.is_dir():
            for subpath in sorted(filepath.iterdir()):
                if not subpath.is_file():
                    continue
                with open(subpath, 'rb') as f:
                    for chunk in iter(lambda: f.read(chunk_size), b""):
                        h.update(chunk)
        else:
            raise ValueError("Cannot hash {}: not a valid file or directory!"
                             .format(filepath))
        return h.name, h.hexdigest()

    def run(self, working_directory, **infiles):
        """Run the pipeline step."""
        working_directory = working_directory.resolve()
        settings_file = working_directory / self.settings_file_base

        infiles = {key: (pathlib.Path(value).resolve() if not isinstance(value, list) else
                         [pathlib.Path(v).resolve() for v in value])
                   for key, value in infiles.items() if value is not None}
        infiles_flat = list(itertools.chain.from_iterable(v if isinstance(v, list) else [v]
                                                          for v in infiles.values()))

        print("Running step {} in directory:\n\t{}".format(type(self).__name__,
                                                           working_directory))
        # Check cache
        if self.use_cache:
            print("Checking cache ... ", end='')
            out = self.verify_cache(settings_file, infiles_flat)
            if out:
                _, outfiles = out
                print("-> Cache verified - skipping run.")
                return outfiles
            else:
                print("-> Not verified - (re)-running.")

        # Go to working directory. (This is useful if some of the tools define relative paths
        # we can't easily modify.)
        cwd = pathlib.Path(os.getcwd())
        try:
            os.chdir(working_directory)
            # If there is an Matlab instance active, go to the working directory.
            # Note that if we instantiate a Matlab engine instance inside the run step, it
            # will already have the correct working directory since it inherits the python one.
            if self._mlab_engine_instance is not None:
                self._mlab_engine_instance.cd(str(working_directory.resolve()))

            # Call the subclass implemented run function.
            outfiles = self._run(working_directory, infiles)
            # Close Matlab session if we opened it ourselves.
            if self._mlab_engine_instance_owned_by_self:
                try:
                    self._mlab_engine_instance.exit()
                except ml.RejectedExecutionError:
                    # It's already dead!
                    pass
                # No matter what happens, mark it as inactive again.
                finally:
                    self._mlab_engine_instance = None
                    self._mlab_engine_instance_owned_by_self = False
            outfiles = [i.resolve() for i in outfiles]
        finally:
            # Return to original directory.
            os.chdir(cwd)
            # Same for the Matlab instance if it's active
            if self._mlab_engine_instance is not None:
                # TODO: This crashes if we use our own created MatLab session
                try:
                    self._mlab_engine_instance.cd(str(cwd.resolve()))
                except ml.RejectedExecutionError:
                    # Is this a bug if this happens? (TODO)
                    pass

        # Update cache
        self.write_settings(settings_file, infiles_flat, outfiles)
        return outfiles

    def _run(self, working_directory, infiles):
        """Run the step.

        This should be implemented by the child class.
        """
        raise NotImplementedError

    # TODO: Think about having an async run alternative.
    # So all steps could run at the same time (as soon as required outputs are updated.)
    # Perhaps some sort of locking mechanism could be used.
