"""Pipeline steps for sbml file handling.

Implements:
    - Parameter Balancing (Lubitz et al 2010)
    - Model kineticization
    - Model parameterization from a steady state flux value.
Author: Rik van Rosmalen
"""
import collections
import json
import pathlib
import warnings

import numpy as np
import pandas as pd
import sympy

import gurobipy as grb
from gurobipy import GRB

import symbolicSBML

from symbolicSBML import Parameters, SBMLModel
from parameter_balancer import ParameterData, Balancer, BalancedParameterData
from parameter_balancer import to_log_mean, NA_IDENTIFIER

from .step import Step
from .util import load_ode_system, derive_parameter


class ModelBalancer(Step):
    default_settings = {
        # Physical constants.
        "R": 8.314 / 1000.0,
        "T": 300,
        # Augment with pseudo data (priors for derived values as data.)
        "augment": False,
        # Ignore any reactions or metabolites when determining the model structure.
        "ignore_reactions": [],
        "ignore_metabolites": [],
        "silence_output": True,
    }
    ignore_settings = {"silence_output"}

    def __init__(self, use_cache=True, **settings):
        """Run balancing based on the model structure, priors, known data and cooperativities.

        Required inputs to run are:
            'model': A sbml file defining the stoichiometry structure.
            'priors': A json file describing the prior distributions for the basic
                parameters as {parameter type:  [mu, sigma]}
            'parameter_data': An csv file with parameter data.
            'cooperativities': A json file with reaction cooperativities.
        Outputs are:
            - Balanced parameters (./balancing_results.npz). This compressed numpy archive
              contains the relevant matrices to reconstruct the balanced parameters.
         """
        super().__init__(mlab_engine_instance=None, use_cache=use_cache, **settings)

    def _run(self, working_directory, infiles):
        """Set up and run the balancing procedure."""
        model_file = infiles["model"]
        prior_file = infiles["priors"]
        data_file = infiles["parameter_data"]
        cooperativity_file = infiles["cooperativities"]

        balancing_results_path = working_directory / pathlib.Path(
            "balancing_results.npz"
        )

        structure = SBMLModel(
            model_file,
            self.settings["ignore_reactions"],
            self.settings["ignore_metabolites"],
        )
        priors = json.loads(prior_file.read_text())
        data = ParameterData.from_dataframe(pd.read_csv(data_file))
        cooperativities = json.loads(cooperativity_file.read_text())

        balancer = Balancer(
            priors,
            data,
            structure,
            self.settings["T"],
            self.settings["R"],
            self.settings["augment"],
            cooperativities,
        )
        result = balancer.balance()
        result.save(balancing_results_path)
        return [balancing_results_path]


class ModelKineticizer(Step):
    default_settings = {
        "rate_law_type": "modular",
        "rate_law_args": {"parametrisation": "weg", "sub_rate_law": "CM"},
        # Reactions to be ignored
        "ignore_reactions": [],
        # Reactions to be fixed at a certain flux value {reaction_id: value}
        "fixed_reactions": {},
        # Metabolites that should be fixed (by turning it into parameters)
        # can be marked as 'keep' to keep the current concentration or instead
        # be given a specific value. [metabolite_id]
        "fixed_metabolites": [],
        # Reactions with a custom rate law. {reaction: expression}
        "replace_reactions": {},
        # Additional inputs that should be added to a metabolite {metabolite: expression}
        "additional_input_reactions": {},
        # for reactions that should have an extra metabolite added for improved
        # thermodynamic balances.
        # List of [[reaction, side (reactant / product), pool name, add as observable?]]
        "add_abstract_metabolites": [],
        # Raise a warning (True) or error (False) when a reaction is missing for the
        # abstract metabolite to be added.
        "allow_missing_abstract_metabolites": True,
        # Limit the total product/reactant stoichiometry to avoid unrealistically high values.
        "limit_total_lumped_stoichiometry": True,
        "max_reaction_order": 2,
        # Reactions to use a simplified rate law.
        "simplify_reactions": [],
        "simplify_lumped_reactions": True,
        "simplified_rate_law_type": "modular",
        "simplified_rate_law_args": {"parametrisation": "weg", "sub_rate_law": "PM"},
        "positive_fractional_powers": False,
        # Scale all fluxes with a biomass concentration to represent growth.
        "biomass_scaling": False,
        # Add an arbitrary scaling factor (useful to shut-off the model at a certain point.)
        "scaling_factor": False,
        # Parameters to be merged.
        "merge_parameters": ["enzyme_rate", "km_merged", "km_rate"],
        "cythonize": False,
        "cythonize_settings": {},
        "silence_output": True,
    }
    ignore_settings = {"silence_output", "validate"}

    def __init__(self, use_cache=True, **settings):
        """Convert a model structure to a kinetic ode model using standardized rate laws.

        Required inputs to run are:
            'model': An SBML model file.
            'lumped': (Optional) A list of lumpings performed by network reduction tools.
        Outputs are:
            - kinetic model (./kinetic_model.xml) in SBML format.
            - cooperativities (./cooperativities.json) Reaction cooperativities.
            - kinetic model cython module. (./cython/ode/*.{so|pyd})
              Optional, dependents on cythonize settings. A compiled (C) python extension
              module that allows for simulating the ODE model from python with high performance.
         """
        super().__init__(mlab_engine_instance=None, use_cache=use_cache, **settings)

    def _run(self, working_directory, infiles):
        """Set-up and run the kinetization procedure."""
        model_file = infiles["model"]
        try:
            deletions_and_lumps = infiles["lumped"]
        except KeyError:
            deletions_and_lumps = False

        kinetic_model_file = working_directory / "kinetic_model.xml"
        cooperativity_file = working_directory / "cooperativities.json"

        # Load structure
        structure = symbolicSBML.SBMLModel(
            model_file, ignore_reactions=self.settings["ignore_reactions"]
        )

        # Load deletions and lumps
        if deletions_and_lumps:
            deletions_and_lumps = json.loads(deletions_and_lumps.read_text())
            removed_metabolites = set(deletions_and_lumps["removed"]["metabolites"])
            removed_reactions = set(deletions_and_lumps["removed"]["reactions"])
            lumps = deletions_and_lumps["lumpings"]
            for reactions in lumps.values():
                removed_reactions |= set(reactions)
        else:
            removed_metabolites = set()
            removed_reactions = set()
            lumps = {}

        # Lump reactions
        structure, lumped_reactions = self.lump_reactions(
            structure, lumps, removed_metabolites
        )

        # Remove reactions and metabolites
        structure = self.remove_reactions(structure, removed_reactions)
        structure = self.remove_metabolites(structure, removed_metabolites)

        # Update structure with abstract metabolites.
        if self.settings["add_abstract_metabolites"]:
            structure = self.add_abstract_metabolites(structure)

        # Add any additional input reactions.
        replace_reactions = self.settings["replace_reactions"].copy()
        if self.settings["additional_input_reactions"]:
            # Update stoichiometry matrix size
            m, r = structure.stoichiometry.shape
            S_new = np.zeros((m, r + len(self.settings["additional_input_reactions"])))
            S_new[:m, :r] = structure.stoichiometry
            enumeration = enumerate(
                self.settings["additional_input_reactions"].items(),
                len(structure.reactions),
            )
            for reaction_idx, (name, (metabolite, expression)) in enumeration:
                # Add a new reaction to the structure
                structure.reactions.append(name)
                # Set correct stoichiometry
                metabolite_idx = structure.metabolite_index[metabolite]
                S_new[metabolite_idx, reaction_idx] = 1
                # Add expression to custom reactions (this will be used later)
                replace_reactions[name] = expression
            structure.stoichiometry = S_new
            structure._reindex()

        # Generate equations
        self.cooperativity_symbol = sympy.Symbol("h", positive=True, real=True)
        simplified_reactions = self.settings["simplify_reactions"][:]
        if self.settings["simplify_lumped_reactions"]:
            # Reactions that we lumped ourself.
            simplified_reactions.extend(lumped_reactions)
            # Reactions that were lumped by network reduction R_lumped_[id].
            simplified_reactions.extend(r for r in structure.reactions if "R_lumped_" in r)
        symbolic_model = self.generate_rate_laws(
            structure, simplified_reactions, replace_reactions
        )
        # Hack to add sbml structure to symbolic model.
        # TODO: Revisit the separation between SBML and Symbolic model
        # to avoid the need for hacks like this.
        symbolic_model.sbml_model = structure

        # Update stoichiometry for kinetics in equations.
        if self.settings["limit_total_lumped_stoichiometry"]:
            cooperativities = self.update_stoichiometries(symbolic_model)
        else:
            cooperativities = {}

        # Clean parameters
        # symbolic_model = self.clean_parameters(symbolic_model)

        # Add optional scaling of the reactions to the biomass.
        if self.settings["biomass_scaling"]:
            biomass = sympy.Symbol(self.settings["biomass_scaling"])
            # Check if the biomass is a metabolite already.
            # We have to add it later because we override the structure.
            add_biomass_metabolite = biomass.name not in symbolic_model.metabolites
            for i, flux in enumerate(symbolic_model.fluxes):
                symbolic_model.fluxes[i] = biomass * flux
        else:
            add_biomass_metabolite = False

        if self.settings["scaling_factor"]:
            scaling_factor = sympy.Symbol(self.settings["scaling_factor"])
            # Check if the scaling_factor is a metabolite already.
            # We have to add it later because we override the structure.
            add_scaling_factor = scaling_factor.name not in symbolic_model.metabolites
            for i, flux in enumerate(symbolic_model.fluxes):
                symbolic_model.fluxes[i] = scaling_factor * flux
        else:
            add_scaling_factor = False

        # Replace fixed metabolites with parameters.
        if self.settings["fixed_metabolites"]:
            fixed_metabolites = self.settings["fixed_metabolites"].copy()
            structure = self.fix_metabolites(structure, fixed_metabolites)
            # Update symbolic model with the new structure.
            # TODO: Fix this when the TODO before is fixed
            # (revisit SBML & Symbolic model separation)
            # Parameters are already added before.
            symbolic_model.metabolites = structure.metabolites
            symbolic_model.stoichiometry = sympy.Matrix(structure.stoichiometry)
            symbolic_model.parameters.extend(fixed_metabolites)
            symbolic_model.observables = structure.observable_terms
            symbolic_model.recreate_symbols()

        # TODO: Clean-up with restructure of SBML / Symbolic model.
        if add_biomass_metabolite:
            n_metabolites, n_reactions = symbolic_model.stoichiometry.shape
            row = sympy.zeros(1, n_reactions)
            symbolic_model.stoichiometry = symbolic_model.stoichiometry.row_insert(
                n_metabolites, row
            )
            symbolic_model.metabolites.append(biomass.name)
            symbolic_model.recreate_symbols()
        if add_scaling_factor:
            n_metabolites, n_reactions = symbolic_model.stoichiometry.shape
            row = sympy.zeros(1, n_reactions)
            symbolic_model.stoichiometry = symbolic_model.stoichiometry.row_insert(
                n_metabolites, row
            )
            symbolic_model.metabolites.append(scaling_factor.name)
            symbolic_model.recreate_symbols()

        # Write outputs.
        symbolic_model.to_sbml(kinetic_model_file)
        cooperativity_file.write_text(
            json.dumps(cooperativities, indent=2, sort_keys=True)
        )

        # Build cython model for simulation.
        if self.settings["cythonize"]:
            cython_module_location = working_directory / "cython" / "ode"
            builder = symbolicSBML.codegen.CythonODEBuilder(
                symbolic_model, **self.settings["cythonize_settings"]
            )
            builder.build(cython_module_location)
            return [kinetic_model_file, cooperativity_file, cython_module_location]
        else:
            return [kinetic_model_file, cooperativity_file]

    def lump_reactions(self, structure, lump_reactions, removed_metabolites):
        """Create lumped reactions and update the model structure."""
        m, r = structure.stoichiometry.shape
        S_new = np.zeros((m, r + len(lump_reactions)))
        S_new[:m, :r] = structure.stoichiometry
        lumped_reactions = []
        for i, lump in enumerate(lump_reactions, start=r):
            # Sum up metabolites
            metabolites = collections.defaultdict(int)
            for reaction in lump:
                for m, s in structure.participating_metabolites(reaction):
                    metabolites[m] += s
            # Add a new reaction
            name = Parameters.sep.join(lump)
            lumped_reactions.append(name)
            structure.reactions.append(name)
            for m, s in metabolites:
                # Remove metabolites summing to 0 stoichiometry or metabolites
                # that are removed from the model.
                if s == 0 or m in removed_metabolites:
                    continue
                else:
                    S_new[structure.metabolite_index[m], r] = s
        return structure, lumped_reactions

    def remove_reactions(self, structure, reactions):
        """Remove reactions and associated fluxes."""
        indeces = []
        for reaction in reactions:
            indeces.append(structure.reaction_index[reaction])
            del structure.fluxes[reaction]

        structure.reactions = [i for i in structure.reactions if i not in reactions]
        structure.stoichiometry = np.delete(structure.stoichiometry, indeces, axis=1)
        structure._reindex()
        return structure

    def remove_metabolites(self, structure, metabolites):
        """Remove metabolites and associated initial concentrations."""
        indeces = []
        for metabolite in metabolites:
            indeces.append(structure.metabolite_index[metabolite])
            del structure.initial_concentrations[metabolite]
            # Delete related observables
            symbol = sympy.Symbol(metabolite)
            for obs, term in structure.observable_terms.copy().items():
                if term.has(symbol):
                    del structure.observable_terms[obs]
                    structure.observables.remove(obs)

        structure.metabolites = [
            i for i in structure.metabolites if i not in metabolites
        ]
        structure.stoichiometry = np.delete(structure.stoichiometry, indeces, axis=0)
        structure._reindex()
        return structure

    def fix_metabolites(self, structure, fixed_metabolites):
        """Fix metabolites as parameters instead."""
        # Remove all the metabolites
        structure = self.remove_metabolites(structure, fixed_metabolites)
        # Add the metabolites as parameters
        for metabolite in fixed_metabolites:
            structure.parameters.append(metabolite)
            structure.parameter_values[metabolite] = 0
        return structure

    def add_abstract_metabolites(self, structure):
        """Add abstract metabolites to the model structure.

        These are useful to prevent thermodynamic imbalances.
        """
        reactions_found = set()
        for reaction, *_ in self.settings["add_abstract_metabolites"]:
            if reaction in structure.reaction_index:
                reactions_found.add(reaction)
            else:
                message = (
                    "Abstract metabolite could not be added because reaction "
                    "({}) is missing.".format(reaction)
                )
                if self.settings["allow_missing_abstract_metabolites"]:
                    warnings.warn(message)
                else:
                    raise ValueError(message)
        # Resize stoichiometry matrix with 1 metabolite for each reaction marked.
        m, r = structure.stoichiometry.shape
        S_new = np.zeros((m + len(reactions_found), r))
        S_new[:m, :r] = structure.stoichiometry

        i_name = 0
        metabolite_index = len(structure.metabolites)
        abstract_metabolites_enumerator = enumerate(
            self.settings["add_abstract_metabolites"]
        )
        for (
            i,
            (reaction, side, name, add_observable),
        ) in abstract_metabolites_enumerator:
            if reaction not in reactions_found:
                metabolite_index -= 1
                continue
            # Add a placeholder metabolite
            r_idx = structure.reaction_index[reaction]
            m_idx = metabolite_index + i
            if side in ("substrate", "reactant") or side == -1:
                S_new[m_idx, r_idx] = -1
            elif side == "product" or side == 1:
                S_new[m_idx, r_idx] = 1
            else:
                raise ValueError(
                    "Invalid side for abstract metabolite in {}".format(reaction)
                )

            if not name:
                name = "abstract_{}".format(i_name)
                i_name += 1
            structure.metabolites.append(name)

            if add_observable:
                o_name = "O_{}".format(name)
                structure.observables.append(o_name)
                structure.observable_terms[o_name] = sympy.Symbol(name)

        # Replace the stoichiometry, and repair the indices.
        structure.stoichiometry = S_new
        structure._reindex()
        return structure

    def generate_rate_laws(self, structure, simplified_reactions, replace_reactions):
        """Generate the right rate law for each reaction."""
        rate_law_args_default = self.settings["rate_law_args"].copy()
        rate_law_type_default = self.settings["rate_law_type"]
        rate_law_args_simple = self.settings["simplified_rate_law_args"].copy()
        rate_law_type_simple = self.settings["simplified_rate_law_type"]

        # TODO: Fix hard coding
        rate_law_args_default["substitutions"] = []
        if (
            self.settings["rate_law_args"]["sub_rate_law"] == "CM"
            and "enzyme_rate" in self.settings["merge_parameters"]
        ):
            rate_law_args_default["substitutions"].append("enzyme_rate")
        rate_law_args_simple["substitutions"] = self.settings["merge_parameters"]

        if self.settings["limit_total_lumped_stoichiometry"]:
            cooperativity_symbol = self.cooperativity_symbol
            rate_law_args_default["cooperativities"] = cooperativity_symbol
            rate_law_args_simple["cooperativities"] = cooperativity_symbol
        else:
            cooperativity_symbol = 1

        all_fluxes = {}
        all_parameters = set()
        for reaction in structure.reactions:
            # Reactions that are fixed at a certain flux.
            if reaction in self.settings["fixed_reactions"]:
                flux = self.settings["fixed_reactions"][reaction]
                parameters = set()

            # Reactions that are fully replaced.
            elif reaction in replace_reactions:
                flux = sympy.sympify(replace_reactions[reaction])
                parameters = {
                    i.name
                    for i in flux.free_symbols
                    if i.name not in structure.metabolites
                }

            # Reactions that need kinetics to be generated.
            else:
                # Get the right slice of the stoichiometry matrix
                r_idx = structure.reaction_index[reaction]
                stoichiometry = np.atleast_2d(structure.stoichiometry[:, r_idx]).T

                # Simple or normal rate law?
                if reaction in simplified_reactions:
                    rate_law_type = rate_law_type_simple
                    rate_law_args = rate_law_args_simple
                else:
                    rate_law_type = rate_law_type_default
                    rate_law_args = rate_law_args_default

                # Generate kinetics
                fluxes, parameters = symbolicSBML.generate_rate_laws(
                    structure.metabolites,
                    [reaction],
                    stoichiometry,
                    rate_law_type,
                    **rate_law_args
                )
                flux = fluxes[reaction]
                parameters = set(parameters)

                # If wanted replace pow(x, y) with pow(max(x, 0), y)
                # this prevents fractional powers from crashing with small negative values.
                if self.settings["positive_fractional_powers"]:
                    replace = {}
                    for term in sympy.preorder_traversal(flux):
                        if (
                            term.is_Pow
                            and term.base.is_Symbol
                            and not term.exp.is_Integer
                            and term.base.name in structure.metabolites
                        ):
                            new_term = sympy.Pow(
                                sympy.Max(term.base, sympy.Integer(0)), term.exp
                            )
                            replace[term] = new_term
                    flux = flux.xreplace(replace)

            # Update the overall reaction flux equation and the parameter set.
            all_fluxes[reaction] = flux
            all_parameters |= parameters

        # Convert to symbolic model
        symbolic_model = symbolicSBML.SymbolicModel(
            structure.metabolites,
            structure.reactions,
            sorted(all_parameters),
            structure.stoichiometry,
            fluxes=all_fluxes,
            observables=structure.observable_terms,
        )

        return symbolic_model

    def update_stoichiometries(self, symbolic_model):
        """Update reactions with unrealistic stoichiometries.

        This uses a cooperative binding model of the modular rate law as
        described by Liebermeister et al. 2010. This reduces the effective
        kinetic stoichiometry, but keeps the thermodynamic balance intact.
        """
        cooperativities = {}
        for i, reaction in enumerate(symbolic_model.reactions):
            metabolites = symbolic_model.sbml_model.participating_metabolites(reaction)
            prod_order = sum(i[1] for i in metabolites if i[1] > 0)
            sub_order = sum(i[1] for i in metabolites if i[1] < 0)
            order = max(prod_order, sub_order)
            if order == int(order):
                order = int(order)
            if order > self.settings["max_reaction_order"]:
                c = sympy.Integer(self.settings["max_reaction_order"]) / order
                cooperativities[reaction] = float(c)
            else:
                c = sympy.Integer(1)
            flux = symbolic_model.fluxes[i].subs({self.cooperativity_symbol: c})
            symbolic_model.fluxes[i] = flux

        # Don't forget to remove the symbol from the parameters.
        symbolic_model.parameters.remove(self.cooperativity_symbol.name)
        symbolic_model.recreate_symbols()
        return cooperativities


class SteadyStateModelParameterizer(Step):
    default_settings = {
        # Physical constants for balancing.
        "R": 8.314 / 1000.0,
        "T": 300,
        # Reactions that will be ignored for everything.
        "ignore_reactions": [],
        # Reactions that will be ignored during balancing only.
        "ignore_reactions_balancing": [],
        # Metabolites that will be ignored during balancing.
        "ignore_metabolites_balancing": [],
        # These metabolites will be treated as parameters.
        "fixed_metabolites": [],
        # Extra parameters that are independent of the balancing procedure,
        # but should be added to the final model.
        "extra_parameters": {},
        # Absolute fluxes below this are seen as zero flux.
        # Set this to the LP optimization tolerance.
        "flux_zero_tolerance": 1e-6,
        # Lowest A seen as a driving force in the right direction.
        "A_tol": 0.1,
        # Maximum driving force constraints.
        "A_max": 100,
        # Amount of standard deviations c or mu can diverge.
        "constraint_factor": 2,
        # Placeholder for concentrations of 0 that would be infeasible in log space.
        "zero_concentration": 1e-9,
        # Relative standard deviation of the optimized mu, c and A values.
        "optimized_directionality_data_error": {
            Parameters.c: 0.01,
            Parameters.mu: 0.01,
            Parameters.A: 0.01,
        },
        # Constrain A of inactive reactions.
        "constrain_inactive_reactions": False,
        # Which parameter should be scaled to correct the flux?
        # Usually either enzyme concentration (u) or enzyme velocity (kv) or the combination (u_v).
        "default_scaling_parameter": Parameters.u,
        # For specific reactions you can overwrite the scaling parameter as well.
        # Enter None as key for no scaling to take place.
        "scaling_parameters": {},
        # Maximum amount of samples taken to find a flux solution in the correct direction.
        "max_samples": 1000,
        # Number of samples that should be tried in a single batch.
        "sample_batch_size": 1000,
        # Maximum error in the sum of absolute fluxes that is still
        # considered to be a valid steady state.
        "ss_error_tolerance": 1e-6,
        # In the pre and post optimization balancing steps, augment the data with
        # priors for dependent quantities or not.
        "augment": {"pre": True, "post": True},
        "silence_output": True,
    }
    ignore_settings = {"silence_output", "sample_batch_size"}

    def __init__(self, use_cache=True, **settings):
        """Create parameter sets for the kinetic model that mimic a certain flux solution.

        Required inputs to run are:
            'model': An SBML model file containing the ODE system used to calculate fluxes values.
            'final_model': An SBML model file containing the ODE system that will use the
                resulting parameters.
            'priors': A json file describing the prior distributions for the basic
                parameters as {parameter type:  [mu, sigma]}
            'parameter_data': An csv file with parameter data.
            'cooperativities': A json file with reaction cooperativities.
            'fluxes': A csv file containing the flux solution to be mimicked.
            'cython_ode': A folder containing a python extension module to simulate the model.
                (This should the model in 'model', not in 'final_model')
        Outputs are:
            - Feasible parameter set (./all_feasible_parameters.h5)
            - Optimal parameter set (./parameter_set.csv)
            - Parameterized version of the final model (./parametrized_model.xml)
            - Various debug outputs (./*.debug.csv)
        """
        super().__init__(mlab_engine_instance=None, use_cache=use_cache, **settings)

    def _run(self, working_directory, infiles):
        """Create a parameter set for the kinetic model that mimics a certain flux solution."""
        model_file = infiles["model"]
        final_model_file = infiles["final_model"]
        prior_file = infiles["priors"]
        data_file = infiles["parameter_data"]
        cooperativity_file = infiles["cooperativities"]
        flux_file = infiles["fluxes"]
        cython_module_location = infiles["cython_ode"]

        # The prior file is not used here but passed directly to the balancer when required.
        data = ParameterData.from_dataframe(pd.read_csv(data_file))
        cooperativities = json.loads(cooperativity_file.read_text())
        fluxes = pd.read_csv(flux_file, index_col=0)

        ode_system = load_ode_system(cython_module_location)

        # We have two types of ignored reactions.
        # the first is ignored for the balancing, qp optimization AND the model structure creation.
        # the second is only ignored for the balancing and the qp directionality optimization.
        ignore_reactions = (
            self.settings["ignore_reactions"]
            + self.settings["ignore_reactions_balancing"]
        )
        ignore_metabolites_balancing = self.settings["ignore_metabolites_balancing"]

        # For the intermediate structure we don't want any of ignored reactions or metabolites.
        structure = SBMLModel(
            model_file, ignore_reactions, ignore_metabolites_balancing
        )
        # For the final structure we do want the second class to be included!
        final_structure = SBMLModel(final_model_file, self.settings["ignore_reactions"])

        # Remove reactions that should be ignored for the parametrization part
        # and update the file.
        for reaction in ignore_reactions:
            if reaction in cooperativities:
                del cooperativities[reaction]
        cooperativity_file = working_directory / "cooperativities_filtered.json"
        cooperativity_file.write_text(
            json.dumps(cooperativities, indent=2, sort_keys=True)
        )

        # Derive directions from fluxes, removing all ignored reactions
        directions = np.sign(fluxes[~fluxes.index.isin(ignore_reactions)])
        directions[np.abs(fluxes) < self.settings["flux_zero_tolerance"]] = 0

        # For the actual fluxes only ignore the reactions in ignore reactions
        # but not those in the ignore for balancing reactions group.
        fluxes = fluxes[~fluxes.index.isin(self.settings["ignore_reactions"])]

        # Create our own balancer object.
        balancer = ModelBalancer(
            use_cache=False,
            augment=False,
            R=self.settings["R"],
            T=self.settings["T"],
            ignore_reactions=ignore_reactions,
            ignore_metabolites=ignore_metabolites_balancing,
            silence_output=self.settings["silence_output"],
        )

        # Filter to save only thermodynamic data and run pre-balancing
        pre_balancing_directory = working_directory / "pre_balancing"
        pre_balancing_directory.mkdir(exist_ok=True)
        thermodynamic_data_file = pre_balancing_directory / "parameter_data.csv"
        selection = data.df[data.df.parameter_type.isin(Parameters.thermodynamic)]
        selection.to_csv(thermodynamic_data_file, index=False)
        balanced_data = self.balance(
            balancer,
            pre_balancing_directory,
            model_file,
            prior_file,
            thermodynamic_data_file,
            cooperativity_file,
            augment=self.settings["augment"]["pre"],
        )

        # Convert balanced data into new input data.
        required = (Parameters.c, Parameters.mu, Parameters.A)
        balanced_data_input = balanced_data.to_frame(
            median=False, mean=True, noRT=True
        ).T.reset_index()
        balanced_data_input.columns = (
            "parameter_type",
            "metabolite",
            "reaction",
            "mean",
            "sd",
        )
        balanced_data_input = balanced_data_input[
            balanced_data_input.parameter_type.isin(required)
        ]
        balanced_data_input = ParameterData.from_dataframe(balanced_data_input)
        # Debug info
        balanced_data.to_frame().T.to_csv(working_directory / "pre_balancing.debug.csv")

        # Optimize the directionality
        directionality_data, debug_df = self.optimize_directionality(
            balanced_data_input, structure, directions
        )
        debug_df.to_csv(working_directory / "qp_terms.debug.csv", header=True)

        # Merge both data sets
        all_data = [
            data.df[~data.df.parameter_type.isin(required)],
            directionality_data[directionality_data.parameter_type.isin(required)],
        ]
        merged_df = pd.concat(all_data)

        # Balance with the new data
        post_balancing_directory = working_directory / "post_balancing"
        post_balancing_directory.mkdir(exist_ok=True)
        merged_data_file = post_balancing_directory / "parameter_data.csv"
        merged_df.to_csv(merged_data_file, index=False)

        final_balanced_data = self.balance(
            balancer,
            post_balancing_directory,
            model_file,
            prior_file,
            merged_data_file,
            cooperativity_file,
            augment=self.settings["augment"]["post"],
        )
        # Debug info
        final_balanced_data.to_frame().T.to_csv(
            working_directory / "post_balancing.debug.csv"
        )

        # Compare final balanced with directionality for debugging.
        final_A = (
            final_balanced_data.to_frame()
            .loc[:, pd.IndexSlice[Parameters.A, :, :]]
            .T.reset_index()
            .drop(["level_0", "level_1"], axis=1)
            .set_index("level_2")
        )
        final_A.index.name = "reaction"
        optimized_A = (
            directionality_data[directionality_data.parameter_type == Parameters.A]
            .drop(["parameter_type", "metabolite"], axis=1)
            .set_index("reaction")
        )
        compare_A = pd.concat([optimized_A, final_A], axis=1, sort=False)
        compare_A.columns = ["opt_median", "opt_sd", "final_mean", "final_sd"]
        compare_A = compare_A.assign(
            correct_sign=np.sign(compare_A.opt_median) == np.sign(compare_A.final_mean)
        )
        compare_A.index.name = "reaction"
        # Debug info
        compare_A.to_csv(working_directory / "directionality_comparison.debug.csv")

        # Compare final parameters to data for debugging.
        debug_data = data.df.copy()
        # for each entry, look up the corresponding result in balanced, optimized and final data.
        debug_entries = np.zeros((len(debug_data), 9)) + np.NaN
        b_df = balanced_data.to_frame(median=False, mean=True, noRT=True)
        d_df = directionality_data.set_index(
            ["parameter_type", "metabolite", "reaction"]
        ).T
        f_df = final_balanced_data.to_frame(median=False, mean=True, noRT=True)
        for i, p, m, r, mean, sd in debug_data.itertuples():
            try:
                debug_entries[i, 0:2] = b_df[(p, m, r)]
                debug_entries[i, 2] = (mean - debug_entries[i, 0]) / sd
            except KeyError:
                pass
            try:
                debug_entries[i, 3:5] = d_df[(p, m, r)]
                debug_entries[i, 5] = (mean - debug_entries[i, 3]) / sd
            except KeyError:
                pass
            try:
                debug_entries[i, 6:8] = f_df[(p, m, r)]
                debug_entries[i, 8] = (mean - debug_entries[i, 6]) / sd
            except KeyError:
                pass
        debug_data = debug_data.assign(
            before_mean=debug_entries[:, 0],
            before_sd=debug_entries[:, 1],
            before_z=debug_entries[:, 2],
            dir_mean=debug_entries[:, 3],
            dir_sd=debug_entries[:, 4],
            dir_z=debug_entries[:, 5],
            after_mean=debug_entries[:, 6],
            after_sd=debug_entries[:, 7],
            after_z=debug_entries[:, 8],
            optimized_z_score=(
                np.abs(debug_entries[:, 0] - debug_entries[:, 3]) / debug_entries[:, 1]
            ),
        )
        debug_data.to_csv(working_directory / "data_propagation.debug.csv")

        # Sample from flux solution
        parameter_sets, errors = self.sample_synced_flux(
            final_balanced_data, fluxes, directions, ode_system, working_directory
        )

        # Save all parameter sets.
        all_parameter_sets_file = working_directory / "all_feasible_parameters.h5"
        # TODO: Fix this repetition
        joined_columns = pd.Index(
            [
                Parameters.sep.join((j for j in i if j and str(j) != "nan"))
                for i in parameter_sets.columns.tolist()
            ]
        )
        # Keep if either a base parameter.
        if self.settings["default_scaling_parameter"] not in (
            Parameters.kv,
            Parameters.u,
        ):
            base = (Parameters.base - {Parameters.kv, Parameters.u}) | {
                self.settings["default_scaling_parameter"]
            }
        base_set = parameter_sets.columns.get_level_values(0).isin(base)
        # Or used as an alternative scaling factor
        alternative = joined_columns.isin(self.settings["scaling_parameters"].values())
        # However, in that case we do NOT want to keep u or kv, since they weren't updated.
        skip_for_alternative = parameter_sets.columns.get_level_values(2).isin(
            self.settings["scaling_parameters"].keys()
        )
        # This should result in all parameters + initial concentrations + R + T
        # except for any extra fixed parameters or fixed metabolites.
        parameter_sets.loc[:, (base_set & ~skip_for_alternative) | alternative].to_hdf(
            all_parameter_sets_file, key="parameters", mode="w", complevel=5
        )
        pd.Series(errors).to_hdf(
            all_parameter_sets_file, key="errors", mode="a", complevel=5
        )

        # Pick a sample
        final_parameter_set = self.select_parameter_set(parameter_sets)
        final_parameter_set_file = working_directory / "parameter_set.csv"
        final_parameter_set.index.names = ["parameter_type", "metabolite", "reaction"]
        final_parameter_set.name = "value"
        final_parameter_set.to_csv(final_parameter_set_file, header=True)

        # Add parameters to the model (Note: to the final model!)
        for p in final_structure.parameters:
            value, p_type = None, None
            if p in self.settings["fixed_metabolites"]:
                p_type = Parameters.c
                m, r = p, NA_IDENTIFIER
            try:
                p_type, m, r = Parameters.split(p, missing_value=NA_IDENTIFIER)
            except ValueError:
                # If we can't split it, it must have been defined in the extra parameters.
                if p in self.settings["extra_parameters"]:
                    value = self.settings["extra_parameters"][p]
            # Next we try to retrieve it based on the type, reaction and/or metabolite.
            # We will override anything that is also defined in extra parameters,
            # since this parameter might have been scaled, but if we don't find it,
            # it hasn't changed and we can use the old value.
            if p_type is not None:
                try:
                    value = final_parameter_set.loc[p_type, m, r]
                except (ValueError, pd.core.indexing.IndexingError):
                    value = derive_parameter(
                        p, final_parameter_set, final_structure, cooperativities
                    )
            else:
                try:
                    value = final_parameter_set.loc[p, NA_IDENTIFIER, NA_IDENTIFIER]
                except KeyError:
                    pass
            # Add the final value to the model.
            if value is None:
                raise ValueError("Parameter {} not found".format(p))
            final_structure.parameter_values[p] = value

        # Add initial states
        for m in final_structure.metabolites:
            if m in self.settings["extra_parameters"]:
                final_structure.initial_concentrations[m] = self.settings[
                    "extra_parameters"
                ][m]
            elif m in final_parameter_set.index.get_level_values(1):
                final_structure.initial_concentrations[m] = final_parameter_set.loc[
                    Parameters.c, m, NA_IDENTIFIER
                ]
            else:
                # Keep the current value.
                pass

        # Save the final model
        parameterized_model_file = working_directory / "parametrized_model.xml"
        final_structure.save(parameterized_model_file)

        return [
            parameterized_model_file,
            final_parameter_set_file,
            all_parameter_sets_file,
        ]

    def balance(
        self,
        balancer,
        sub_directory,
        model_file,
        prior_file,
        data_file,
        cooperativity_file,
        augment,
    ):
        """Run the initial balancing round so we can work with the complete data set."""
        # Only include the thermodynamic parameters and do not augment the data.
        balancer.settings["augment"] = augment
        sub_directory.mkdir(exist_ok=True)

        # Run balancing
        balancer_result_path = balancer.run(
            sub_directory,
            model=model_file,
            priors=prior_file,
            parameter_data=data_file,
            cooperativities=cooperativity_file,
        )[0]
        # Load and return the results
        return BalancedParameterData.load(balancer_result_path)

    def optimize_directionality(self, parameters, structure, directions):
        """Optimize the parameters in such a way that reaction directionality is correct.

        The optimization is constrained by the uncertainty of c and mu, and the settings
        for A_max (or A_tol for forward reactions) and A_min (or -A_tol for backwards
        reactions) for active reactions.

        :param parameters:
        :type parameters: balancer.ParameterData
        :param structure:
        :type structure: sbml2symbolic.SBMLModel
        :param directions: Numpy array denoting the directionality
        (backward (-1), forward (1) or inactive (0))
        :type directions: (R) array of (-1, 0, 1)

        :returns: Dataframe with optimized mu, c and A.
        :rtype: pd.Dataframe
        """
        R, T = self.settings["R"], self.settings["T"]
        A_tol = self.settings["A_tol"]
        A_min, A_max = -self.settings["A_max"], self.settings["A_max"]
        zero_concentration = self.settings["zero_concentration"]
        constraint_factor = self.settings["constraint_factor"]

        # Create the Gurobi QP problem
        qp_model = grb.Model("QP Flux sign optimization")
        # Silence the output if required.
        qp_model.Params.OutputFlag = 0 if self.settings["silence_output"] else 1
        qp_model.Params.LogFile = (
            "" if self.settings["silence_output"] else "gurobi.log"
        )

        # Create the variables and objective terms.
        terms, c_list, mu_list = [], [], []
        for p_type, var_list in zip((Parameters.c, Parameters.mu), (c_list, mu_list)):
            for met in structure.metabolites:
                entry = parameters.df[
                    (parameters.df.parameter_type == p_type)
                    & (parameters.df.metabolite == met)
                ]
                id_ = Parameters.separator.join((p_type, met))
                mean, sd = entry.iloc[0][["mean", "sd"]]
                p_min, p_max = (
                    mean - constraint_factor * sd,
                    mean + constraint_factor * sd,
                )
                # Scale concentrations to log scale so we don't have to in the
                # QP constraint calculation. Don't forget to scale back later!
                if p_type == Parameters.c:
                    # Can't have negative or 0 concentration,
                    # so clamp the minimum to a small value.
                    p_min = np.log(max(p_min, zero_concentration))
                    p_max = np.log(p_max)
                    mean, sd = [float(i) for i in to_log_mean(mean, sd)]
                var = qp_model.addVar(p_min, p_max, 0, GRB.CONTINUOUS, id_)
                var_list.append(var)
                # The final term used for optimization is the number of standard deviations
                # the optimized value diverges from the mean of the original data.
                terms.append((var - mean) / sd)

        # Create the constraints for the driving force.
        # These force A to be positive or negative depending on the flux direction.
        A_list, A_constr_list = [], []
        for r_i, (reaction, direction) in enumerate(directions.itertuples(name=None)):
            metabolites = structure.participating_metabolites(
                structure.reactions[r_i], False
            )
            # Note that cooperativities are NOT included here, as this is a
            # thermodynamic relation.
            constraint_terms = [
                n * (mu_list[i] + R * T * c_list[i]) for i, n in metabolites
            ]
            expr = -grb.quicksum(constraint_terms)
            A_list.append((structure.reactions[r_i], expr))

            r_id = Parameters.separator.join((Parameters.A, reaction))
            if direction > 0:
                constr_1 = qp_model.addConstr(
                    expr, GRB.GREATER_EQUAL, A_tol, r_id + "_ge"
                )
                constr_2 = qp_model.addConstr(expr, GRB.LESS_EQUAL, A_max, r_id + "_le")
                A_constr_list.append((constr_1, constr_2, direction))
            elif direction < 0:
                constr_1 = qp_model.addConstr(
                    expr, GRB.LESS_EQUAL, -A_tol, r_id + "_ge"
                )
                constr_2 = qp_model.addConstr(
                    expr, GRB.GREATER_EQUAL, A_min, r_id + "_le"
                )
                A_constr_list.append((constr_1, constr_2, direction))
            else:
                # What to do with 0 fluxes? Could be constrained to be small in principle.
                # But that can have two reasons:
                #    (1) No enzyme for this reaction
                #    (2) thermodynamic balance
                # In addition, one might want to set A_min and A_max regardless of whether
                # the reaction is active (see comment below.)
                if self.settings["constrain_inactive_reactions"]:
                    constr_1 = qp_model.addConstr(
                        expr, GRB.LESS_EQUAL, A_max, r_id + "_ge"
                    )
                    constr_2 = qp_model.addConstr(
                        expr, GRB.GREATER_EQUAL, A_min, r_id + "_le"
                    )
                    A_constr_list.append((constr_1, constr_2, direction))
                else:
                    pass

        # Create objective (Use the square for each term so it can easily be minimized.)
        # By taking the quadratic terms (L2-norm) we penalize more for larger deviations.
        objective_terms = [term * term for term in terms]
        objective_expr = grb.quicksum(objective_terms)
        qp_model.setObjective(objective_expr, GRB.MINIMIZE)

        # Initialize model structure, optimize and check for optimality of the result.
        qp_model.update()
        qp_model.optimize()

        # Do the optimality check
        if qp_model.status != GRB.status.OPTIMAL:
            qp_model.computeIIS()
            qp_model.write("qp_model.debug.ilp")
            status_codes = {getattr(GRB.status, k): k for k in dir(GRB.status) if k.isupper()}
            raise RuntimeError(
                "Model solution not optimal: {}. Check qp_model.debug.ilp"
                " for the ISS solution.".format(status_codes[qp_model.status])
            )

        # Save some debug info
        names, values = [], []
        for term in objective_terms:
            names.append(term.getVar1(0).VarName)
            values.append(term.getValue())
        with np.errstate(invalid="ignore"):
            debug_df = pd.DataFrame(data=np.sqrt(values), index=names).fillna(0)
        debug_df.columns = ["z-score"]

        # Extract results and assign error margin.
        data_error = self.settings["optimized_directionality_data_error"]
        solution = []
        for var in qp_model.getVars():
            p_type, met_reac = var.VarName.split(Parameters.separator, 1)
            if p_type in Parameters.log:
                value = np.exp(var.X)
            else:
                value = var.X
            if p_type in Parameters.specific_to_both:
                met, reac = met_reac.split(Parameters.separator)
            elif p_type in Parameters.specific_to_reaction:
                reac = met_reac
                met = NA_IDENTIFIER
            elif p_type in Parameters.specific_to_metabolite:
                met = met_reac
                reac = NA_IDENTIFIER
            if (p_type, met, reac) in data_error:
                sd = abs(value) * data_error[(p_type, met, reac)]
            else:
                sd = abs(value) * data_error[p_type]
            solution.append((p_type, met, reac, value, sd))

        for reaction, expr in A_list:
            p_type = Parameters.A
            value = expr.getValue()
            if (p_type, NA_IDENTIFIER, reac) in data_error:
                sd = abs(value) * data_error[(p_type, NA_IDENTIFIER, reac)]
            else:
                sd = abs(value) * data_error[p_type]
            solution.append((p_type, NA_IDENTIFIER, reaction, value, sd))
        solution_df = pd.DataFrame(solution, columns=parameters.df.columns)

        return solution_df, debug_df

    def sample_synced_flux(
        self, balanced_parameters, fluxes, directions, ode_system, working_directory
    ):
        """Sample from a set of balanced parameters, forcing dynamic fluxes to match fluxes.

        The fluxes are matched by discarding any parameter sets with the wrong directionality,
        as marked by the sign of 'A'. Scaling is done through the enzyme concentration.
        Reactions that have a flux of 0, are assumed to be inactive and will have the enzyme
        concentration set to 0.
        :param balanced_parameters: Balanced Parameter data object to sample from.
        :type balanced_parameters: :class: BalancedParameterData
        :param fluxes: Data frames of fluxes, with a 'flux' column denoting the flux
        :type fluxes: :class: pandas.DataFrame
        :param fluxes: Data frames of fluxes directions (a -1, 0, or 1)
        :type fluxes: :class: pandas.DataFrame
        :param ode_system: OdeSystem, allowing the calculation of flux and updating the parameters.
        :type ode_system: :class: OdeSystem
        :param working_directory: Working directory to write any debug output too.
        :type working_directory: :class: Pathlib.Path

        :returns: Dataframe[n_samples x n_parameters] with parameters fixed to the flux set.
        :rtype: :class: Dataframe

        :raises RunTimeError: If no samples could be generated successfully within `max_samples`
                              or if the optimized steady state flux exceeds
                              `steady_state_error_tolerance`.
        :raises ValueError: If the ode system and the parameter set do not match.
        """
        nonzero_signs_mask = (directions != 0).squeeze()
        nonzero_signs = directions[nonzero_signs_mask].squeeze()

        sampled = 0
        samples = []
        correct = None
        while sampled < self.settings["max_samples"]:
            sampled += self.settings["sample_batch_size"]
            # Sample parameters
            candidates = balanced_parameters.sample(self.settings["sample_batch_size"])
            # Get values for A and compare signs for non masked reactions
            A_values = candidates.loc[:, pd.IndexSlice[Parameters.A, :, :]]
            A_values.columns = A_values.columns.droplevel(0).droplevel(0)
            nonzero_A_values = A_values.loc[:, nonzero_signs_mask]
            candidate_signs = np.sign(nonzero_A_values)
            correct_signs = candidate_signs == nonzero_signs
            if correct is not None:
                correct += correct_signs.sum()
            else:
                correct = correct_signs.sum()
            correct_indeces = np.all(correct_signs, axis=1)
            samples.append(candidates[correct_indeces])
        samples = pd.concat(samples).reset_index(drop=True)

        if samples.empty:
            # Save some debug output.
            debug_path = working_directory / "flux_directions.debug.csv"
            correct = correct.to_frame()
            correct.index.name = "reaction"
            correct.columns = ["n_correct_sign"]
            correct.to_csv(debug_path)
            raise RuntimeError(
                "Did not manage to sample any parameter sets with correct sign "
                "structure: 0 / {}. See '{}' for total correct counts per reaction.".format(
                    sampled, debug_path
                )
            )

        # TODO: Fix this repetition
        joined_columns = pd.Index(
            [
                Parameters.sep.join((j for j in i if j and str(j) != "nan"))
                for i in samples.columns.tolist()
            ]
        )

        # Add extra parameters if they are in the ode_system.
        for key, value in self.settings["extra_parameters"].items():
            if key in ode_system.parameters and key not in joined_columns:
                try:
                    p, m, r = Parameters.split(key, missing_value=NA_IDENTIFIER)
                except ValueError:
                    samples[key, NA_IDENTIFIER, NA_IDENTIFIER] = value
                else:
                    samples[p, m, r] = value
        # Also check metabolites that were not included in the balancing procedure.
        for met in self.settings["ignore_metabolites_balancing"]:
            if met in ode_system.metabolites:
                try:
                    samples[Parameters.c, met, NA_IDENTIFIER] = self.settings[
                        "extra_parameters"
                    ][met]
                except KeyError:
                    raise ValueError(
                        "{} was ignored during balancing, but no replacement initial "
                        "value was passed as extra_parameter.".format(met)
                    )

        # Same format as multi-index, but in joined strings - easier comparison to structure.
        joined_columns = pd.Index(
            [
                Parameters.sep.join((j for j in i if j and str(j) != "nan"))
                for i in samples.columns.tolist()
            ]
        )
        multi_columns = samples.columns
        samples.columns = joined_columns

        # Sort for easier processing later.
        order = np.argsort(joined_columns)
        multi_columns = multi_columns[order]
        joined_columns = joined_columns[order]
        samples = samples.iloc[:, order]

        # Get indices of right parameters for ode system assuming sorted names
        # Make sure to verify, because searchsorted won't complain about missing matches.
        parameter_idx = np.searchsorted(samples.columns, ode_system.parameters)
        if not np.all(samples.columns[parameter_idx] == ode_system.parameters):
            raise ValueError("Parameter set mismatch.")

        metabolites = [
            Parameters.sep.join((Parameters.c, m)) for m in ode_system.metabolites
        ]
        metabolite_idx = np.searchsorted(samples.columns, metabolites)
        if not np.all(samples.columns[metabolite_idx] == metabolites):
            raise ValueError("Metabolite set mismatch.")

        # First check for a custom parameter, otherwise use the default scaling parameter (u or kv)
        # we also have to skip any reactions that should not be scaled (custom scaling is None).
        no_scaling = [
            k for k, v in self.settings["scaling_parameters"].items() if v is None
        ] + self.settings["ignore_reactions"]
        default = self.settings["default_scaling_parameter"]
        us = [
            self.settings["scaling_parameters"].get(
                i, Parameters.sep.join((default, i))
            )
            for i in ode_system.reactions
            # except for ignored reactions or reactions that shouldn't be scaled.
            if i not in no_scaling
        ]
        u_idx = np.searchsorted(samples.columns, us)
        if not np.all(samples.columns[u_idx] == us):
            raise ValueError("Enzyme scaling parameter set mismatch.")

        ode_flux_mask = [i not in no_scaling for i in ode_system.reactions]
        fluxes = fluxes[~fluxes.index.isin(no_scaling)]
        if not np.all(fluxes.index == ode_system.reactions[ode_flux_mask]):
            raise ValueError("Reaction set mismatch.")

        updated_samples = samples.copy()
        errors = []
        for i, series in samples.iterrows():
            # Set parameters from sample.
            ode_system.update_parameters(series[parameter_idx].values)

            # Load steady state concentrations.
            y = series[metabolites].values

            # Calculate fluxes.
            dynamic_flux = ode_system.flux(y)[ode_flux_mask]

            # Rescale rate parameters.
            ratio = np.squeeze((fluxes.T / dynamic_flux).fillna(0).values)
            updated_samples.iloc[i, u_idx] = ratio * samples.iloc[i, u_idx]

            # Load new parameters and verify achieving the given flux state.
            ode_system.update_parameters(updated_samples.iloc[i, parameter_idx].values)
            dynamic_flux = ode_system.flux(y)[ode_flux_mask]
            ss_error = np.abs(fluxes.T - dynamic_flux).sum(axis=1)[0]
            if ss_error > self.settings["ss_error_tolerance"]:
                raise RuntimeError(
                    "Exceeded error: {} > {}".format(
                        ss_error, self.settings["ss_error_tolerance"]
                    )
                )
            errors.append(ss_error)

        updated_samples.columns = multi_columns
        return updated_samples, errors

    def select_parameter_set(self, parameters):
        """Select the parameter set with the smallest sum of enzyme speeds."""
        # Since they scale logarithmically, this should select a moderate middle ground
        # where everything is more or less in a realistic range.
        df = parameters.loc[:, pd.IndexSlice[[Parameters.u, Parameters.kv], :, :]]
        idx = df.groupby(level=2, axis=1).prod().sum(axis=1).idxmin()
        return parameters.iloc[idx]
