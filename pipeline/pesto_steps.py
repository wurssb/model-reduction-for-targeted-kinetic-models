"""Pipeline steps for optimization with Pesto.

Implements:
    - A step to optimize a compiled Amici Model.

Author: Rik van Rosmalen
"""
import json

from .step import Step
from .util import (matlab_path_context, call_matlab_silenced,
                   merge_hdf, assign_parameter_ranges)
from .symbolic_util import ModelConditions


class PestoOptimizer(Step):
    # TODO: Add more Pesto and Amici settings
    default_settings = {
        # 'parallel' or 'sequential' multi-start optimization
        'comp_type': 'sequential',
        # Number of multi-start starting points
        'n_starts': 3,
        # Local optimizer
        'localOptimizer': 'fmincon',
        # Settings for fmincon local optimization.
        'localOptimizerOptions': {
            'Algorithm': 'interior-point',
            'Display': 'off',
            'GradObj': 'on',
            'MaxIter': 2000,
            'PrecondBandWidth': float('inf'),
        },
        # Save the final hessian (unimplemented?)
        'localOptimizerSaveHessian': False,
        # Way to find starting points can be 'latin hypercube', 'uniform' or 'user-supplied',
        # if 'user-supplied', a Dataframe of starting parameters has to be provided.
        'proposal': 'latin hypercube',
        # Save the trace (deprecated) - keep on True
        'trace': True,
        # Sensitivity order.
        'sensi': 1,
        # Sensitivity method (0 - finite difference, 1 - forward, 2 - adjoint).
        'sensi_meth': 1,
        # CVODE integration tolerances
        'atol': 1e-16, 'rtol': 1e-8,
        # CVODE integration maximum steps.
        'maxsteps': 1e6,
        # Scale variables in logarithmic and linear space automatically based on name and range?
        'auto_scale': True,
        # If the parameter type cannot be inferred from the parameter name automatically,
        # use the default ('linear'/'lin', 'logarithmic'/'log' or 'poslog')
        # For 'poslog', the default is logarithmic when the minimum is positive, linear otherwise.
        'scale_default': 'linear',
        'deflate': 4,
        'shuffle': True,
        'silence_output': True
    }
    h5_settings = {'deflate', 'shuffle'}
    pesto_settings = {
        'comp_type', 'n_starts', 'localOptimizer',
        'localOptimizerOptions', 'localOptimizerSaveHessian',
        'proposal', 'trace',
    }
    ami_settings = {'sensi', 'sensi_meth', 'atol', 'rtol', 'maxsteps'}
    ignore_settings = {'silence_output', 'comp_type'}

    def __init__(self, pesto_mlab_location, amici_mlab_location,
                 mlab_engine_instance, use_cache, **settings):
        """Run optimization using the Pesto Toolbox.

        Required inputs to run are:
            'compiled_model': A compiled Amici model file.
            'conditions': A file containing model conditions.
                          (Only the initial concentrations) are used.
            'data': A file containing data that we can fit to.
            'model_description': A model description file that describes which states and
                                 parameters we have so we can match it to the data
                                 and the conditions.
            'parameter_ranges': A file with parameter ranges for the multi-starts
                                in the optimization.
        Outputs are:
            - Pesto optimization results (in ./results):
                'optimization_results.json':
                    A json file containing the output of Pesto (Note that this is not sorted
                    in the run order, but in the order of the final score.)
                'traces.h5':
                    An hdf5 file containing for each run the score and time traces, and
                    the associated parameter set. If runs were stopped because of convergence
                    or error, the final steps will be filled with all NaN values.
        """
        super().__init__(mlab_engine_instance, use_cache, **settings)
        self.amici_mlab_location = amici_mlab_location
        self.pesto_mlab_location = pesto_mlab_location
        self._dir_name = 'pesto_optimizer'

    def _run(self, working_directory, infiles):
        """Set up for and run the optimization."""
        compiled_model_paths = infiles['compiled_models']
        conditions = [ModelConditions.from_hdf(i) for i in infiles['conditions']]
        data_paths = infiles['data']
        model_description_paths = infiles['model_descriptions']
        parameter_range_path = infiles['parameter_ranges']

        with open(parameter_range_path, 'r') as p_file:
            parameter_ranges = json.load(p_file)

        result_path = working_directory / 'results'
        result_path.mkdir(exist_ok=True)

        if self.settings['proposal'] == 'user-supplied':
            try:
                starting_points_path = infiles['starting_points']
            except KeyError:
                raise ValueError("Starting points marked as user-supplied "
                                 "but not provided by user.")
        else:
            starting_points_path = None

        parameter_settings_path = working_directory / 'parameter_settings.json'
        self.optimize(compiled_model_paths, model_description_paths, conditions,
                      parameter_ranges, working_directory, data_paths, starting_points_path,
                      result_path, parameter_settings_path)

        results = [result_path / 'optimization_results.json', parameter_settings_path]
        if self.settings['trace']:
            results.append(result_path / 'traces.h5')
        return results

    def _assign_parameter_ranges(self, parameter_names, parameter_ranges):
        """For each parameter get a min, max and guess to give to Pesto."""
        # specific = parameter_ranges.get('specific', {})
        # general = parameter_ranges.get('general', {})
        # values = np.zeros((len(parameter_names), 3))
        # for idx, name in enumerate(parameter_names):
        #     # If it is specified exactly, use those values.
        #     if name in specific:
        #         values[idx, :2] = specific[name]
        #     # Else we just look at the parameter type.
        #     else:
        #         ptype, _ = name.split(symbolicSBML.Parameters.separator, 1)
        #         values[idx, :2] = general[ptype]
        # # Our guess is just the mean.
        # values[:, 2] = np.mean(values[:, :2], axis=1)

        # # Pesto uses a struct, so we pass Matlab a dictionary that it will convert to a struct.
        # return {
        #     'min': values[:, 0].tolist(),
        #     'max': values[:, 1].tolist(),
        #     'guess': values[:, 2].tolist(),
        # }
        range_df = assign_parameter_ranges(parameter_names, parameter_ranges,
                                           self.settings['auto_scale'],
                                           self.settings['scale_default'])
        p_range = {
            'min': range_df['minimum'].tolist(),
            'max': range_df['maximum'].tolist(),
            # TODO: This is wrong for logarithmically distributed variables.
            # If changed - make sure to fix the case with a minimum of 0.
            'guess': range_df[['minimum', 'maximum']].mean(axis=1).tolist(),
        }
        p_scale = range_df.log.astype(int).tolist()
        return p_range, p_scale

    def optimize(self, compiled_model_paths, model_description_paths, conditions,
                 parameter_ranges, working_directory, data_paths, starting_points_path,
                 result_path, parameter_settings_path):
        """Run the optimization."""
        locations = [self.amici_mlab_location, self.pesto_mlab_location,
                     self.MATLAB_FUNCTION_DIR, self.MATLAB_LIB_DIR]
        locations.extend([p.parent for p in compiled_model_paths])
        mlab = self.get_matlab_engine()

        settings = {'pesto_settings': {}, 'ami_settings': {}, 'h5_settings': {}}
        for key in self.pesto_settings:
            settings['pesto_settings'][key] = self.settings[key]
        for key in self.ami_settings:
            settings['ami_settings'][key] = self.settings[key]
        for key in self.h5_settings:
            settings['h5_settings'][key] = self.settings[key]
        for key in set(self.settings.keys()) - (self.pesto_settings |
                                                self.ami_settings | self.h5_settings):
            settings[key] = self.settings[key]

        model_data_struct = {
            'model_paths': [],
            'data_paths': [],
            'model_description_paths': [],
            'initial_concentrations': [],
        }

        # TODO: What if different experiments have different parameters?
        all_parameter_names = []
        for description in model_description_paths:
            with open(description, 'r') as model_description_file:
                parameter_names = json.load(model_description_file)['parameters']
                if not all_parameter_names:
                    all_parameter_names.extend(parameter_names)
                else:
                    if all_parameter_names != parameter_names:
                        raise NotImplementedError("Optimization currently only supports "
                                                  "experiments with the same parameters")
        parameters, pscale = self._assign_parameter_ranges(parameter_names, parameter_ranges)
        settings['ami_settings']['pscale'] = pscale

        # Save parameter settings for debugging.
        parameter_settings = parameters.copy()
        parameter_settings['scale'] = pscale
        parameter_settings['names'] = parameter_names
        with open(parameter_settings_path, 'w') as outfile:
            json.dump(parameter_settings, outfile, indent=2, sort_keys=True)

        iterable = zip(compiled_model_paths, model_description_paths,
                       conditions, data_paths)
        for compiled, description, condition, data in iterable:
            # Find out which initial conditions are required.
            states = json.loads(description.read_text())['states']
            # Get concentration for these states only.
            c = condition.initial_concentrations[states].iloc[0].values.tolist()
            model_data_struct['initial_concentrations'].append(c)
            model_data_struct['model_paths'].append(compiled.with_suffix('').name)
            model_data_struct['data_paths'].append(str(data))
            model_data_struct['model_description_paths'].append(str(description))

        # Pesto wants a relative path.
        result_path_str = str(result_path.name)

        # Optional starting points
        if starting_points_path is None:
            starting_points_path = False
        else:
            starting_points_path = str(starting_points_path.resolve())

        with matlab_path_context(mlab, locations):
            call_matlab_silenced(mlab.run_Pesto,
                                 model_data_struct,
                                 parameters,
                                 starting_points_path,
                                 result_path_str,
                                 settings,
                                 # Matlab engine settings
                                 nargout=0,
                                 silence_std_out=self.settings['silence_output'],
                                 silence_std_err=self.settings['silence_output'],
                                 )

        # If we ran things in parallel, we need to merge our outputs before we're finished.
        if self.settings['trace'] and self.settings['comp_type'] == 'parallel':
            # Find all separate output files and sort them.
            # TODO (Fix): Currently will also merge leftover results from previous runs.
            files = sorted(working_directory.glob('traces_*.h5'),
                           key=lambda x: int(x.stem.rsplit('_', 1)[-1]))
            # Merge results into a single file.
            outfile = result_path / 'traces.h5'
            h5_args = {
                'compression': self.settings['deflate'],
                'shuffle': self.settings['shuffle'],
                'chunks': 'last',
            }
            merge_hdf(files, outfile, overwrite=True, **h5_args)
            # Remove separate result files.
            for f in files:
                f.unlink()
