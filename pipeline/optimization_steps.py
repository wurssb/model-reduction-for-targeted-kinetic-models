"""Pipeline step to generate Optimization starting points.

Implements:
    - LHS starting points

Author: Rik van Rosmalen
"""
import json

import numpy as np
import pandas as pd
import h5py

try:
    import pyDOE2 as pyDOE

    _randomstate_allowed = True
except ImportError:
    import pyDOE

    _randomstate_allowed = False

from .util import assign_parameter_ranges
from .step import Step


class LHSStartingPoints(Step):
    default_settings = {
        # Parameters that should be fixed to the same value in each starting point.
        "fixed_parameters": None,
        # Number of starting points.
        "n_samples": 100,
        # Available are 'center', 'maximin', 'centermaximin' and 'correlation'.
        # The default is 'random', without any corrections.
        # Check the pyDOE documentation for the details.
        "criterion": "random",
        # Number of iterations in the maximin and correlation algorithms.
        "iterations": 5,
        # Scale variables in logarithmic and linear space automatically based on name and range?
        "auto_scale": True,
        # If the parameter type cannot be inferred from the parameter name automatically,
        # use the default ('linear'/'lin', 'logarithmic'/'log' or 'poslog')
        # For 'poslog', the default is logarithmic when the minimum is positive, linear otherwise.
        "scale_default": "linear",
        # Optional random seed. If None, the starting points won't be reproducible between runs.
        "random_seed": None,
        # H5 saving options.
        "deflate": 4,
        "shuffle": True,
    }
    ignore_settings = {}

    def __init__(self, use_cache, **settings):
        """Create starting points use pyDOE.

        Required inputs to run are:
            'parameter_names': A file with the parameters required to be included.
            'parameter_ranges': A file with parameter ranges for the multi-starts
                                in the optimization.
        Outputs are:
            - Parameter starting points (./starting_points.h5)
        """
        if not _randomstate_allowed and self.settings["random_seed"] is not None:
            raise ValueError(
                "pyDOE does not support setting a random seed. "
                "Please install pyDOE2."
            )
        super().__init__(mlab_engine_instance=None, use_cache=use_cache, **settings)

    def _run(self, working_directory, infiles):
        """Run the starting points generation."""
        parameter_range_path = infiles["parameter_ranges"]
        parameter_names_path = infiles["parameter_names"]

        with open(parameter_range_path, "r") as p_file:
            parameter_ranges = json.load(p_file)

        with open(parameter_names_path, "r") as parameter_names:
            parameter_names = json.load(parameter_names)

        parameter_path = working_directory / "starting_points.h5"

        n_parameters = len(parameter_names)

        lhs = self.generate_lhs(n_parameters)
        parameters = self.rescale_parameters(parameter_names, parameter_ranges, lhs)

        # TODO: Move this before the LHS, so we don't ruin any correlation structure etc.
        if self.settings["fixed_parameters"] is not None:
            self.fix_parameters(parameters, self.settings["fixed_parameters"])

        self.save_parameters(parameters, parameter_path)
        return [parameter_path]

    def generate_lhs(self, n_parameter):
        """Generate the LHS in the [0-1] uniform space using pyDOE."""
        criterion = self.settings["criterion"]
        if self.settings["criterion"] == "random":
            criterion = None

        return pyDOE.lhs(
            n_parameter,
            self.settings["n_samples"],
            criterion,
            self.settings["iterations"],
            self.settings["random_seed"],
        ).T

    def rescale_parameters(self, parameter_names, parameter_ranges, lhs):
        """Rescale the LHS to the correct parameter range."""
        ranges = assign_parameter_ranges(
            parameter_names,
            parameter_ranges,
            self.settings["auto_scale"],
            self.settings["scale_default"],
        )
        scaled_lhs = np.zeros(lhs.shape)
        # Apply LHS to linear part
        linear = ~ranges.log
        if linear.any():
            scaled_lhs[linear] = (
                ranges.minimum[linear].values[:, np.newaxis]
                + lhs[linear]
                * (ranges.maximum[linear] - ranges.minimum[linear]).values[
                    :, np.newaxis
                ]
            )
        # Apply LHS to log part.
        if ranges.log.any():
            # Convert to log space.
            # Add a small value to make sure that values are > 0.
            # Since these are starting points, not actual bounds, they can still be
            # optimized away.
            e = 1e-20
            log_ranges = np.log(ranges[ranges.log][["minimum", "maximum"]] + e)
            # Transform.
            scaled_lhs[ranges.log] = (
                log_ranges.minimum.values[:, np.newaxis]
                + lhs[ranges.log]
                * (log_ranges.maximum - log_ranges.minimum).values[:, np.newaxis]
            )
            # Convert back to linear space.
            scaled_lhs[ranges.log] = np.exp(scaled_lhs[ranges.log])

        assert np.isnan(scaled_lhs).sum() == 0
        return pd.DataFrame(data=scaled_lhs, index=parameter_names)

    def save_parameters(self, parameters, parameter_path):
        """Save the starting points to a hdf5 file at parameter_path.

        The file will contain:
            parameter_names: a p x 1 dataset with the parameter names.
            parameter_values: a p x n dataset with the starting points for each run.
        """
        compression = self.settings["deflate"]
        shuffle = self.settings["shuffle"]
        with h5py.File(parameter_path, "w") as hf:
            hf.create_dataset(
                "parameter_names",
                compression=compression,
                shuffle=shuffle,
                track_times=False,
                data=parameters.index.values.astype("S"),
            )
            hf.create_dataset(
                "parameter_values",
                compression=compression,
                shuffle=shuffle,
                track_times=False,
                data=parameters.values,
            )

    def fix_parameters(self, parameters, fixed_parameters):
        """Replace parameters with fixed values."""
        for p, value in fixed_parameters.items():
            if p in parameters.index:
                parameters.loc[p, :] = value
