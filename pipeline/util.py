"""Utility functions.

Contains:
    - A context manager to temporarily change the Matlab path of a Matlab instance.
    - Helper functions to:
        - Load a config file containing file paths for dependencies.
        - Make a Matlab call without polluting std_out or std_err.
        - Merge multiple hdf5 files as produced by the parallel optimization step.
        - Scan the possibility of a FBA model carrying flux in a certain medium.
        - Fit a distribution based on a given low and high and a confidence interval.
        - Get the last valid index of an array.
        - Load hdf5 simulation results into a pandas Dataframe.

Author: Rik van Rosmalen
"""
import collections
import pathlib
import contextlib
import io
import json
import sys
import importlib
import warnings

import h5py
import numpy as np
import scipy.stats
import pandas as pd

try:
    import cobra
except ImportError:
    cobra = False

from symbolicSBML.parameters import Parameters
import symbolicSBML


def read_config(path):
    """Read the config file and return as dictionary.

    File paths (under paths) will be converted to pathlib Paths.
    """
    config = json.loads(pathlib.Path(path).read_text())
    for key, value in config.get("paths", {}).items():
        path = pathlib.Path(value).expanduser()
        if not path.exists():
            raise ValueError("Invalid path: {}".format(path))
        config["paths"][key] = path

    return config


@contextlib.contextmanager
def matlab_path_context(mlab_engine_instance, locations):
    """Context manager to temporarily add locations to the Matlab path.

    Resets the path on exit or error, unless the instance is not alive any more.
    """
    # Save the old path.
    path = mlab_engine_instance.path()
    # Add new paths
    mlab_engine_instance.addpath(*[str(i.resolve()) for i in locations])
    # Return to context
    try:
        yield
    # Always clean up, even if we get an error during the context.
    finally:
        import matlab.engine as ml
        try:
            # Reset the path to the old path
            mlab_engine_instance.path(path)
        except ml.RejectedExecutionError:
            # It's already dead, no need to clean up the path any more.
            pass


def call_matlab_silenced(func, *vargs, **kwargs):
    """Call a Matlab function with std_out and/or std_err output suppressed.

    Will return the same number of outputs as nargout, supplemented with a
    buffer for each of the silenced outputs (std_out and/or std_err)
    """
    # Create buffers to store std_out and std_err
    std_out_buffer = io.StringIO() if kwargs["silence_std_out"] else None
    std_err_buffer = io.StringIO() if kwargs["silence_std_err"] else None

    # Remove these since we don't want to provide them to the Matlab
    # function downstream.
    del kwargs["silence_std_out"]
    del kwargs["silence_std_err"]

    # This we do want to provide to the matlab call.
    kwargs["stdout"] = std_out_buffer
    kwargs["stderr"] = std_err_buffer

    # Call the Matlab function
    output = func(*vargs, **kwargs)

    # If we are returning multiple arguments, make sure we have a tuple
    # so we can combine them.
    if output is not isinstance(output, tuple) and (std_out_buffer or std_err_buffer):
        if output is None:
            output = tuple()
        else:
            output = tuple(output)
    # Add the buffers to the return values
    if std_out_buffer:
        output = output + tuple(std_out_buffer)
    if std_err_buffer:
        output = output + tuple(std_err_buffer)

    return output


def load_ode_system(location, name=""):
    """Load the cython ode system module at `location` starting with `name`.

    The import path is corrected afterwards.
    """
    # Find .so (unix) or .pyd (windows) files starting with name prefix (or any prefixes).
    modules = [
        p
        for p in location.iterdir()
        if p.suffix in (".pyd", ".so") and p.stem.startswith(name)
    ]
    if not modules:
        raise ValueError("Module could not be found at path: {}".format(location))
    elif len(modules) > 1:
        raise ValueError("Multiple modules found at path: {}".format(location))

    module_name, platform, extension = modules[0].name.split(".")

    sys.path.insert(0, str(location))
    try:
        importlib.invalidate_caches()
        ode_system = importlib.import_module(module_name).CythonODE()
    finally:
        if sys.path[0] == str(location):
            sys.path.pop(0)
    return ode_system


def merge_hdf(inputs, output, overwrite=False, **h5_args):
    """Merge multiple hdf5 files into a single file.

    This is used to merge results from parallel runs back into a single file.

    Note: All inputs must have the keys and size.
    Note: Chunks can be set to 'last' to create chunks of size of a single value
          of the last dimension, this is similar to what is done in the Matlab scripts.
          For example (30, 20, 10) would become chunked using (30, 20, 1).
    """
    # Set and update default arguments.
    h5_default_args = {
        "chunks": True,  # Automatically determine chunk size.
        "compression": 4,  # Gzip level 4
        "shuffle": True,
        # Do not track time or order for reproducible hashing
        "track_times": False,
        "track_order": False,
    }
    h5_merged_args = h5_default_args
    h5_merged_args.update(h5_args)

    # Load all files, check sizes
    values = collections.defaultdict(list)
    sizes = {}
    for input_file in inputs:
        f = h5py.File(input_file, "r")
        f_keys = set(f.keys())
        # If keys are not yet set or match the already set keys,
        # we can continue with this dataset.
        if not values or (f_keys == set(values.keys())):
            if not sizes:
                for key in f_keys:
                    sizes[key] = f[key].shape
            for key in f_keys:
                if sizes[key] != f[key].shape:
                    raise ValueError("Size mismatch in {}".format(key))
                values[key].append(f[key])
        else:
            raise ValueError("Input files have mismatching keys.")

    # Create the output file, overwriting if allowed.
    with h5py.File(output, "x" if not overwrite else "w") as output_file:
        for key, value in values.items():
            # Determine the data set size.
            shape = (len(value),) + sizes[key]
            # If chunked is 'last', calculate the chunk size as well.
            if h5_merged_args["chunks"] == "last":
                chunks = shape[:-1] + (1,)
                h5_args = h5_merged_args.copy()
                h5_args["chunks"] = chunks
            else:
                h5_args = h5_merged_args
            # Write to file.
            data = np.stack(value)
            output_file.create_dataset(key, shape=data.shape, data=data, **h5_args)


def scan_flux_possibility(
    model, max_steps=10, tol=1e-6, fraction_of_optimum=1.0, uptake=10
):
    """Scan the models reactions for the possibility of carrying flux.

    :param model: A cobra model. A relevant objective and valid starting point should be set.
    :type model: cobra model
    :param max_steps: The maximum amount of iterations to search for convergence
    of the active reactions, defaults to 10
    :type max_steps: int, optional
    :param tol: Tolerance of the optimization, below which a flux is assumed to be non-existent
    , defaults to 1e-6
    :type tol: float, optional
    :param fraction_of_optimum: Fraction of optimum for the FVA solution, defaults to 1.0
    :type fraction_of_optimum: float, optional
    :param uptake: Default uptake for possible components that could be in the medium
    , defaults to 10
    :type uptake: int, optional
    :return: active reactions and reactions inactive under all tested conditions
    :rtype: set(str), set(str)
    """
    if not cobra:
        raise ImportError("Cobrapy required.")

    reactions = {i.id for i in model.reactions}
    exchanges = {i.id for i in model.exchanges}
    allowed_imports = set()
    old = set()
    generations = []

    with model:
        for i in range(max_steps):
            # Do a FVA
            df = cobra.flux_analysis.flux_variability_analysis(
                model, fraction_of_optimum=fraction_of_optimum
            )
            # Check active reactions
            active = set(df[np.logical_or(df.minimum < -tol, df.maximum > tol)].index)
            inactive = reactions - active

            # If it didn't update - break
            if active <= old:
                return old, reactions - old

            # Allow for uptake of newly exportable metabolites
            new_exports = (
                set(df[(df.maximum > tol)].index) & exchanges
            ) - allowed_imports
            for r in new_exports:
                model.reactions.get_by_id(r).lower_bound = -uptake

            allowed_imports |= new_exports
            old |= active

        return old | active, reactions - (old | active)


def fit_distribution_from_ci(
    low, high, distribution=scipy.stats.lognorm, start=(1.0, 1.0), interval=0.95
):
    """Fit a distribution using a given confidence interval.

    :param low: Low end of the confidence interval
    :type low: float
    :param high: high end of the confidence interval
    :type high: float
    :param distribution: Type of distribution to fit, defaults to scipy.stats.lognorm
    :type distribution: scipy.stats distribution, optional
    :param start: Starting point of the optimization, defaults to (1, 1)
    :type start: tuple(float, float), optional
    :param interval: Width of the confidence interval being fitted, defaults to 0.95 (95% ci)
    :type interval: float, optional
    """
    data = np.array((low, high))

    def error_f(x):
        """Calculate the error of fit for the distribution.

        :param x: Distribution size and scale (see scipy distributions)
        :type s: [float, float]
        :return: error of low end and high end of the distribution
        :rtype: [type]
        """
        low_fit, high_fit = distribution.interval(interval, s=x[0], scale=x[1])
        return np.array((low_fit, high_fit)) - data

    result = scipy.optimize.least_squares(
        error_f, np.array(start), method="lm", ftol=1e-12, xtol=1e-12, gtol=1e-12
    )
    s, scale = result["x"]
    result.distribution = distribution(s=s, scale=scale)
    result.ci_data = low, high
    result.ci_pred = result.distribution.interval(interval)
    result.ci_error = low - result.ci_pred[0], high - result.ci_pred[1]
    return result


def parse_reference_simulations(simulation_dir, run_name, categorical=True):
    """From the simulations directory, parse reference simulations."""
    reference_dir = simulation_dir / run_name
    if not reference_dir.exists():
        raise ValueError("Reference directory does not exists.")
    references = []
    for path in sorted(reference_dir.glob("experiment_*")):
        data_path = path / "simulations.h5"
        with h5py.File(data_path, "r") as data_file:
            time = np.array(data_file["t"]).squeeze()
            states = np.array(data_file["states"]).astype("U")
            values = np.array(data_file["x"]).squeeze()
        df = pd.DataFrame(data=values.T, index=time, columns=states)
        df.columns = df.columns.str.strip()
        # Convert to long form
        df = df.unstack().reset_index()
        df.columns = ("state", "time", "value")
        df = df.assign(experiment=path.stem)
        references.append(df)
    if references:
        df_reference = pd.concat(references, ignore_index=True)
        if categorical:
            df_reference = df_reference.assign(
                state=df_reference.state.astype("category"),
                experiment=df_reference.experiment.astype("category"),
            )
    else:
        df_reference = None
    return df_reference


def parse_optimized_simulations(simulation_dir, run_name, categorical=True):
    """From the simulations directory, parse optimized simulations."""
    solution_dir = simulation_dir / "solutions" / run_name
    solutions = []
    for path in sorted(solution_dir.glob("experiment_*")):
        data_path = path / "simulations.h5"
        with h5py.File(data_path, "r") as data_file:
            for run in data_file.keys():
                time = np.array(data_file[run]["t"]).squeeze()
                states = np.array(data_file[run]["states"]).astype("U")
                values = np.array(data_file[run]["x"]).squeeze()
                df = pd.DataFrame(data=values.T, index=time, columns=states)
                df.columns = df.columns.str.strip()
                # Convert to long form
                df = df.unstack().reset_index()
                df.columns = ("state", "time", "value")
                df = df.assign(experiment=path.stem, run=int(run))
                solutions.append(df)
    try:
        df_optimized = pd.concat(solutions, ignore_index=True)
    except ValueError:
        warnings.warn(
            "No simulation found for {} in {}".format(run_name, simulation_dir)
        )
        return None
    if categorical:
        df_optimized = df_optimized.assign(
            state=df_optimized.state.astype("category"),
            experiment=df_optimized.experiment.astype("category"),
        )
    return df_optimized


def parse_simulation_results(working_dir, run_name, categorical=True):
    """From the simulations directory, parse reference and optimized simulations."""
    working_dir = pathlib.Path(working_dir)
    simulation_dir = working_dir / "amici_simulator"
    df_reference = parse_reference_simulations(simulation_dir, run_name, categorical)
    df_optimized = parse_optimized_simulations(simulation_dir, run_name, categorical)
    return df_reference, df_optimized


def load_ensemble_samples(path, limit_samples_testing=False):
    """Load simulation samples and sensitivities of ensemble modelling procedure."""
    with h5py.File(path, "r") as h5f:
        # Used for column headers
        metabolite_names = np.array(h5f["metabolite_names"]).astype("U")
        reaction_names = np.array(h5f["reaction_names"]).astype("U")
        parameter_names = np.array(h5f["parameter_names"]).astype("U")

        # Get the max size if we don't limit.
        if not limit_samples_testing:
            limit_samples_testing = h5f["parameters"].shape[0]

        # Load all the data up to limit_samples_testing.
        state = pd.DataFrame(
            data=np.array(h5f["state"][:limit_samples_testing, ...]),
            columns=metabolite_names,
        )
        dydt = pd.DataFrame(
            data=np.array(h5f["dydt"][:limit_samples_testing, ...]),
            columns=metabolite_names,
        )
        parameters = pd.DataFrame(
            data=np.array(h5f["parameters"][:limit_samples_testing, ...]),
            columns=parameter_names,
        )
        flux = pd.DataFrame(
            data=np.array(h5f["flux"][:limit_samples_testing, ...]),
            columns=reaction_names,
        )
        jacobian = pd.DataFrame(
            data=np.array(h5f["jacobian"][:limit_samples_testing, ...]).reshape(
                limit_samples_testing, -1
            ),
            columns=pd.MultiIndex.from_product((metabolite_names, metabolite_names)),
        )
        sensitivities = pd.DataFrame(
            data=np.array(h5f["sensitivities"][:limit_samples_testing, ...]).reshape(
                limit_samples_testing, -1
            ),
            columns=pd.MultiIndex.from_product((metabolite_names, parameter_names)),
        )
        flux_sensitivities = pd.DataFrame(
            data=np.array(
                h5f["flux_sensitivities"][:limit_samples_testing, ...]
            ).reshape(limit_samples_testing, -1),
            columns=pd.MultiIndex.from_product((reaction_names, parameter_names)),
        )
    dfs = (state, dydt, parameters, flux, jacobian, sensitivities, flux_sensitivities)
    return dfs


def get_last_valid_index(arr, axis=0, strict=False):
    """Get the index of the last value that is not NaN in the axis.

    It is assumed that the series will switch only once from valid to NaN.
    Note: If strict is set, it will error is the first value is invalid.
          Otherwise this case will return -1.
    """
    length = arr.shape[axis] - 1
    # Check if the last index is valid
    last_is_valid = ~np.isnan(np.take(arr, length, axis=axis))
    # Use argmax to get the last valid index in each other case.
    max_valid_index_middle = np.argmax(np.isnan(arr), axis=axis) - 1
    if strict and np.any(max_valid_index_middle == -1):
        return ValueError(
            "Encountered series in axis ({}) without any valid values.".format(axis)
        )
    # If the last index is valid, return length, otherwise return from max_valid_index
    return np.where(last_is_valid, length, max_valid_index_middle)


def parse_optimization_results(working_dir, run_name):
    """From the optimization results directory, parse the settings, traces and other outputs."""
    working_dir = pathlib.Path(working_dir)
    # Read parameter names
    model_description_path = (
        sorted((working_dir / "amici_compiler" / run_name).glob("*"))[0]
        / "model_description.json"
    )
    with open(model_description_path, "r") as model_description_file:
        description = json.load(model_description_file)
    parameter_names = description["parameters"]

    # Read parameter traces
    trace_path = working_dir / "pesto_optimizer" / run_name / "results" / "traces.h5"
    with h5py.File(trace_path, "r") as trace_file:
        fval = np.array(trace_file["fval"])
        time = np.array(trace_file["time"])
        par = np.array(trace_file["par"])

    assert len(parameter_names) == par.shape[-1]

    # Reshape data and create the data frame
    data = np.concatenate(
        (time[:, :, np.newaxis], fval[:, :, np.newaxis], par), axis=-1
    )
    data.shape = (-1, par.shape[-1] + 2)
    index = pd.MultiIndex.from_product(
        (range(fval.shape[0]), range(fval.shape[1])), names=("run", "iteration")
    )
    df_trace = pd.DataFrame(
        data=data, columns=["time", "fval"] + parameter_names, index=index
    )

    # Read settings
    settings_path = working_dir / "pesto_optimizer" / run_name / "settings.json"
    with open(settings_path, "r") as settings_file:
        settings = json.load(settings_file)

    # Get a view for the last valid values
    last_valid = get_last_valid_index(time, axis=1)
    if (last_valid == -1).all():
        raise ValueError(
            "No successful optimization traces found in {} for {}.".format(
                working_dir, run_name
            )
        )
    # Runs witouth valid values will show as having an iteration of -1.
    df_trace_end = df_trace.reindex(zip(np.arange(time.shape[0]), last_valid))

    # Retrieve allowed parameter ranges and real parameter value
    range_path = working_dir / "inputs" / "parameter_ranges.json"
    with open(range_path, "r") as range_file:
        ranges = json.load(range_file)

    model_path = working_dir / "parameterizer" / "parametrized_model.xml"
    real_values = symbolicSBML.SBMLModel(model_path).parameter_values

    range_data = {}
    for parameter in parameter_names:
        # Look up min and max for the optimization
        if parameter in ranges["specific"]:
            p_range = list(ranges["specific"][parameter])
        else:
            p_type, *_ = parameter.split(Parameters.sep, 1)
            p_range = list(ranges["general"][p_type])
        # Look up real value
        if parameter in real_values:
            p_range.append(real_values[parameter])
        else:
            p_range.append(np.NaN)
        range_data[parameter] = p_range
    range_df = pd.DataFrame(range_data, index=["min", "max", "truth"]).T

    return df_trace, df_trace_end, range_df, settings


def assign_parameter_ranges(
    parameter_names, parameter_ranges, auto_scale=True, default_scale="poslog"
):
    """For each parameter get a min, max and whether it is log scaled."""
    specific = parameter_ranges.get("specific", {})
    general = parameter_ranges.get("general", {})
    values = np.zeros((len(parameter_names), 3))
    for idx, name in enumerate(parameter_names):
        # If it is specified exactly, use those values.
        if name in specific:
            values[idx, :2] = specific[name]
        # Else we just look at the parameter type.
        # In order to support arbitrary types here we do not use
        # the symbolicSBML split.
        else:
            ptype, _ = name.split(symbolicSBML.Parameters.separator, 1)
            values[idx, :2] = general[ptype]
        if auto_scale:
            try:
                values[idx, -1] = parameter_is_log_scaled(name)
                auto_scale_failed = False
            except ValueError:
                auto_scale_failed = True
        if not auto_scale or auto_scale_failed:
            if default_scale in ("linear", "lin"):
                values[idx, -1] = False
            elif default_scale in ("logarithmic", "log"):
                values[idx, -1] = True
            elif default_scale == "poslog":
                values[idx, -1] = values[idx, 0] > 0

    df = pd.DataFrame(
        data=values, index=parameter_names, columns=["minimum", "maximum", "log"]
    )
    df.log = df.log.astype(bool)
    return df


def parameter_is_log_scaled(name, default="error"):
    """Check if a parameter is log-scaled."""
    try:
        ptype, *_ = symbolicSBML.Parameters.split(name)
        return ptype in symbolicSBML.Parameters.log
    except ValueError:
        if default == "error":
            raise ValueError("Could not infer parameter scaling based on name.")
        else:
            return default


# TODO: This could be useful to generalize in symbolicSBML.parameters or in parameter balancer.
#       Possibly as part of a customizable Q matrix?
def derive_parameter(
    parameter, parameter_set, structure, cooperativities, NA_IDENTIFIER=""
):
    """Derive a combined parameter type from the parameter set."""
    p_type, met, reac = Parameters.split(parameter, missing_value=NA_IDENTIFIER)
    # Generalize to work in 1d for series, 2d for data frames.
    if isinstance(parameter_set, pd.Series):
        parameter_set = parameter_set.to_frame().T
        index_slice = pd.IndexSlice[0]
    else:
        index_slice = pd.IndexSlice[:]
    if p_type in Parameters.combined:
        # We might need these for the calculation.
        if "rate" in p_type or p_type == Parameters.u_v:
            u = parameter_set.loc[index_slice, pd.IndexSlice[Parameters.u, met, reac]]
            kv = parameter_set.loc[index_slice, pd.IndexSlice[Parameters.kv, met, reac]]
        if "km" in p_type:
            h = cooperativities.get(reac, 1)
            km_prod = 1
            km_sub = 1
            for metabolite_id, s in structure.participating_metabolites(reac):
                km = parameter_set.loc[
                    index_slice, pd.IndexSlice[Parameters.km, metabolite_id, reac]
                ]
                if s > 0:
                    km_prod *= km ** (s * h)
                elif s < 0:
                    km_sub *= km ** (s * h)

        # Calculate the actual value.
        if p_type == Parameters.u_v:
            value = u * kv
        elif p_type == Parameters.km_sub:
            value = km_sub
        elif p_type == Parameters.km_prod:
            value = km_prod
        elif p_type == Parameters.km_tot:
            value = km_sub * km_prod
        elif p_type == Parameters.km_sub_rate:
            value = u * kv * km_sub
        elif p_type == Parameters.km_prod_rate:
            value = u * kv * km_prod
        elif p_type == Parameters.km_tot_rate:
            value = u * kv * km_sub * km_prod
        return value
    else:
        raise ValueError("Not a derived parameter type - {}.".format(p_type))
    return value
