"""Pipeline steps for sbml file handling.

Implements:
    - SBML normalization (to simplify the format for other tools)
    - Parsing incorrect SBML (from other tools) and mapping this back
        to an original model (for Network Reduction).
        (TODO - Lift implementation out of Reducer class and move here.)
    - Adding kinetics
        (TODO - use symbolicSBML)

TODO: Refactor this into more of an utilities step instead of a full step.
      Some methods could be functions as well.
      Finally: Think about moving the functionality to symbolicSBML to make
        it more complete and user-friendly.

Author: Rik van Rosmalen
"""
import collections
import json
import pathlib
import re

import lxml.etree

import pandas as pd
import scipy
import numpy as np
import sympy

import libsbml
import cobra

import symbolicSBML

from .step import Step


def validate_sbml_document(document):
    """Clear the error log and re-validate."""
    document.getErrorLog().clearLog()
    document.validateSBML()

    n_errors = document.getNumErrors()
    for i in range(n_errors):
        error = document.getError(i)
        if error.isWarning():
            continue
        else:
            raise RuntimeError(
                "Constructed invalid SBML. First Error (line {}): {}".format(
                    error.getLine(), error.getShortMessage()
                )
            )


def write_sbml_document(document, sbml_path):
    """Write new SBML document."""
    sbml_path = str(sbml_path.resolve())
    success = libsbml.writeSBMLToFile(document, sbml_path)
    # Note: This is just True / False and not a libsbml error code constant!
    if not success:
        raise ValueError("Could not write SBMLfile to: {}".format(sbml_path))


def write_json(values, path):
    with open(path, "w") as f:
        json.dump(values, f, sort_keys=True, indent=2)


class SBMLNormalizer(Step):
    default_settings = {
        "replace_ids": True,
        "replace_names_with_ids": True,
        "merge_compartments": True,
        "convert_to_fbc": True,
        "convert_to_cobra_fbc": False,
        "remove_annotation": True,
        "remove_observables": True,
        "remove_rate_laws_and_parameters": True,
        "SBML_level": 3,
        "SBML_version": 2,
        "SBML_FBC_version": 2,
        "set_level_version": True,
        "validate": True,
        "validate_initial_model": True,
        "silence_output": True,
    }
    ignore_settings = {"silence_output", "validate_initial_model", "validate"}

    def __init__(self, use_cache=True, **settings):
        """Create a normalizer for SBML.

        Simplifies the SBML model in order for different tools to accept it.
        Annotation and compound ids are saved in a seperate file and can be
        mapped back to the original.

        Required inputs to run are:
            'input_model': An SBML model file.
        Outputs are:
            - normalized model (./normalzized_model.xml) in SBML format.
            - annotation (./annotation.json) Mapping of old names to new names.
            - constraints (./constraints.json) FBA constraints and objectives per reaction.
        """
        super().__init__(mlab_engine_instance=None, use_cache=use_cache, **settings)
        if self.settings["convert_to_cobra_fbc"] and self.settings["convert_to_fbc"]:
            raise ValueError(
                "Cannot convert to and from cobra and fbc at the same time!"
            )

    @staticmethod
    def _try_conversion(document, conversion):
        """Try a conversion on the document, converting the return code to an error.

        success -> return None
        failed -> raise ValueError
        n/a -> raise NotImplementedError
        """
        success = document.convert(conversion)
        if success == libsbml.LIBSBML_OPERATION_FAILED:
            raise ValueError("SBML conversion failed.")
        elif success == libsbml.LIBSBML_CONV_CONVERSION_NOT_AVAILABLE:
            raise NotImplementedError("SBML conversion not available")

    def normalize(
        self, input_model, normalized_model_path, annotation_path, constraints_path
    ):
        """Normalize the model."""
        # Load the model
        if not (input_model.exists() and input_model.is_file()):
            raise FileNotFoundError(
                "File is not valid or does not exist: {}".format(input_model)
            )
        document = libsbml.readSBML(str(input_model.resolve()))

        if self.settings["validate_initial_model"]:
            validate_sbml_document(document)

        # We save all changed annotation here so we can map things back to the old model.
        annotation = {}

        if self.settings["convert_to_fbc"]:
            self.convert_to_fbc(document, self.settings["SBML_FBC_version"])
        if self.settings["remove_observables"]:
            self.remove_observables(document)
        if self.settings["remove_rate_laws_and_parameters"]:
            self.remove_rate_laws_and_parameters(document)
        if self.settings["replace_ids"]:
            self.replace_ids(document, annotation)
        if self.settings["replace_names_with_ids"]:
            self.replace_names_with_ids(document, annotation)
        if self.settings["merge_compartments"]:
            self.merge_compartments(document, annotation)
        if self.settings["remove_annotation"]:
            self.remove_annotation(document)
        if self.settings["convert_to_cobra_fbc"]:
            self.convert_to_cobra_fbc(document)
        if self.settings["validate"]:
            validate_sbml_document(document)
        if self.settings["set_level_version"]:
            self.set_level(
                document, self.settings["SBML_level"], self.settings["SBML_version"]
            )
        write_sbml_document(document, normalized_model_path)
        with open(annotation_path, "w") as f:
            json.dump(annotation, f, indent=2, sort_keys=True)

        # Save the constraints
        constraints = {}
        cobra_model = cobra.io.read_sbml_model(str(input_model), f_replace={})
        constraints["bounds"] = {r.id: r.bounds for r in cobra_model.reactions}
        constraints["objective"] = {}
        constraints["objective"]["direction"] = cobra_model.objective.direction
        constraints["objective"]["reactions"] = {
            r.id: r.objective_coefficient
            for r in cobra_model.reactions
            if r.objective_coefficient != 0.0
        }
        with open(constraints_path, "w") as f:
            json.dump(constraints, f, indent=2, sort_keys=True)

    def convert_to_fbc(self, document, target_fbc_version):
        """Convert to FBC constraints."""
        # Check if FBC is in use"
        model = document.getModel()
        fbc = model.getPlugin("fbc")
        if fbc is None:
            conversion = libsbml.ConversionProperties()
            conversion.addOption("convert cobra")
            self._try_conversion(document, conversion)
            fbc = model.getPlugin("fbc")

        # Check new FBC version
        current_fbc_version = fbc.getVersion()
        if target_fbc_version == current_fbc_version:
            return

        # Convert to correct version if required.
        conversion = libsbml.ConversionProperties()
        if current_fbc_version == 2 and target_fbc_version == 1:
            conversion.addOption("convert fbc v2 to fbc v1")
        elif current_fbc_version == 1 and target_fbc_version == 2:
            conversion.addOption("convert fbc v1 to fbc v2")
        else:
            raise ValueError(
                "Invalid SBML FBC package version: {}".format(target_fbc_version)
            )
        self._try_conversion(document, conversion)

    def convert_to_cobra_fbc(self, document):
        """Convert to Cobra FBC constraints."""
        conversion = libsbml.ConversionProperties()
        conversion.addOption("convert fbc to cobra")
        self._try_conversion(document, conversion)

    def replace_ids(self, document, annotation):
        """Create unique identifiers for all metabolites and reactions."""
        model = document.getModel()

        # Make new ids
        new, old = [], []
        for i, metabolite in enumerate(model.getListOfSpecies()):
            old.append(metabolite.getId())
            # Note: Do not use M_XXX format as that will mess up some parsers due to
            # Cobra Toolbox using it and stripping it automatically in some cases.
            new.append(("metabolites", "Met_{}".format(i)))
        for i, reaction in enumerate(model.getListOfReactions()):
            old.append(reaction.getId())
            # Note: Same as above with using R_XXX
            new.append(("reactions", "Reac_{}".format(i)))

        # Convert SBML document
        conversion = libsbml.ConversionProperties()
        conversion.addOption("renameSIds")
        conversion.addOption("currentIds", ",".join(i for i in old))
        conversion.addOption("newIds", ",".join(i[1] for i in new))
        document = model.getSBMLDocument()
        self._try_conversion(document, conversion)

        # Save old -> new mapping
        annotation["id_map"] = {"reactions": {}, "metabolites": {}}
        for (t, new_id), old_id in zip(new, old):
            annotation["id_map"][t][new_id] = old_id

    def replace_names_with_ids(self, document, annotation):
        """Make sure name and id are the same."""
        model = document.getModel()
        annotation["name_id_map"] = {"reactions": {}, "metabolites": {}}

        for metabolite in model.getListOfSpecies():
            id_ = metabolite.getId()
            name = metabolite.getName()
            annotation["name_id_map"]["metabolites"][id_] = name
            metabolite.setName(id_)
            if metabolite.isSetMetaId():
                metabolite.unsetMetaId()
        for reaction in model.getListOfReactions():
            id_ = reaction.getId()
            name = reaction.getName()
            annotation["name_id_map"]["reactions"][id_] = name
            reaction.setName(id_)
            if reaction.isSetMetaId():
                reaction.unsetMetaId()

    def remove_observables(self, document):
        """Remove observables (Defined as SBO:0000406)."""
        model = document.getModel()
        # You cannot loop over an SBML list while remove entries
        # at the same time, so first gather all entries to be removed
        # and then remove them afterwards.
        to_remove = []
        for metabolite in model.getListOfSpecies():
            if metabolite.getSBOTermID() == "SBO:0000406":
                to_remove.append(metabolite.getId())
        for m_id in to_remove:
            model.removeSpecies(m_id)

    def remove_rate_laws_and_parameters(self, document):
        """Remove rate laws and parameters."""
        model = document.getModel()
        for reaction in model.getListOfReactions():
            reaction.unsetKineticLaw()
        # If FBC level is 2, we have to conserve the "fb_.*" parameters
        # or parameters marked with SBO terms 625 (flux bound) or 626 (default flux bound)
        to_remove = []
        for parameter in model.getListOfParameters():
            p_id = parameter.getId()
            if not p_id.startswith("fb_") and parameter.getSBOTerm() not in (625, 626):
                to_remove.append(p_id)
        for p_id in to_remove:
            model.removeParameter(p_id)

    def merge_compartments(self, document, annotation):
        """Merge compartments."""
        model = document.getModel()

        # Remove old compartments
        model.getListOfCompartments().clear(True)

        # Create a new main compartment
        c = model.createCompartment()
        c.setId("main")
        c.setName("main")
        c.setConstant(True)
        c.setSize(1)

        # Reassign everything to the new compartment and save the old compartment.
        annotation["compartment_map"] = {}
        for metabolite in model.getListOfSpecies():
            id_ = metabolite.getId()
            compartment = metabolite.getCompartment()
            annotation["compartment_map"][id_] = compartment
            metabolite.setCompartment("main")

    def remove_annotation(self, document):
        """Remove annotation."""
        model = document.getModel()
        model.unsetAnnotation()
        model.unsetNotes()
        for metabolite in model.getListOfSpecies():
            metabolite.unsetAnnotation()
            metabolite.unsetNotes()
        for reaction in model.getListOfReactions():
            reaction.unsetAnnotation()
            reaction.unsetNotes()

        fbc = model.getPlugin("fbc")
        if fbc is not None:
            fbc.getListOfGeneAssociations().clear(True)

    def set_level(self, document, level, version, strict=False):
        """Set correct SBML level."""
        document.setLevelAndVersion(level, version, strict)

    def _run(self, working_directory, infiles):
        """Set up and run the normalization step."""
        input_model = infiles["input_model"]

        normalized_model_file = working_directory / pathlib.Path("normalized.xml")
        annotation_file = working_directory / pathlib.Path("annotation.json")
        constraints_file = working_directory / pathlib.Path("constraints.json")

        self.normalize(
            input_model, normalized_model_file, annotation_file, constraints_file
        )

        return [normalized_model_file, annotation_file, constraints_file]


class ReductionMapper(Step):
    default_settings = {
        # Rate law settings for lumped reactions that need to be recreated.
        # These are the same as in model kineticizer.
        # (TODO: Remove this part of the step and just re-run kineticizer?)
        # "rate_law_type": "modular",
        # "rate_law_args": {"parametrisation": "weg", "sub_rate_law": "PM"},
        # "merge_parameters": ["enzyme_rate", "km_merged", "km_rate"],
        # "limit_total_lumped_stoichiometry": True,
        # "max_reaction_order": 2,
        # "positive_fractional_powers": False,
        "validate": True,
        "silence_output": True,
    }
    ignore_settings = {"silence_output", "validate"}

    def __init__(self, use_cache=True, **settings):
        """Map reduction results back to the original model, updating rate laws as required.

        Required inputs to run are:
            'reduced_model': A SBML file containing the model as reduced by one of the tools.
            'full_model': A SBML file containing the original reference model.
            'reduced_model_annotation': A json file containing the mapping as
                produced by SBMLNormalizer.
        Outputs are:
            - Reduced model (./reduced.xml) with the reductions mapped back to the original model.
                Note: FBC annotation (constraints/objectives) is NOT conserved.
            - Reductions made (./lumpings.json) describes which reactions/metabolites
                were removed or combined.
        """
        super().__init__(mlab_engine_instance=None, use_cache=use_cache, **settings)

    def _run(self, working_directory, infiles):
        """Run the reduction mapping step."""
        model_file = infiles["reduced_model"]
        original_file = infiles["full_model"]
        annotation_file = infiles["reduce_model_annotation"]

        reactions, metabolites, used_metabolites = self.parse_tolerant(model_file)
        results = self.map_ids(reactions, metabolites, annotation_file)
        mapped_metabolites, mapped_reactions, reaction_stoichiometries, lumped_reactions = (
            results
        )

        standardized_file = working_directory / pathlib.Path("reduced.xml")
        lump_file = working_directory / pathlib.Path("lumpings.json")

        self.update_original(
            original_file,
            standardized_file,
            lump_file,
            mapped_metabolites,
            mapped_reactions,
            reaction_stoichiometries,
            lumped_reactions,
            used_metabolites,
        )
        return [standardized_file, lump_file]

    def parse_tolerant(self, reduced_path):
        """Parse the (invalid) sbml output of a reduction script.

        Note:
            lxml is used in stead of libsbml, as for some of these tools
            the SBMl is not valid.
            Only relevant entries are extracted: reactions and metabolites.
        """
        # Since not all of the tools return standard (or even correct) SBML,
        # we extract the information directly from the xml instead.
        root = lxml.etree.parse(str(reduced_path)).getroot()

        # Dict of 'id': {metabolite: stoichiometry}
        reactions = collections.OrderedDict()
        # List of 'id'
        metabolites = []
        used_metabolites = set()

        for reaction in root.findall("*/*/reaction", root.nsmap):
            r = reaction.attrib["id"]
            reactions[r] = {}

            reactants = reaction.find("listOfReactants", root.nsmap)
            if reactants is not None:
                for reactant in reactants.findall("speciesReference", root.nsmap):
                    species = reactant.attrib["species"]
                    stoich = -float(reactant.attrib["stoichiometry"])
                    reactions[r][species] = stoich
                    used_metabolites.add(species)

            products = reaction.find("listOfProducts", root.nsmap)
            if products is not None:
                for product in products.findall("speciesReference", root.nsmap):
                    species = product.attrib["species"]
                    stoich = float(product.attrib["stoichiometry"])
                    reactions[r][species] = stoich
                    used_metabolites.add(species)

        # Get all the metabolites
        metabolites = [i.attrib["id"] for i in root.findall("*/*/species", root.nsmap)]

        return reactions, metabolites, used_metabolites

    def map_ids(self, reactions, metabolites, annotation_path):
        """Map the ids of reactions and metabolites back to the normalized_model."""
        # Load the mapping from the original to the reduced model variable names etc.
        annotation = json.loads(annotation_path.read_text())
        metabolite_map = annotation["id_map"]["metabolites"]
        original_metabolites = sorted(list(metabolite_map.keys()), reverse=True)
        reaction_map = annotation["id_map"]["reactions"]
        original_reactions = list(reaction_map.keys())

        fc_metabolite_regex = re.compile(
            r"^({mets})__[0-9]+__[a-z]+__[0-9]+".format(
                mets="|".join(original_metabolites)
            )
        )
        nw_lumped_regex = re.compile(
            r"^((?:(?:{reactions})__)+)lumped$".format(
                reactions="|".join(original_reactions)
            )
        )
        lumped_split_regex = re.compile(
            "_" + "|_".join(sorted(original_reactions, reverse=True))
        )

        mapped_metabolites = {}  # reduction_output: original
        # Check what is conserved or not. Some metabolites might be renamed.
        for metabolite in metabolites:
            # metabolite = annotation['id_map']['metabolites'][metabolite]
            match = re.match(fc_metabolite_regex, metabolite)
            key = metabolite
            if match:
                metabolite = match.group(1)
            mapped_metabolites[key] = metabolite_map[metabolite]

        # Reactions can be lumped and will need to be treated in differently if that's the case.
        mapped_reactions = {}  # reduction_output: original
        lumped_reactions = {}  # reduction_output: [original, ...]
        reaction_stoichiometries = (
            {}
        )  # reduction_output: {original: stoichiometry, ...}
        for reaction, stoichiometry in reactions.items():
            # First check for lumping.
            match = re.match(nw_lumped_regex, reaction)
            # If lumped, find original reactions lumped together.
            if match:
                lumpees = re.findall(lumped_split_regex, "_" + match.group(1))
                lumped_reactions[reaction] = [reaction_map[i[1:]] for i in lumpees]
            # If not, find the single original reaction.
            else:
                mapped_reactions[reaction] = reaction_map[reaction]

            # Finally, find the original metabolites.
            reaction_stoichiometries[reaction] = {}
            for met, value in stoichiometry.items():
                reaction_stoichiometries[reaction][mapped_metabolites[met]] = value

        return (
            mapped_metabolites,
            mapped_reactions,
            reaction_stoichiometries,
            lumped_reactions,
        )

    def update_original(
        self,
        original_file,
        standardized_file,
        lump_file,
        mapped_metabolites,
        mapped_reactions,
        reaction_stoichiometries,
        lumped_reactions,
        used_metabolites,
    ):
        original_doc = libsbml.readSBMLFromFile(str(original_file))
        original_model = original_doc.getModel()

        # Remove FBC constraints
        for reaction in original_model.getListOfReactions():
            reaction.disablePackage(
                "http://www.sbml.org/sbml/level3/version1/fbc/version2", "fbc"
            )

        # For convenience we read some of the stuff from symbolic SBML,
        # but all the modifications are done on the SBML itself right now.
        symbolic_model = symbolicSBML.SymbolicModel.from_sbml(original_file)

        changes = self.check_stoichiometries(
            original_model, mapped_reactions, reaction_stoichiometries, lumped_reactions
        )
        removed_reactions = self.remove_reactions(original_model, mapped_reactions)
        used_metabolites = {mapped_metabolites[i] for i in used_metabolites}
        removed_metabolites = self.remove_metabolites(
            original_model, symbolic_model, used_metabolites
        )

        lumped_renames = self.add_lumped_reactions(
            original_model, symbolic_model, lumped_reactions, reaction_stoichiometries
        )

        json_content = {
            "lumpings": lumped_reactions,
            "lumped_renames": lumped_renames,
            "changes": changes,
            "removed": {
                "metabolites": removed_metabolites,
                "reactions": removed_reactions,
            },
        }
        write_json(json_content, lump_file)

        self.clean_parameters(
            original_model, symbolic_model, removed_reactions, new_rate_laws=None
        )
        if self.settings["validate"]:
            validate_sbml_document(original_doc)

        write_sbml_document(original_doc, standardized_file)

    def check_stoichiometries(
        self, model, mapped_reactions, reaction_stoichiometries, lumped_reactions
    ):
        # Extract all the originals
        original_stoichiometries = collections.defaultdict(dict)
        for reaction in model.getListOfReactions():
            r_id = reaction.getId()
            for m in reaction.getListOfProducts():
                original_stoichiometries[r_id][m.getSpecies()] = m.getStoichiometry()
            for m in reaction.getListOfReactants():
                original_stoichiometries[r_id][m.getSpecies()] = -m.getStoichiometry()

        changed_stoichiometries = []
        # Compare all conserved reactions, save changes.
        for out_r, original_r in mapped_reactions.items():
            out_s = set(reaction_stoichiometries[out_r].items())
            original_s = set(original_stoichiometries[original_r].items())
            difference = out_s ^ original_s
            if difference:
                changed_stoichiometries.append(
                    {
                        "new": out_r,
                        "old": original_r,
                        "before": dict(out_s),
                        "afer": dict(original_s),
                        "difference": dict(difference),
                    }
                )

        # Compare the sum of lumped reactions
        for out_r, original_r in lumped_reactions.items():
            # Create a small stoichiometry matrix.
            stoich = pd.DataFrame(
                data=[original_stoichiometries[r] for r in original_r]
                + [reaction_stoichiometries[out_r]],
                index=original_r + [out_r],
            ).fillna(0)
            # Calculate back the scaling coefficients.
            coeff, residues, *_ = scipy.linalg.lstsq(
                stoich.loc[original_r].T, stoich.loc[out_r]
            )
            coeff = pd.Series(coeff, index=original_r)
            if residues > 1e-6:
                raise RuntimeError(
                    "Inaccurate solve for reaction lumping of {}".format(out_r)
                )
            # Check whether the coefficients hold when rounded to integers,
            # and if so, round it.
            if np.allclose(
                coeff.round().astype(int).dot(stoich.loc[original_r]),
                (stoich.loc[out_r]),
            ):
                coeff = coeff.round().astype(int)
            changed_stoichiometries.append(
                {
                    "new": out_r,
                    "old": original_r,
                    # Filter out zeros.
                    "before": {
                        row: {
                            column: value
                            for column, value in values.items()
                            if value != 0
                        }
                        for row, values in stoich.loc[original_r].T.to_dict().items()
                    },
                    # Same here
                    "after": {
                        column: value
                        for column, value in stoich.loc[out_r].T.to_dict().items()
                        if value != 0
                    },
                    "coefficients": coeff.to_dict(),
                }
            )
        return changed_stoichiometries

    def remove_reactions(self, model, conserved_reactions):
        reactions = {r.getId() for r in model.getListOfReactions()}
        reactions -= set(conserved_reactions.values())
        for r in reactions:
            model.removeReaction(r)
        return list(reactions)

    def remove_metabolites(self, model, symbolic_model, used_metabolites):
        metabolites = {m.getId() for m in model.getListOfSpecies()}
        removed = []
        for m in metabolites:
            if m not in used_metabolites and m not in symbolic_model.observables:
                model.removeSpecies(m)
                removed.append(m)
        # Remove observables that depend on the species.
        removed_symbols = set(sympy.symbols(removed))
        for obs, obs_term in symbolic_model.observables.items():
            # Check for the set overlap between free symbols in the expression
            # of the observable and the deleted metabolites.
            if removed_symbols & obs_term.free_symbols:
                model.removeSpecies(obs)
                model.removeRuleByVariable(obs)
                removed.append(obs)
        return removed

    def clean_parameters(
        self, model, symbolic_model, removed_reactions, new_rate_laws=None
    ):
        used_parameters = set()
        for idx, reaction in enumerate(symbolic_model.reactions):
            if reaction not in removed_reactions:
                flux = symbolic_model.fluxes[idx]
                used_parameters |= {p.name for p in flux.free_symbols}
        if new_rate_laws:
            for flux in new_rate_laws:
                used_parameters |= {p.name for p in flux.free_symbols}

        to_remove = [
            p.getId()
            for p in model.getListOfParameters()
            if p.getId() not in used_parameters
        ]
        for p_id in to_remove:
            model.removeParameter(p_id)
        return to_remove

    def add_lumped_reactions(
        self, model, symbolic_model, lumped_reactions, reaction_stoichiometries
    ):
        # used_parameters = {p.name for p in symbolic_model.fluxes.free_symbols} | {
        #     p.name for p in symbolic_model.parameter_symbols
        # }
        # new_rate_laws = []
        lumped_renames = {}
        for i, reaction in enumerate(lumped_reactions.keys()):
            # Get reaction name and stoichometry
            metabolites, stoichiometry = zip(
                *reaction_stoichiometries[reaction].items()
            )
            reaction_id = "R_lumped_{}".format(i)
            lumped_renames[reaction_id] = reaction
            reaction_name = "__".join(lumped_reactions[reaction])

            # Add reaction to SBML model
            r = model.createReaction()
            r.setId(reaction_id)
            r.setName(reaction_name)
            r.setFast(False)
            r.setReversible(True)

            # Add reactants and products to reaction SBML object.
            for metabolite, stoich in zip(metabolites, stoichiometry):
                if stoich < 0:
                    m = r.createReactant()
                    stoich = abs(stoich)
                elif stoich > 0:
                    m = r.createProduct()
                else:
                    continue
                m.setStoichiometry(stoich)
                m.setSpecies(metabolite)
                m.setConstant(True)

        return lumped_renames
