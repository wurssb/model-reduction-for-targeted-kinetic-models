"""Base Pipeline class for stringing together steps.

Since the steps can have different requirements, no automatic pipeline is created at this time.

However, the aim is to require a minimum effort to create a new pipeline.

Author: Rik van Rosmalen
"""
import pathlib
import json
import shutil
import collections
import warnings

# import copy

import pandas as pd
import numpy as np
import h5py

import cobra

import symbolicSBML
import parameter_balancer
from symbolicSBML import Parameters

from .symbolic_util import ModelConditions
from .sbml_steps import SBMLNormalizer, ReductionMapper
from .util import parse_optimization_results, load_ode_system


class Pipeline(object):
    def __init__(self, working_directory, use_cache=True):
        """Create a base pipeline object.

        This class is meant to be subclassed and to gather shared functionality:
            - Setting up the working directory.
        :param working_directory: File path (absolute or relative) to working directory.
        :type working_directory: str or :class: Pathlib.Path
        :param use_cache: Use cached versions of the reduced models if settings match,
        defaults to True.
        :type use_cache: bool
        """
        self.working_directory = pathlib.Path(working_directory)
        self.create_working_directory()
        self.use_cache = use_cache

    def create_working_directory(self):
        """Create a working directory and a subdirectory for pipeline inputs."""
        self.working_directory.mkdir(exist_ok=True, parents=True)
        self.input_directory = self.working_directory / "inputs"
        self.input_directory.mkdir(exist_ok=True, parents=True)


class ReductionOptimizationPipeline(Pipeline):
    def __init__(
        self,
        working_directory,
        sbml_model_path,
        experiments,
        parameter_ranges,
        fluxes,
        priors,
        parameter_data,
        kineticizer,
        kineticizer_parameterizer,
        parameterizer,
        starting_points_generator,
        reducers,
        compiler,
        simulator_data,
        simulator_solutions,
        optimizer,
        use_cache=True,
    ):
        """Run an model reducer - optimizer test case.

        A test case consists of:
            - A single model, used as:
                - Flux model for parametrization
                - Basis of the kinetic "ground truth" model
                - Basis of the reduced model
            - 1 or more perturbations
                Used to generate simulation data with the base model for optimization of
                the reduced models. All perturbations will be applied to the same simulation.
            - 1 or more reducers
                If not provided, this step and subsequent the optimization is skipped.
            - A simulator, to generate data for the full model which is used to optimize the
                reduced models.
            - An optional optimizer (will not optimize if not provided)
                If not provided, the optimization is skipped.
        :param working_directory: File path (absolute or relative) to working directory.
        :type working_directory: str or :class: Pathlib.Path
        :param sbml_model_path: Path to SBML model.
        :type sbml_model_path: :class: Pathlib.Path
        :param experiments: A dictionary of experiments. Each experiment should have a list of
        perturbations, and a dictionary of fixed parameters.
        :param reducers: Reducer objects to reduce the model
        :type reducers: list[:class: Reducer]
        :param compiler: Compiler object to compile the experiments with model into an executable
        format for use by the simulator and optimizer.
        :type compiler: :class: Step
        :param simulator: Simulator object to simulate the experiments with the full model.
        :type simulator: :class: Step
        :param optimizer: Optimizer object to optimize the reduced models with the data from
        the experiments.
        :type optimizer: :class: Step
        :param parameter_ranges: Parameter ranges for the optimization. Can be for general types
        or specific parameters.
        :type parameter_ranges: dict['general': dict[parameter:(min, max)],
                                     'specific': dict[parameter:(min, max)]]
        :param use_cache: Use cached versions of the reduced models if settings match.
        :type use_cache: bool
        """
        super().__init__(working_directory, use_cache)

        self.sbml_model_path = pathlib.Path(sbml_model_path)
        self.reducers = reducers if reducers is not None else []
        self.compiler = compiler
        self.optimizer = optimizer
        self.simulator_data = simulator_data
        self.simulator_solutions = simulator_solutions
        self.kineticizer = kineticizer
        self.kineticizer_parameterizer = kineticizer_parameterizer
        self.parameterizer = parameterizer
        self.starting_points_generator = starting_points_generator
        self.use_cache = use_cache

        # Make a copy of the model into the working directory
        new_model_path = self.input_directory / "input_model.xml"
        shutil.copy(self.sbml_model_path, new_model_path)
        self.sbml_model_path = new_model_path.resolve()

        # Make a copy of the model without observables into the working directory
        # These slight simplified models are provided to the reduction tools
        # to avoid incompatibilities.
        self.normalized_input_directory = self.input_directory / "normalized"
        self.normalized_input_directory.mkdir(exist_ok=True)

        self.normalizer = SBMLNormalizer(validate=False, use_cache=self.use_cache)
        (mod, ann, con) = self.normalizer.run(
            self.normalized_input_directory, input_model=self.sbml_model_path
        )
        self.sbml_model_path_reduction = mod
        self.sbml_annotation_path_reduction = ann
        self.sbml_constraints_path_reduction = con

        # Create a mapper that maps the reduced results back to the original model.
        self.reduction_mapper = ReductionMapper(use_cache=self.use_cache)

        # Save the parameter ranges for optimization into the working directory.
        self.parameter_range_path = (
            self.input_directory / "parameter_ranges.json"
        ).resolve()
        with open(self.parameter_range_path, "w") as outfile:
            json.dump(parameter_ranges, outfile, indent=2, sort_keys=True)

        # Save the priors, parameter data and fluxes into the working directory.
        self.flux_path = self.input_directory / "fluxes.csv"
        fluxes.index.name = "reaction"
        fluxes.to_csv(self.flux_path)

        self.prior_path = self.input_directory / "priors.json"
        self.prior_path.write_text(json.dumps(priors, sort_keys=True, indent=2))

        self.parameter_data_path = self.input_directory / "data.csv"
        if parameter_data:
            parameter_data = parameter_balancer.ParameterData(*zip(*parameter_data))
        else:
            parameter_data = parameter_balancer.ParameterData.empty()
        parameter_data.df.to_csv(self.parameter_data_path, index=False)

        # These will be updated later, but save them for now.
        self.experiments = experiments

    def update_experiments(self):
        # Save the perturbations and fixed parameters for each experiment
        # the parametrization.
        self.experiment_base_path = self.input_directory / "experiments"
        old_experiments = self.experiments
        self.experiments = {}
        self.experiment_sets = collections.defaultdict(list)

        for i, experiment in enumerate(old_experiments):
            # Make name and base directory.
            experiment_name = "experiment_{}".format(i)
            experiment_path = self.experiment_base_path / experiment_name
            experiment_path.mkdir(exist_ok=True, parents=True)

            # Save all perturbations
            perturbation_paths = []
            for p_i, p in enumerate(experiment["perturbations"]):
                path = experiment_path / "perturbation_{}.json".format(p_i)
                p.to_json(path)
                perturbation_paths.append(path)

            # Save the fixed parameters
            fixed_parameters = experiment["fixed_parameters"]
            fixed_parameter_path = experiment_path / "fixed_parameters.json"
            fixed_parameter_path.write_text(
                json.dumps(fixed_parameters, sort_keys=True, indent=2)
            )

            # Save the conditions
            conditions_path = experiment_path / "conditions.h5"
            base = self.balanced_initial_concentrations.copy()
            for metabolite, value in experiment["initial_concentrations"].items():
                base[metabolite] = value
            conditions = ModelConditions(self.balanced_parameter_values, base)
            conditions.to_hdf(conditions_path)

            # Add to right set.
            self.experiment_sets[experiment["set"]].append(experiment_name)

            self.experiments[experiment_name] = {
                "base_path": experiment_path,
                "perturbation_paths": perturbation_paths,
                "fixed_parameter_path": fixed_parameter_path,
                "conditions_path": conditions_path,
            }
        experiment_sets_path = self.experiment_base_path / "experiment_sets.json"
        experiment_sets_path.write_text(
            json.dumps(self.experiment_sets, sort_keys=True, indent=2)
        )

    def run(self):
        """Run all steps of the pipeline."""
        # Reference model.
        self.parameterize()
        self.update_experiments()

        # Reduced models.
        self.reduce()
        self.compile()

        # Optimization.
        self.generate_data()
        self.generate_starting_points()
        self.optimize_self()
        self.optimize_reduced()

        # Simulate results.
        self.simulate_solutions()

    def parameterize(self):
        """Run the Kinetizer and parameterizer to create the reference model."""
        # Run Kinetizer (for model used in simulations)
        directory = self.working_directory / "kineticizer"
        directory.mkdir(exist_ok=True, parents=True)
        out = self.kineticizer.run(directory, model=self.sbml_model_path)
        self.full_kinetic_model_file = out[0]

        # Run kinetizer (for model used in parameterization)
        # This model does not have simplified rate laws
        directory = self.working_directory / "parameterizer" / "kineticizer"
        directory.mkdir(exist_ok=True, parents=True)
        out = self.kineticizer_parameterizer.run(directory, model=self.sbml_model_path)
        kinetic_model_file, cooperativity_file, cython_module_location = out

        # Run parameterizer
        directory = self.working_directory / "parameterizer"
        out = self.parameterizer.run(
            directory,
            model=kinetic_model_file,
            final_model=self.full_kinetic_model_file,
            priors=self.prior_path,
            parameter_data=self.parameter_data_path,
            cooperativities=cooperativity_file,
            fluxes=self.flux_path,
            cython_ode=cython_module_location,
        )
        self.full_parameterized_model = out[0]
        self.full_all_parameter_sets = out[2]

        # Save simulation conditions
        # Load parameterized model to get the model conditions.
        model = symbolicSBML.SBMLModel(self.full_parameterized_model)
        initial_concentrations = {
            k: v
            for k, v in model.initial_concentrations.items()
            if k in model.metabolites
        }
        parameter_values = model.parameter_values
        conditions = ModelConditions(parameter_values, initial_concentrations)

        # Save the simulation conditions into the working directory
        self.balanced_conditions_path = self.input_directory / "conditions.hd5"
        # We need these to update the experimental conditions.
        self.balanced_parameter_values = parameter_values
        self.balanced_initial_concentrations = initial_concentrations
        conditions.to_hdf(self.balanced_conditions_path)

    def reduce(self):
        """Run all reducers and save results to `reducers` subdirectory."""
        annotation = self.sbml_annotation_path_reduction
        self.reduced_models = []
        self.reduced_model_modules = []
        self.reduced_model_lumpings = []
        for reducer in self.reducers:
            # Run reducer
            directory_temp = (
                self.working_directory / "reducers" / reducer._dir_name / "reduced"
            )
            directory_temp.mkdir(exist_ok=True, parents=True)
            reduced, *_ = reducer.run(
                directory_temp,
                reduce_model=self.sbml_model_path_reduction,
                reduce_model_annotation=annotation,
            )
            # Map reduced model back to original model
            directory = (
                self.working_directory / "reducers" / reducer._dir_name / "restored"
            )
            directory.mkdir(exist_ok=True, parents=True)
            restored, lumpings = self.reduction_mapper.run(
                directory,
                full_model=self.sbml_model_path,
                reduced_model=reduced,
                reduce_model_annotation=annotation,
            )
            self.reduced_model_lumpings.append(lumpings)
            # Add kinetics to the restored model
            directory = (
                self.working_directory / "reducers" / reducer._dir_name / "kinetic"
            )
            directory.mkdir(exist_ok=True, parents=True)

            if self.kineticizer.settings["cythonize"]:
                # Get the module name if defined before we overwrite it.
                old_name = self.kineticizer.settings["cythonize_settings"].get(
                    "module_name", None
                )

                self.kineticizer.settings["cythonize_settings"][
                    "module_name"
                ] = "ode_{}".format(reducer._dir_name)

            out = self.kineticizer.run(directory, model=restored)
            self.reduced_models.append(out[0])

            if self.kineticizer.settings["cythonize"]:
                self.reduced_model_modules.append(out[2])

                # Put old module name back into place if it was defined from before.
                if old_name is None:
                    del self.kineticizer.settings["cythonize_settings"]["module_name"]
                else:
                    self.kineticizer.settings["cythonize_settings"][
                        "module_name"
                    ] = old_name

    def compile(self):
        """Compile and save all full and reduced models for all experiments."""
        names = ["full"] + [i._dir_name for i in self.reducers]
        models = [self.full_kinetic_model_file] + self.reduced_models

        self.compiled_models = collections.defaultdict(dict)
        self.compiled_model_descriptions = collections.defaultdict(dict)
        self.compiled_full_model = {}
        self.compiled_full_model_description = {}

        # Loop over all combinations of models and experiments
        # This could be cleaned up to only check unique perturbations / fixed parameters,
        # since changes of conditions do not require recompilation.
        for model_name, model in zip(names, models):
            for exp_name, exp_paths in self.experiments.items():

                # Create working directory specific to model and experiment.
                directory = (
                    self.working_directory
                    / self.compiler._dir_name
                    / model_name
                    / exp_name
                )
                directory.mkdir(exist_ok=True, parents=True)

                # Get relevant paths
                perturbations = exp_paths["perturbation_paths"]
                fixed_parameters = exp_paths["fixed_parameter_path"]

                # Run
                out = self.compiler.run(
                    directory,
                    model=model,
                    fixed_parameters=fixed_parameters,
                    perturbation=perturbations,
                )

                # Save output locations
                _, compiled_path, description_path = out
                if model_name == "full":
                    self.compiled_full_model[exp_name] = compiled_path
                    self.compiled_full_model_description[exp_name] = description_path
                else:
                    self.compiled_models[model_name][exp_name] = compiled_path
                    self.compiled_model_descriptions[model_name][
                        exp_name
                    ] = description_path

        # Convert default-dictionaries back to normal.
        self.compiled_models = dict(self.compiled_models)
        self.compiled_model_descriptions = dict(self.compiled_model_descriptions)

    def generate_starting_points(self):
        """Generate starting points for optimization."""
        # Start by check if we delegated this to pesto. (if optimizer exists!)
        if (
            hasattr(self, "optimizer")
            and self.optimizer is not None
            and self.optimizer.settings["proposal"] != "user-supplied"
        ):
            self.starting_points_generator = False
            self.parameter_starting_points_path = None
            return

        if self.starting_points_generator is None:
            raise ValueError(
                "Starting points not provided while optimization step"
                " requires is set to user-supplied."
            )

        directory = self.working_directory / "optimization_starts"
        directory.mkdir(exist_ok=True)
        # We need to generate parameters for all possible parameters of each model.
        parameters = set()
        descriptions = list(self.compiled_full_model_description.values())
        for model in self.compiled_model_descriptions:
            for description_path in self.compiled_model_descriptions[model].values():
                descriptions.append(description_path)
        # Combine all descriptions' parameters.
        for description_path in descriptions:
            with open(description_path, "r") as description:
                parameters |= set(json.load(description)["parameters"])
        # Sort and save the parameters.
        parameters = sorted(parameters)
        parameter_names_path = directory / "parameters.json"
        with open(parameter_names_path, "w") as outfile:
            json.dump(parameters, outfile, indent=2, sort_keys=True)

        out = self.starting_points_generator.run(
            directory,
            parameter_ranges=self.parameter_range_path,
            parameter_names=parameter_names_path,
        )
        self.parameter_starting_points_path = out[0]

    def generate_data(self):
        """Run full model to generate base data to optimize against."""
        self.full_simulation_data = {}
        for experiment_name, experiment_paths in self.experiments.items():
            directory = (
                self.working_directory
                / self.simulator_data._dir_name
                / "full"
                / experiment_name
            )
            directory.mkdir(exist_ok=True, parents=True)

            conditions_path = experiment_paths["conditions_path"]
            model_path = self.compiled_full_model[experiment_name]
            description_path = self.compiled_full_model_description[experiment_name]

            data, *_ = self.simulator_data.run(
                directory,
                compiled_model=model_path,
                conditions=conditions_path,
                model_description=description_path,
            )
            self.full_simulation_data[experiment_name] = data

            # Do a quick check if the simulation was successful before continuing.
            with h5py.File(data, "r") as f:
                if np.any(np.isnan(f["y"])):
                    t = f["t"][0, np.min(np.argmax(np.isnan(f["y"]), axis=1))]
                    raise RuntimeError(
                        "Encountered NaN value in data simulation for {} "
                        "at t={}".format(experiment_name, t)
                    )
                    # import warnings
                    # warnings.warn('Encountered NaN value in data simulation for {} '
                    #              'at t={}'.format(experiment_name, t))

    def optimize_self(self):
        """Optimize full model against full model data for baseline."""
        directory = self.working_directory / self.optimizer._dir_name / "full"
        directory.mkdir(exist_ok=True, parents=True)

        models, conditions, descriptions, data = [], [], [], []
        for experiment_name in self.experiments:
            if experiment_name in self.experiment_sets["optimization"]:
                models.append(self.compiled_full_model[experiment_name])
                conditions.append(self.experiments[experiment_name]["conditions_path"])
                descriptions.append(
                    self.compiled_full_model_description[experiment_name]
                )
                data.append(self.full_simulation_data[experiment_name])
        parameter_ranges = self.parameter_range_path

        out = self.optimizer.run(
            directory,
            compiled_models=models,
            conditions=conditions,
            data=data,
            model_descriptions=descriptions,
            parameter_ranges=parameter_ranges,
            starting_points=self.parameter_starting_points_path,
        )

    def optimize_reduced(self):
        """Optimize reduced models against full model data."""
        # The conditions, data and parameter ranges are the same for all reduced models.
        conditions, data = [], []
        for experiment_name in self.experiments:
            if experiment_name in self.experiment_sets["optimization"]:
                conditions.append(self.experiments[experiment_name]["conditions_path"])
                data.append(self.full_simulation_data[experiment_name])
        parameter_ranges = self.parameter_range_path

        # Run optimization step for each of the reduced models.
        self.optimization_results = {}
        for name in self.compiled_models.keys():
            # Compiled models and their description are per reduced model.
            models, descriptions = [], []
            for experiment_name in self.experiments:
                if experiment_name in self.experiment_sets["optimization"]:
                    models.append(self.compiled_models[name][experiment_name])
                    descriptions.append(
                        self.compiled_model_descriptions[name][experiment_name]
                    )

            # Create working directory
            directory = self.working_directory / self.optimizer._dir_name / name
            directory.mkdir(exist_ok=True)

            out = self.optimizer.run(
                directory,
                compiled_models=models,
                conditions=conditions,
                data=data,
                model_descriptions=descriptions,
                parameter_ranges=parameter_ranges,
                starting_points=self.parameter_starting_points_path,
            )

    def simulate_solutions(self, simulate_self=True, simulate_reduced=True):
        """Run simulation for each optimized model."""
        model_names = []
        if simulate_self:
            model_names.append("full")
        if simulate_reduced:
            model_names.extend(list(self.compiled_models.keys()))

        self.optimized_simulation_data = {}
        for m_name in model_names:
            # Create working directory
            model_directory = (
                self.working_directory
                / self.simulator_solutions._dir_name
                / "solutions"
                / m_name
            )
            model_directory.mkdir(exist_ok=True, parents=True)

            # Retrieve optimized parameters & update conditions
            # TODO: Refactor the parsing - it should be easier to get out the final solutions.
            df_trace_end = parse_optimization_results(self.working_directory, m_name)[1]
            parameters = df_trace_end.drop(["time", "fval"], axis=1).reset_index(
                drop=True
            )
            repeats = parameters.shape[0]

            self.optimized_simulation_data[m_name] = {}
            for e_name in self.experiments:
                # Retrieve original conditions for this experiment
                conditions = ModelConditions.from_hdf(
                    self.experiments[e_name]["conditions_path"]
                )
                # Update with optimized parameters and save again.
                updated_conditions = ModelConditions(
                    parameters,
                    pd.concat(
                        [conditions.initial_concentrations] * repeats, ignore_index=True
                    ),
                )
                updated_conditions_path = (
                    model_directory / "updated_conditions_{}.h5".format(e_name)
                )
                updated_conditions.to_hdf(updated_conditions_path)

                # Create working directory
                directory = model_directory / e_name
                directory.mkdir(exist_ok=True, parents=True)

                # Retrieve the right model + description based on the experiment name.
                if m_name == "full":
                    model_path = self.compiled_full_model[e_name]
                    description_path = self.compiled_full_model_description[e_name]
                else:
                    model_path = self.compiled_models[m_name][e_name]
                    description_path = self.compiled_model_descriptions[m_name][e_name]

                data, *_ = self.simulator_solutions.run(
                    directory,
                    compiled_model=model_path,
                    conditions=updated_conditions_path,
                    model_description=description_path,
                )
                self.optimized_simulation_data[m_name][e_name] = data


class ReductionSamplingPipeline(ReductionOptimizationPipeline):
    def __init__(
        self,
        working_directory,
        sbml_model_path,
        fluxes,
        priors,
        parameter_data,
        kineticizer,
        kineticizer_parameterizer,
        parameterizer,
        reducers,
        use_cache=True,
    ):
        Pipeline.__init__(self, working_directory, use_cache)

        self.sbml_model_path = pathlib.Path(sbml_model_path)

        self.reducers = reducers if reducers is not None else []
        self.kineticizer = kineticizer
        self.kineticizer_parameterizer = kineticizer_parameterizer
        # self.reduction_mapper = reduction_mapper
        self.parameterizer = parameterizer
        self.use_cache = use_cache

        # Make a copy of the model into the working directory
        new_model_path = self.input_directory / "input_model.xml"
        shutil.copy(self.sbml_model_path, new_model_path)
        self.sbml_model_path = new_model_path.resolve()

        # Make a copy of the model without observables into the working directory
        # These slight simplified models are provided to the reduction tools
        # to avoid incompatibilities.
        self.normalized_input_directory = self.input_directory / "normalized"
        self.normalized_input_directory.mkdir(exist_ok=True)

        self.normalizer = SBMLNormalizer(validate=False, use_cache=self.use_cache)
        (mod, ann, con) = self.normalizer.run(
            self.normalized_input_directory, input_model=self.sbml_model_path
        )
        self.sbml_model_path_reduction = mod
        self.sbml_annotation_path_reduction = ann
        self.sbml_constraints_path_reduction = con

        # Create a mapper that maps the reduced results back to the original model.
        self.reduction_mapper = ReductionMapper(use_cache=self.use_cache)

        # Save the priors, parameter data and fluxes into the working directory.
        self.flux_path = self.input_directory / "fluxes.csv"
        fluxes.index.name = "reaction"
        fluxes.to_csv(self.flux_path)

        self.prior_path = self.input_directory / "priors.json"
        self.prior_path.write_text(json.dumps(priors, sort_keys=True, indent=2))

        self.parameter_data_path = self.input_directory / "data.csv"
        if parameter_data:
            parameter_data = parameter_balancer.ParameterData(*zip(*parameter_data))
        else:
            parameter_data = parameter_balancer.ParameterData.empty()
        parameter_data.df.to_csv(self.parameter_data_path, index=False)

        # Run kinetizer (for model used in simulations)
        directory = self.working_directory / "kineticizer"
        directory.mkdir(exist_ok=True, parents=True)
        out = self.kineticizer.run(directory, model=self.sbml_model_path)
        self.full_kinetic_model_file = out[0]
        if len(out) > 2:
            self.full_kinetic_model_module = out[2]

    def run(self):
        """Run all steps of the pipeline."""
        self.parameterize()
        self.reduce()
        self.parameterize_reduced_models()
        self.simulate_samples()

    def parameterize_reduced_models(self):
        """Run the Kinetizer and parameterizer to create the reduced models."""
        self.reduced_parameterized_models = []
        self.reduced_all_parameter_sets = []
        self.reduced_parameterized_modules = []

        for reducer, lumping_path, reduced_model_path in zip(
            self.reducers, self.reduced_model_lumpings, self.reduced_models
        ):
            name = reducer._dir_name
            base_directory = self.working_directory / "reducers" / name
            parameterizer_directory = base_directory / "parameterizer"
            parameterizer_directory.mkdir(exist_ok=True, parents=True)

            # Remove the data that got reduced.
            new_data_path = parameterizer_directory / "filtered_data.csv"
            reduced_model = symbolicSBML.SBMLModel(reduced_model_path)
            data = pd.read_csv(self.parameter_data_path)
            valid_metabolite = data.metabolite.isin(
                reduced_model.metabolites + [np.NaN, None, ""]
            )
            valid_reaction = data.reaction.isin(
                reduced_model.reactions + [np.NaN, None, ""]
            )
            data[valid_reaction & valid_metabolite].to_csv(new_data_path, index=False)

            # Get new fluxes for the same flux profile.
            # TODO: This should probably be moved to sbmlsteps.ReductionMapper
            cobra_model = cobra.io.read_sbml_model(
                str(reduced_model_path), f_replace={}
            )
            constraints = json.loads(self.sbml_constraints_path_reduction.read_text())
            lumpings = json.loads(lumping_path.read_text())
            # Add back objective
            for reaction, coefficient in constraints["objective"]["reactions"].items():
                cobra_model.reactions.get_by_id(
                    reaction
                ).objective_coefficient = coefficient
            cobra_model.objective_direction = constraints["objective"]["direction"]
            # Add back all constraints to original reactions
            for reaction in cobra_model.reactions:
                if reaction.id in constraints["bounds"]:
                    reaction.bounds = constraints["bounds"][reaction.id]
                # For the lumped reactions, we check the min/max of all combined reactions
                # and take the most constrained version
                elif reaction.id in lumpings["lumped_renames"]:
                    lumped_reactions = lumpings["lumpings"][
                        lumpings["lumped_renames"][reaction.id]
                    ]
                    lb = max(constraints["bounds"][r][0] for r in lumped_reactions)
                    ub = min(constraints["bounds"][r][1] for r in lumped_reactions)
                    reaction.bounds = (lb, ub)
                else:
                    warnings.warn(
                        "Unknown reaction after reducing {}: cannot be bounded!".format(
                            reaction.id
                        )
                    )
            new_flux_path = parameterizer_directory / "filtered_fluxes.csv"
            solution = cobra_model.optimize()
            if solution.status != "optimal":
                raise ValueError(
                    "Infeasible flux model after reduction by {}".format(name)
                )
            fluxes = solution.to_frame().fluxes.to_frame() / 3600
            if (fluxes.loc[constraints["objective"]["reactions"], "fluxes"] <= 0).any():
                raise ValueError(
                    "Feasible model with no objective flux after reduction by {}".format(
                        name
                    )
                )
            fluxes.index.name = "reaction"
            fluxes.to_csv(new_flux_path)

            # Create model used in fitting parameter sets to match reaction directionality.
            kineticizer_directory = parameterizer_directory / "kineticizer"
            kineticizer_directory.mkdir(exist_ok=True, parents=True)
            # Update fixed reaction fluxes. (TODO: Expose this properly in the settings!)
            old_fixed_reactions = self.kineticizer_parameterizer.settings[
                "fixed_reactions"
            ].copy()
            for reaction in self.kineticizer_parameterizer.settings["fixed_reactions"]:
                if reaction in fluxes.index:
                    self.kineticizer_parameterizer.settings["fixed_reactions"][
                        reaction
                    ] = fluxes.loc[reaction, "fluxes"]
            self.kineticizer_parameterizer.settings["cythonize_settings"][
                "module_name"
            ] = "par_{}_ode".format(name)
            out = self.kineticizer_parameterizer.run(
                kineticizer_directory, model=reduced_model_path
            )
            kinetic_model_file, cooperativity_file, cython_module_location = out
            self.kineticizer_parameterizer.settings[
                "fixed_reactions"
            ] = old_fixed_reactions

            # Since we might have more lumped reactions now,
            # we need to add them to the extra parameters.
            # Otherwise they cannot be used for scaling.
            # TODO: Expose a setting for this?
            old_scaling_parameters = self.parameterizer.settings["scaling_parameters"]
            scaling_parameters = old_scaling_parameters.copy()
            old_extra_parameters = self.parameterizer.settings["extra_parameters"]
            extra_parameters = old_extra_parameters.copy()
            for reaction in lumpings["lumped_renames"]:
                parameter_id = Parameters.join(
                    Parameters.km_tot_rate, parameter_balancer.NA_IDENTIFIER, reaction
                )
                scaling_parameters[reaction] = parameter_id
                extra_parameters[parameter_id] = 1
            try:
                # Update settings for individual model.
                self.parameterizer.settings["scaling_parameters"] = scaling_parameters
                self.parameterizer.settings["extra_parameters"] = extra_parameters
                # Run parameterizer.
                out = self.parameterizer.run(
                    parameterizer_directory,
                    model=kinetic_model_file,
                    final_model=reduced_model_path,
                    priors=self.prior_path,
                    parameter_data=new_data_path,
                    cooperativities=cooperativity_file,
                    fluxes=new_flux_path,
                    cython_ode=cython_module_location,
                )
                self.reduced_parameterized_modules.append(cython_module_location)
                self.reduced_parameterized_models.append(out[0])
                self.reduced_all_parameter_sets.append(out[2])
            finally:
                # Reset the scaling parameters to the original.
                self.parameterizer.settings[
                    "scaling_parameters"
                ] = old_scaling_parameters
                self.parameterizer.settings["extra_parameters"] = old_extra_parameters

    def simulate_samples(self):
        """Simulate the sensitivities using the compiled model and the generated parameters."""
        names = ["full"] + [i._dir_name for i in self.reducers]
        # modules = [self.full_kinetic_model_module] + self.reduced_model_modules
        modules = [self.full_kinetic_model_module] + self.reduced_model_modules
        parameters = [self.full_all_parameter_sets] + self.reduced_all_parameter_sets

        base_directory = self.working_directory / "sampling"

        for name, module, parameter in zip(names, modules, parameters):
            results_directory = base_directory / name
            results_directory.mkdir(exist_ok=True, parents=True)
            results_file = results_directory / "samples.h5"

            # Loading C extensions with the same name consecutively does not work,
            # so we customized the names before.
            ode_system = load_ode_system(
                module, name="ode_{}".format(name) if name != "full" else "ode"
            )

            # load steady-state parameter data set.
            parameter_data = pd.read_hdf(parameter, key="parameters")
            # Match parameter & metabolite order with order expected in model.
            # parameter_indices = None
            # metabolites_indices = None

            # Add R & T (TODO: Refactor)
            parameter_data["R"] = self.parameterizer.settings["R"]
            parameter_data["T"] = self.parameterizer.settings["T"]

            # TODO: Fix this repetition with SteadyStateModelParameterizer.sample_synced_flux
            joined_columns = pd.Index(
                [
                    Parameters.sep.join((j for j in i if j and str(j) != "nan"))
                    for i in parameter_data.columns.tolist()
                ]
            )
            # TODO: Fix if model has extra parameters
            # # Add extra parameters if they are in the ode_system.
            # for key, value in self.settings["extra_parameters"].items():
            #     if key in ode_system.parameters and key not in joined_columns:
            #         try:
            #             p, m, r = Parameters.split(key, missing_value=NA_IDENTIFIER)
            #         except ValueError:
            #             parameter_data[key, NA_IDENTIFIER, NA_IDENTIFIER] = value
            #         else:
            #             parameter_data[p, m, r] = value
            # # Also check metabolites that were not included in the balancing procedure.
            # for met in self.settings["ignore_metabolites_balancing"]:
            #     if met in ode_system.metabolites:
            #         try:
            #             parameter_data[Parameters.c, met, NA_IDENTIFIER] = self.settings[
            #                 "extra_parameters"
            #             ][met]
            #         except KeyError:
            #             raise ValueError(
            #                 "{} was ignored during balancing, but no replacement initial "
            #                 "value was passed as extra_parameter.".format(met)
            #             )

            # Same format as multi-index, but in joined strings - easier comparison to structure.
            joined_columns = pd.Index(
                [
                    Parameters.sep.join((j for j in i if j and str(j) != "nan"))
                    for i in parameter_data.columns.tolist()
                ]
            )
            multi_columns = parameter_data.columns
            parameter_data.columns = joined_columns

            # Sort for easier processing later.
            order = np.argsort(joined_columns)
            multi_columns = multi_columns[order]
            joined_columns = joined_columns[order]
            parameter_data = parameter_data.iloc[:, order]

            # Get indices of right parameters for ode system assuming sorted names
            # Make sure to verify, because searchsorted won't complain about missing matches.
            parameter_idx = np.searchsorted(
                parameter_data.columns, ode_system.parameters
            )
            if not np.all(
                parameter_data.columns[parameter_idx] == ode_system.parameters
            ):
                raise ValueError("Parameter set mismatch.")

            metabolites = [
                Parameters.sep.join((Parameters.c, m)) for m in ode_system.metabolites
            ]
            metabolite_idx = np.searchsorted(parameter_data.columns, metabolites)
            if not np.all(parameter_data.columns[metabolite_idx] == metabolites):
                raise ValueError("Metabolite set mismatch.")

            n_samples = len(parameter_data)
            state = np.zeros((n_samples, ode_system.n_metabolites))
            parameters = np.zeros((n_samples, ode_system.n_parameters))
            dydt = np.zeros((n_samples, ode_system.n_metabolites))
            flux = np.zeros((n_samples, ode_system.n_reactions))
            jacobian = np.zeros(
                (n_samples, ode_system.n_metabolites, ode_system.n_metabolites)
            )
            sensitivities = np.zeros(
                (n_samples, ode_system.n_metabolites, ode_system.n_parameters)
            )
            flux_sensitivities = np.zeros(
                (n_samples, ode_system.n_reactions, ode_system.n_parameters)
            )

            for i in range(n_samples):
                # Load parameters and metabolite state.
                p = parameter_data.iloc[i, parameter_idx].values
                metabolites = parameter_data.iloc[i, metabolite_idx].values
                ode_system.update_parameters(p)

                # Save state, parameters and outputs
                state[i, :] = metabolites
                parameters[i, :] = p
                dydt[i, :] = ode_system.dydt(0.0, metabolites)
                flux[i, :] = ode_system.flux(metabolites)
                jacobian[i, :, :] = ode_system.jacobian(0.0, metabolites)
                sensitivities[i, :, :] = ode_system.dydp(metabolites)
                flux_sensitivities[i, :, :] = ode_system.dfdp(metabolites)

            # Save output to hdf.
            # If required this could be combined into the last loop to avoid
            # large memory usage by writing directly to the file instead of an intermediate buffer.
            # This way should be faster for small enough arrays, however.
            with h5py.File(results_file, "w") as hf:
                data = [
                    ("state", state),
                    ("parameters", parameters),
                    ("dydt", dydt),
                    ("flux", flux),
                    ("jacobian", jacobian),
                    ("sensitivities", sensitivities),
                    ("flux_sensitivities", flux_sensitivities),
                    ("parameter_names", ode_system.parameters.astype("S")),
                    ("metabolite_names", ode_system.metabolites.astype("S")),
                    ("reaction_names", ode_system.reactions.astype("S")),
                ]
                for name, array in data:
                    hf.create_dataset(
                        name, compression=5, shuffle=True, track_times=False, data=array
                    )
