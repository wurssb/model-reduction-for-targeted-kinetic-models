--------
Overview
--------
Repository for network reduction algorithms. 
The scripts themselves are not included due to licensing, but can be found at the provided locations.
The diff files implements some fixes for the algorithms for new versions of Matlab and other dependencies and are licensed under the MIT license.

-------
Changes
-------
MinNW:
    - Changed the cplex mip parameter ('{Cplex}.Param.mip.display.Cur') 
        from 'off' to 0 to work with newer versions of the Matlab cplex implementation.
    - Renamed SBML loading function in minNW to avoid name clashes with the Cobra Toolbox. 
        (`minnw/readSBML.m` -> `minnw/readSBML_minnw.m`)
    - Corrected F2C2_Red internal package references.
    - Added two missing files to the embedded version F2C2. 
CellNetAnalyzer:
    - Added a copy of CellNetAnalyzer, since the NetworkReducer examples won't work without it.

----------------------
References / locations
----------------------
FASTCORE:
    Reference:
        Vlassis, N., Pacheco, M. P., & Sauter, T. (2014). Fast Reconstruction of Compact Context-Specific Metabolic Network Models. PLoS Computational Biology, 10(1), e1003424. http://doi.org/10.1371/journal.pcbi.1003424

    Website:
        https://bio.uni.lu/systems_biology/software

    Retrieved from:
        https://wwwen.uni.lu/content/download/66434/841165/file/FASTCORE_1.0.zip

    Retrieval date:
        27 September 2018

    File hash (FASTCORE_1.0.zip):
        MD5     = bb479c93cc600bb4df14725827f0ee24
        SHA-256 = 5d2de65b2e87556b6bebaf979e4fce10d4a11849c0d64ac5e7f4b58009a71be3

NetworkReducer:
    Reference:
        Erdrich, P., Steuer, R., & Klamt, S. (2015). An algorithm for the reduction of genome-scale metabolic network models to meaningful core models. BMC Systems Biology, 9(1), 48. http://doi.org/10.1186/s12918-015-0191-x

    Website:
        http://www2.mpi-magdeburg.mpg.de/projects/cna/etcdownloads.html

    Retrieved from:
        http://www.mpi-magdeburg.mpg.de/projects/cna/NetworkReducer.zip

    Retrieval date:
        27 September 2018

    File hash (NetworkReducer.zip):
        MD5     = d2d7e4d787e2d5b889eabf14b26c1e33
        SHA-256 = ee292e4076fe2a94dad2c79301c4e91d43609c09083a311cd86c8c56ded0ef40

MinNW:
    Reference:
        Röhl, A., & Bockmayr, A. (2017). A mixed-integer linear programming approach to the reduction of genome-scale metabolic networks. BMC Bioinformatics, 18(1), 2. http://doi.org/10.1186/s12859-016-1412-z
    
    Website:
        https://sourceforge.net/projects/minimalnetwork/
    
    Retrieved from: 
        https://sourceforge.net/projects/minimalnetwork/files/MinNW.tar.gz/download
    
    Retrieval date:
        27 September 2018

    File hash (MinNW.tar.gz):
        MD5     = 01f453e933a0deae58674ac16ad26521
        SHA-256 = 786389b36a543ef9135460ae4c8284de4dde6f27193689b410cf02358c5d6c42
